/* Retrieving All Distribution List */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/?go#retrieving-distribution-lists) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');	

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();


/*

=============== IMPORTANT WARNING ===============

- Whispir API only returns the partial DL details when a GET is done without a single DL id ( meaning - get all DL) 
- so creating a new DL objecct with those limited details is not an issue. But doing an UPDATE on that limited DL properties object results in loss of properties that are not present in this object.
- so ensure that when you want to update a DL:
    - Fetch all DL
    - create an array of DLids from them
    - loop on this array
    - follow the update-distributionlist.js file code
*/


/* Methods to handle Promise resolve and reject */
var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

/* Using the Fetch */
wAPIClient.Fetch('DistributionLists').go().then(function (results) {

    var dlObjects = results.body.distributionLists; //its 

    //loop through dlObjects array for any operation
    dlObjects.forEach(function(dl) {
        
        var dl = wAPIClient.New('DL', dl);

        console.log(dl.get('mri'));

    });

}, onFailure);

// If you want to retrieve the activty in a specific workspace then -
var dlOptions = {
    workSpaceId: 'AZYPK123ER54HG23'
};

/* Using the Util Washing Machine */

wAPIClient.Fetch('DistributionLists').go().then(function (results) {

    return wAPIClient.Util('WashingMachine', results.body.distributionLists)
            .createNewResources('dl')
            .afterDry();

}).then(function (dlObjects) {

    //loop through dlObjects array for any operation
    dlObjects.forEach(function(dl){
        console.log(dl.get('id'));
    });

}).catch(onFailure);