/* Retrieving DL and Sending Message to them */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/?go#retrieving-distribution-lists) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();


/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    console.log('The DL is', result.body);
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

var DLDefn = {
    id: 'OIUW987WEORIJJ76'
};

// direct via initiation
wAPIClient.DL(DLDefn).retrieve().then( function (results) {
	var dl = results.body;

	return wAPIClient.Message()
			.to(dl.mri) // here we pass the mri of the DL; whispir shall resolve the MRI to individual contacts in the DL and sent message to all of them
			.subject('Sending message using DL')
			.body('Every Contact in the DL shall get the same message body and subject');

}).catch(onFailure);


// If you want to retrieve the DL in a specific workspace then -
var DLOptions = {
	workSpaceId: 'AZYPK123ER54HG23'
};

wAPIClient.DL(DLOptions).id(DLDefn.id).retrieve().then( function (results) {
	var dl = results.body;

	return wAPIClient.Message(DLOptions)
			.to(dl.mri) // here we pass the mri of the DL; whispir shall resolve the MRI to individual contacts in the DL and sent message to all of them
			.subject('Sending message using DL')
			.body('Every Contact in the DL shall get the same message body and subject');

}).catch(onFailure);
