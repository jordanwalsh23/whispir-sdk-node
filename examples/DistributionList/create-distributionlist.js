/* Create DL */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/?go#creating-distribution-lists) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');	

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();


/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    // result contains the location, id of the dl
    console.log('The id is `%s`, the mri is `%s`, and location is `%s`', result.id, result.mri, result.location);
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

var distributionListDefn = {
    name : 'US-Sales',
    description : 'DL for US Sales Group',
    access : 'Open',
    visibility : 'Public',
    contactIds : 'CB4558257DD86D09,AF48A9EC3F02E43C',
    userIds : 'E49ED9EC343CF02E,C727BCE3A813E2B1',
    distListIds : 'CF5AF1AE49ED07A6,9FF7C2B470CCEC1E'
};

// during Initiation
wAPIClient.DL(dlDefn).create().then(onSuccess, onFailure); // dl is short for DistributionList

// direct via create method
wAPIClient.DL().create(DLDefn).then(onSuccess, onFailure);

// If you want to create the DL in a specific workspace then -
var DLOptions = {
	workSpaceId: 'AZYPK123ER54HG23'
};

wAPIClient.DL(DLOptions)
    .create(DLDefn)
    .then(onSuccess, onFailure);

// method chaining
wAPIClient.DL(DLOptions)
  .name(distributionListDefn.name)
  .description(distributionListDefn.description)
  .access(distributionListDefn.access)
  .visibility(distributionListDefn.visibility)
  .addMembers('contact', distributionListDefn.contactIds) // `contact` Or `contactIds`
  .addMembers('user', distributionListDefn.userIds) // `user` Or `userIds`
  .addMembers('dl', distributionListDefn.distListIds) // `dl` Or `distListIds`
  .create()
  .then(onSuccess, onFailure);