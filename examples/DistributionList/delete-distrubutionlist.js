/* Delete DL */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/?go#deleting-distribution-lists) */

/* Note: delete() automatically retrieves the custom fields also */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();


/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    console.log('The DL is', result.body);
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

var DLDefn = {
    id: 'OIUW987WEORIJJ76'
};

// direct via initiation
wAPIClient.DL(DLDefn).delete().then(onSuccess, onFailure);

// using chain method
wAPIClient.DL().id(DLDefn.id).delete().then(onSuccess, onFailure);


// If you want to delete the DL in a specific workspace then -
var DLOptions = {
	workSpaceId: 'AZYPK123ER54HG23'
};

wAPIClient.DL(DLOptions).id(DLDefn.id).delete().then(onSuccess, onFailure);