/* Create DL */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/?go#adding-contacts) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');	

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();


/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    console.log('The DL is update');
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

var distributionListDefn = {
    id: 'C727BCE3A813E2B1'
};

var contactIds = {
  remove:'CB4558257DD86D09',
  add: 'AF48A9EC3F02E43C'
};

var userIds = ['id1', 'id2', 'id3'];

// Updating a single/ multiple fields in DL is done via PUT. There is no PATCH. So the whole DL object has to be passed again, with the updated values.
// So, to easily achieve this, we need to do a GET on the DL first, then update the necessary values, and then do a PUT


wAPIClient.DL().set('id', distributionListDefn.id).retrieve().then( function (result) {
  
  var theDL = wAPIClient.New('DL', result.body)
                        .removeMember('contact', contactIds.remove) // can also be a plural removeMembers('type', arrayOfIds);
                        .addMember('contact', contactIds.add) // can also be a plural addMembers('type', arrayOfIds)
                        .addMembers('user', userIds);

      return theDL.update();

}).then(onSuccess, onFailure);

// If you want to retrieve the DL in a specific workspace then -
var DLOptions = {
  workSpaceId: 'AZYPK123ER54HG23'
};

wAPIClient.DL('DLOptions').set('id', DLDefn.id).retrieve().then( function (result) {
  
  var theDL = wAPIClient.New('DL', result.body)
                        .removeMember('contact', contactIds.remove) // can also be a plural removeMembers('type', arrayOfIds)
                        .addMember('contact', contactIds.add); // can also be a plural addMembers('type', arrayOfIds)

      return theDL.update();

}).then(onSuccess, onFailure);
