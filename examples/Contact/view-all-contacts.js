/* Retrieving All Contact */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/?go#retrieving-contacts) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');	

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();

/*

=============== IMPORTANT WARNING ===============

- Whispir API only returns the partial contact details when a GET is done without a single contact id ( meaning - get all Contacts) 
- so creating a new contact objecct with those limited details is not an issue. But doing an UPDATE on that limited contact properties object results in loss of properties that are not present in this object.
- so ensure that when you want to update a contact:
    - Fetch all contacts
    - create an array of contactids from them
    - loop on this array
    - follow the update-contact.js file code
*/


/* Methods to handle Promise resolve and reject */
var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

/* Using the Fetch */
wAPIClient.Fetch('contacts').go().then(function (results) {

    var contactObjects = results.body.contacts;

    //loop through contactObjects array for any operation
    contactObjects.forEach(function(contact){
        
        contact = wAPIClient.New('contact', contact);

        console.log(contact.get('id'));

    });

}, onFailure);

// If you want to retrieve the activty in a specific workspace then -
var contactOptions = {
	workSpaceId: 'AZYPK123ER54HG23'
};

/* Using the Util Washing Machine */

wAPIClient.Fetch('contacts', contactOptions).go().then(function (results) {

    return wAPIClient.Util('WashingMachine', results.body.contacts)
            .createNewResources('contact')
            .afterDry();

}).then(function (contactObjects) {

    //loop through contactObjects array for any operation
    contactObjects.forEach(function(contact){
        console.log(contact.get('id'));
    });

}).catch(onFailure);