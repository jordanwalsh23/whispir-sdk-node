/* Updating Contact */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/?go#updating-contacts) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');	

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();


/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    console.log('The contact is updated');
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

var contactDefn = {
    firstName: 'Updated John',
    id: 'OIUW987WEORIJJ76'
};

// Updating a single/ multiple fields in contact is done via PUT. There is no PATCH. So the whole contact object has to be passed again, with the updated values.
// So, to easily achieve this, we need to do a GET on the contact first, then update the necessary values, and then do a PUT


wAPIClient.Contact().set('id', contactDefn.id).retrieve().then( function (result) {
  
  var theContact = wAPIClient.New('contact', result.body)
                             .set('firstName', contactDefn.firstName);

      return theContact.update();

}).then(onSuccess, onFailure);

// If you want to retrieve the contact in a specific workspace then -
var contactOptions = {
  workSpaceId: 'AZYPK123ER54HG23'
};

wAPIClient.Contact('contactOptions').set('id', contactDefn.id).retrieve().then( function (result) {
  
  var theContact = wAPIClient.New('contact', result.body)
                              .set('firstName', contactDefn.firstName);

      return theContact.update();

}).then(onSuccess, onFailure);