/* Deleting Contact */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/?go#deleting-a-contact) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');	

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();


/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    console.log('The contact is deleted');
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

var contactDefn = {
    id: 'OIUW987WEORIJJ76'
};

// direct via initiation
wAPIClient.Contact(contactDefn).delete().then(onSuccess, onFailure);

// using chain method
wAPIClient.Contact().set('id', contactDefn.id).delete().then(onSuccess, onFailure);


// If you want to delete the contact in a specific workspace then -
var contactOptions = {
	workSpaceId: 'AZYPK123ER54HG23'
};

wAPIClient.Contact(contactOptions).set('id', contactDefn.id).delete().then(onSuccess, onFailure);