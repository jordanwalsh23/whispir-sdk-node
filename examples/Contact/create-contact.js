/* Create Contact */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/?go#creating-new-contacts) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');	

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();


/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    // result contains the location, id of the contact; oh! yeah contact upon creation gives out its MRI value
    console.log('The id is `%s`, the mri is `%s`, and location is `%s`', result.id, result.mri, result.location);
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

var contactDefn = {
    'firstName': 'John',
    'lastName': 'Wick',
    'status': 'A',
    'timezone': 'Australia/Melbourne',
    'workEmailAddress1': 'jsmith@testcompany.com',
    'workMobilePhone1': '61423456789',
    'workCountry': 'Australia',
    'messagingoptions': [{
          'channel': 'sms',
          'enabled': 'true',
          'primary': 'WorkMobilePhone1'
    },{
          'channel': 'email',
          'enabled': 'true',
          'primary': 'WorkEmailAddress1'
    },{
          'channel': 'voice',
          'enabled': 'true',
          'primary': 'WorkMobilePhone1'
    }]
};

// during Initiation
wAPIClient.Contact(contactDefn).create().then(onSuccess, onFailure);

// direct via create method
wAPIClient.Contact().create(contactDefn).then(onSuccess, onFailure);

// If you want to create the contact in a specific workspace then -
var contactOptions = {
	workSpaceId: 'AZYPK123ER54HG23'
};

wAPIClient.Contact(contactOptions)
    .create(contactDefn)
    .then(onSuccess, onFailure);

// method chaining; in contact every propery (except workSpaceId) is set via the .set(propertyname, value) method
wAPIClient.Contact(contactOptions)
	.set('firstName', contactDefn.firstName)
	.set('lastName', contactDefn.lastName)
	.set('status', contactDefn.status)
	.set('timezone', contactDefn.timezone)
	.set('workEmailAddress1', contactDefn.workEmailAddress1)
	.set('workMobilePhone1', contactDefn.workMobilePhone1)
	.set('workCountry', contactDefn.workCountry)
	.set('messagingoptions', contactDefn.messagingoptions)
	.create()
	.then(onSuccess, onFailure);

// if you like method chaining but this .set() chain is as boring to you as watching paint dry, 
// then you can simply pass the whole object to set() and it will internally set all the individual properties; easy peasy - lemon sqeazy

wAPIClient.Contact(contactOptions)
	.set(contactDefn)
	.create()
	.then(onSuccess, onFailure);