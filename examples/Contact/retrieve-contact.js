/* Retrieving Contact */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/?go#retrieving-contacts) */

/* Note: retrieve() automatically retrieves the custom fields also */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();


/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    console.log('The contact is', result.body);
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

var contactDefn = {
    id: 'OIUW987WEORIJJ76'
};

// direct via initiation
wAPIClient.Contact(contactDefn).retrieve().then(onSuccess, onFailure);

// using chain method
wAPIClient.Contact().set('id', contactDefn.id).retrieve().then(onSuccess, onFailure);


// If you want to retrieve the contact in a specific workspace then -
var contactOptions = {
	workSpaceId: 'AZYPK123ER54HG23'
};

wAPIClient.Contact(contactOptions).set('id', contactDefn.id).retrieve().then(onSuccess, onFailure);