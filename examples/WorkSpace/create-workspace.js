/* Create WorkSpace */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/?go#creating-a-new-workspace) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');	

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();


/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    // result contains the location, id of the WorkSpace
    console.log('The id is `%s`, and location is `%s`', result.id, result.location);
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

var workSpaceDefn = {
  "projectName": "A Space Odessey",
  "projectNumber": "2001",
  "status": "A",
  "billingcostcentre": "Hollywood"
};

wAPIClient.WorkSpace(workSpaceDefn).create().then(onSuccess, onFailure);
wAPIClient.WorkSpace().create(workSpaceDefn).then(onSuccess, onFailure);

// method chaining
wAPIClient.WorkSpace()
		.name(workSpaceDefn.name)
		.number(workSpaceDefn.number)
		.status(workSpaceDefn.status)
		.billingcostcentre(workSpaceDefn.billingcostcentre)
		.create()
		.then(onSuccess, onFailure);