/* Retrieve WorkSpaces */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/?go#retrieving-workspaces) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
//var wAPIClient = new WhispirSDK('username', 'password', 'apikey');
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');	
/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    console.log('WorkSpace is', result);
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

var myWorkSpace = {
  id: '123GKLJKJ23'
};

// directly during initiation
wAPIClient.WorkSpace(myWorkSpace)
		.retrieve()
		.then(onSuccess, onFailure);

// or as method chaining
wAPIClient.WorkSpace()
		.id(myWorkSpace.id)
		.retrieve()
		.then(onSuccess, onFailure);

