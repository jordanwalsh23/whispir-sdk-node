/* Retrieve WorkSpaces */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/?go#retrieving-workspaces) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');	

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();


/* Methods to handle Promise resolve and reject */

var onFailure = function (fail) {
    // contains the response code and error
    console.log(fail);
};

wAPIClient.Fetch('WorkSpaces').go().then(function (results) {

    var WorkSpaceObjects = results.body.workspaces;

    //loop through WorkSpaceObjects array for any operation
    WorkSpaceObjects.forEach(function(WorkSpace){
        
        WorkSpace = wAPIClient.New('WorkSpace', WorkSpace);

        console.log(WorkSpace.get('id'));

    });

}, onFailure);

/* Using the Util Washing Machine */

wAPIClient.Fetch('WorkSpaces').go().then(function (results) {

    return wAPIClient.Util('WashingMachine', results.body.workspaces)
            .createNewResources('WorkSpace')
            .afterDry();

}).then(function (WorkSpaceObjects) {

    //loop through WorkSpaceObjects array for any operation
    WorkSpaceObjects.forEach(function(WorkSpace){
        console.log(WorkSpace.get('id'));
    });

}).catch(onFailure);