/* Retrieve Custom List */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/#retrieving-custom-lists) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
//var wAPIClient = new WhispirSDK('username', 'password', 'apikey');
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');	
/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    console.log('CustomList', result);
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

var myCustomList= {
  id: '123GKLJKJ23'
};

// directly during initiation
wAPIClient.CustomList(myCustomList)
		.retrieve()
		.then(onSuccess, onFailure);

// or as method chaining
wAPIClient.CustomList()
		.id(myCustomList.id)
		.retrieve()
		.then(onSuccess, onFailure);

// If you want to retrieve the CustomList in a specific workspace then -
var CustomListOptions = {
	workSpaceId: 'AZYPK123ER54HG23'
};

wAPIClient.CustomList(CustomListOptions)
		.id(myCustomList.id)
		.retrieve()
		.then(onSuccess, onFailure);
