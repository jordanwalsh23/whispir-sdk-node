/* Retrieve ALL Custom Lists */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/#retrieving-custom-lists) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
//var wAPIClient = new WhispirSDK('username', 'password', 'apikey');
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');	
/* Methods to handle Promise resolve and reject */

/* Methods to handle Promise resolve and reject */

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

wAPIClient.Fetch('CustomLists').go().then(function (results) {

    var CustomListObjects = results.body.customLists;

    //loop through CustomListObjects array for any operation
    CustomListObjects.forEach(function(CustomList){
        
        CustomList = wAPIClient.New('CustomList', CustomList);

        console.log(CustomList.get('id'));

    });

}, onFailure);

// If you want to retrieve the activty in a specific workspace then -
var CustomListOptions = {
	workSpaceId: 'AZYPK123ER54HG23'
};

/* Using the Util Washing Machine */

wAPIClient.Fetch('CustomLists', CustomListOptions).go().then(function (results) {

    return wAPIClient.Util('WashingMachine', results.body.customLists)
            .createNewResources('CustomList')
            .afterDry();

}).then(function (CustomListObjects) {

    //loop through CustomListObjects array for any operation
    CustomListObjects.forEach(function(CustomList){
        console.log(CustomList.get('id'));
    });

}).catch(onFailure);