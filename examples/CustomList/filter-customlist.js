/* Search/Filter Custom List */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/#search/filter-on-custom-lists) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
//var wAPIClient = new WhispirSDK('username', 'password', 'apikey');
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');	
/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    console.log('CustomList', result);
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

var myCustomList= {
  filter: 'name=Category'
};

// directly during initiation
wAPIClient.CustomList()
		.filter(myCustomList.filter)
		.then(onSuccess, onFailure);

// or as method chaining
wAPIClient.CustomList()
		.filter(myCustomList.filter)
		.then(onSuccess, onFailure);

// If you want to retrieve the CustomList in a specific workspace then -
var CustomListOptions = {
	workSpaceId: 'AZYPK123ER54HG23'
};

wAPIClient.CustomList(CustomListOptions)
		.filter(myCustomList.filter)
		.then(onSuccess, onFailure);
