/* Updating Resource */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/?go#updating-a-resource) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');	

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();


/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    console.log('The Resource is updated');
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

var ResourceDefn = {
    id: 'OIUW987WEORIJJ76'
};

// Updating a Resource is done via PUT (all fields are mandatory). There is no PATCH. So the whole Resource object has to be passed again, with the updated values.
// To easily achieve this, we need to do a GET on the Resource first, then update the necessary values, and then do a PUT


wAPIClient.Resource().id(ResourceDefn.id).retrieve().then( function (result) {
  
  var theResource = wAPIClient.New('Resource', result.body)
                             .name('NewFileName.extn');

      return theResource.update();

}).then(onSuccess, onFailure);

// If you want to retrieve the Resource in a specific workspace then -
var ResourceOptions = {
  workSpaceId: 'AZYPK123ER54HG23'
};

wAPIClient.Resource(ResourceOptions).id(ResourceDefn.id).retrieve().then( function (result) {
  
  var theResource = wAPIClient.New('Resource', result.body)
                              .name('NewFileName.extn');

      return theResource.update();

}).then(onSuccess, onFailure);