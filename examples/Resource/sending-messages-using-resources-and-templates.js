/* Using Resource  & Template to send messages*/
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/#sending-messages-using-template-&-resources) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');  

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();

/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    // result contains the location, id of the Message
    console.log('The id is `%s`, and location is `%s`', result.id, result.location);
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

var sendBulkMessage = function (resource){
  // sending a message using a template
  var resourceId = resource.id;

  var message = wAPIClient.Message()// {workSpaceId: workSpaceId} <- pass this if the resource is created in a specific workspace
                  .resource({
                    resource: {
                      resourceId: resourceId,
                      smsMappingField: mobile,
                      emailMappingField: email,
                      voiceMappingField: ""
                    }
                  })
                  .usingTemplateId(templateId); // use of template id optional. You can compose the message directly also

    return message.bulkSend(); // dont use send(); use bulkSend();
};

var resourceDefn = {
    name: "just-two-columns.csv",
    scope: "private",
    mimeType: "text/csv",
    derefUri: "Y29sdW1uMSxjb2x1bW4y"
};

var templateId = 'UY7823BSDNJ7340HSDS';

wAPIClient.Resource(resourceDefn)
    .create()
    .then(sendBulkMessage)
    .then(onSuccess)
    .catch(onFailure);

// If you want to create the activty in a specific workspace then -
var resourceOptions = {
  workSpaceId: 'AZYPK123ER54HG23'
};

wAPIClient.Resource(resourceOptions).create(resourceDefn).then(sendBulkMessage).then(onSuccess).catch(onFailure);

// method chaining
wAPIClient.Resource(resourceOptions)
    .name(resourceDefn.name)
    .scope(resourceDefn.scope)
    .mimeType(resourceDefn.mimeType)
    .derefUri(resourceDefn.derefUri)
    .create()
    .then(sendBulkMessage)
    .then(onSuccess)
    .catch(onFailure);