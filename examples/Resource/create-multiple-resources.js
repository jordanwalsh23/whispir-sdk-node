/* Create Resource */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/?go#creating-a-resource) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');	

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();

/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    // result contains the location, id of the Resource
    console.log('The id is `%s`, and location is `%s`', result.id, result.location);
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

var resourceFiles = [{
        name: "fileName1.csv",
        scope: "private",
        mimeType: "text/csv",
        filePath: 'absolute.path.to.fileName1.csv'
    },{
        name: "fileName2.pdf",
        scope: "public",
        mimeType: "application/pdf",
        filePath: 'absolute.path.to.fileName2.pdf'
    },{
        name: "just-two-columns.csv",
        scope: "private",
        mimeType: "text/csv",
        filePath: 'absolute.path.to.just-two-columns.csv'
    },{
        name: "fileName3.doc",
        scope: "private",
        mimeType: "application/doc",
        filePath: 'absolute.path.to.fileName3.doc'
    }
];

resourceFiles.forEach(function(resourceDefn){

    wAPIClient.Resource() // <- you can pass the workSpaceId here `resourceOptions`
        .name(resourceDefn.name)
        .scope(resourceDefn.scope)
        .mimeType(resourceDefn.mimeType)
        .readFileSync(resourceDefn.filePath)
        .create()
        .then(onSuccess, onFailure);

});

// If you want to create the activty in a specific workspace then -
var resourceOptions = {
	workSpaceId: 'AZYPK123ER54HG23'
};