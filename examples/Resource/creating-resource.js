/* Create Resource */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/?go#creating-a-resource) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');	

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();

/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    // result contains the location, id of the Resource
    console.log('The id is `%s`, and location is `%s`', result.id, result.location);
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

var resourceDefn = {
    name: "just-two-columns.csv",
    scope: "private",
    mimeType: "text/csv",
    derefUri: "Y29sdW1uMSxjb2x1bW4y"
};

wAPIClient.Resource(resourceDefn).create().then(onSuccess, onFailure);
wAPIClient.Resource().create(resourceDefn).then(onSuccess, onFailure);

// If you want to create the activty in a specific workspace then -
var resourceOptions = {
	workSpaceId: 'AZYPK123ER54HG23'
};

wAPIClient.Resource(resourceOptions).create(resourceDefn).then(onSuccess, onFailure);


// method chaining
wAPIClient.Resource(resourceOptions)
		.name(resourceDefn.name)
		.scope(resourceDefn.scope)
		.mimeType(resourceDefn.mimeType)
		.derefUri(resourceDefn.derefUri)
		.create()
		.then(onSuccess, onFailure);