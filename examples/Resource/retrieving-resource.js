/* Retrieving Resource */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/?go#retrieving-existing-resources) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');	

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();


/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    console.log('The Resource is', result);
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

var ResourceDefn = {
    id: 'OIUW987WEORIJJ76'
};

// A GET on resource (without the Id) excludes the derefUri variable. to get it, one must use the individual resourceID and do a retrieve


wAPIClient.Resource().id(ResourceDefn.id).retrieve().then(onSuccess, onFailure);

// If you want to retrieve the Resource in a specific workspace then -
var ResourceOptions = {
  workSpaceId: 'AZYPK123ER54HG23'
};

wAPIClient.Resource(ResourceOptions).id(ResourceDefn.id).retrieve().then(onSuccess, onFailure);