/* Creating Contacts */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/#bulk-contact-import) */


var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');	

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();


/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    // result contains the location, id of the Import
    console.log('The id is `%s`, and location is `%s`', result.id, result.location);
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

var importDefn = {
    resourceId: '4FBBC384BCE3DAABFE3',
    importType : 'contact',
    importOptions: {
        fieldMapping : {
            firstName: 'firstName',
            lastName: 'lastName',
            workMobilePhone1: 'workEmailAddress1',
            workCountry: 'workCountry',
            timezone: 'timezone'
        },
        importMode : 'replace'
    }
};

// during initiation
wAPIClient.Import(importDefn)
    .create()
    .then(onSuccess, onFailure);

// or directly via create method
wAPIClient.Import()
    .create(importDefn)
    .then(onSuccess, onFailure);

// If you want to create the activty in a specific workspace then -
var ImportOptions = {
	workSpaceId: 'AZYPK123ER54HG23'
};

wAPIClient.Import(ImportOptions)
    .create(importDefn)
    .then(onSuccess, onFailure);


// method chaining
wAPIClient.Import(ImportOptions)
	.resourceId(importDefn.resourceId)
	.importType(importDefn.importType)
	.importOptions(importDefn.importOptions)
    .create()
	.then(onSuccess, onFailure);

// method chaining
wAPIClient.Import(ImportOptions)
	.resourceId(importDefn.resourceId)
	.importType(importDefn.importType)
	.fieldMapping(importDefn.importOptions.fieldMapping)
	.importMode(importDefn.importOptions.importMode
	.then(onSuccess, onFailure);