/* Create ResponseRule */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/#creating-response-rules) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');	

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();


/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    // result contains the location, id of the ResponseRule
    console.log('The id is `%s`, and location is `%s`', result.id, result.location);
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

var ResponseRuleDefn = {
  name : 'Response Rule for YES',
  description : '1 for Yes',
  responseTemplatePatterns : {
    responseTemplatePattern : [ {
      name : 'Yes Rule',
      textPrompt : 'YES',
      voicePrompt : '1',
      spokenVoicePrompt : 'to select YES',
      pattern : 'startswith',
      colour : '#00947d'
    } ]
  }
};

wAPIClient.ResponseRule(ResponseRuleDefn).create().then(onSuccess, onFailure);
wAPIClient.ResponseRule().create(ResponseRuleDefn).then(onSuccess, onFailure);

// If you want to create the ResponseRule in a specific workspace then -
var ResponseRuleOptions = {
	workSpaceId: 'AZYPK123ER54HG23'
};

wAPIClient.ResponseRule(ResponseRuleOptions).create(ResponseRuleDefn).then(onSuccess, onFailure);

// method chaining
wAPIClient.ResponseRule(ResponseRuleOptions)
		.name(ResponseRuleDefn.name)
		.description(ResponseRuleDefn.description)
		.responseTemplatePatterns(ResponseRuleDefn.responseTemplatePatterns)
		.create()
		.then(onSuccess, onFailure);

// ----- USING the ResponseTemplatePattern UTIL ----

var wResponseTemplatePattern = wAPIClient.Util('ResponseTemplatePattern');

wResponseTemplatePattern.name('Yes Rule')
    .textReply('YES')
    .toSay('to select YES')
    .press('1')
    .pattern('startswith')
    .colour('#00947d'); // can also be color('#00947d')

wAPIClient.ResponseRule(ResponseRuleOptions)
    .name('Response Rule for YES')
    .description('1 for Yes')
    .responseTemplatePatterns({
        responseTemplatePattern: [wResponseTemplatePattern.toJson()]
    })
    .create()
    .then(onSuccess, onFailure);