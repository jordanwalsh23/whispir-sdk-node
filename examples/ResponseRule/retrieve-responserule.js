/* retrieve ResponseRule */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/#response-rules) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');	

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();


/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    // result contains the location, id of the ResponseRule
    console.log('ResponseRule is', result);
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

var ResponseRuleDefn = {
  id: 'UY8934NJNSDOF8HN'
};

wAPIClient.ResponseRule(ResponseRuleDefn).retrieve().then(onSuccess, onFailure);
wAPIClient.ResponseRule().id(ResponseRuleDefn.id).retrieve().then(onSuccess, onFailure);

// If you want to retrieve the ResponseRule in a specific workspace then -
var ResponseRuleOptions = {
	workSpaceId: 'AZYPK123ER54HG23'
};

wAPIClient.ResponseRule(ResponseRuleOptions).id(ResponseRuleDefn.id).retrieve().then(onSuccess, onFailure);

// method chaining
wAPIClient.ResponseRule(ResponseRuleOptions)
    .id(ResponseRuleDefn.id)
		.retrieve()
		.then(onSuccess, onFailure);
