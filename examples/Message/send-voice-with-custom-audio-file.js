/* Sending SMS */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/#voice-messages) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();


/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    // result contains the location, id of the sent message
    console.log('The id is `%s`, and location is `%s`', result.id, result.location);
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

/* ---------------------------------------------------------------------------- */
/* Single Message Defn */
/* Note that the resources section is inside the email{} not outside of it */
/* ---------------------------------------------------------------------------- */
var VoiceDefn = {
    workSpaceId: 'AZYPK123ER54HG23',
    to : '+10000000',
    subject : 'Test voice call with attachments',
    voice : {
        header : 'Will be replaced by the voiceintro.wav file',
        body : 'Will be replaced by the voicebody.wav file',
        type : 'ConfCall:,ConfAccountNo:,ConfPinNo:,ConfModPinNo:,Pin:'
        resources : {
            attachment : [{
                attachmentName : 'Introduction.wav',
                attachmentDesc : 'voiceintro.wav', //has to be named exactly like this
                derefUri : '...'
            },{
                attachmentName : 'Body.wav',
                attachmentDesc : 'voicebody.wav', // has to be named exactly like this
                derefUri : '...'
            }]
        }
    }
};

/* Defining the message when Initiating it */
wAPIClient.Message(VoiceDefn).send().then(onSuccess, onFailure);

// you can also use the method chaining and direct send method. and .Email()



/* Using the File Reader UTIL */

/*
 * get a base64 representation from the FileReader
 * pass that into the derefUri of the email object under resources
*/

var messageOptions = {
    workSpaceId: 'AZYPK123ER54HG23'
};

var messageProperties = {
    to : '+10000000',
    subject : 'Test voice call with attachments',
     voice : {
        header : 'This is the introduction of the voice call',
        body : 'This is the body of the message',
        type : 'ConfCall:,ConfAccountNo:,ConfPinNo:,ConfModPinNo:,Pin:'
    }
};

var attachmentFiles = ['absolute.path.to.Introduction.wav', 'absolute.path.to.Body.wav'];

var wFileReader = wAPIClient.Util('FileReader', attachmentFiles); // takes an array of absolute file paths

var wVoice = wAPIClient.Message(messageOptions)
               .to(messageProperties.to)
               .subject(messageProperties.subject)
               .voice(messageProperties.voice);

wFileReader.toBase64().then( function (arrayOfbase64ObjectsWithFileNames) { // returns [ {name: 'filename.ext', base64: 'base64encoding'}, {...}, {...} ] 
    
    var attachments = [{
        attachmentName: arrayOfbase64ObjectsWithFileNames[0].name,
        attachmentDesc: 'voiceintro.wav'
        derefUri: arrayOfbase64ObjectsWithFileNames[0].base64 
    },
    {
        attachmentName: arrayOfbase64ObjectsWithFileNames[1].name,
        attachmentDesc: 'voicebody.wav'
        derefUri: arrayOfbase64ObjectsWithFileNames[1].base64 
    }];

    return wVoice.attach('voice', attachments).send();

}).then(onSuccess, onFailure);