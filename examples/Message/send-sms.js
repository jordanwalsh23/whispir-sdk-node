/* Sending SMS */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/#sms-messages) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();


/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    // result contains the location, id of the sent message
    console.log('The id is `%s`, and location is `%s`', result.id, result.location);
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

/* ---------------------------------------------------------------------------- */
/* Single Message Defn */
/* ---------------------------------------------------------------------------- */

var messageDefn = {
    workSpaceId: 'AZYPK123ER54HG23',
    to : '+100000000',
    subject: 'Message Subject',
    body: 'Message Body'
};

/* Defining the message when Initiating it */
wAPIClient.Message(messageDefn).send().then(onSuccess, onFailure);


/* ---------------------------------------------------------------------------- */
/* Method Chaining */
/* ---------------------------------------------------------------------------- */

/* workSpaceId can only be sent while Initiating; then using method chains to define the rest of message properties */

var messageOptions = {
    workSpaceId: 1
};
var messageProperties = {
    to: '+6598765432',
    subject: 'message subject',
    body: 'message body'
};

wAPIClient.Message(messageOptions)
    .to(messageProperties.to)
    .subject(messageProperties.subject)
    .body(messageProperties.body)
    .send().then(onSuccess, onFailure);


/* ---------------------------------------------------------------------------- */
/* via Send Method */
/* ---------------------------------------------------------------------------- */

/* sending the message property directly to the send method | workSpaceId should be sent at initialization only */
wAPIClient.Message(messageOptions)
    .send(messageProperties).then(onSuccess, onFailure);


// Note: If workspaceId is not passed (in any of the above example), the message will be sent in your Default workspace
wAPIClient.Message()
    .to(messageProperties.to)
    .subject(messageProperties.subject)
    .body(messageProperties.body)
    .send().then(onSuccess, onFailure); // message sent in Default workspace

/* you may also use .SMS() in place of .Message() in all the examples above */
wAPIClient.SMS() === wAPIClient.Message(); // is same as


/* Sending same SMS to multiple reciepients */

var messageOptions = {
    workSpaceId: 1
};
var messageProperties = {
    to: '+6598765432, +100000000, +619782370870, +918735803475', // + prefix before countrycode is optional; country code is mandatory as Whispir delivers worldwide, we treat the first 2-3 places of number as country code.
    subject: 'message subject',
    body: 'message body'
};

wAPIClient.Message(messageOptions)
    .to(messageProperties.to)
    .subject(messageProperties.subject)
    .body(messageProperties.body)
    .send().then(onSuccess, onFailure);