/* Sending SMS */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/#voice-messages) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();


/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    // result contains the location, id of the sent message
    console.log('The id is `%s`, and location is `%s`', result.id, result.location);
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

/* ---------------------------------------------------------------------------- */
/* Same as make-voice-call.js. One has to provide the `type` defn with more values as per your callbridge.
/* ---------------------------------------------------------------------------- */
var VoiceDefn = {
    workSpaceId: 'AZYPK123ER54HG23',
    to : "+10000000",
    subject : "Test Voice Call",
    voice : {
        header : "This is the introduction of the voice call",
        body : "This is the body of the message",
        type : "ConfCall:1800500536,ConfAccountNo:12345678,ConfPinNo:1234,ConfModPinNo:1234,Pin:"
    }
};


/* Defining the message when Initiating it */
wAPIClient.Message(VoiceDefn).send().then(onSuccess, onFailure);