/* Sending SMS */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/#email-messages) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();


/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    // result contains the location, id of the sent message
    console.log('The id is `%s`, and location is `%s`', result.id, result.location);
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

/* ---------------------------------------------------------------------------- */
/* Single Message Defn */
/* Note that the resources section is inside the email{} not outside of it */
/* ---------------------------------------------------------------------------- */
var EmailDefn = {
    workSpaceId: 'AZYPK123ER54HG23',
    to : 'john.smith@test.com',
    subject : 'Test Email Message',
    email : {
        body : '<div id="content"><p>This is my content</p></div>',
        footer : 'Email signature goes here.',
        type : 'text/html',
        resources : {
            attachment : [{
                attachmentName : 'fileName.pdf',
                attachmentDesc : 'fileDescription.pdf',
                derefUri : '...base64 representation of the file being uploaded...'
            }]
        }
    }
};

/* Defining the message when Initiating it */
wAPIClient.Message(EmailDefn).send().then(onSuccess, onFailure);

// you can also use the method chaining and direct send method. and .Email()



/* Using the File Reader UTIL */

/*
 * get a base64 representation from the FileReader
 * pass that into the derefUri of the email object under resources
*/

var messageOptions = {
    workSpaceId: 'AZYPK123ER54HG23'
};

var messageProperties = {
    to : 'john.smith@test.com',
    subject : 'Test Email Message',
    email : {
        body : 'Email Body goes here.',
        footer : 'Email signature goes here.',
        type : 'text/plain'
    }
};

var attachmentFiles = ['absolute.path.to.file1.extn', 'absolute.path.to.file2.extn'];

var wFileReader = wAPIClient.Util('FileReader', attachmentFiles); // takes an array of absolute file paths

var wEmail = wAPIClient.Message(messageOptions)
               .to(messageProperties.to)
               .subject(messageProperties.subject)
               .email(messageProperties.email);

wFileReader.toBase64().then( function (arrayOfbase64ObjectsWithFileNames) { // returns [ {name: 'filename.ext', base64: 'base64encoding'}, {...}, {...} ] 
    var attachments = [];
    arrayOfbase64ObjectsWithFileNames.forEach( function (e, i, a) {
        attachments.push({
            attachmentName: e.name,
            attachmentDesc: 'fileDescription'
            derefUri: e.base64 
        });
    });
    return wEmail.attach('email', attachments).send();
}).then(onSuccess, onFailure);