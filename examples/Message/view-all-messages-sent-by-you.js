/* Retrieve/View All Messages */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/#messages) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();


/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    // result contains the location, id of the sent message
    console.log('The messages', result.body);
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Occured', fail);
};

var myWorkSpace = {
    workSpaceId: 'AZYPK123ER54HG23' 
};

/* Using the Fetch */

wAPIClient.Fetch('Messages').go().then(function (results) {

    var messageObjects = results.body.messages;

    //loop through messageObjects array for any operation
    messageObjects.forEach(function(message){
        
        message = wAPIClient.New('message', message);

        console.log(message.get('id'));

    });


}, onFailure);


/* Using the Util Washing Machine */
wAPIClient.Fetch('Messages').go().then(function (results) {

    return wAPIClient.Util('WashingMachine', results.body.messages)
            .createNewResources('Message')
            .afterDry();

}).then(function (messageObjects) {

    //loop through messageObjects array for any operation
    messageObjects.forEach(function(message){
        console.log(message.get('id'));
    });

}).catch(onFailure);