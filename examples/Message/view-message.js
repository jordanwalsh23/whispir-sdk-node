/* Retrieve/View Messages */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/#messages) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();


/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    // result contains the location, id of the sent message
    console.log('The message', result);
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

var myMessage = {
    workSpaceId: 'AZYPK123ER54HG23',
    id: 'MSGF63898HG578GF'
};

// retrieve a message
wAPIClient.Message(myTemplate).retrieve().then(onSuccess, onFailure);


// method chaining

var messageOptions = {
    workSpaceId: 'AZYPK123ER54HG23',
};

var messageProperties = {
    id: 'MSGF63898HG578GF'
};


wAPIClient.Message(messageOptions)
		.id(messageProperties.id)
		.retrieve().then(onSuccess, onFailure);