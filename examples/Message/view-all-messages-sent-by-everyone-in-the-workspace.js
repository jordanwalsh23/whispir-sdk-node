/* Retrieve/View All Messages */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/#messages) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();


/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    // result contains the location, id of the sent message
    console.log('The messages', result.body);
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Occured', fail);
};

var myWorkSpace = {
    workSpaceId: 'AZYPK123ER54HG23' 
};

// retrieve messages in a given workspace. if workspace is not passed, you will get the messages sent from the default workspace
wAPIClient.Message(myWorkSpace).search('viewtype=shared').then(onSuccess, onFailure);


// method chaining
wAPIClient.Message(myWorkSpace)
		.search('viewtype=shared')
        .then(onSuccess, onFailure);


/* ===== Creating message objects ===== */
wAPIClient.Message().search('viewtype=shared').then(function (results) {
    
    var messageObjects = results.body.messages;

    //loop through messageObjects array for any operation
    messageObjects.forEach(function(message){
        
        message = wAPIClient.New('Message', message);

        console.log(message.get('id'));

    });

}, onFailure);

/* ========== using Washing Machine to create Message Objects ========= */
/* WashingMachine is an Util in the SDK. it gives you option to return messages as message Objects; same as above */

wAPIClient.Message().search('viewtype=shared').then(function (results) {

    return wAPIClient.Util('WashingMachine', results.body.messages)
            .createNewResources('Message')
            .afterDry();

}).then(function (messageObjects) {

    //loop through messageObjects array for any operation
    messageObjects.forEach(function(message){
        console.log(message.get('id'));
    });

}).catch(onFailure);
