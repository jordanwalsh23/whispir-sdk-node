/* Sending SMS */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/#voice-messages) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();


/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    // result contains the location, id of the sent message
    console.log('The id is `%s`, and location is `%s`', result.id, result.location);
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

/* ---------------------------------------------------------------------------- */
/* Single Message Defn */
/* ---------------------------------------------------------------------------- */
var VoiceDefn = {
    workSpaceId: 'AZYPK123ER54HG23',
    to : "+100000000",
    subject : "Test Voice Call",
    voice : {
        header : "This is the introduction of the voice call",
        body : "This is the body of the message",
        type : "ConfCall:,ConfAccountNo:,ConfPinNo:,ConfModPinNo:,Pin:"
    }
};


/* Defining the message when Initiating it */
wAPIClient.Message(VoiceDefn).send().then(onSuccess, onFailure);

/* ---------------------------------------------------------------------------- */
/* Method Chaining */
/* ---------------------------------------------------------------------------- */

/* workSpaceId can only be sent while Initiating; then using method chains to define the rest of message properties */

var messageOptions = {
    workSpaceId: 'AZYPK123ER54HG23'
};
var messageProperties = {
    to : "+100000000",
    subject : "Test Voice Call",
    voice : {
        header : "This is the introduction of the voice call",
        body : "This is the body of the message",
        type : "ConfCall:,ConfAccountNo:,ConfPinNo:,ConfModPinNo:,Pin:"
    }
};

wAPIClient.Call(messageOptions)
    .to(messageProperties.to)
    .subject(messageProperties.subject)
    .voice(messageProperties.voice)
    .make().then(onSuccess, onFailure);


/* ---------------------------------------------------------------------------- */
/* via Send Method */
/* ---------------------------------------------------------------------------- */

/* sending the message property directly to the send method | workSpaceId should be sent at initialization only */
wAPIClient.Message(messageOptions)
    .send(messageProperties).then(onSuccess, onFailure);

// you may also use .Voice() in place of .Message() in all the examples above
wAPIClient.Call() === wAPIClient.Message(); // is same as

// send and make are same too - pure convenience in names
wAPIClient.Call().make() === wAPIClient.Message().make() === wAPIClient.Call().send() === wAPIClient.Message().send()