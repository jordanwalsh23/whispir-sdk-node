/* Sending SMS */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/#messages) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();


/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    // result contains the location, id of the sent message
    console.log('The id is `%s`, and location is `%s`', result.id, result.location);
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

/* ---------------------------------------------------------------------------- */
/* All Channels in One Single Message */
/* ---------------------------------------------------------------------------- */

var messageInAllChannels = {
    workSpaceId: 'AZYPK123ER54HG23',
    to: '+10000000, joe@example.com', // passing in both the phonenumber and email address.
    subject: 'Test SMS Message',
    body: 'This is the body of my test SMS message',
    email: {
        body : 'This is the body of my test Email message',
        footer : 'This is the footer of my message (generally where a signature would go)',
        type : 'text/plain'
    },
    voice : {
        header : 'This is the introduction, read out prior to any key press',
        body : 'This is the body of the voice call, read out after the key press',
        type : 'ConfCall:,ConfAccountNo:,ConfPinNo:,ConfModPinNo:,Pin:'
    },
    web : {
        body : 'This is the content of my web publishing or Rich Push Message',
        type : 'text/plain'
    },
    social : {
        social : {
            id : 'social',
            body : 'Twitter Content.'
        },
        social : {
            id : 'social_long',
            body : 'Facebook Content.',
            type : 'text/plain'
        }
    },
    type : 'defaultNoReply',
    features : {
        pushOptions : {
            notifications : 'enabled',
            escalationMins : '3'
        }
    }
};

/* Defining the message when Initiating it */
wAPIClient.Message(messageInAllChannels).send().then(onSuccess, onFailure);


/* ---------------------------------------------------------------------------- */
/* Method Chaining */
/* ---------------------------------------------------------------------------- */

/* workSpaceId can only be sent while Initiating; then using method chains to define the rest of message properties */

var messageOptions = {
    workSpaceId: 1
};
var messageProperties = {
    to: '+6598765432',
    subject: 'message subject',
    body: 'message body'
};

wAPIClient.Message(messageOptions)
    .to(messageProperties.to)
    .subject(messageProperties.subject)
    .body(messageProperties.body)
    .email(templateProperties.email)
    .voice(templateProperties.voice)
    .web(templateProperties.web)
    .social(templateProperties.social)
    .type(templateProperties.type)
    .features(templateProperties.features)
    .send().then(onSuccess, onFailure);


/* ---------------------------------------------------------------------------- */
/* via Send Method */
/* ---------------------------------------------------------------------------- */

/* sending the message property directly to the send method | workSpaceId should be sent at initialization only */
wAPIClient.Message(messageOptions)
    .send(messageProperties).then(onSuccess, onFailure);


// Note: If workspaceId is not passed (in any of the above example), the message will be sent in your Default workspace
wAPIClient.Message()
    .to(messageProperties.to)
    .subject(messageProperties.subject)
    .body(messageProperties.body)
    .email(templateProperties.email)
    .voice(templateProperties.voice)
    .web(templateProperties.web)
    .social(templateProperties.social)
    .type(templateProperties.type)
    .features(templateProperties.features)
    .send().then(onSuccess, onFailure); // message sent in Default workspace

/* you may also use .SMS() in place of .Message() in all the examples above */
wAPIClient.SMS() === wAPIClient.Message(); // is same as