/* Receive Message Status */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/#message-status) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();


/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    // result contains the location, id of the sent message
    console.log(result.body);
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

/* ---------------------------------------------------------------------------- */
/* Single Message Defn */
/* ---------------------------------------------------------------------------- */

var messageDefn = {
    workSpaceId: 'AZYPK123ER54HG23',
    id: 'LKU2487HBKN3343GHJ'
};

/* Defining the message when Initiating it */
wAPIClient.Message(messageDefn).getMessageResponses('summary').then(onSuccess, onFailure);
wAPIClient.Message(messageDefn).getResponses().then(onSuccess, onFailure); //summary is default if nothing is passed; getResponses() is alias for getMessageResponses()


wAPIClient.Message(messageDefn).getResponses('detailed').then(onSuccess, onFailure); //other status available 'detailed'

/* using method chaining */
wAPIClient.Message().id('LKU2487HBKN3343GHJ').getResponses().then(onSuccess, onFailure);
