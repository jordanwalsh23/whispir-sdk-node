/* Sending SMS */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/#email-messages) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();


/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    // result contains the location, id of the sent message
    console.log('The id is `%s`, and location is `%s`', result.id, result.location);
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

/* ---------------------------------------------------------------------------- */
/* Single Message Defn */
/* ---------------------------------------------------------------------------- */
var EmailDefn = {
    workSpaceId: 'AZYPK123ER54HG23',
    to : 'john.smith@test.com',
    subject : 'Test Email Message',
    email : {
        body : '<div id="content"><p>This is my content</p></div>',
        footer : 'Email signature goes here.',
        type : 'text/html'
    }
};

/* Defining the message when Initiating it */
wAPIClient.Message(EmailDefn).send().then(onSuccess, onFailure);

// you can also use the method chaining and direct send method. and .Email()
