/* Sending SMS */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/#email-messages) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();


/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    // result contains the location, id of the sent message
    console.log('The id is `%s`, and location is `%s`', result.id, result.location);
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

/* ---------------------------------------------------------------------------- */
/* Single Message Defn */
/* ---------------------------------------------------------------------------- */
var EmailDefn = {
    workSpaceId: 'AZYPK123ER54HG23',
    to : 'john.smith@test.com',
    subject : 'Test Email Message',
    email : {
        body : 'Email Body goes here.',
        footer : 'Email signature goes here.',
        type : 'text/plain'
    }
};

/* Defining the message when Initiating it */
wAPIClient.Message(EmailDefn).send().then(onSuccess, onFailure);

/* ---------------------------------------------------------------------------- */
/* Method Chaining */
/* ---------------------------------------------------------------------------- */

/* workSpaceId can only be sent while Initiating; then using method chains to define the rest of message properties */

var messageOptions = {
    workSpaceId: 'AZYPK123ER54HG23'
};
var messageProperties = {
    to : 'john.smith@test.com',
    subject : 'Test Email Message',
    email : {
        body : 'Email Body goes here.',
        footer : 'Email signature goes here.',
        type : 'text/plain'
    }
};

wAPIClient.Message(messageOptions)
    .to(messageProperties.to)
    .subject(messageProperties.subject)
    .email(messageProperties.email)
    .send().then(onSuccess, onFailure);


/* ---------------------------------------------------------------------------- */
/* via Send Method */
/* ---------------------------------------------------------------------------- */

/* sending the message property directly to the send method | workSpaceId should be sent at initialization only */
wAPIClient.Message(messageOptions)
    .send(messageProperties).then(onSuccess, onFailure);

// you may also use .Email() in place of .Message() in all the examples above
wAPIClient.Email() === wAPIClient.Message(); // is same as