/* Retrieving Messages Sent via this Event */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/#retrieving-messages-sent-in-relation-to-the-event) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');	

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();


/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    console.log('sent Messages are', result);
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

var EventDefn = {
    label: '2701095 - Outage of Local Systems in Sydney'
};

wAPIClient.Message().label(EventDefn.label).retrieveEventMessages().then(onSuccess, onFailure);

// If you want to retrieve the Event in a specific workspace then -
var EventOptions = {
  workSpaceId: 'AZYPK123ER54HG23'
};

wAPIClient.Message(EventOptions).id(EventDefn.label).retrieveEventMessages().then(onSuccess, onFailure);