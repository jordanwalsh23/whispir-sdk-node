/* Retrieving All Event */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/#events) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();

/* Methods to hanevente Promise resolve and reject */
var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

/* Using the Fetch */
wAPIClient.Fetch('Events').go().then(function (results) {

    var eventObjects = results.body.events; //its 

    //loop through eventObjects array for any operation
    eventObjects.forEach(function(event) {
        
        var event = wAPIClient.New('event', event);

        console.log(event.get('id'));

    });

}, onFailure);

// If you want to retrieve the activty in a specific workspace then -
var eventOptions = {
    workSpaceId: 'AZYPK123ER54HG23'
};

/* Using the Util Washing Machine */

wAPIClient.Fetch('Events').go().then(function (results) {

    return wAPIClient.Util('WashingMachine', results.body.events)
            .createNewResources('event')
            .afterDry();

}).then(function (eventObjects) {

    //loop through eventObjects array for any operation
    eventObjects.forEach(function(event){
        console.log(event.get('id'));
    });

}).catch(onFailure);