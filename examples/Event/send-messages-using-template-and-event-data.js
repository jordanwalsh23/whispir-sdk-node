/* Sending SMS/Email/Voice Using Event Data and predefined Template */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/#sending-messages-using-event-data) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();


/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    // result contains the location, id of the sent message
    console.log('The id is `%s`, and location is `%s`', result.id, result.location);
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

/* ---------------------------------------------------------------------------- */
/* Single Message Defn */
/* ---------------------------------------------------------------------------- */

var messageDefn = {
    workSpaceId: 'AZYPK123ER54HG23',
    to : '+100000000',
    messageTemplateName: 'Sev3 Events', // this can also be messageTemplateId: _templateId_ id you prefer templateID than its name
    eventId: '2EE7FEA3343662BE',
};

/* Defining the message when Initiating it */
wAPIClient.Message(messageDefn).send().then(onSuccess, onFailure);


/* ---------------------------------------------------------------------------- */
/* Method Chaining */
/* ---------------------------------------------------------------------------- */

/* workSpaceId can only be sent while Initiating; then using method chains to define the rest of message properties */

var messageOptions = {
    workSpaceId: 'AZYPK123ER54HG23'
};
var messageProperties = {
    to : '+100000000',
    messageTemplateName: 'Sev3 Events',
    eventId: '2EE7FEA3343662BE',
};

wAPIClient.Message(messageOptions)
    .to(messageProperties.to)
    .usingTemplateName(messageProperties.messageTemplateName)
    .usingEventId(messageProperties.eventId)
    .send().then(onSuccess, onFailure);
