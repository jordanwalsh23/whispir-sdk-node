/* Create Event */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/?go#creating-an-Event) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');	

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();

/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    // result contains the location, id of the Event
    console.log('The id is `%s`, and location is `%s`', result.id, result.location);
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

var EventDefn = {
    eventLabel : '2701095 - Outage of Local Systems in Sydney',
    status : 'Open',
    eventFormList : [ {
        formName : 'MetroEvent',
        eventFieldList : [ 
            {
              name : 'summary',
              value : 'Outage of systems in Sydney'
            }, {
              name : 'status',
              value : 'Open'
            }, {
              name : 'description',
              value : 'ATMs are non responsive, teams to be sent to investigate.'
            }, {
              name : 'category',
              value : 'Internal'
            }, {
              name : 'startDate',
              value : '11/02/2016 17:41:00'
            }, {
              name : 'actionOwner1',
              value : 'John Wick'
            }, {
              name : 'actionDate1',
              value : '11/02/2016 17:41:00'
            }, {
              name : 'actionDetails1',
              value : 'investigation to take place asap.'
            }, {
              name : 'severity',
              value : 'Severity 3 - Minor Outage (Some Service Degradation)'
            } 
        ]
    } ]
};

wAPIClient.Event(EventDefn).create().then(onSuccess, onFailure);
wAPIClient.Event().create(EventDefn).then(onSuccess, onFailure);

// If you want to create the activty in a specific workspace then -
var EventOptions = {
	workSpaceId: 'AZYPK123ER54HG23'
};

wAPIClient.Event(EventOptions).create(EventDefn).then(onSuccess, onFailure);

// method chaining
wAPIClient.Event(EventOptions)
		.eventLabel(EventDefn.eventLabel) // this can also be just .label(EventDefn.eventLabel)
		.status(EventDefn.status)
		.eventFormList(EventDefn.eventFormList)
		.create()
		.then(onSuccess, onFailure);