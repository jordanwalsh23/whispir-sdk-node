/* Create Event - Using Event Util*/
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/?go#creating-an-Event) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');	

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();

/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    // result contains the location, id of the Event
    console.log('The id is `%s`, and location is `%s`', result.id, result.location);
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

var EventForm = wAPIClient.Util('EventForm');

    EventForm.name('MetroEvent')
             .field('summary', 'Outage of systems in Sydney')
             .field('status', 'Open')
             .field('description', 'ATMs are non responsive, teams to be sent to investigate')
             .field('category', 'Internal')
             .field('startDate', '11/02/2016 17:41:00')
             .field('severity', 'Severity 3 - Minor Outage (Some Service Degradation)')
             .action('JohnWick', '11/09/2016 17:41:00', 'investigation to begin immediately', 1); // (owner name, date, details, priority order)

var Event = wAPIClient.Event();

// method chaining
Event(EventOptions)
		.label('2701095 - Outage of Local Systems in Sydney')
		.status('Open')
		.attachEventForm(EventForm.toJSON())
		.create()
		.then(onSuccess, onFailure);

/*

  Above creates an event like this -

var EventDefn = {
    eventLabel : '2701095 - Outage of Local Systems in Sydney',
    status : 'Open',
    eventFormList : [ {
        formName : 'MetroEvent',
        eventFieldList : [ 
            {
              name : 'summary',
              value : 'Outage of systems in Sydney'
            }, {
              name : 'status',
              value : 'Open'
            }, {
              name : 'description',
              value : 'ATMs are non responsive, teams to be sent to investigate.'
            }, {
              name : 'category',
              value : 'Internal'
            }, {
              name : 'startDate',
              value : '11/02/2016 17:41:00'
            }, {
              name : 'actionOwner1',
              value : 'John Wick'
            }, {
              name : 'actionDate1',
              value : '11/09/2015 17:41:00'
            }, {
              name : 'actionDetails1',
              value : 'investigation to take place asap.'
            }, {
              name : 'severity',
              value : 'Severity 3 - Minor Outage (Some Service Degradation)'
            } 
        ]
    } ]
};
*/