/* Deleting an Event */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/#deleting-an-event) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');	

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();


/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    console.log('Event is deleted');
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

var EventDefn = {
    id: '2EE7FEA3343662BE'
};

wAPIClient.Event(EventDefn).delete().then(onSuccess, onFailure);
wAPIClient.Event().id(EventDefn.id).delete().then(onSuccess, onFailure);

// If you want to delete the Event in a specific workspace then -
var EventOptions = {
  workSpaceId: 'AZYPK123ER54HG23'
};

wAPIClient.Event(EventOptions).id(EventDefn.id).delete().then(onSuccess, onFailure);