/* Retrieving User */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/?go#retrieving-Users) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();


/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    console.log('The User is', result.body);
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

var UserDefn = {
    id: 'OIUW987WEORIJJ76'
};

// direct via initiation
wAPIClient.User(UserDefn).retrieve().then(onSuccess, onFailure);

// using chain method
wAPIClient.User().set('id', UserDefn.id).retrieve().then(onSuccess, onFailure);


// If you want to retrieve the User in a specific workspace then -
var UserOptions = {
	workSpaceId: 'AZYPK123ER54HG23'
};

wAPIClient.User(UserOptions).set('id', UserDefn.id).retrieve().then(onSuccess, onFailure);