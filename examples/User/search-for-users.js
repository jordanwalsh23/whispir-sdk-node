/* Searching User */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/?go#searching-for-users) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');	

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();


/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    console.log('The user is', result.body);
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

// in default workspace
wAPIClient.User().search('firstName=John').then(onSuccess, onFailure);


// If you want to search the user in a specific workspace then -
var userOptions = {
	workSpaceId: 'AZYPK123ER54HG23'
};

wAPIClient.User(userOptions).search('firstName=John').then(onSuccess, onFailure);