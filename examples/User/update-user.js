/* Updating User */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/?go#updating-Users) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');	

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();


/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    console.log('The User is updated');
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

var UserDefn = {
    firstName: 'Updated John',
    id: 'OIUW987WEORIJJ76'
};

// Updating a single/ multiple fields in User is done via PUT. There is no PATCH. So the whole User object has to be passed again, with the updated values.
// So, to easily achieve this, we need to do a GET on the User first, then update the necessary values, and then do a PUT

wAPIClient.User().set('id', UserDefn.id).retrieve().then( function (result) {
  
  var theUser = wAPIClient.New('User', result.body)
                          .set('firstName', UserDefn.firstName);

      return theUser.update();

}).then(onSuccess, onFailure);

// If you want to retrieve the User in a specific workspace then -
var UserOptions = {
  workSpaceId: 'AZYPK123ER54HG23'
};

wAPIClient.User('UserOptions').set('id', UserDefn.id).retrieve().then( function (result) {
  
  var theUser = wAPIClient.New('User', result.body)
                          .set('firstName', UserDefn.firstName);

      return theUser.update();

}).then(onSuccess, onFailure);