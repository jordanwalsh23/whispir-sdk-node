/* Retrieving All Users */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/?go#retrieving-users) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');	

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();


/*

=============== IMPORTANT WARNING ===============

- Whispir API only returns the partial user details when a GET is done without a single user id ( meaning - get all users) 
- so creating a new user objecct with those limited details is not an issue. But doing an UPDATE on that limited user properties object results in loss of properties that are not present in this object.
- so ensure that when you want to update a user:
    - Fetch all users
    - create an array of userids from them
    - loop on this array
    - follow the update-user.js file code
*/


/* Methods to handle Promise resolve and reject */
var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

/* Using the Fetch */
wAPIClient.Fetch('users').go().then(function (results) {

    var userObjects = results.body.users;

    //loop through userObjects array for any operation
    userObjects.forEach(function(user){
        
        user = wAPIClient.New('user', user);

        console.log(user.get('id'));

    });

}, onFailure);

// If you want to retrieve the activty in a specific workspace then -
var userOptions = {
	workSpaceId: 'AZYPK123ER54HG23'
};

/* Using the Util Washing Machine */

wAPIClient.Fetch('users', userOptions).go().then(function (results) {

    return wAPIClient.Util('WashingMachine', results.body.users)
            .createNewResources('user')
            .afterDry();

}).then(function (userObjects) {

    //loop through userObjects array for any operation
    userObjects.forEach(function(user){
        console.log(user.get('id'));
    });

}).catch(onFailure);