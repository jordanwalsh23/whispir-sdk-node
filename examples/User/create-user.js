/* Create User */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/?go#creating-new-Users) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');	

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();


/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    // result contains the location, id of the user
    console.log('The id is `%s`, the mri is `%s`, and location is `%s`', result.id, result.mri, result.location);
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

var UserDefn = {
    "firstName": "John",
    "lastName": "Wick",
    "userName": "John.Wick",
    "password": "AmF10gtxy",
    "timezone": "Australia/Melbourne",
    "workEmailAddress1": "jwick@testcompany.com",
    "workMobilePhone1": "61423456789",
    "workCountry": "Australia"
};

// during Initiation
wAPIClient.User(UserDefn).create().then(onSuccess, onFailure);

// direct via create method
wAPIClient.User().create(UserDefn).then(onSuccess, onFailure);

// If you want to create the User in a specific workspace then -
var UserOptions = {
	workSpaceId: 'AZYPK123ER54HG23'
};

wAPIClient.User(UserOptions)
    .create(UserDefn)
    .then(onSuccess, onFailure);

// method chaining; in User every propery (except workSpaceId) is set via the .set(propertyname, value) method
wAPIClient.User(UserOptions)
	.set('firstName', UserDefn.firstName)
	.set('lastName', UserDefn.lastName)
	.set('username', UserDefn.username)
	.set('password', UserDefn.password)
  .set('timezone', UserDefn.timezone)
  .set('workEmailAddress1', UserDefn.workEmailAddress1)
  .set('workMobilePhone1', UserDefn.workMobilePhone1)
  .set('workCountry', UserDefn.workCountry)
	.create()
	.then(onSuccess, onFailure);

// if you like method chaining but this .set() chain is as boring to you as watching paint dry, 
// then you can simply pass the whole object to set() and it will internally set all the individual properties; easy peasy - lemon sqeazy

wAPIClient.User(UserOptions)
	.set(UserDefn)
	.create()
	.then(onSuccess, onFailure);