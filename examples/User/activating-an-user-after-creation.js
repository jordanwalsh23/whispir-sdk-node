/* Activating an User after creation*/
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/?go#deleting-an-user) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');	

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();


/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    // result contains the location, id of the user
    console.log('The id is `%s`, and status is `%s`', result.id, result.body.status);
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};


var userId;

function createUser(userDefn){
  var theUser = wAPIClient.User(userDefn);
    return theUser.create();
}

function getUser(userDetails) {
  if (userDetails.id !== null) {
    userId = userDetails.id;
  }
  return wAPIClient.User({id: userId}).retrieve();
}

function setUserToInActive(result) {
  var userDetails = result.body;
  var user = wAPIClient.User(userDetails)
      .set('status', 'INACTIVE');
      .set('password', UserDefn.password); // password must be sent again
      return user.update();
}

function setUserToActive(result) {
  var userDetails = result.body;
  var user = wAPIClient.User(userDetails)
      .set('status', 'ACTIVE')
      .set('password', UserDefn.password); // password must be sent again
      return user.update();
}

var UserDefn = {
    "firstName": "John",
    "lastName": "WickX",
    "userName": "John.Wick",
    "password": "AmF10gtxy",
    "timezone": "Australia/Melbourne",
    "workEmailAddress1": "jwick89@testcompany.com",
    "workMobilePhone1": "6566688917",
    "workCountry": "Singapore"
  };

createUser(UserDefn)
  .then(getUser)
    .then(setUserToInActive)
      .then(getUser)
        .then(setUserToActive)
          .then(getUser)
            .then(onSuccess)
  .catch(onFailure);