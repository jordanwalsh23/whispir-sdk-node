/* Retrieve All Scenarios */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/?go#retrieving-scenarios) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');	

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();


/* Methods to handle Promise resolve and reject */

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

wAPIClient.Fetch('Scenarios').go().then(function (results) {

    var ScenarioObjects = results.body.scenarios;

    //loop through ScenarioObjects array for any operation
    ScenarioObjects.forEach(function(Scenario){
        
        Scenario = wAPIClient.New('Scenario', Scenario);

        console.log(Scenario.get('id'));

    });

}, onFailure);

// If you want to retrieve the activty in a specific workspace then -
var ScenarioOptions = {
	workSpaceId: 'AZYPK123ER54HG23'
};

/* Using the Util Washing Machine */

wAPIClient.Fetch('Scenarios', ScenarioOptions).go().then(function (results) {

    return wAPIClient.Util('WashingMachine', results.body.scenarios)
            .createNewResources('Scenario')
            .afterDry();

}).then(function (ScenarioObjects) {

    //loop through ScenarioObjects array for any operation
    ScenarioObjects.forEach(function(Scenario){
        console.log(Scenario.get('id'));
    });

}).catch(onFailure);