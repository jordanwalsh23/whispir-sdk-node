/* update Scenario */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/#creating-a-new-scenario) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');	

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();


/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    // result contains the location, id of the Scenario
    console.log('The id is `%s`, and location is `%s`', result.id, result.location);
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

var ScenarioDefn = {
    id: 'IWUPER98345JWNE879',
    title : 'Fire Evacuation Block A',
    description : 'Fire evacuation notification for A block residents',
    message : {
        to : '+1000000000',
        subject : 'Fire Evacuation',
        body : 'A fire has been detected at level 55. Please evacuate the building immediately. Please do not use the lifts.'
    }
};

wAPIClient.Scenario(ScenarioDefn).update().then(onSuccess, onFailure);
wAPIClient.Scenario().update(ScenarioDefn).then(onSuccess, onFailure);

// If you want to update the scenario in a specific workspace then -
var ScenarioOptions = {
	workSpaceId: 'AZYPK123ER54HG23'
};

wAPIClient.Scenario(ScenarioOptions).update(ScenarioDefn).then(onSuccess, onFailure);

// method chaining
wAPIClient.Scenario(ScenarioOptions)
		.title(ScenarioDefn.title)
		.description(ScenarioDefn.description)
		.message(ScenarioDefn.message)
		.update()
		.then(onSuccess, onFailure);