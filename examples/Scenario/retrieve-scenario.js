/* Retrieve Scenario */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/?go#retrieving-scenarios) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
//var wAPIClient = new WhispirSDK('username', 'password', 'apikey');
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');	
/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    console.log('Scenario', result);
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

var myScenario= {
  id: '123GKLJKJ23'
};

// directly during initiation
wAPIClient.Scenario(myScenario)
		.retrieve()
		.then(onSuccess, onFailure);

// or as method chaining
wAPIClient.Scenario()
		.id(myScenario.id)
		.retrieve()
		.then(onSuccess, onFailure);

// If you want to retrieve the scenario in a specific workspace then -
var ScenarioOptions = {
	workSpaceId: 'AZYPK123ER54HG23'
};

wAPIClient.Scenario(ScenarioOptions)
		.id(myScenario.id)
		.retrieve()
		.then(onSuccess, onFailure);
