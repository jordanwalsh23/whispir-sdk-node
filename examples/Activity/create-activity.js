/* Create Activity */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/?go#creating-an-activity-log) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');	

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();


/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    // result contains the location, id of the Activity
    console.log('The id is `%s`, and location is `%s`', result.id, result.location);
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

var activityDefn = {
  "module": "Message",
  "action" : "Send",
  "status" : "Successful",
  "description" : "Message sent via the Whispir's NodeJS library"
};

wAPIClient.Activity(activityDefn).create().then(onSuccess, onFailure);
wAPIClient.Activity().create(activityDefn).then(onSuccess, onFailure);

// If you want to create the activty in a specific workspace then -
var activityOptions = {
	workSpaceId: 'AZYPK123ER54HG23'
};

wAPIClient.Activity(activityOptions).create(activityDefn).then(onSuccess, onFailure);


// method chaining
wAPIClient.Activity(activityOptions)
		.module(activityDefn.module)
		.action(activityDefn.action)
		.status(activityDefn.status)
		.description(activityDefn.description)
		.create()
		.then(onSuccess, onFailure);