/* Retrieve Activities */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/?go#search/filter-on-activity-logs) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
//var wAPIClient = new WhispirSDK('username', 'password', 'apikey');
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');	
/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    console.log('Activity is', result);
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

// get all activities where action is `map` and module is message
wAPIClient.Activity()
		.search('action=Map&module=Message')
		.then(onSuccess, onFailure);

// If you want to retrieve the activty in a specific workspace then -
var activityOptions = {
	workSpaceId: 'AZYPK123ER54HG23'
};

wAPIClient.Activity(activityOptions)
		.search('action=Map&module=Message')
		.then(onSuccess, onFailure);

// refer to the documentation for more details on using the search function

/*

Note: 

The API currently results in a 404 Not Found when there are no activities present in the log for a given search criteria. 
This should not be not confused with a failed response. But rather as No Data Found.

*/