/* Retrieve Activities */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/?go#retrieving-activities) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
//var wAPIClient = new WhispirSDK('username', 'password', 'apikey');
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');	
/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    console.log('Activity is', result);
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

var myActivity = {
  id: '123GKLJKJ23'
};

// directly during initiation
wAPIClient.Activity(myActivity)
		.retrieve()
		.then(onSuccess, onFailure);

// or as method chaining
wAPIClient.Activity()
		.id(myActivity.id)
		.retrieve()
		.then(onSuccess, onFailure);

// If you want to retrieve the activty in a specific workspace then -
var activityOptions = {
	workSpaceId: 'AZYPK123ER54HG23'
};

wAPIClient.Activity(activityOptions)
		.id(myActivity.id)
		.retrieve()
		.then(onSuccess, onFailure);
