/* Retrieve Callback */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/?go#retrieving-callbacks) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');	

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();


/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    console.log('Callback', result);
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

var callbackDefn = {
  id: 'YUN67BH4577IU'
};

// during initiation
wAPIClient.Callback(callbackDefn)
    .retrieve()
    .then(onSuccess, onFailure);

// or directly via retrieve method
wAPIClient.Callback()
    .id(callbackDefn.id)
    .retrieve()
    .then(onSuccess, onFailure);

// If you want to retrieve the activty in a specific workspace then -
var callbackOptions = {
	workSpaceId: 'AZYPK123ER54HG23'
};

wAPIClient.Callback(callbackOptions)
    .id(callbackDefn.id)
    .retrieve()
    .then(onSuccess, onFailure);