/* Delete Callback */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/?go#creating-new-callbacks) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');	

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();


/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    console.log('callback deleted');
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

var callbackDefn = {
  id: 'YUN67BH4577IU'
};

// during initiation
wAPIClient.Callback(callbackDefn)
    .delete()
    .then(onSuccess, onFailure);

// or directly via delete method
wAPIClient.Callback()
    .id(callbackDefn.id)
    .delete()
    .then(onSuccess, onFailure);

// If you want to delete the activty in a specific workspace then -
var callbackOptions = {
	workSpaceId: 'AZYPK123ER54HG23'
};

wAPIClient.Callback(callbackOptions)
    .id(callbackDefn.id)
    .delete()
    .then(onSuccess, onFailure);