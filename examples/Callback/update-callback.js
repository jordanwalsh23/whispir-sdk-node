/* Create Callback */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/?go#creating-new-callbacks) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');	

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();


/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    console.log('callback updated', result);
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

var callbackDefn = {
  'id': 'JHJSHDF87324KJHSJD',
  'name' : 'Updated Callback Name',
  'url' : 'http://myserver.com/mycallback.php',
  'auth' : {
    'type' : 'querystring',
    'key' : 'MY_AUTH_KEY'
  },
  'contentType' : 'json',
  'email' : 'me@example.com',
  'callbacks' : {
    'reply' : 'enabled',
    'undeliverable' : 'enabled'
  }
};

// during initiation
wAPIClient.Callback(callbackDefn)
    .update()
    .then(onSuccess, onFailure);

// or directly via update method
wAPIClient.Callback()
    .id(callbackDefn.id)
    .update(callbackDefn)
    .then(onSuccess, onFailure);

// If you want to update the activty in a specific workspace then -
var callbackOptions = {
	workSpaceId: 'AZYPK123ER54HG23'
};

wAPIClient.Callback(callbackOptions)
    .id(callbackDefn.id)
    .update(callbackDefn)
    .then(onSuccess, onFailure);


// method chaining
wAPIClient.Callback(callbackOptions)
    .id(callbackDefn.id)
		.name(callbackDefn.name)
		.url(callbackDefn.url)
		.contentTypeAsJSON() // can also be .contentType('json')
		.email(callbackDefn.email)
    .authByQueryString(callbackDefn.auth.key) // can also be .auth(callbackDefn.auth)
    .enableForReply()
		.enableForUndeliverable() // can also be just .callbacks(callbackDefn.callbacks)
    .update()
		.then(onSuccess, onFailure);