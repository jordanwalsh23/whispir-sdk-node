/* Retrieve Callback */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/?go#retrieving-callbacks) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');	

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();


/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    // result contains the location, id of the sent callback
    console.log('Callback', result);
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

wAPIClient.Fetch('Callbacks').go().then(function (results) {

    var callbackObjects = results.body.callbacks;

    //loop through callbackObjects array for any operation
    callbackObjects.forEach(function(callback){
        
        callback = wAPIClient.New('Callback', callback);

        console.log(callback.get('id'));

    });

}, onFailure);

// If you want to retrieve the activty in a specific workspace then -
var callbackOptions = {
	workSpaceId: 'AZYPK123ER54HG23'
};

/* Using the Util Washing Machine */

wAPIClient.Fetch('Callbacks', callbackOptions).go().then(function (results) {

    return wAPIClient.Util('WashingMachine', results.body.callbacks)
            .createNewResources('Callback')
            .afterDry();

}).then(function (callbackObjects) {

    //loop through callbackObjects array for any operation
    callbackObjects.forEach(function(callback){
        console.log(callback.get('id'));
    });

}).catch(onFailure);