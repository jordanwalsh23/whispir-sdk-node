/* Sending custom callback paramters that you want to recieve along with message response */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/?go#sending-custom-parameters-in-callback-response) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();


/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    // result contains the location, id of the sent message
    console.log('The id is `%s`, and location is `%s`', result.id, result.location);
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

/* ---------------------------------------------------------------------------- */
/* Single Message Defn */
/* ---------------------------------------------------------------------------- */

var messageDefn = {
    workSpaceId: 'AZYPK123ER54HG23',
    to : '+100000000',
    subject: 'Message Subject',
    body: 'Message Body',
    callbackId: 'MyCallBackName',
    callbackParameters: {
        customerId: '12SIA3GH'
    }
};

/* Defining the message when Initiating it */
wAPIClient.Message(messageDefn).send().then(onSuccess, onFailure);


/* ---------------------------------------------------------------------------- */
/* Method Chaining */
/* ---------------------------------------------------------------------------- */

/* workSpaceId can only be sent while Initiating; then using method chains to define the rest of message properties */

var messageOptions = {
    workSpaceId: 1
};
var messageProperties = {
    to: '+6598765432',
    subject: 'message subject',
    body: 'message body',
    callbackId: 'MyCallBackName',
    callbackParameters: {
        customerId: '12SIA3GH',
        departmentId: 45
    }
};

wAPIClient.Message(messageOptions)
    .to(messageProperties.to)
    .subject(messageProperties.subject)
    .body(messageProperties.body)
    .callbackId(messageProperties.callbackId)
    .callbackParameters(messageProperties.callbackParameters)
    .send().then(onSuccess, onFailure);


/* ---------------------------------------------------------------------------- */
/* via Send Method */
/* ---------------------------------------------------------------------------- */

/* sending the message property directly to the send method | workSpaceId should be sent at initialization only */
wAPIClient.Message(messageOptions)
    .send(messageProperties).then(onSuccess, onFailure);


// Note: If workspaceId is not passed (in any of the above example), the message will be sent in your Default workspace
wAPIClient.Message()
    .to(messageProperties.to)
    .subject(messageProperties.subject)
    .body(messageProperties.body)
    .callbackId(messageProperties.callbackId)
    .callbackParameters(messageProperties.callbackParameters)
    .send().then(onSuccess, onFailure); // message sent in Default workspace