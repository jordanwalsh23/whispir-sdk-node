/* Retrieve Template */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/#retrieving-templates) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();


/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    // result contains the location, id of the sent message
    console.log('The id is `%s`, and location is `%s`', result.id, result.location);
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

var myTemplate = {
    workSpaceId: 'AZYPK123ER54HG23',
    id: 'VJTF12398HG578GF'
};

// retrieve a template
wAPIClient.Template(myTemplate).retrieve().then(onSuccess, onFailure);


// method chaining

var templateOptions = {
    workSpaceId: 'AZYPK123ER54HG23',
};

var templateProperties = {
    id: 'VJTF12398HG578GF'
};


wAPIClient.Template(templateOptions)
		.id(templateProperties.id)
		.retrieve().then(onSuccess, onFailure);