/* Sending SMS Overriding Template */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/#overriding-templates) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();


/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    // result contains the location, id of the sent message
    console.log('The id is `%s`, and location is `%s`', result.id, result.location);
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

/* ---------------------------------------------------------------------------- */
/* Single Message Defn */
/* ---------------------------------------------------------------------------- */

var messageDefn = {
    workSpaceId: 'AZYPK123ER54HG23',
    to: '+100000000',
    subject: 'My subject',
    body: 'My Body Content',
    messageTemplateId: 'VJTF12398HG578GF'
};

/* Defining the message when Initiating it */
wAPIClient.Message(messageDefn).send().then(onSuccess, onFailure);


/* ---------------------------------------------------------------------------- */
/* Method Chaining */
/* ---------------------------------------------------------------------------- */

/* workSpaceId can only be sent while Initiating; then using method chains to define the rest of message properties */

/* ===== Template Id ===== */

/*
 * One can use any of the following 3 chainable methods -
 *
 * .messageTemplateId(templateId)
 * .useTemplateWithIdAs(templateId)
 * .usingTemplateId(templateId)
 *
 */

var messageOptions = {
    workSpaceId: 1
};
var messageProperties = {
    to: '+6598765432',
    subject: 'My subject',
    body: 'My Body Content',
    messageTemplateId: 'VJTF12398HG578GF'
};

wAPIClient.Message(messageOptions)
    .to(messageProperties.to)
    .messageTemplateId(messageProperties.messageTemplateId)
    .send().then(onSuccess, onFailure);


/* ===== Template Name ===== */


var messageOptions = {
    workSpaceId: 1
};
var messageProperties = {
    to: '+6598765432',
    subject: 'My subject',
    body: 'My Body Content',
    messageTemplateName: 'SMS Template' 
};

wAPIClient.Message(messageOptions)
    .to(messageProperties.to)
    .messageTemplateName(messageProperties.messageTemplateName)
    .send().then(onSuccess, onFailure);

/* ---------------------------------------------------------------------------- */
/* via Send Method */
/* ---------------------------------------------------------------------------- */

/* sending the message property directly to the send method | workSpaceId should be sent at initialization only */
wAPIClient.Message(messageOptions)
    .send(messageProperties).then(onSuccess, onFailure);