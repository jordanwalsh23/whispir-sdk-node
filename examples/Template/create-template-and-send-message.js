/* Creating Templates and send Message using that template */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/#creating-templates) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();


/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    // result contains the location, id of the sent message
    console.log('The id is `%s`, and location is `%s`', result.id, result.location);
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

var templateForSMS = {
    workSpaceId: 'AZYPK123ER54HG23',
    messageTemplateName: 'Sample Template',
    messageTemplateDescription: 'Template Description',
    subject: 'message subject',
    body: 'message body',
    email: {},
    voice: {},
    web: {}
};

// creates a template
wAPIClient.Template(templateForSMS).create().then( function (templateDetails) {

    console.log('The template id is `%s`, and location is `%s`', templateDetails.id, templateDetails.location);
    
    wAPIClient.Message({workSpaceId: templateForSMS.workSpaceId})
                    .to('6598765432')
                    .usingTemplateId(templateDetails.id)
                    .send()
                    .then(onSuccess, onFailure);

}, onFailure);