/* Update Template */
/* Refer to Online API Documentation [Link](https://whispir.github.io/api/#updating-templates) */

var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');

// if you want the creds to be picked from the auth.json file (read the Initiating a client section in the readme.md), you can do this
//var wAPIClient = new WhispirSDK();


/* Methods to handle Promise resolve and reject */

var onSuccess = function (result) {
    // result contains the location, id of the sent message
    console.log('Template successfully updated');
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

var templateForAll = {
    workSpaceId: 'AZYPK123ER54HG23',
    id: 'VJTF12398HG578GF', // this is the ID of the template that has to be updated
    name: 'Sample SMS Template', 
    description: 'Template to provide an example on whispir.io',
    responseTemplateId: 'YPK123ER5478GF',
    subject: 'Test SMS Message',
    body: 'This is the body of my test SMS message',
    email: {
        body : 'This is the body of my test Email message',
        footer : 'This is the footer of my message (generally where a signature would go)',
        type : 'text/plain'
    },
    voice : {
        header : 'This is the introduction, read out prior to any key press',
        body : 'This is the body of the voice call, read out after the key press',
        type : 'ConfCall:,ConfAccountNo:,ConfPinNo:,ConfModPinNo:,Pin:'
    },
    web : {
        body : 'This is the content of my web publishing or Rich Push Message',
        type : 'text/plain'
    },
    social : {
        social : {
            id : 'social',
            body : 'Twitter Content.'
        },
        social : {
            id : 'social_long',
            body : 'Facebook Content.',
            type : 'text/plain'
        }
    },
    type : 'defaultNoReply',
    features : {
        pushOptions : {
            notifications : 'enabled',
            escalationMins : '3'
        }
    }
};

// creates a template
wAPIClient.Template(templateForAll).create().then(onSuccess, onFailure);

// you can also send it directly via the create() method

var templateOptions = {
    workSpaceId: 'AZYPK123ER54HG23',
    id: 'VJTF12398HG578GF'
};

var templateProperties = {
    name: 'Sample SMS Template', 
    description: 'Template to provide an example on whispir.io',
    responseTemplateId: 'YPK123ER5478GF',
    subject: 'Test SMS Message',
    body: 'This is the body of my test SMS message',
    email: {
        body : 'This is the body of my test Email message',
        footer : 'This is the footer of my message (generally where a signature would go)',
        type : 'text/plain'
    },
    voice : {
        header : 'This is the introduction, read out prior to any key press',
        body : 'This is the body of the voice call, read out after the key press',
        type : 'ConfCall:,ConfAccountNo:,ConfPinNo:,ConfModPinNo:,Pin:'
    },
    web : {
        body : 'This is the content of my web publishing or Rich Push Message',
        type : 'text/plain'
    },
    social : {
        social : {
            id : 'social',
            body : 'Twitter Content.'
        },
        social : {
            id : 'social_long',
            body : 'Facebook Content.',
            type : 'text/plain'
        }
    },
    type : 'defaultNoReply',
    features : {
        pushOptions : {
            notifications : 'enabled',
            escalationMins : '3'
        }
    }
};

//template id should only be passed in via the templateOption while initiating the template object Or via the .id() method
wAPIClient.Template(templateOptions).update(templateProperties).then(onSuccess, onFailure); 

// method chaining

wAPIClient.Template(templateOptions)
		.name(templateProperties.messageTemplateName)
		.description(templateProperties.messageTemplateDescription)
		.subject(templateProperties.subject)
		.body(templateProperties.body)
		.email(templateProperties.email)
		.voice(templateProperties.voice)
        .web(templateProperties.web)
        .social(templateProperties.social)
        .type(templateProperties.type)
		.features(templateProperties.features)
		.update()
        .then(onSuccess, onFailure);