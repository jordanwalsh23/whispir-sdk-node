var request = require('request');
var Q = require('q');
var $ = require('underscore');

var Scenario = function Scenario(options, clientConfig) {
	var structureKeys = ['id', 'subject', 'message',
	                      'from', 'link', 'createdTime', 'allowedUsers', 'allowedUserIds',
	                      'name', 'description'];
	var links, url, res;

	options = options || {};
	this.structure = {};
	var self = this;
	
	// only add valid properties to Scenario structure
	if (options && options !== "" && Object.keys(options).length !== 0) {
		structureKeys.forEach(function (element) {
			if (options.hasOwnProperty(element)) {
				self.structure[element] = options[element];
			}
		});
	}

	// workSpaceId is pushed into the client config part. this does not update
	// the client in any way as clientConfig contains the client configuration which is `passed by value`
	clientConfig.workSpaceId = options.hasOwnProperty("workSpaceId") ? options.workSpaceId : clientConfig.workSpaceId;

	links = options.hasOwnProperty("link") ? options.link : "";

	if (links !== "" && links != []) {
		url = links[0].uri;
		if (!this.structure.id || this.structure.id === "") {
			// Pulls the ID out of the Scenario URL and pushes it into the ID
			// field.
			res = url.split("?")[0].split("/");
			this.structure.id = res[res.length - 1];
		}
	}

	if (options.workSpaceId) {
		clientConfig.workSpaceId = options.workSpaceId;
	}

	this.client = function () {
		return clientConfig;
	};

};

Scenario.prototype.describe = function () {
	console.log(['\n\n - This Scenario structure is: \n' + this.toJsonString()
	             , '\n\n Scenarios allows users to simplify the message delivery processes through automated one click communications. The process involves creating a fixed message Or a message template combined with a contact or a distribution list. When the scenario is executed, the message is sent to the contacts.'
	             , 'Mandatory fields are: '
	             , '\n\t {'
	             , '\n\t   name : "Fire Alert N1-B10", '
	             , '\n\t   description: "Fire Alert for everyone in Neighbourhood 1, Block 10",'
	             , '\n\t   message: {'
	             , '\n\t     to: "+1000000000",'
	             , '\n\t     subject: "Scenario Emergency Comms - Fire",'
	             , '\n\t     body: "A fire has been detected at level 55 in your Block. Please evacuate the building immediately. Please do not use the lifts.",'
	             , '\n\t     label: "",'
	             , '\n\t 	 email: {},'
	             , '\n\t 	 web: {},'
	             , '\n\t     voice: {},'
	             , '\n\t     social: {}'
	             , '\n\t   }'
	             , '\n\t }'
	             , '\n\n Refer to Online Documentation - https://whispir.github.io/api/#Scenarios'
	             ].join(" "));
	return this;
};

Scenario.prototype.get = function (property) {
	if (this.structure.hasOwnProperty(property)) {
		return this.structure[property];
	}
	if (property === 'workSpaceId') {
		return this.client().workSpaceId;
	}
	throw new Error(property + ' is undefined in Scenario Object');
};

Scenario.prototype.id = function (id) {
	this.structure.id = id;
	return this;
};

Scenario.prototype.name = function (name) {
	this.structure.name = name;
	return this;
};

Scenario.prototype.description = function (description) {
	this.structure.description = description;
	return this;
};

Scenario.prototype.allowedUsers = function (permission, Ids) {
	var permissions = 'EVERYONE, SELECTEDUSERS';

	if (!permission || permission == '' || permissions.indexOf(permission.toUpperCase) == -1) {
		throw new Error(permission + ' is invalid for usage permission. Valid ones are ' + permissions);
	}

	permission = permission.toUpperCase();
	this.structure.allowedUsers = permission;

	if (permission === 'SELECTEDUSERS') {

		if (!Ids || Ids.length === 0) { //length is applicable for array and string
			throw new Error('You did not specify the user Ids that should have access.' 
					+ 'Do it like this, `wScenario.allowedUsers(\'SELECTEDUSERS\', \'CB4558257DD86D09, E49ED9EC343CF02E\')` ' 
					+ 'or `wScenario.allowedUsers(\'SELECTEDUSERS\', [\'CB4558257DD86D09\', \'E49ED9EC343CF02E\'])`');
		}
		//if array is passed, then make it into a string
		if (Array.isArray(Ids)) {
			Ids = Ids.join(',');
		}

		if (this.structure.hasOwnProperty('allowedUserIds')) {
			this.structure.allowedUserIds += ',' + Ids;
		} else {
			this.structure['allowedUserIds'] = Ids;
		}
		//make them unique
		this.structure.allowedUserIds = $.unique(this.structure.allowedUserIds.split(','), false).join();

	} else {
		this.structure.allowedUsers = ''; //everybody now.. rock this scenario now.. backstreet's back... alright!!.. 
	}

	return this;
};

Scenario.prototype.message = function (message) {
	if (message && (typeof message === "object") && (message !== null)) {
		this.structure.message = message;
		return this;
	}
	throw new Error('message should be an object');
};

Scenario.prototype.toJsonString = function () {
	return JSON.stringify(this.structure);
};

Scenario.prototype.create = function (raw) {

	var clientConfig = this.client();

	var headers = {
		'Accept': 'application/vnd.whispir.scenario-v1+json',
		'Content-Type': 'application/vnd.whispir.scenario-v1+json'
	};

	var endpoint = '/scenarios' + '?apikey=' + clientConfig.apiKey

	var data = raw || this.toJsonString();

	return trigger.apply(this, ['post', endpoint, headers, data]);
};

Scenario.prototype.retrieve = function () {

	var ScenarioId = this.structure.id;

	if (!ScenarioId) {
		throw new Error('Scenario id is not provided. You should provide one like this \n\t - wClient.Scenario().id(1) \n\t - wClient.Scenario({id: 1})');
	}

	var clientConfig = this.client();

	var headers = {
		'Accept': 'application/vnd.whispir.scenario-v1+json'
	};

	var endpoint = '/scenarios/' + ScenarioId + '?apikey=' + clientConfig.apiKey;

	return trigger.apply(this, ['get', endpoint, headers]);
};

Scenario.prototype.update = function (raw) {

	var ScenarioId = this.structure.id;

	if (!ScenarioId) {
		throw new Error('Scenario id is not provided. You should provide one like this \n\t - wClient.Scenario().id(1) \n\t - wClient.Scenario({id: 1})');
	}

	var clientConfig = this.client();

	var headers = {
		'Accept': 'application/vnd.whispir.scenario-v1+json',
		'Content-Type': 'application/vnd.whispir.scenario-v1+json'
	};

	var endpoint = '/scenarios/' + ScenarioId + '?apikey=' + clientConfig.apiKey;

	var data = raw || this.toJsonString();

	return trigger.apply(this, ['put', endpoint, headers, data]);
};

Scenario.prototype.delete = function () {

	var ScenarioId = this.structure.id;

	if (!ScenarioId) {
		throw new Error('Scenario id is not provided. You should provide one like this \n\t - wClient.Scenario().id(1) \n\t - wClient.Scenario({id: 1})');
	}

	var clientConfig = this.client();

	var headers = {
		'Accept': 'application/vnd.whispir.scenario-v1+json'
	};

	var endpoint = '/scenarios/' + ScenarioId + '?apikey=' + clientConfig.apiKey;

	return trigger.apply(this, ['delete', endpoint, headers]);
};

var trigger = function trigger(method, fullyFormedEndpoint, headers, data) {
	var deferred = Q.defer();
	var successCodes = [200, 201, 204];
	var clientConfig = this.client();
	var url = function url(clientProperties, endpoint) {
		return clientProperties.host
		+ (clientProperties.workSpaceId === 0 ? '' : ('/workspaces/' + clientProperties.workSpaceId))
		+ endpoint;
	};

	var reqOptions = {
			method: method,
			url: url(clientConfig, fullyFormedEndpoint),
			auth: clientConfig.auth,
			headers: $.extend(headers, clientConfig.headers)
	};

	if (data) {
		reqOptions.body = ( (typeof data === 'string') ? data : JSON.stringify(data));
	}

	var callback = function callback(err, response, body) {
		var result = {
				id: null,
				location: null,
				statusCode: response.statusCode,
				body: ''
		};
		try {
			result.body = JSON.parse(body);
			if (result.body.hasOwnProperty('status') && result.body.status === "No records found") {
				result.body = [];
			}
		} catch (e) {
			result.body = body;
		}

		if (!err && successCodes.indexOf(response.statusCode) != -1) {

			if (reqOptions.method === 'post') {
				result.location = response.headers.location;
				result.id = response.headers.location.split("?")[0].split("/").slice(-1)[0];
			}

			deferred.resolve(result);
		} else {
			deferred.reject(result);
		}
	};
	// console.log(reqOptions);
	// making the request with the reqOptions and call the callback function
	// which will resolve/reject the promise
	request(reqOptions, callback);

	return deferred.promise;
};

module.exports = Scenario;