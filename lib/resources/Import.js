var request = require('request');
var Q = require('q');
var $ = require('underscore');

var Import = function Import(options, clientConfig) {
	var structureKeys = ['id', 'link', 'resourceId', 'importType', 'importOptions'];
	var links, url, res;
	var self = this;

	options = options || {};
	this.structure = {};

	// only add valid properties to Import structure
	if (options && options !== "" && Object.keys(options).length !== 0) {
		structureKeys.forEach(function (element) {
			if (options.hasOwnProperty(element)) {
				self.structure[element] = options[element];
			}
		});
	}

	this.structure.importType = 'contact';

	// workSpaceId is pushed into the client config part. this does not update the client in any way
	clientConfig.workSpaceId = options.hasOwnProperty("workSpaceId") ? options.workSpaceId : clientConfig.workSpaceId;

	links = options.hasOwnProperty("link") ? options.link : "";

	if (links !== "" && links != []) {
		url = links[0].uri;
		if (!this.structure.id || this.structure.id === "") {
			//Pulls the ID out of the Import URL and pushes it into the ID field.
			res = url.split("?")[0].split("/");
			this.structure.id = res[res.length - 1];
		}
	}

	if (options.workSpaceId) {
		clientConfig.workSpaceId = options.workSpaceId;
	}

	this.client = function () {
		return clientConfig;
	}

};

Import.prototype.describe = function () {
	console.log(['\n\n - This Import structure is: \n' + this.toJsonString()
	             , '\n\n Whispir API /imports allow users to import a CSV/JSON/XML file of contacts (via /resources) to be added to the contacts database.\n'
	             , 'This is slightly different from the contact API as it doesn’t support the deleting of contacts. It will only add and update'
	             , 'Mandatory fields are: '
	             , ' \n\t {'
	             , ' \n\t     resourceId: "4FBBC384BCE3DAABFE3",'
	             , ' \n\t     importType: "contact",'
	             , ' \n\t     importOptions: {'
	             , ' \n\t         fieldMapping : {'
	             , ' \n\t             firstName: "firstName",'
	             , ' \n\t             lastName: "lastName",'
	             , ' \n\t             workMobilePhone1: "workEmailAddress1",'
	             , ' \n\t             workCountry: "workCountry",'
	             , ' \n\t             timezone: "timezone"'
	             , ' \n\t         },'
	             , ' \n\t         importMode: "replace"'
	             , ' \n\t     }'
	             , ' \n\t '
	             , ' \n\t }'
	             , '\n\n Refer to Online Documentation - https://whispir.github.io/api/#imports'
	             ].join(" "));
	return this;
};

Import.prototype.get = function (property) {	
	if (this.structure.hasOwnProperty(property)) {
		return this.structure[property];
	}
	if (property === 'workSpaceId') {
		return this.client().workSpaceId;
	}
	throw new Error(property + ' is undefined in Import Object');
};

Import.prototype.id = function (id) {
	this.structure.id = id;
	return this;
};

Import.prototype.resourceId = function (resourceId) {
	this.structure.resourceId = resourceId;
	return this;
};

Import.prototype.importType = function (importType) {
	if (importType.toLowerCase() !== 'contact') {
		throw new Error('importType can only be `contact`');
	}
	this.structure.importType = importType;
	return this;
};

Import.prototype.importOptions = function (importOptions) {
	if ((typeof importOptions === "object") && (importOptions !== null)) {
		this.structure.importOptions = importOptions;
		return this;
	}
	throw new Error('importOptions data should be an object');
};

Import.prototype.fieldMapping = function (fieldMapping) {
	if (!this.structure.importOptions) {
		this.structure.importOptions = {};
	}
	this.structure.importOptions.fieldMapping = fieldMapping;
	return this;
};

Import.prototype.importMode = function (importMode) {
	if (!this.structure.importOptions) {
		this.structure.importOptions = {};
	}
	if ('replace,duplicate,ignore'.indexOf(importMode.toLowerCase()) === -1) {
		throw new Error(importMode + ' is invalid. Valid ones are replace,duplicate,ignore');
	}
	this.structure.importOptions.importMode = importMode;
	return this;
};

Import.prototype.toJsonString = function () {
	return JSON.stringify(this.structure);
};

Import.prototype.create = Import.prototype.run = function (raw) {

	var clientConfig = this.client(),
		headers = {
			'Accept': 'application/vnd.whispir.import-v1+json',
			'Content-Type': 'application/vnd.whispir.import-v1+json'
		},
		endpoint = '/imports?apikey=' + clientConfig.apiKey,
		data = raw || this.toJsonString();

	return trigger.apply(this, ['post', endpoint, headers, data]);
};

Import.prototype.retrieve = function () {
	throw new Error('Retrieving an Import is not supported via the API');
};

Import.prototype.update = function (raw) {
	throw new Error('Updating an Import is not supported via the API');
};

Import.prototype.delete = function () {
	throw new Error('Deleting an Import is not supported via the API');
};

var trigger = function trigger(method, fullyFormedEndpoint, headers, data) {
	var deferred = Q.defer();
	var successCodes = [202];
	var clientConfig = this.client();
	var url = function url(clientProperties, endpoint) {
		return clientProperties.host
		+ (clientProperties.workSpaceId === 0 ? '' : ('/workspaces/' + clientProperties.workSpaceId))
		+ endpoint;
	};

	var reqOptions = {
			method: method,
			url: url(clientConfig, fullyFormedEndpoint),
			auth: clientConfig.auth,
			headers: $.extend(headers, clientConfig.headers)
	};

	if (data) {
		reqOptions.body = ( (typeof data === 'string') ? data : JSON.stringify(data));
	}

	var callback = function callback(err, response, body) {
		var result = {
				id: null,
				location: null,
				url: null,
				statusCode: response.statusCode,
				body: ''
		};
		try {
			result.body = JSON.parse(body);
			if (result.body.hasOwnProperty('status') && result.body.status === "No records found") {
				result.body = [];
			}
		} catch (e) {
			result.body = body;
		}

		if (!err && successCodes.indexOf(response.statusCode) != -1) {

			if (reqOptions.method === 'post') {
				result.location = response.headers.location;
				result.id = response.headers.location.split("?")[0].split("/").slice(-1)[0];
				if (result.body.url) {
					result.url = result.body.url;
				}
			}

			deferred.resolve(result);
		} else {
			deferred.reject(result);
		}
	};
	// console.log(reqOptions);
	// making the request with the reqOptions and call the callback function which will resolve/reject the promise
	request(reqOptions, callback);

	return deferred.promise;
};

module.exports = Import;