var request = require('request');
var Q = require('q');
var $ = require('underscore');

var Event = function Event(options, clientConfig) {
	var structureKeys = ['id', 'eventLabel', 'status', 'eventFormList', 'link'];
	var links, url, res;

	options = options || {};
	this.structure = {};
	var self = this;
	
	if (options && options !== "" && Object.keys(options).length !== 0) {
		structureKeys.forEach(function (element) {
			if (options.hasOwnProperty(element)) {
				self.structure[element] = options[element];
			}
		});
	}

	// workSpaceId is pushed into the client config part. this does not update
	// the client in any way
	clientConfig.workSpaceId = options.hasOwnProperty("workSpaceId") ? options.workSpaceId : clientConfig.workSpaceId;

	links = options.hasOwnProperty("link") ? options.link : "";

	if (links !== "" && links != []) {
		url = links[0].uri;
		if (!this.structure.id || this.structure.id === "") {
			// Pulls the ID out of the Event URL and pushes it into the ID
			// field.
			res = url.split("?")[0].split("/");
			this.structure.id = res[res.length - 1];
		}
	}

	if (options.workSpaceId) {
		clientConfig.workSpaceId = options.workSpaceId;
	}

	this.client = function () {
		return clientConfig;
	};

};

Event.prototype.describe = function () {
	console.log(['\n\n - This Event structure is: \n' + this.toJsonString()
	             , '\n\n Events allow customers to easily input, invoke and track communications about current events that are taking place within their organisation.'
	             , 'Mandatory fields are: '
	             , ' \n\t {'
	             , ' \n\t 	eventLabel : "378563 Outage of local systems in Melbourne",'
	             , ' \n\t 	status : "Active",'
	             , ' \n\t 	eventFormList : [ ]'
	             , ' \n\t }'
	             , '\n\n Refer to Online Documentation - https://whispir.github.io/api/#Events'
	             ].join(" "));
	return this;
};

Event.prototype.get = function (property) {
	if (this.structure.hasOwnProperty(property)) {
		return this.structure[property];
	}
	if (property === 'workSpaceId') {
		return this.client().workSpaceId;
	}
	throw new Error(property + ' is undefined in Event Object');
};

Event.prototype.eventLabel = Event.prototype.label = function (eventLabel) {
	this.structure.eventLabel = eventLabel;
	return this;
};

Event.prototype.status = function (status) {
	var validStatus = 'open,active,closed,resolved';
	if(validStatus.toLowerCase().indexOf(status.toLowerCase()) === -1){
		throw new Error(status + ' is invalid. Valid status are ' + validStatus);
	}
	this.structure.status = status;
	return this;
};

Event.prototype.eventFormList = function (eventFormList) {
	if ((typeof eventFormList === "object") && (eventFormList !== null)) {
		this.structure.eventFormList = eventFormList;
		return this;
	}
	throw new Error('eventFormList should be an array object');
};

Event.prototype.addEventForm = Event.prototype.attachEventForm = function (eventForm) {
	if ((typeof eventForm === "object") && (eventForm !== null)) {
		
		if (!this.structure.eventFormList) {
			this.structure.eventFormList = [];
		}
		
		this.structure.eventFormList.push(eventForm);
		return this;
	}
	throw new Error('eventForm should be an object');
};

Event.prototype.id = function (id) {
	this.structure.id = id;
	return this;
};

Event.prototype.toJsonString = function () {
	return JSON.stringify(this.structure);
};

Event.prototype.create = function (raw) {

	var clientConfig = this.client();

	var headers = {
		'Accept': 'application/vnd.whispir.event-v1+json',
		'Content-Type': 'application/vnd.whispir.event-v1+json'
	};

	var endpoint = '/events' + '?apikey=' + clientConfig.apiKey

	var data = raw ? raw : this.toJsonString();

	return trigger.apply(this, ['post', endpoint, headers, data]);
};

Event.prototype.retrieve = function () {

	var EventId = this.structure.id;

	if (!EventId) {
		throw new Error('Event id is not provided. You should provide one like this \n\t - wClient.Event().id(1) \n\t - wClient.Event({id: 1})');
	}

	var clientConfig = this.client();

	var headers = {
		'Accept': 'application/vnd.whispir.event-v1+json'
	};

	var endpoint = '/events/' + EventId + '?apikey=' + clientConfig.apiKey;

	return trigger.apply(this, ['get', endpoint, headers]);
};

Event.prototype.update = function (raw) {
	var EventId = this.structure.id;

	if (!EventId) {
		throw new Error('Event id is not provided. You should provide one like this \n\t - wClient.Event().id(1) \n\t - wClient.Event({id: 1})');
	}
	
	var clientConfig = this.client();

	var headers = {
		'Accept': 'application/vnd.whispir.event-v1+json',
		'Content-Type': 'application/vnd.whispir.event-v1+json'
	};

	var endpoint = '/events/' + EventId + '?apikey=' + clientConfig.apiKey

	var data = raw || this.toJsonString();

	return trigger.apply(this, ['put', endpoint, headers, data]);
};

Event.prototype.delete = function () {
	var EventId = this.structure.id;

	if (!EventId) {
		throw new Error('Event id is not provided. You should provide one like this \n\t - wClient.Event().id(1) \n\t - wClient.Event({id: 1})');
	}

	var clientConfig = this.client();

	var headers = {
		'Accept': 'application/vnd.whispir.event-v1+json'
	};

	var endpoint = '/events/' + EventId + '?apikey=' + clientConfig.apiKey;

	return trigger.apply(this, ['delete', endpoint, headers]);
};

var trigger = function trigger(method, fullyFormedEndpoint, headers, data) {
	var deferred = Q.defer();
	var successCodes = [200, 201, 204];
	var clientConfig = this.client();
	var url = function url(clientProperties, endpoint) {
		return clientProperties.host
		+ (clientProperties.workSpaceId === 0 ? '' : ('/workspaces/' + clientProperties.workSpaceId))
		+ endpoint;
	};

	var reqOptions = {
			method: method,
			url: url(clientConfig, fullyFormedEndpoint),
			auth: clientConfig.auth,
			headers: $.extend(headers, clientConfig.headers)
	};

	if (data) {
		reqOptions.body = ( (typeof data === 'string') ? data : JSON.stringify(data));
	}

	var callback = function callback(err, response, body) {
		var result = {
				id: null,
				location: null,
				statusCode: response.statusCode,
				body: ''
		};
		try {
			result.body = JSON.parse(body);
			if(result.body.hasOwnProperty('status') && result.body.status === "No records found"){
				result.body = [];
			}
		} catch (e) {
			result.body = body;
		}

		if (!err && successCodes.indexOf(response.statusCode) != -1) {

			if (reqOptions.method === 'post') {
				result.location = response.headers.location;
				result.id = response.headers.location.split("?")[0].split("/").slice(-1)[0];
			}

			deferred.resolve(result);
		} else {
			deferred.reject(result);
		}
	};
	// console.log(reqOptions);
	// making the request with the reqOptions and call the callback function
	// which will resolve/reject the promise
	request(reqOptions, callback);

	return deferred.promise;
};

module.exports = Event;