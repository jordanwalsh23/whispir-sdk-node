var request = require('request');
var Q = require('q');
var $ = require('underscore');

var Template = function Template(options, clientConfig) {
	var structureKeys = ['id', 'subject', 'body', 'label', 'email', 'voice', 'web', 'social',
		'messageTemplateName', 'messageTemplateDescription', 'responseTemplateId', 'notes',
		'type', 'features', 'link',
		'name', 'description', 'responserule'];
	var links, url, res;
	var aliasKeys = { name: 'messageTemplateName', description: 'messageTemplateDescription', responserule: 'responseTemplateId'};

	options = options || {};
	this.structure = {};
	var self = this;

	// only add valid properties to Template structure
	if (options && options !== "" && Object.keys(options).length !== 0) {
		structureKeys.forEach(function (element) {
			if (options.hasOwnProperty(element)) {
				self.structure[element] = options[element];
			}
		});
	}

	// assign alias to original
	Object.keys(aliasKeys).forEach(function (key) {
		if (self.structure[key]) {
			self.structure[aliasKeys[key]] = self.structure[key];
		}
	});

	// workSpaceId is pushed into the client config part. this does not update the client in any way
	clientConfig.workSpaceId = options.hasOwnProperty("workSpaceId") ? options.workSpaceId : clientConfig.workSpaceId;

	links = options.hasOwnProperty("link") ? options.link : "";

	if (links !== "" && links != []) {
		url = links[0].uri;
		if (!this.structure.id || this.structure.id === "") {
			//Pulls the ID out of the Template URL and pushes it into the ID field.
			res = url.split("?")[0].split("/");
			this.structure.id = res[res.length - 1];
		}
	}

	if (options.workSpaceId) {
		clientConfig.workSpaceId = options.workSpaceId;
	}

	this.client = function () {
		return clientConfig;
	}

};

Template.prototype.describe = function () {
	console.log(['\n\n - This Template structure is: \n' + this.toJsonString()
		, '\n\n Template() helps you send a pre-set communication via SMS, Email, Voice, Push, Social channels.'
		, 'Mandatory fields are: '
		, '\n\t {'
		, '\n\t   messageTemplateName : "Sample SMS Template", '
		, '\n\t   messageTemplateDescription: "Template to provide an example on whispir.io",'
		, '\n\t   subject : "common subject for both SMS, Email",'
		, '\n\t   body : "",'
		, '\n\t   email : {},'
		, '\n\t   voice : {},'
		, '\n\t   web : {}'
		, '\n\t }'
		, '\n\n Refer to Online Documentation - https://whispir.github.io/api/#Templates'
	].join(" "));
	return this;
};

Template.prototype.get = function (property) {
	var actuals = { name: 'messageTemplateName', description: 'messageTemplateDescription', responserule: 'responseTemplateId' };
	if (actuals.hasOwnProperty(property)) {
		property = actuals[property];
	}
	if (this.structure.hasOwnProperty(property)) {
		return this.structure[property];
	}
	if (property === 'workSpaceId') {
		return this.client().workSpaceId;
	}
	throw new Error(property + ' is undefined in Template Object');
};

Template.prototype.id = function (id) {
	this.structure.id = id;
	return this;
};

Template.prototype.name = function (name) {
	this.structure.messageTemplateName = name;
	return this;
};

Template.prototype.description = function (description) {
	this.structure.messageTemplateDescription = description;
	return this;
};

Template.prototype.responseTemplateId = Template.prototype.responseRule = function (responseTemplateId) {
	this.structure.responseTemplateId = responseTemplateId;
	return this;
};

Template.prototype.subject = function (subject) {
	this.structure.subject = subject;
	return this;
};

Template.prototype.body = function (body) {
	this.structure.body = body;
	return this;
};

Template.prototype.email = function (email) {
	if ((typeof email === "object") && (email !== null)) {
		this.structure.email = email;
		return this;
	}
	throw new Error('email should be an object');
};

Template.prototype.voice = function (voice) {
	if ((typeof voice === "object") && (voice !== null)) {
		this.structure.voice = voice;
		return this;
	}
	throw new Error('voice should be an object');
};

Template.prototype.web = function (web) {
	if ((typeof web === "object") && (web !== null)) {
		this.structure.web = web;
		return this;
	}
	throw new Error('web should be an object');
};

Template.prototype.social = function (social) {
	if ((typeof social === "object") && (social !== null)) {
		this.structure.social = social;
		return this;
	}
	throw new Error('social should be an object');
};

Template.prototype.type = function (type) {
	this.structure.type = type;
	return this;
};

Template.prototype.features = function (features) {
	if ((typeof features === "object") && (features !== null)) {
		this.structure.features = features;
		return this;
	}
	throw new Error('features should be an object');
};

Template.prototype.toJsonString = function () {
	return JSON.stringify(this.structure);
};

Template.prototype.create = function (raw) {

	var clientConfig = this.client();

	var headers = {
		'Accept': 'application/vnd.whispir.template-v1+json',
		'Content-Type': 'application/vnd.whispir.template-v1+json'
	};

	var endpoint = '/templates' + '?apikey=' + clientConfig.apiKey

	var data = raw || this.toJsonString();

	return trigger.apply(this, ['post', endpoint, headers, data]);
};

Template.prototype.retrieve = function () {

	var TemplateId = this.structure.id;

	if (!TemplateId) {
		throw new Error('Template id is not provided. You should provide one like this \n\t - wClient.Template().id(1) \n\t - wClient.Template({id: 1})');
	}

	var clientConfig = this.client();

	var headers = {
		'Accept': 'application/vnd.whispir.template-v1+json'
	};

	var endpoint = '/templates/' + TemplateId + '?apikey=' + clientConfig.apiKey;

	return trigger.apply(this, ['get', endpoint, headers]);
};

Template.prototype.update = function (raw) {

	var TemplateId = this.structure.id;

	if (!TemplateId) {
		throw new Error('Template id is not provided. You should provide one like this \n\t - wClient.Template().id(1) \n\t - wClient.Template({id: 1})');
	}

	var clientConfig = this.client();

	var headers = {
		'Accept': 'application/vnd.whispir.template-v1+json',
		'Content-Type': 'application/vnd.whispir.template-v1+json'
	};

	var endpoint = '/templates/' + TemplateId + '?apikey=' + clientConfig.apiKey;

	var data = raw || this.toJsonString();

	return trigger.apply(this, ['put', endpoint, headers, data]);
};

Template.prototype.delete = function () {

	var TemplateId = this.structure.id;

	if (!TemplateId) {
		throw new Error('Template id is not provided. You should provide one like this \n\t - wClient.Template().id(1) \n\t - wClient.Template({id: 1})');
	}

	var clientConfig = this.client();

	var headers = {
		'Accept': 'application/vnd.whispir.template-v1+json'
	};

	var endpoint = '/templates/' + TemplateId + '?apikey=' + clientConfig.apiKey;

	return trigger.apply(this, ['delete', endpoint, headers]);
};

var trigger = function trigger(method, fullyFormedEndpoint, headers, data) {
	var deferred = Q.defer();
	var successCodes = [200, 201, 204];
	var clientConfig = this.client();
	var url = function url(clientProperties, endpoint) {
		return clientProperties.host
				+ (clientProperties.workSpaceId === 0 ? '' : ('/workspaces/' + clientProperties.workSpaceId))
				+ endpoint;
		};

	var reqOptions = {
		method: method,
		url: url(clientConfig, fullyFormedEndpoint),
		auth: clientConfig.auth,
		headers: $.extend(headers, clientConfig.headers)
	};

	if (data) {
		reqOptions.body = ( (typeof data === 'string') ? data : JSON.stringify(data));
	}

	var callback = function callback(err, response, body) {
		var result = {
			id: null,
			location: null,
			statusCode: response.statusCode,
			body: ''
		};
		try {
			result.body = JSON.parse(body);
			if(result.body.hasOwnProperty('status') && result.body.status === "No records found"){
				result.body = [];
			}
		} catch (e) {
			result.body = body;
		}

		if (!err && successCodes.indexOf(response.statusCode) != -1) {

			if (reqOptions.method === 'post') {
				result.location = response.headers.location;
				result.id = response.headers.location.split("?")[0].split("/").slice(-1)[0];
			}

			deferred.resolve(result);
		} else {
			deferred.reject(result);
		}
	};
	//console.log(reqOptions);
	// making the request with the reqOptions and call the callback function which will resolve/reject the promise
	request(reqOptions, callback);

	return deferred.promise;
};

module.exports = Template;
