var request = require('request');
var Q = require('q');
var $ = require('underscore');

var ResponseRule = function ResponseRule(options, clientConfig) {
	var structureKeys = ['id', 'name', 'description', 'responseTemplatePatterns', 'link'];
	var links, url, res;

	options = options || {};
	this.structure = {};
	var self = this;

	if (options && options !== "" && Object.keys(options).length !== 0) {
		structureKeys.forEach(function (element) {
			if (options.hasOwnProperty(element)) {
				self.structure[element] = options[element];
			}
		});
	}

	// workSpaceId is pushed into the client config part. this does not update
	// the client in any way
	clientConfig.workSpaceId = options.hasOwnProperty("workSpaceId") ? options.workSpaceId : clientConfig.workSpaceId;

	links = options.hasOwnProperty("link") ? options.link : "";

	if (links !== "" && links != []) {
		url = links[0].uri;
		if (!this.structure.id || this.structure.id === "") {
			// Pulls the ID out of the ResponseRule URL and pushes it into the ID
			// field.
			res = url.split("?")[0].split("/");
			this.structure.id = res[res.length - 1];
		}
	}

	if (options.workSpaceId) {
		clientConfig.workSpaceId = options.workSpaceId;
	}

	this.client = function () {
		return clientConfig;
	};
};

ResponseRule.prototype.describe = function () {
	console.log(['\n\n - This ResponseRule structure is: \n' + this.toJsonString()
	             , '\n\n Using Response Rules in combination with Message Templates allows users to automatically group responses to messages into different response groups for reporting purposes.'
	             , 'Mandatory fields are: '
	             , '\n\t {'
	             , '\n\t   "name" : "Response Rule for Yes",'
	             , '\n\t   "description" : "",'
	             , '\n\t   "responseTemplatePatterns" : {'
	             , '\n\t     "responseTemplatePattern" : [ {'
	             , '\n\t       "name" : "Yes Rule",'
	             , '\n\t       "textPrompt" : "YES",'
	             , '\n\t       "voicePrompt" : "1",'
	             , '\n\t       "spokenVoicePrompt" : "to select YES",'
	             , '\n\t       "pattern" : "startswith",'
	             , '\n\t       "colour" : "#00947d"'
	             , '\n\t     } ]'
	             , '\n\t   }'
	             , '\n\t }'
	             , '\n\n Refer to Online Documentation - https://whispir.github.io/api/#response-rules'
	             ].join(" "));
	return this;
};

ResponseRule.prototype.get = function (property) {
	if (this.structure.hasOwnProperty(property)) {
		return this.structure[property];
	}
	if (property === 'workSpaceId') {
		return this.client().workSpaceId;
	}
	throw new Error(property + ' is undefined in ResponseRule Object');
};

ResponseRule.prototype.id = function (id) {
	this.structure.id = id;
	return this;
};

ResponseRule.prototype.name = function (name) {
	this.structure.name = name;
	return this;
};

ResponseRule.prototype.description = function (description) {
	this.structure.description = description;
	return this;
};

ResponseRule.prototype.responseTemplatePatterns = function (patterns) {
	if ((typeof patterns === "object") && (patterns !== null)) {
		this.structure.responseTemplatePatterns = patterns;
		return this;
	}
	throw new Error('patterns should be an object');
};

ResponseRule.prototype.toJsonString = function () {
	return JSON.stringify(this.structure);
};

ResponseRule.prototype.create = function (raw) {

	var clientConfig = this.client();

	var headers = {
		'Accept': 'application/vnd.whispir.responserule-v1+json',
		'Content-Type': 'application/vnd.whispir.responserule-v1+json'
	};

	var endpoint = '/responserules' + '?apikey=' + clientConfig.apiKey

	var data = raw || this.toJsonString();

	return trigger.apply(this, ['post', endpoint, headers, data]);
};

ResponseRule.prototype.retrieve = function () {

	var ResponseRuleId = this.structure.id;

	if (!ResponseRuleId) {
		throw new Error('ResponseRule id is not provided. You should provide one like this \n\t - wClient.ResponseRule().id(1) \n\t - wClient.ResponseRule({id: 1})');
	}

	var clientConfig = this.client();

	var headers = {
		'Accept': 'application/vnd.whispir.responserule-v1+json'
	};

	var endpoint = '/responserules/' + ResponseRuleId + '?apikey=' + clientConfig.apiKey;

	return trigger.apply(this, ['get', endpoint, headers]);
};

ResponseRule.prototype.update = function (raw) {

	var ResponseRuleId = this.structure.id;

	if (!ResponseRuleId) {
		throw new Error('ResponseRule id is not provided. You should provide one like this \n\t - wClient.ResponseRule().id(1) \n\t - wClient.ResponseRule({id: 1})');
	}

	var clientConfig = this.client();

	var headers = {
		'Accept': 'application/vnd.whispir.responserule-v1+json',
		'Content-Type': 'application/vnd.whispir.responserule-v1+json'
	};

	var endpoint = '/responserules/' + ResponseRuleId + '?apikey=' + clientConfig.apiKey;

	var data = raw || this.toJsonString();

	return trigger.apply(this, ['put', endpoint, headers, data]);
};

ResponseRule.prototype.delete = function () {

	var ResponseRuleId = this.structure.id;

	if (!ResponseRuleId) {
		throw new Error('ResponseRule id is not provided. You should provide one like this \n\t - wClient.ResponseRule().id(1) \n\t - wClient.ResponseRule({id: 1})');
	}

	var clientConfig = this.client();

	var headers = {
		'Accept': 'application/vnd.whispir.responserule-v1+json'
	};

	var endpoint = '/responserules/' + ResponseRuleId + '?apikey=' + clientConfig.apiKey;

	return trigger.apply(this, ['delete', endpoint, headers]);
}

var trigger = function trigger(method, fullyFormedEndpoint, headers, data) {
	var deferred = Q.defer();
	var successCodes = [200, 201, 204];
	var clientConfig = this.client();
	var url = function url(clientProperties, endpoint) {
		return clientProperties.host
		+ (clientProperties.workSpaceId === 0 ? '' : ('/workspaces/' + clientProperties.workSpaceId))
		+ endpoint;
	};

	var reqOptions = {
			method: method,
			url: url(clientConfig, fullyFormedEndpoint),
			auth: clientConfig.auth,
			headers: $.extend(headers, clientConfig.headers)
	};

	if (data) {
		reqOptions.body = ( (typeof data === 'string') ? data : JSON.stringify(data));
	}

	var callback = function callback(err, response, body) {
		var result = {
				id: null,
				location: null,
				statusCode: response.statusCode,
				body: ''
		};
		try {
			result.body = JSON.parse(body);
			if(result.body.hasOwnProperty('status') && result.body.status === "No records found"){
				result.body = [];
			}
		} catch (e) {
			result.body = body;
		}

		if (!err && successCodes.indexOf(response.statusCode) != -1) {

			if (reqOptions.method === 'post') {
				result.location = response.headers.location;
				result.id = response.headers.location.split("?")[0].split("/").slice(-1)[0];
			}

			deferred.resolve(result);
		} else {
			deferred.reject(result);
		}
	};
	// console.log(reqOptions);
	// making the request with the reqOptions and call the callback function
	// which will resolve/reject the promise
	request(reqOptions, callback);

	return deferred.promise;
};

module.exports = ResponseRule;