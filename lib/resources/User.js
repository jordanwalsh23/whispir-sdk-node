var request = require('request');
var Q = require('q');
var $ = require('underscore');

var User = function User(options, clientConfig) {
	var structureKeys = ['id', 'link'];
	var links, url, res;
	var self = this;

	options = options || {};
	this.structure = {};

	// only add valid properties to User structure
	if (options && options !== "" && Object.keys(options).length !== 0) {
		Object.keys(options).forEach(function (key) {
			self.structure[key] = options[key];
		});
	}

	// workSpaceId is pushed into the client config part. this does not update the client in any way
	clientConfig.workSpaceId = options.hasOwnProperty("workSpaceId") ? options.workSpaceId : clientConfig.workSpaceId;

	links = options.hasOwnProperty("link") ? options.link : "";

	if (links !== "" && links != []) {
		url = links[0].uri;
		if (!this.structure.id || this.structure.id === "") {
			//Pulls the ID out of the User URL and pushes it into the ID field.
			res = url.split("?")[0].split("/");
			this.structure.id = res[res.length - 1];
		}
	}

	if (options.workSpaceId) {
		clientConfig.workSpaceId = options.workSpaceId;
	}

	this.client = function () {
		return clientConfig;
	}

	return this;
};

User.prototype.describe = function () {
	console.log(['\n\n - This User structure is: \n' + this.toJsonString()
		, '\n\n Users serve the purpose of managing the users of the platform. One can - Retrieve users of a workspace - Add new users - Modify user status (except their own) - Modify user’s information - Delete user'
		, 'Mandatory fields are: '
		, '\n\t {'
		, '\n\t     "firstName": "John",'
		, '\n\t     "lastName": "Wick",'
		, '\n\t     "userName": "John.Wick",'
		, '\n\t     "password": "AmF10gtx",'
		, '\n\t     "timezone": "Australia/Melbourne",'
		, '\n\t     "workEmailAddress1": "jwick@testcompany.com",'
		, '\n\t     "workMobilePhone1": "61423456789",'
		, '\n\t     "workCountry": "Australia"'
		, '\n\t }'
		, '\n\n Refer to Online Documentation - https://whispir.github.io/api/#Users'
	].join(" "));
	return this;
}

User.prototype.get = function (property) {
	if (this.structure.hasOwnProperty(property)) {
		return this.structure[property];
	}
	if (property === 'workSpaceId') {
		return this.client().workSpaceId;
	}
	throw new Error(property + ' is undefined in User Object');
}

User.prototype.set = function (property, value) {
	var self = this;
	if(typeof property === 'object'){
		Object.keys(property).forEach(function (key) {
			self.set(key, property[key]);
		});
	}else{
		if (property.toLowerCase() === 'workspaceid') {
			throw new Error('workSpaceId cannot be set using this method. It should only be sent in while creating the User instance. Ex: `wClient.User({workSpaceId: \'' + value + '\', ...})`');
		}else{
			self.structure[property] = value;
		}
	}
	return self;
}

User.prototype.toJsonString = function () {
	return JSON.stringify(this.structure);
}

User.prototype.handlePasswordForUpdate = function () {
	if (this.structure.password) {
		delete this.structure.password;
	}
	return this;
};

User.prototype.create = function (raw) {

	var clientConfig = this.client();

	var headers = {
		'Accept': 'application/vnd.whispir.user-v1+json',
		'Content-Type': 'application/vnd.whispir.user-v1+json'
	};

	var endpoint = '/users' + '?apikey=' + clientConfig.apiKey

	var data = raw ? raw : this.toJsonString();

	return trigger.apply(this, ['post', endpoint, headers, data]);
}

User.prototype.retrieve = function () {

	var UserId = this.structure.id;

	if (!UserId) {
		throw new Error('User id is not provided. You should provide one like this \n\t - wClient.User().set(\'id\',1) \n\t - wClient.User({id: 1})');
		return;
	}

	var clientConfig = this.client();

	var headers = {
		'Accept': 'application/vnd.whispir.user-v1+json'
	};

	var endpoint = '/users/' + UserId + '?apikey=' + clientConfig.apiKey;

	return trigger.apply(this, ['get', endpoint, headers]);
}

User.prototype.search = function (params) {

	var clientConfig = this.client();

	var headers = {
		'Accept': 'application/vnd.whispir.user-v1+json'
	};

	var endpoint = '/users' + '?apikey=' + clientConfig.apiKey + (params ? ('&' + params) : '');

	return trigger.apply(this, ['get', endpoint, headers]);
}

User.prototype.update = function (raw) {

	var UserId = this.structure.id;

	if (!UserId) {
		throw new Error('User id is not provided. You should provide one like this \n\t - wClient.User().set(\'id\',1) \n\t - wClient.User({id: 1})');
		return;
	}

	var clientConfig = this.client();

	var headers = {
		'Accept': 'application/vnd.whispir.user-v1+json',
		'Content-Type': 'application/vnd.whispir.user-v1+json'
	};

	var endpoint = '/users/' + UserId + '?apikey=' + clientConfig.apiKey;

	var data = raw ? raw : this.toJsonString();

	return trigger.apply(this, ['put', endpoint, headers, data]);
}

User.prototype.delete = function () {

	var UserId = this.structure.id;

	if (!UserId) {
		throw new Error('User id is not provided. You should provide one like this \n\t - wClient.User().set(\'id\',1) \n\t - wClient.User({id: 1})');
		return;
	}

	var clientConfig = this.client();

	var headers = {
		'Accept': 'application/vnd.whispir.user-v1+json'
	};

	var endpoint = '/users/' + UserId + '?apikey=' + clientConfig.apiKey;

	return trigger.apply(this, ['delete', endpoint, headers]);
}

var trigger = function trigger(method, fullyFormedEndpoint, headers, data) {
	var deferred = Q.defer();
	var successCodes = [200, 201, 204];
	var clientConfig = this.client();
	var url = function url(clientProperties, endpoint) {
		return clientProperties.host
			+ (clientProperties.workSpaceId === 0 ? '' : ('/workspaces/' + clientProperties.workSpaceId))
			+ endpoint;
	};

	var reqOptions = {
		method: method,
		url: url(clientConfig, fullyFormedEndpoint),
		auth: clientConfig.auth,
		headers: $.extend(headers, clientConfig.headers)
	};

	if (data) {
		reqOptions.body = ( (typeof data === 'string') ? data : JSON.stringify(data));
	}

	var callback = function callback(err, response, body) {
		var result = {
			id: null,
			location: null,
			statusCode: response.statusCode,
			body: ''
		};
		try {
			result.body = JSON.parse(body);
			if(result.body.hasOwnProperty('status') && result.body['status'] === "No records found"){
				result.body = [];
			}
		} catch (e) {
			result.body = body;
		}

		if (!err && successCodes.indexOf(response.statusCode) != -1) {

			if (reqOptions.method === 'post') {
				result.location = response.headers.location;
				result.id = response.headers.location.split("?")[0].split("/").slice(-1)[0];
			}

			deferred.resolve(result);
		} else {
			deferred.reject(result);
		}
	};
	//console.log(reqOptions);
	// making the request with the reqOptions and call the callback function which will resolve/reject the promise
	request(reqOptions, callback);

	return deferred.promise;
}

module.exports = User;
