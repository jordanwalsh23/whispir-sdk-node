var request = require('request');
var Q = require('q');
var $ = require('underscore');

var CustomList = function CustomList(options, clientConfig) {
	var structureKeys = ['id', 'name', 'type', 'createdDate',
	                      'sortType', 'linked', 'link'];
	var links, url, res;

	options = options || {};
	this.structure = {};
	var self = this;
	
	if (options && options !== "" && Object.keys(options).length !== 0) {
		structureKeys.forEach(function (element) {
			if (options.hasOwnProperty(element)) {
				self.structure[element] = options[element];
			}
		});
	}

	// workSpaceId is pushed into the client config part. this does not update
	// the client in any way
	clientConfig.workSpaceId = options.hasOwnProperty("workSpaceId") ? options.workSpaceId : clientConfig.workSpaceId;

	links = options.hasOwnProperty("link") ? options.link : "";

	if (links !== "" && links != []) {
		url = links[0].uri;
		if (!this.structure.id || this.structure.id === "") {
			// Pulls the ID out of the CustomList URL and pushes it into the ID
			// field.
			res = url.split("?")[0].split("/");
			this.structure.id = res[res.length - 1];
		}
	}

	if (options.workSpaceId) {
		clientConfig.workSpaceId = options.workSpaceId;
	}

	this.client = function () {
		return clientConfig;
	};

};

CustomList.prototype.describe = function () {
	console.log(['\n\n - This CustomList structure is: \n' + this.toJsonString()
	             , '\n\n Custom Lists are user specific translations or preferred options in the various list controls used in the platform tool. These provide the freedom for the user to customize the list options in various modules like Messages, Events. These custom list either can complement or completely overwrite the existing list items.'
	             , '\n\n Refer to Online Documentation - https://whispir.github.io/api/#custom-lists'
	             ].join(" "));
	return this;
};

CustomList.prototype.get = function (property) {
	if (this.structure.hasOwnProperty(property)) {
		return this.structure[property];
	}
	if (property === 'workSpaceId') {
		return this.client().workSpaceId;
	}
	throw new Error(property + ' is undefined in CustomList Object');
};

CustomList.prototype.id = function (id) {
	this.structure.id = id;
	return this;
};

CustomList.prototype.toJsonString = function () {
	return JSON.stringify(this.structure);
};

CustomList.prototype.create = function () {
	throw new Error('Creating a CustomList is not supported via the API');
};

CustomList.prototype.retrieve = function () {

	var CustomListId = this.structure.id;

	if (!CustomListId) {
		throw new Error('CustomList id is not provided. You should provide one like this \n\t - wClient.CustomList().id(1) \n\t - wClient.CustomList({id: 1})');
	}

	var clientConfig = this.client();

	var headers = {
		'Accept': 'application/vnd.whispir.customlist-v1+json'
	};

	var endpoint = '/customlists/' + CustomListId + '?apikey=' + clientConfig.apiKey;

	return trigger.apply(this, ['get', endpoint, headers]);
};

CustomList.prototype.filter = function (params) {

	var clientConfig = this.client();

	var headers = {
		'Accept': 'application/vnd.whispir.customlist-v1+json'
	};

	var endpoint = '/customlists/' + CustomListId + '?apikey=' + clientConfig.apiKey + '&' + params;

	return trigger.apply(this, ['get', endpoint, headers]);
};

CustomList.prototype.update = function () {
	throw new Error('Updating a CustomList is not supported via the API');
};

CustomList.prototype.delete = function () {
	throw new Error('Deleting a CustomList is not supported via the API');
};

var trigger = function trigger(method, fullyFormedEndpoint, headers, data) {
	var deferred = Q.defer();
	var successCodes = [200, 204];
	var clientConfig = this.client();
	var url = function url(clientProperties, endpoint) {
		return clientProperties.host
		+ (clientProperties.workSpaceId === 0 ? '' : ('/workspaces/' + clientProperties.workSpaceId))
		+ endpoint;
	};

	var reqOptions = {
			method: method,
			url: url(clientConfig, fullyFormedEndpoint),
			auth: clientConfig.auth,
			headers: $.extend(headers, clientConfig.headers)
	};

	if (data) {
		reqOptions.body = ( (typeof data === 'string') ? data : JSON.stringify(data));
	}

	var callback = function callback(err, response, body) {
		var result = {
				id: null,
				location: null,
				statusCode: response.statusCode,
				body: ''
		};
		try {
			result.body = JSON.parse(body);
			if(result.body.hasOwnProperty('status') && result.body.status === "No records found"){
				result.body = [];
			}
		} catch (e) {
			result.body = body;
		}

		if (!err && successCodes.indexOf(response.statusCode) != -1) {
			deferred.resolve(result);
		} else {
			deferred.reject(result);
		}
	};
	// console.log(reqOptions);
	// making the request with the reqOptions and call the callback function
	// which will resolve/reject the promise
	request(reqOptions, callback);

	return deferred.promise;
};

module.exports = CustomList;