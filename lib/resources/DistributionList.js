var request = require('request');
var Q = require('q');
var $ = require('underscore');

var DistributionList = function DistributionList(options, clientConfig) {
	var structureKeys = ['id', 'link', 'name', 'description', 'access', 'visibility', 'contactIds', 'userIds', 'distListIds', 'memberCount', 'mri', 'location'];
	var links, url, res;
	var self = this;

	options = options || {};
	this.structure = {};

	// only add valid properties to message structure
	if (options !== "" && options !== {}) {
		structureKeys.forEach(function (element) {
			if (options.hasOwnProperty(element)) {
				self.structure[element] = options[element];
			}
		});
	}

	// workSpaceId is pushed into the client config part. this does not update
	// the client in any way
	clientConfig.workSpaceId = options.hasOwnProperty("workSpaceId") ? options.workSpaceId : clientConfig.workSpaceId;

	links = options.hasOwnProperty("link") ? options.link : "";

	if (links !== "" && links != []) {
		url = links[0].uri;
		if (!this.structure.id || this.structure.id === "") {
			// Pulls the ID out of the DistributionList URL and pushes it into the ID
			// field.
			res = url.split("?")[0].split("/");
			this.structure.id = res[res.length - 1];
		}
	}

	if (options.workSpaceId) {
		clientConfig.workSpaceId = options.workSpaceId;
	}

	this.validProperties = function () {
		return structureKeys;
	}

	this.client = function () {
		return clientConfig;
	}

};

DistributionList.prototype.describe = function () {
	console.log(['\n\n - This DistributionList structure is: \n' + this.toJsonString()
	             , 'Whispir’s API allows users to categorise their contacts into different groups to simplify the distribution of messages. This allows for'
	             , '\n\n'
	             , '\n\t - Effective management and maintenance of contacts'
	             , '\n\t - Group messaging'
	             , '\n\t - Reporting'
	             , '\n\n'
	             , '\n These logical groups can help to target specific communications to specific people. A contact can be part of any number of groups.', 'Mandatory fields are: '
	             , 'Mandatory fields are: '
	             , '\n\t{'
	             , '\n\t    name : "My Distribution List",'
	             , '\n\t    description: "",'
	             , '\n\t    access: "Open",'
	             , '\n\t    visibility: "Public",'
	             , '\n\t    contactIds: "CB4558257DD86D09,AF48A9EC3F02E43C",'
	             , '\n\t    userIds: "E49ED9EC343CF02E,C727BCE3A813E2B1"'
	             , '\n\t    distListIds: "CF5AF1AE49ED07A6,9FF7C2B470CCEC1E"'
	             , '\n\t}'
	             , '\n\n Refer to Online Documentation - https://whispir.github.io/api/#DistributionLists'
	             ].join(" "));
	return this;
};

DistributionList.prototype.get = function (property) {
	if (this.structure.hasOwnProperty(property)) {
		return this.structure[property];
	}
	if (property === 'workSpaceId') {
		return this.client().workSpaceId;
	}
	throw new Error(property + ' is undefined in DistributionList Object');
};

DistributionList.prototype.set = function (property, value) {
	var self = this;
	if (typeof property === 'object') {
		Object.keys(property).forEach(function (key) {
			self.set(key, property[key]);
		});
		return self;
	} else {
		if (property.toLowerCase() === 'workspaceid') {
			throw new Error('workSpaceId cannot be set using this method. It should only be sent in while creating the DistributionList instance. Ex: `wClient.DistributionList({workSpaceId: \'' + value + '\', ...})`');
		} else {
			if (this.validProperties().indexOf(property) !== -1) {
				self.structure[property] = value;
				return self;
			}
			throw new Error(property + ' is undefined in DistributionList Object');
		}
	}
};

DistributionList.prototype.id = function (id) {
	this.structure.id = id;
	return this;
};

DistributionList.prototype.name = function (name) {
	this.structure.name = name;
	return this;
};

DistributionList.prototype.description = function (description) {
	this.structure.description = description;
	return this;
};

DistributionList.prototype.access = function (access) {
	var validAccess = 'Open, ByApproval, Restricted';
	if (validAccess.toLowerCase().indexOf(access.toLowerCase()) == -1) {
		throw new Error(access + ' is invalid. Valid access are - ' + validAccess);
	}
	this.structure.access = access;
	return this;
};

DistributionList.prototype.visibility = function (visibility) {
	var validVisibility = 'Public, Private';
	if (validVisibility.toLowerCase().indexOf(visibility.toLowerCase()) == -1) {
		throw new Error(visibility + ' is invalid. Valid visibility are - ' + validVisibility);
	}
	this.structure.visibility = visibility;
	return this;
};

DistributionList.prototype.addMember = DistributionList.prototype.addMembers = function(type, memberId) {
	var typeMapping = {contact: 'contactIds', user: 'userIds', dl :'distListIds'};
	var prop;
	if (!memberId || memberId.length === 0) {
		throw new Error('You did not specify the memberId to add. Do it like this, `wDistributionList.addMember(\'contact\', \'CB4558257DD86D09\')`');
	}
	if (Array.isArray(memberId)) {
		memberId = memberId.join(',');
	}
	type = type.toLowerCase();
	if (typeMapping.hasOwnProperty(type)) {
		prop = typeMapping[type];
		if (this.structure.hasOwnProperty(prop)) {
			this.structure[prop] += ',' + memberId;
		} else {
			this.structure[prop] = memberId;
		}
		this.structure[prop] = $.unique(this.structure[prop].split(','), false).join();
		return this;
	}
	throw new Error('Cannot add a member to `' + type + '` type. It is invalid. Valid ones are - ' + Object.keys(typeMapping).join(', '));
};

DistributionList.prototype.removeMember = DistributionList.prototype.removeMembers = function(type, memberId) {
	var typeMapping = {contact: 'contactIds', user: 'userIds', dl :'distListIds'};
	var prop;
	if (!memberId || memberId.length === 0) {
		throw new Error('Remove no one? You did not specify whom to remove. Do it like this, `wDistributionList.removeMember(\'contact\', \'CB4558257DD86D09\')`');
	}
	type = type.toLowerCase();
	if (typeMapping.hasOwnProperty(type)) {
		prop = typeMapping[type];
		if (this.structure.hasOwnProperty(prop)) { // is this property already set in the structure ?
			if (Array.isArray(memberId)) {
				this.structure[prop] = $.difference(this.structure[prop].split(','), memberId).join();
			} else {
				this.structure[prop] = $.without(this.structure[prop].split(','), memberId).join();
			}
		}
		return this;
	}
	throw new Error('Cannot remove a member from `' + type + '` type. It is invalid. Valid ones are - ' + Object.keys(typeMapping).join(', '));
};

DistributionList.prototype.location = function (location) {
	this.structure.location = location;
	return this;
};

DistributionList.prototype.toJsonString = function () {
	return JSON.stringify(this.structure);
};

DistributionList.prototype.create = function (raw) {

	var clientConfig = this.client();

	var headers = {
		'Accept': 'application/vnd.whispir.distributionlist-v1+json',
		'Content-Type': 'application/vnd.whispir.distributionlist-v1+json'
	};

	var endpoint = '/distributionlists' + '?apikey=' + clientConfig.apiKey

	var data = raw || this.toJsonString();

	return trigger.apply(this, ['post', endpoint, headers, data]);
};

DistributionList.prototype.retrieve = function () {

	var DistributionListId = this.structure.id;

	if (!DistributionListId) {
		throw new Error('DistributionList id is not provided. You should provide one like this \n\t - wClient.DistributionList().id(1) \n\t - wClient.DistributionList({id: 1})');
	}

	var clientConfig = this.client();

	var headers = {
		'Accept': 'application/vnd.whispir.distributionlist-v1+json'
	};

	var endpoint = '/distributionlists/' + DistributionListId + '?apikey=' + clientConfig.apiKey;

	return trigger.apply(this, ['get', endpoint, headers]);
};

DistributionList.prototype.update = function (raw) {

	var DistributionListId = this.structure.id;

	if (!DistributionListId) {
		throw new Error('DistributionList id is not provided. You should provide one like this \n\t - wClient.DistributionList().id(1) \n\t - wClient.DistributionList({id: 1})');
	}

	var clientConfig = this.client();

	var headers = {
		'Accept': 'application/vnd.whispir.distributionlist-v1+json',
		'Content-Type': 'application/vnd.whispir.distributionlist-v1+json'
	};

	var endpoint = '/distributionlists/' + DistributionListId + '?apikey=' + clientConfig.apiKey;

	var data = raw || this.toJsonString();

	return trigger.apply(this, ['put', endpoint, headers, data]);
};

DistributionList.prototype.delete = function () {

	var DistributionListId = this.structure.id;

	if (!DistributionListId) {
		throw new Error('DistributionList id is not provided. You should provide one like this \n\t - wClient.DistributionList().id(1) \n\t - wClient.DistributionList({id: 1})');
	}

	var clientConfig = this.client();

	var headers = {
		'Accept': 'application/vnd.whispir.distributionlist-v1+json'
	};

	var endpoint = '/distributionlists/' + DistributionListId + '?apikey=' + clientConfig.apiKey;

	return trigger.apply(this, ['delete', endpoint, headers]);
};

var trigger = function trigger(method, fullyFormedEndpoint, headers, data) {
	var deferred = Q.defer();
	var successCodes = [200, 201, 204];
	var clientConfig = this.client();
	var url = function url(clientProperties, endpoint) {
		return clientProperties.host
		+ (clientProperties.workSpaceId === 0 ? '' : ('/workspaces/' + clientProperties.workSpaceId))
		+ endpoint;
	};

	var reqOptions = {
			method: method,
			url: url(clientConfig, fullyFormedEndpoint),
			auth: clientConfig.auth,
			headers: $.extend(headers, clientConfig.headers)
	};

	if (data) {
		reqOptions.body = ( (typeof data === 'string') ? data : JSON.stringify(data));
	}

	var callback = function callback(err, response, body) {
		var result = {
				id: null,
				location: null,
				mri: null,
				statusCode: response.statusCode,
				body: ''
		};
		try {
			result.body = JSON.parse(body);
			if (result.body.hasOwnProperty('status') && result.body.status === "No records found") {
				result.body = [];
			}
		} catch (e) {
			result.body = body;
		}

		if (!err && successCodes.indexOf(response.statusCode) != -1) {

			if (reqOptions.method === 'post') {
				result.location = response.headers.location;
				result.id = response.headers.location.split("?")[0].split("/").slice(-1)[0];
				result.mri = result.body.mri;
			}

			deferred.resolve(result);
		} else {
			deferred.reject(result);
		}
	};
	// console.log(reqOptions);
	// making the request with the reqOptions and call the callback function
	// which will resolve/reject the promise
	request(reqOptions, callback);

	return deferred.promise;
};

module.exports = DistributionList;
