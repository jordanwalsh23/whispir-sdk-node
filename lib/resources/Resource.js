var request = require('request');
var Q = require('q');
var $ = require('underscore');
var fs = require('fs');

var Resource = function Resource(options, clientConfig) {
	var structureKeys = ['id', 'link', 'name', 'scope', 'mimeType', 'derefUri', 'url'];
	var links, url, res;
	var self = this;

	options = options || {};
	this.structure = {};
	
	// only add valid properties to Resource structure
	if (options && options !== "" && Object.keys(options).length !== 0) {
		structureKeys.forEach(function (element) {
			if (options.hasOwnProperty(element)) {
				self.structure[element] = options[element];
			}
		});
	}

	// workSpaceId is pushed into the client config part. this does not update the client in any way
	clientConfig.workSpaceId = options.hasOwnProperty("workSpaceId") ? options.workSpaceId : clientConfig.workSpaceId;

	links = options.hasOwnProperty("link") ? options.link : "";

	if (links !== "" && links != []) {
		url = links[0].uri;
		if (!this.structure.id || this.structure.id === "") {
			//Pulls the ID out of the Resource URL and pushes it into the ID field.
			res = url.split("?")[0].split("/");
			this.structure.id = res[res.length - 1];
		}
	}

	if (options.workSpaceId) {
		clientConfig.workSpaceId = options.workSpaceId;
	}

	this.client = function () {
		return clientConfig;
	}

	return this;
};

Resource.prototype.describe = function () {
	console.log(['\n\n - This Resource structure is: \n' + this.toJsonString()
		, '\n\n Whispir API /resources allow application developers to submit, retrieve, update, and delete resources. These resources can be used as part of either Whispir Bulk (Dynamic) Messages, or to be used for importing Contacts.'
		, 'Mandatory fields are: '
		, ' \n\t {'
		, ' \n\t   name : "sample-file.csv",'
		, ' \n\t   scope : "private",'
		, ' \n\t   mimeType : "text/csv",'
		, ' \n\t   derefUri : "...base64 representation..."'
		, ' \n\t }'
		, '\n\n Refer to Online Documentation - https://whispir.github.io/api/#resources'
	].join(" "));
	return this;
};

Resource.prototype.get = function (property) {	
	if (this.structure.hasOwnProperty(property)) {
		return this.structure[property];
	}
	if (property === 'workSpaceId') {
		return this.client().workSpaceId;
	}
	throw new Error(property + ' is undefined in Resource Object');
}

Resource.prototype.id = function (id) {
	this.structure.id = id;
	return this;
};

Resource.prototype.name = function (name) {
	this.structure.name = name;
	return this;
};

Resource.prototype.scope = function (scope) {
	if('public,private'.indexOf(scope.toLowerCase()) === -1){
		throw new Error(scope + ' is invalid scope. Valid ones are public,private');
	}
	this.structure.scope = scope;
	return this;
};

Resource.prototype.mimeType = function (mimeType) {
	this.structure.mimeType = mimeType;
	return this;
};

Resource.prototype.derefUri = Resource.prototype.base64Content = Resource.prototype.derefUri = function (derefUri) {
	this.structure.derefUri = derefUri;
	return this;
};

Resource.prototype.readFileSync = function (fileWithPath) {
    var fileContent = fs.readFileSync(fileWithPath);
    this.structure.derefUri = new Buffer(fileContent).toString('base64');
	return this;
};
/*
Resource.prototype.file = function (fileWithPath) {
	var deferred = Q.defer();
	fs.readFile(fileWithPath, 'utf-8', function (err, data){
		if (err){
			deferred.reject(err);
		}else{
			deferred.resolve(new Buffer(data).toString('base64'));
		}
	});
	return deferred.promise; 
}*/

Resource.prototype.toJsonString = function () {
	return JSON.stringify(this.structure);
};

Resource.prototype.create = function (raw) {

	var clientConfig = this.client();

	var headers = {
		'Accept': 'application/vnd.whispir.resource-v1+json',
		'Content-Type': 'application/vnd.whispir.resource-v1+json'
	};

	var endpoint = '/resources' + '?apikey=' + clientConfig.apiKey

	var data = raw ? raw : this.toJsonString();

	return trigger.apply(this, ['post', endpoint, headers, data]);
};

Resource.prototype.retrieve = function () {

	var ResourceId = this.structure.id;

	if (!ResourceId) {
		throw new Error('Resource id is not provided. You should provide one like this \n\t - wClient.Resource().id(1) \n\t - wClient.Resource({id: 1})');
		return;
	}

	var clientConfig = this.client();

	var headers = {
		'Accept': 'application/vnd.whispir.resource-v1+json'
	};

	var endpoint = '/resources/' + ResourceId + '?apikey=' + clientConfig.apiKey;

	return trigger.apply(this, ['get', endpoint, headers]);
};

Resource.prototype.update = function (raw) {

	var ResourceId = this.structure.id;

	if (!ResourceId) {
		throw new Error('Resource id is not provided. You should provide one like this \n\t - wClient.Resource().id(1) \n\t - wClient.Resource({id: 1})');
		return;
	}

	var clientConfig = this.client();

	var headers = {
		'Accept': 'application/vnd.whispir.resource-v1+json',
		'Content-Type': 'application/vnd.whispir.resource-v1+json'
	};

	var endpoint = '/resources/' + ResourceId + '?apikey=' + clientConfig.apiKey;

	var data = raw ? raw : this.toJsonString();

	return trigger.apply(this, ['put', endpoint, headers, data]);
};

Resource.prototype.delete = function () {

	var ResourceId = this.structure.id;

	if (!ResourceId) {
		throw new Error('Resource id is not provided. You should provide one like this \n\t - wClient.Resource().id(1) \n\t - wClient.Resource({id: 1})');
		return;
	}

	var clientConfig = this.client();

	var headers = {
		'Accept': 'application/vnd.whispir.resource-v1+json'
	};

	var endpoint = '/resources/' + ResourceId + '?apikey=' + clientConfig.apiKey;

	return trigger.apply(this, ['delete', endpoint, headers]);
};

var trigger = function trigger(method, fullyFormedEndpoint, headers, data) {
	var deferred = Q.defer();
	var successCodes = [200, 201, 204];
	var clientConfig = this.client();
	var url = function url(clientProperties, endpoint) {
		return clientProperties.host
			+ (clientProperties.workSpaceId === 0 ? '' : ('/workspaces/' + clientProperties.workSpaceId))
			+ endpoint;
	};

	var reqOptions = {
		method: method,
		url: url(clientConfig, fullyFormedEndpoint),
		auth: clientConfig.auth,
		headers: $.extend(headers, clientConfig.headers)
	};

	if (data) {
		reqOptions.body = ( (typeof data === 'string') ? data : JSON.stringify(data));
	}

	var callback = function callback(err, response, body) {
		var result = {
			id: null,
			location: null,
			url: null,
			statusCode: response.statusCode,
			body: ''
		};
		try {
			result.body = JSON.parse(body);
			if(result.body.hasOwnProperty('status') && result.body.status === "No records found"){
				result.body = [];
			}
		} catch (e) {
			result.body = body;
		}

		if (!err && successCodes.indexOf(response.statusCode) != -1) {

			if (reqOptions.method === 'post') {
				result.location = response.headers.location;
				result.id = response.headers.location.split("?")[0].split("/").slice(-1)[0];
				if(result.body.url){
					result.url = result.body.url;
				}
			}

			deferred.resolve(result);
		} else {
			deferred.reject(result);
		}
	};
	//console.log(reqOptions);
	// making the request with the reqOptions and call the callback function which will resolve/reject the promise
	request(reqOptions, callback);

	return deferred.promise;
};

module.exports = Resource;