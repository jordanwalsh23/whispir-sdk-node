var request = require('request');
var Q = require('q');
var $ = require('underscore');

var Activity = function Activity(options, clientConfig) {
	var structureKeys = ['id', 'action', 'module',
	                      'workspace', 'user', 'status', 'description', 'time', 'link'];
	var links, url, res;

	options = options || {};
	this.structure = {};
	var self = this;
	
	if (options && options !== "" && Object.keys(options).length !== 0) {
		structureKeys.forEach(function (element) {
			if (options.hasOwnProperty(element)) {
				self.structure[element] = options[element];
			}
		});
	}

	// workSpaceId is pushed into the client config part. this does not update
	// the client in any way
	clientConfig.workSpaceId = options.hasOwnProperty("workSpaceId") ? options.workSpaceId : clientConfig.workSpaceId;

	links = options.hasOwnProperty("link") ? options.link : "";

	if (links !== "" && links != []) {
		url = links[0].uri;
		if (!this.structure.id || this.structure.id === "") {
			// Pulls the ID out of the Activity URL and pushes it into the ID
			// field.
			res = url.split("?")[0].split("/");
			this.structure.id = res[res.length - 1];
		}
	}

	if (options.workSpaceId) {
		clientConfig.workSpaceId = options.workSpaceId;
	}

	this.client = function () {
		return clientConfig;
	};

};

Activity.prototype.describe = function () {
	console.log(['\n\n - This Activity structure is: \n' + this.toJsonString()
	             , '\n\n Activities are all the individual changes, tasks, calls, messages.. just about everything performed in a given company workspaces. Each Activity log entry contains information about the workspace, the module(message, email, IVR..), the user who performed it, time stamps, description of the activity, and its status (success, fail) etc.'
	             , 'Mandatory fields are: '
	             , '\n\t {'
	             , '\n\t   module: "Message",'
	             , '\n\t   action : "Send",'
	             , '\n\t   status : "Successful",'
	             , '\n\t   description : "Message sent via the Whispir\'s API library"'
	             , '\n\t }'
	             , '\n\n Refer to Online Documentation - https://whispir.github.io/api/#Activities'
	             ].join(" "));
	return this;
};

Activity.prototype.get = function (property) {
	if (this.structure.hasOwnProperty(property)) {
		return this.structure[property];
	}
	if (property === 'workSpaceId') {
		return this.client().workSpaceId;
	}
	throw new Error(property + ' is undefined in Activity Object');
};

Activity.prototype.action = function (action) {
	var validActions = 'Create,Update,Move,Copy,Draft,Send,Modified,Delete,Contact Import File,Login,Approve,Reject,Dispatch,Register,Accept,Closed,Map,Un-map';
	if(validActions.toLowerCase().indexOf(action.toLowerCase()) == -1){
		throw new Error(action + ' is invalid. Valid actions are ' + validActions);
	}
	this.structure.action = action;
	return this;
};

Activity.prototype.module = function (module) {
	var validModules = 'System,Message,Scheduled Message,User,Contact,DistributionList,Template,Workspace,Event,WebService,Settings,Conversation,Gateway,Workspace Mapping,Folders,Team,RSS,API Mapping,Asset,Instruction';
	if(validModules.toLowerCase().indexOf(module.toLowerCase()) == -1){
		throw new Error(module + ' is invalid. Valid modules are ' + validModules);
	}
	this.structure.module = module;
	return this;
};

Activity.prototype.status = function (status) {
	var validStatus = 'Successful,Failed,Rejected';
	if(validStatus.toLowerCase().indexOf(status.toLowerCase()) == -1){
		throw new Error(status + ' is invalid. Valid status are ' + validStatus);
	}
	this.structure.status = status;
	return this;
};

Activity.prototype.id = function (id) {
	this.structure.id = id;
	return this;
};

Activity.prototype.description = function (description) {
	this.structure.description = description;
	return this;
};

Activity.prototype.toJsonString = function () {
	return JSON.stringify(this.structure);
};

Activity.prototype.create = function (raw) {

	var clientConfig = this.client();

	var headers = {
		'Accept': 'application/vnd.whispir.activity-v1+json',
		'Content-Type': 'application/vnd.whispir.activity-v1+json'
	};

	var endpoint = '/activities' + '?apikey=' + clientConfig.apiKey

	var data = raw || this.toJsonString();

	return trigger.apply(this, ['post', endpoint, headers, data]);
};

Activity.prototype.retrieve = function () {

	var ActivityId = this.structure.id;

	if (!ActivityId) {
		throw new Error('Activity id is not provided. You should provide one like this \n\t - wClient.Activity().id(1) \n\t - wClient.Activity({id: 1})');
	}

	var clientConfig = this.client();

	var headers = {
			'Accept': 'application/vnd.whispir.activity-v1+json'
	};

	var endpoint = '/activities/' + ActivityId + '?apikey=' + clientConfig.apiKey;

	return trigger.apply(this, ['get', endpoint, headers]);
};

Activity.prototype.search = function (params) {

	var clientConfig = this.client();
	
	var headers = {
		'Accept': 'application/vnd.whispir.activity-v1+json'
	};

	var endpoint = '/activities' + '?apikey=' + clientConfig.apiKey + (params ? ('&' + params) : '');

	return trigger.apply(this, ['get', endpoint, headers]);
};

Activity.prototype.update = function () {
	throw new Error('Updating an Activity is not supported');
};

Activity.prototype.delete = function () {
	throw new Error('Deleting an Activity is not supported');
};

var trigger = function trigger(method, fullyFormedEndpoint, headers, data) {
	var deferred = Q.defer();
	var successCodes = [200, 201, 404]; //404 ? yes. coz /activities give a 404 when no data is found, rather than 204. yep. its a bug. sorry.
	var clientConfig = this.client();
	var url = function url(clientProperties, endpoint) {
		return clientProperties.host
		+ (clientProperties.workSpaceId === 0 ? '' : ('/workspaces/' + clientProperties.workSpaceId))
		+ endpoint;
	};

	var reqOptions = {
			method: method,
			url: url(clientConfig, fullyFormedEndpoint),
			auth: clientConfig.auth,
			headers: $.extend(headers, clientConfig.headers)
	};

	if (data) {
		reqOptions.body = ( (typeof data === 'string') ? data : JSON.stringify(data));
	}

	var callback = function callback(err, response, body) {
		var result = {
				id: null,
				location: null,
				statusCode: response.statusCode,
				body: ''
		};
		try {
			result.body = JSON.parse(body);
			if(result.body.hasOwnProperty('status') && result.body.status === "No records found"){
				result.body = [];
			}
		} catch (e) {
			result.body = body;
		}

		if (!err && successCodes.indexOf(response.statusCode) != -1) {

			if (reqOptions.method === 'post') {
				result.location = response.headers.location;
				result.id = response.headers.location.split("?")[0].split("/").slice(-1)[0];
			}

			deferred.resolve(result);
		} else {
			deferred.reject(result);
		}
	};
	// console.log(reqOptions);
	// making the request with the reqOptions and call the callback function
	// which will resolve/reject the promise
	request(reqOptions, callback);

	return deferred.promise;
};

module.exports = Activity;