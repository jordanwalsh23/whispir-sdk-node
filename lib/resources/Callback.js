var request = require('request');
var Q = require('q');
var $ = require('underscore');

var Callback = function Callback(options, clientConfig) {
	var structureKeys = ['id', 'name', 'url', 'auth', 'contentType', 'email', 'callbacks', 'removeHTML'];
	var links, url, res, self;

	options = options || {};
	this.structure = {};
	self = this;

	// only add valid properties to message structure
	if (options !== "" && options !== {}) {
		structureKeys.forEach(function (element) {
			if (options.hasOwnProperty(element)) {
				self.structure[element] = options[element];
			}
		});
	}

	// workSpaceId is pushed into the client config part. this does not update the client in any way
	clientConfig.workSpaceId = options.hasOwnProperty("workSpaceId") ? options.workSpaceId : clientConfig.workSpaceId;

	links = options.hasOwnProperty("link") ? options.link : "";

	if (links !== "" && links != []) {
		url = links[0].uri;
		if (!this.structure.id || this.structure.id === "") {
			//Pulls the ID out of the message URL and pushes it into the ID field.
			res = url.split("?")[0].split("/");
			this.structure.id = res[res.length - 1];
		}
	}

	if (options.workSpaceId) {
		clientConfig.workSpaceId = options.workSpaceId;
	}

	this.client = function () {
		return clientConfig;
	}

};

Callback.prototype.describe = function () {
	console.log(['\n\n - This Callback structure is: \n' + this.toJsonString()
		, '\n\n Callbacks allow custom applications to register URLs with Whispir that are used for notifications when certain events occur e.g. a response to a message is received, or a message was undeliverable.'
		, 'Make sure that your URL is on SSL (https:// NOT http://), and port is normal HTTP (80/443). Do not use custom ports or IP address based URL unless otherwise needed'
		, 'Mandatory fields are: '
		, '\n\t {'
		, '\n\t   "name" : "Callback Name",'
		, '\n\t   "url" : "http://myserver.com/mycallback.php",'
		, '\n\t   "auth" : {'
		, '\n\t     "type" : "querystring",'
		, '\n\t     "key" : "MYAUTHKEY"'
		, '\n\t   },'
		, '\n\t   "contentType" : "json",'
		, '\n\t   "removeHTML" : "disabled",'
		, '\n\t   "email" : "me@example.com",'
		, '\n\t   "callbacks" : {'
		, '\n\t     "reply" : "enabled",'
		, '\n\t     "undeliverable" : "enabled"'
		, '\n\t   }'
		, '\n\t }'
		, '\n\n Refer to Online Documentation - https://whispir.github.io/api/#Callbacks'
	].join(" "));
	return this;
};

Callback.prototype.get = function (property) {
	if (this.structure.hasOwnProperty(property)) {
		return this.structure[property];
	}
	if (property === 'workSpaceId') {
		return this.client().workSpaceId;
	}
	throw new Error(property + ' is undefined in Callback Object');
};

Callback.prototype.id = function (id) {
	this.structure.id = id;
	return this;
};

Callback.prototype.name = function (name) {
	this.structure.name = name;
	return this;
};

Callback.prototype.url = function (url) {
	this.structure.url = url;
	return this;
};

Callback.prototype.contentType = function (contentType) {
	this.structure.contentType = contentType;
	return this;
};

Callback.prototype.contentTypeAsJSON = function () {
	this.structure.contentType = 'JSON';
	return this;
};

Callback.prototype.contentTypeAsXML = function () {
	this.structure.contentType = 'XML';
	return this;
};

Callback.prototype.email = function (email) {
	this.structure.email = email;
	return this;
};

Callback.prototype.auth = function (auth) {
	if ((typeof auth === "object") && (auth !== null)) {
		this.structure.auth = auth;
		return this;
	}
	throw new Error('auth should be an object');
};

Callback.prototype.authByQueryString = function (key) {
	if (key) {
		this.structure.auth = {
			'type': 'querystring',
			'key': key
		};
		return this;
	}
	throw new Error('key should be defined');
};

Callback.prototype.authByHeader = function (key) {
	if (key) {
		this.structure.auth = {
			'type': 'httpheader',
			'key': key
		};
		return this;
	}
	throw new Error('key should be defined');
};

Callback.prototype.callbacks = function (callbacks) {
	if ((typeof callbacks === "object") && (callbacks !== null)) {
		this.structure.callbacks = callbacks;
		return this;
	}
	throw new Error('callbacks should be an object');
};

Callback.prototype.enableForReply = function (enable) {
	this.structure.callbacks = this.structure.callbacks || {};
	this.structure.callbacks.reply = "enabled";
	if (enable === false) {
		this.structure.callbacks.reply = "disabled";
	}
	return this;
};

Callback.prototype.enableForUndeliverable = function (enable) {
	this.structure.callbacks = this.structure.callbacks || {};
	this.structure.callbacks.undeliverable = "enabled";
	if (enable === false) {
		this.structure.callbacks.undeliverable = "disabled";
	}
	return this;
};

Callback.prototype.removeHTMLInReplies = function (remove) {
	this.structure.removeHTML = "enabled";
	if (remove === false) {
		this.structure.removeHTML = "disabled";
	}
	return this;
};

Callback.prototype.toJsonString = function () {
	return JSON.stringify(this.structure);
};

Callback.prototype.create = function (raw) {

	var clientConfig = this.client();

	var headers = {
		'Accept': 'application/vnd.whispir.api-callback-v1+json',
		'Content-Type': 'application/vnd.whispir.api-callback-v1+json'
	};

	var endpoint = '/callbacks' + '?apikey=' + clientConfig.apiKey

	var data = raw || this.toJsonString();

	return trigger.apply(this, ['post', endpoint, headers, data]);
};

Callback.prototype.retrieve = function () {

	var callbackId = this.structure.id;

	if (!callbackId) {
		throw new Error('Callback id is not provided. You should provide one like this \n\t - wClient.Callback().id(1) \n\t - wClient.Callback({id: 1})');
	}

	var clientConfig = this.client();

	var headers = {
		'Accept': 'application/vnd.whispir.api-callback-v1+json'
	};

	var endpoint = '/callbacks/' + callbackId + '?apikey=' + clientConfig.apiKey;

	return trigger.apply(this, ['get', endpoint, headers]);
};

Callback.prototype.update = function (raw) {

	var callbackId = this.structure.id;

	if (!callbackId) {
		throw new Error('Callback id is not provided. You should provide one like this \n\t - wClient.Callback().id(1) \n\t - wClient.Callback({id: 1})');
	}

	var clientConfig = this.client();

	var headers = {
		'Accept': 'application/vnd.whispir.api-callback-v1+json',
		'Content-Type': 'application/vnd.whispir.api-callback-v1+json'
	};

	var endpoint = '/callbacks/' + callbackId + '?apikey=' + clientConfig.apiKey;

	var data = raw || this.toJsonString();

	return trigger.apply(this, ['put', endpoint, headers, data]);
};

Callback.prototype.delete = function () {

	var callbackId = this.structure.id;

	if (!callbackId) {
		throw new Error('Callback id is not provided. You should provide one like this \n\t - wClient.Callback().id(1) \n\t - wClient.Callback({id: 1})');
	}

	var clientConfig = this.client();

	var headers = {
		'Accept': 'application/vnd.whispir.api-callback-v1+json'
	};

	var endpoint = '/callbacks/' + callbackId + '?apikey=' + clientConfig.apiKey;

	return trigger.apply(this, ['delete', endpoint, headers]);
};

var trigger = function trigger(method, fullyFormedEndpoint, headers, data) {
	var deferred = Q.defer();
	var successCodes = [200, 201, 204];
	var clientConfig = this.client();
	var url = function url(clientProperties, endpoint) {
		return clientProperties.host
			+ (clientProperties.workSpaceId === 0 ? '' : ('/workspaces/' + clientProperties.workSpaceId))
			+ endpoint;
	};

	var reqOptions = {
		method: method,
		url: url(clientConfig, fullyFormedEndpoint),
		auth: clientConfig.auth,
		headers: $.extend(headers, clientConfig.headers)
	};

	if (data) {
		reqOptions.body = ( (typeof data === 'string') ? data : JSON.stringify(data));
	}

	var callback = function callback(err, response, body) {
		var result = {
			id: null,
			location: null,
			statusCode: response.statusCode,
			body: ''
		};
		try {
			result.body = JSON.parse(body);
			if(result.body.hasOwnProperty('status') && result.body.status === "No records found"){
				result.body = [];
			}
		} catch (e) {
			result.body = body;
		}

		if (!err && successCodes.indexOf(response.statusCode) != -1) {

			if (reqOptions.method === 'post') {
				result.location = response.headers.location;
				result.id = response.headers.location.split("?")[0].split("/").slice(-1)[0];
			}

			deferred.resolve(result);
		} else {
			deferred.reject(result);
		}
	};
	// console.log(reqOptions);
	// making the request with the reqOptions and call the callback function which will resolve/reject the promise
	request(reqOptions, callback);

	return deferred.promise;
};

module.exports = Callback;
