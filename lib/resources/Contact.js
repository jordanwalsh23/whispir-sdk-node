var request = require('request');
var Q = require('q');
var $ = require('underscore');

var Contact = function Contact(options, clientConfig) {
	var structureKeys = ['id', 'link'];
	var links, url, res;
	var self = this;

	options = options || {};
	this.structure = {};

	// only add valid properties to Contact structure
	if (options && options !== "" && Object.keys(options).length !== 0) {
		Object.keys(options).forEach(function (key) {
			self.structure[key] = options[key];
		});
	}

	// workSpaceId is pushed into the client config part. this does not update
	// the client in any way
	clientConfig.workSpaceId = options.hasOwnProperty("workSpaceId") ? options.workSpaceId : clientConfig.workSpaceId;

	links = options.hasOwnProperty("link") ? options.link : "";

	if (links !== "" && links != []) {
		url = links[0].uri;
		if (!this.structure.id || this.structure.id === "") {
			// Pulls the ID out of the Contact URL and pushes it into the ID
			// field.
			res = url.split("?")[0].split("/");
			this.structure.id = res[res.length - 1];
		}
	}

	if (options.workSpaceId) {
		clientConfig.workSpaceId = options.workSpaceId;
	}

	this.client = function () {
		return clientConfig;
	}

	return this;
};

Contact.prototype.describe = function () {
	console.log(['\n\n - This Contact structure is: \n' + this.toJsonString()
		, '\n\n Contacts form the core of the Whispir offerings. They make up the base data to which and from all the communications are performed.'
		, '\n\n The Whispir API provides secure cloud based storage for your contact information. This can then easily be retrieved by any application or device that requires access, and has permission to do so.'
		, 'Mandatory fields are: '
		,'\n\t {'
		,'\n\t     "firstName": "John",'
		,'\n\t     "lastName": "Wick",'
		,'\n\t     "status": "A",'
		,'\n\t     "timezone": "Australia/Melbourne",'
		,'\n\t     "workEmailAddress1": "jsmith@testcompany.com",'
		,'\n\t     "workMobilePhone1": "61423456789",'
		,'\n\t     "workCountry": "Australia",'
		,'\n\t     "messagingoptions": [{'
		,'\n\t           "channel": "sms",'
		,'\n\t           "enabled": "true",'
		,'\n\t           "primary": "WorkMobilePhone1"'
		,'\n\t     },{'
		,'\n\t           "channel": "email",'
		,'\n\t           "enabled": "true",'
		,'\n\t           "primary": "WorkEmailAddress1"'
		,'\n\t     },{'
		,'\n\t           "channel": "voice",'
		,'\n\t           "enabled": "true",'
		,'\n\t           "primary": "WorkMobilePhone1"'
		,'\n\t     }]'
		,'\n\t }'
		, '\n\n Refer to Online Documentation - https://whispir.github.io/api/#Contacts'
	].join(" "));
	return this;
};

Contact.prototype.get = function (property) {
	if (this.structure.hasOwnProperty(property)) {
		return this.structure[property];
	}
	if (property === 'workSpaceId') {
		return this.client().workSpaceId;
	}
	throw new Error(property + ' is undefined in Contact Object');
};

Contact.prototype.set = function (property, value) {
	var self = this;
	if(typeof property === 'object'){ /* when directly object is passed, property is defined & value is undefined*/
		Object.keys(property).forEach(function (key) {
			self.set(key, property[key]);
		});
	}else{
		if (property.toLowerCase() === 'workspaceid') {
			throw new Error('workSpaceId cannot be set using this method. It should only be sent in while creating the Contact instance. Ex: `wClient.Contact({workSpaceId: \'' + value + '\', ...})`');
		}else{
			self.structure[property] = value;
		}
	}
	return self;
};

Contact.prototype.toJsonString = function () {
	return JSON.stringify(this.structure);
};

Contact.prototype.create = function (raw) {

	var clientConfig = this.client();

	var headers = {
		'Accept': 'application/vnd.whispir.contact-v1+json',
		'Content-Type': 'application/vnd.whispir.contact-v1+json'
	};

	var endpoint = '/contacts' + '?apikey=' + clientConfig.apiKey

	var data = raw || this.toJsonString();

	return trigger.apply(this, ['post', endpoint, headers, data]);
};

Contact.prototype.search = function (params) {

	var clientConfig = this.client();

	var headers = {
		'Accept': 'application/vnd.whispir.contact-v1+json'
	};

	var endpoint = '/contacts' + '?apikey=' + clientConfig.apiKey + (params ? ('&' + params) : '');

	return trigger.apply(this, ['get', endpoint, headers]);
};

Contact.prototype.retrieve = function () {

	var ContactId = this.structure.id;

	if (!ContactId) {
		throw new Error('Contact id is not provided. You should provide one like this \n\t - wClient.Contact().set(\'id\',1) \n\t - wClient.Contact({id: 1})');
		return;
	}

	var clientConfig = this.client();

	var headers = {
		'Accept': 'application/vnd.whispir.contact-v1+json'
	};

	var endpoint = '/contacts/' + ContactId + '?apikey=' + clientConfig.apiKey + '&customFields=true';

	return trigger.apply(this, ['get', endpoint, headers]);
};

Contact.prototype.update = function (raw) {

	var ContactId = this.structure.id;

	if (!ContactId) {
		throw new Error('Contact id is not provided. You should provide one like this \n\t - wClient.Contact().set(\'id\',1) \n\t - wClient.Contact({id: 1})');
		return;
	}

	var clientConfig = this.client();

	var headers = {
		'Accept': 'application/vnd.whispir.contact-v1+json',
		'Content-Type': 'application/vnd.whispir.contact-v1+json'
	};

	var endpoint = '/contacts/' + ContactId + '?apikey=' + clientConfig.apiKey;

	var data = raw || this.toJsonString();

	return trigger.apply(this, ['put', endpoint, headers, data]);
};

Contact.prototype.delete = function () {

	var ContactId = this.structure.id;

	if (!ContactId) {
		throw new Error('Contact id is not provided. You should provide one like this \n\t - wClient.Contact().set(\'id\',1) \n\t - wClient.Contact({id: 1})');
		return;
	}

	var clientConfig = this.client();

	var headers = {
		'Accept': 'application/vnd.whispir.contact-v1+json'
	};

	var endpoint = '/contacts/' + ContactId + '?apikey=' + clientConfig.apiKey;

	return trigger.apply(this, ['delete', endpoint, headers]);
};

var trigger = function trigger(method, fullyFormedEndpoint, headers, data) {
	var deferred = Q.defer();
	var successCodes = [200, 201, 204];
	var clientConfig = this.client();
	var url = function url(clientProperties, endpoint) {
		return clientProperties.host
			+ (clientProperties.workSpaceId === 0 ? '' : ('/workspaces/' + clientProperties.workSpaceId))
			+ endpoint;
	};

	var reqOptions = {
		method: method,
		url: url(clientConfig, fullyFormedEndpoint),
		auth: clientConfig.auth,
		headers: $.extend(headers, clientConfig.headers)
	};

	if (data) {
		reqOptions.body = ( (typeof data === 'string') ? data : JSON.stringify(data));
	}

	var callback = function callback(err, response, body) {
		var result = {
			id: null,
			location: null,
			mri: null,
			statusCode: response.statusCode,
			body: ''
		};
		try {
			result.body = JSON.parse(body);
			if(result.body.hasOwnProperty('status') && result.body['status'] === "No records found"){
				result.body = [];
			}
		} catch (e) {
			result.body = body;
		}

		if (!err && successCodes.indexOf(response.statusCode) != -1) {

			if (reqOptions.method === 'post') {
				result.location = response.headers.location;
				result.id = response.headers.location.split("?")[0].split("/").slice(-1)[0];
				result.mri = result.body.mri;
			}

			deferred.resolve(result);
		} else {
			deferred.reject(result);
		}
	};
	// console.log(reqOptions);
	// making the request with the reqOptions and call the callback function
	// which will resolve/reject the promise
	request(reqOptions, callback);

	return deferred.promise;
};

module.exports = Contact;
