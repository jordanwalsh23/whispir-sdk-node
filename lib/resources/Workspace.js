var request = require('request');
var Q = require('q');
var $ = require('underscore');

var WorkSpace = function WorkSpace(options, clientConfig) {
	var structureKeys = ['id', 'name', 'status', 'number', 'billingcostcentre', 'link'];
	var links, url, res;
	var aliasKeys = { name: 'projectName', number: 'projectNumber' };

	options = options || {};
	this.structure = {};
	var self = this;
	
	// only add valid properties to WorkSpace structure
	if (options && options !== "" && Object.keys(options).length !== 0) {
		structureKeys.forEach(function (element) {
			if (options.hasOwnProperty(element)) {
				self.structure[element] = options[element];
			}
		});
	}
	
	// assign alias to original
	Object.keys(aliasKeys).forEach(function (key) {
		if (self.structure[key]) {
			self.structure[aliasKeys[key]] = self.structure[key];
		}
	});

	links = options.hasOwnProperty("link") ? options.link : "";

	if (links !== "" && links != []) {
		url = links[0].uri;
		if (!this.structure.id || this.structure.id === "") {
			// Pulls the ID out of the WorkSpace URL and pushes it into the ID
			// field.
			res = url.split("?")[0].split("/");
			this.structure.id = res[res.length - 1];
		}
	}

	this.client = function () {
		return clientConfig;
	}

	return this;
};

WorkSpace.prototype.describe = function () {
	console.log(['\n\n - This WorkSpace structure is: \n' + this.toJsonString()
		, '\n\n WorkSpace allows application developers to create new work areas for the varying use cases of their application. They provides different functions that can be provided to users of the application, e.g. creating messages, executing scenarios, or viewing distribution lists'
		, 'Mandatory fields are: '
		, '\n\t {'
		, '\n\t   projectName : "WorkSpace - Sales", '
		, '\n\t   projectNumber: "sales1",'
		, '\n\t   status : "A"'
		, '\n\t }'
		, '\n\n Refer to Online Documentation - https://whispir.github.io/api/#WorkSpaces'
	].join(" "));
	return this;
};

WorkSpace.prototype.get = function (property) {
	var actuals = { name: 'projectName', number: 'projectNumber' };
	if (actuals.hasOwnProperty(property)) {
		property = actuals[property];
	}
	if (this.structure.hasOwnProperty(property)) {
		return this.structure[property];
	}
	throw new Error(property + ' is undefined in WorkSpace Object');
};

WorkSpace.prototype.id = function(id){
	this.structure.id = id;
	return this;
};

WorkSpace.prototype.name = function (name) {
	this.structure.projectName = name;
	return this;
};

WorkSpace.prototype.number = function (number) {
	this.structure.projectNumber = number;
	return this;
};

WorkSpace.prototype.status = function (status) {
	this.structure.status = status;
	return this;
};

WorkSpace.prototype.billingcostcentre = function (billingcostcentre) {
	this.structure.billingcostcentre = billingcostcentre;
	return this;
};

WorkSpace.prototype.toJsonString = function () {
	return JSON.stringify(this.structure);
};

WorkSpace.prototype.create = function (raw) {

	var clientConfig = this.client();

	var headers = {
		'Accept': 'application/vnd.whispir.workspace-v1+json',
		'Content-Type': 'application/vnd.whispir.workspace-v1+json'
	};

	var endpoint = '/workspaces' + '?apikey=' + clientConfig.apiKey

	var data = raw || this.toJsonString();

	return trigger.apply(this, ['post', endpoint, headers, data]);
};

WorkSpace.prototype.retrieve = function () {

	var WorkSpaceId = this.structure.id;

	if (!WorkSpaceId) {
		throw new Error('WorkSpace id is not provided. You should provide one like this \n\t - wClient.WorkSpace().id(1) \n\t - wClient.WorkSpace({id: 1})');
		return;
	}

	var clientConfig = this.client();

	var headers = {
		'Accept': 'application/vnd.whispir.workspace-v1+json'
	};

	var endpoint = '/workspaces/' + WorkSpaceId + '?apikey=' + clientConfig.apiKey;

	return trigger.apply(this, ['get', endpoint, headers]);
};

WorkSpace.prototype.update = function () {
	throw new Error('Updating a WorkSpace is not supported via the API');
};

WorkSpace.prototype.delete = function () {
	throw new Error('Deleting a WorkSpace is not supported via the API');
};

var trigger = function trigger(method, fullyFormedEndpoint, headers, data) {
	var deferred = Q.defer();
	var successCodes = [200, 201];
	var clientConfig = this.client();
	var url = function url(clientProperties, endpoint) {
		return clientProperties.host + endpoint;
	};

	var reqOptions = {
		method: method,
		url: url(clientConfig, fullyFormedEndpoint),
		auth: clientConfig.auth,
		headers: $.extend(headers, clientConfig.headers)
	};

	if (data) {
		reqOptions.body = ( (typeof data === 'string') ? data : JSON.stringify(data));
	}

	var callback = function callback(err, response, body) {
		var result = {
			id: null,
			location: null,
			statusCode: response.statusCode,
			body: ''
		};
		try {
			result.body = JSON.parse(body);
			if(result.body.hasOwnProperty('status') && result.body.status === "No records found"){
				result.body = [];
			}
		} catch (e) {
			result.body = body;
		}

		if (!err && successCodes.indexOf(response.statusCode) != -1) {

			if (reqOptions.method === 'post') {
				result.location = response.headers.location;
				result.id = response.headers.location.split("?")[0].split("/").slice(-1)[0];
			}

			deferred.resolve(result);
		} else {
			deferred.reject(result);
		}
	};
	// console.log(reqOptions);
	// making the request with the reqOptions and call the callback function
	// which will resolve/reject the promise
	request(reqOptions, callback);

	return deferred.promise;
};

module.exports = WorkSpace;