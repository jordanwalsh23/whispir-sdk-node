/*jslint node: true */
'use strict';
var request = require('request');
var Q = require('q');
var $ = require('underscore');

var Message = function Message(options, clientConfig) {
    var structureKeys = ['id', 'to', 'subject', 'body', 'label', 'email', 'voice',
                          'web', 'social', 'callbackId', 'callbackParameters',
                          'messageTemplateName', 'messageTemplateId', 'eventId',
                          'resource', 'messageattributes', 'type', 'features',
                          'messageType', 'scheduleType', 'scheduleDate', 'repetitionCount', 'repeatDays', 'repeatHrs', 'repeatMin',
                          'link'],
        links,
        url,
        res,
        self = this;

    options = options || {};
    this.structure = {};

    // only add valid properties to message structure
    if (options !== "" && Object.keys(options).length !== 0) {
        structureKeys.forEach(function (element) {
            if (options.hasOwnProperty(element)) {
                self.structure[element] = options[element];
            }
        });
    }

    // workSpaceId is pushed into the client config part. this does not update the client in any way
    clientConfig.workSpaceId = options.hasOwnProperty("workSpaceId") ? options.workSpaceId : clientConfig.workSpaceId;

    links = options.hasOwnProperty("link") ? options.link : "";

    if (links.length !== 0) {
        url = links[0].uri;
        if (!this.structure.id || this.structure.id === "") {
            //Pulls the ID out of the message URL and pushes it into the ID field.
            res = url.split("?")[0].split("/");
            this.structure.id = res[res.length - 1];
        }
    }

    if (options.workSpaceId) {
        clientConfig.workSpaceId = options.workSpaceId;
    }

    this.client = function () {
        return clientConfig;
    };

};

Message.prototype.describe = function () {
    console.log(['\n\n - This message structure is: \n' + this.toJsonString(),
                    '\n\n Message() helps you send a communication via SMS, Email, Voice, Push, Social channels.',
                    'Mandatory fields for: ',
                    '\n\n SMS - to, subject, body',
                    '\n Email - to, subject, email {}',
                    '\n Voice - to, subject, voice {}',
                    '\n Using Template - to, messageTemplateId Or messageTemplateName',
                    '\n\n',
                    '- You can also send multiple channels comms in a single call. ',
                    '\n Ex: sms, email',
                    '\n\t { ',
                    '\n\t   to: \'6598765432, john@smith.com\', ',
                    '\n\t   subject: \'common subject for both SMS, Email\', ',
                    '\n\t   body: \'SMS Content\', ',
                    '\n\t   email: {...} ',
                    '\n\t }',
                    '\n\n',
                    '- A \'to\' field can take phonenumber, emails to a max of 1000 entries',
                    '\n\n Refer to Online Documentation - https://whispir.github.io/api/#messages'
                     ].join(" "));
    return this;
};

Message.prototype.get = function (property) {
    if (this.structure.hasOwnProperty(property)) {
        return this.structure[property];
    }
    if (property === 'workSpaceId') {
        return this.client().workSpaceId;
    }
    throw new Error(property + ' is undefined in Message Object');
};
/*
Message.prototype.set = function (property, value) {
    if (this.structure.hasOwnProperty(property)) {
        this.structure[property] = value;
        return this;
    }
    if (property === 'workSpaceId') {
        throw new Error('workSpaceId cannot be set using this method. It should be sent in while creating the Message instance. Ex: `wClient.Message({workSpaceId: 1, ...})`');
    }
    throw new Error(property + ' is undefined in Message Object');
}
 */
Message.prototype.to = function (to) {
    this.structure.to = to;
    return this;
};

Message.prototype.subject = function (subject) {
    this.structure.subject = subject;
    return this;
};

Message.prototype.body = function (body) {
    this.structure.body = body;
    return this;
};

Message.prototype.type = function (type) {
    this.structure.type = type;
    return this;
};

Message.prototype.email = function (email) {
    if ((typeof email === "object") && (email !== null)) {
        this.structure.email = email;
        return this;
    }
    throw new Error('email should be an object');
};

Message.prototype.voice = Message.prototype.call = function (voice) {
    if ((typeof voice === "object") && (voice !== null)) {
        if (!voice.hasOwnProperty('type')) {
            voice.type = 'ConfCall:,ConfAccountNo:,ConfPinNo:,ConfModPinNo:,Pin:';
        }
        this.structure.voice = voice;
        return this;
    }
    throw new Error('voice should be an object');
};

Message.prototype.web = function (web) {
    if ((typeof web === "object") && (web !== null)) {
        this.structure.web = web;
        return this;
    }
    throw new Error('web should be an object');
};

Message.prototype.social = function (social) {
    if ((typeof social === "object") && (social !== null)) {
        this.structure.social = social;
        return this;
    }
    throw new Error('social should be an object');
};

Message.prototype.messageattributes = function (messageattributes) {
    if ((typeof messageattributes === "object") && (messageattributes !== null)) {
        this.structure.messageattributes = messageattributes;
        return this;
    }
    throw new Error('messageattributes should be an object');
};

Message.prototype.resource = function (resource) {
    if ((typeof resource === "object") && (resource !== null)) {
        this.structure.resource = resource;
        return this;
    }
    throw new Error('resource should be an object');
};

Message.prototype.features = function (features) {
    if ((typeof features === "object") && (features !== null)) {
        this.structure.features = features;
        return this;
    }
    throw new Error('features should be an object');
};

Message.prototype.schedule = function (schedule) {
    var validKeys = ['count', 'days', 'hours', 'minutes', 'startDate', 'type'].join(', '), gotKeys = '';
    if ((typeof schedule === 'object') && (schedule !== null)) {
        gotKeys = Object.keys(schedule).sort().join(', ');
        if (gotKeys === validKeys) {
            this.structure.messageType = 'SCHEDULED';
            this.structure.scheduleType = schedule.type.toUpperCase();
            this.structure.scheduleDate = schedule.startDate;
            this.structure.repetitionCount = schedule.count + '';
            this.structure.repeatDays = schedule.days + '';
            this.structure.repeatHrs = schedule.hours + '';
            this.structure.repeatMin = schedule.minutes + '';
            return this;
        }
        throw new Error('missing/invalid schedule params. Need - ' + validKeys + '. Got - ' + gotKeys);
    }
    throw new Error('schedule should be an object');
};

Message.prototype.eventId = Message.prototype.usingEventId = function (eventId) {
    this.structure.eventId = eventId;
    return this;
};

Message.prototype.messageTemplateId = Message.prototype.useTemplateWithIdAs = Message.prototype.usingTemplateId = function (messageTemplateId) {
    this.structure.messageTemplateId = messageTemplateId;
    return this;
};

Message.prototype.messageTemplateName = Message.prototype.useTemplateWithNameAs = Message.prototype.usingTemplateName = function (messageTemplateName) {
    this.structure.messageTemplateName = messageTemplateName;
    return this;
};

Message.prototype.callbackId = function (callbackId) {
    this.structure.callbackId = callbackId;
    return this;
};

Message.prototype.callbackParameters = function (callbackParameters) {
    if ((typeof callbackParameters === "object") && (callbackParameters !== null)) {
        this.structure.callbackParameters = callbackParameters;
        return this;
    }
    throw new Error('callbackParameters should be an object');
};

Message.prototype.label = function (label) {
    this.structure.label = label;
    return this;
};

Message.prototype.id = function (id) {
    this.structure.id = id;
    return this;
};

Message.prototype.toJsonString = function () {
    return JSON.stringify(this.structure);
};

Message.prototype.search = Message.prototype.retrieve = function (params) {

    var messageId, clientConfig, headers, endpoint;

    messageId = this.structure.id;

    clientConfig = this.client();

    headers = {
        'Accept': 'application/vnd.whispir.message-v1+json'
    };

    endpoint = '/messages';

    if (messageId) {
        endpoint += '/' + messageId;
    }

    endpoint += '?apikey=' + clientConfig.apiKey + (params ? ('&' + params) : '');

    return trigger.apply(this, ['get', endpoint, headers]);
};

Message.prototype.getMessageStatus = Message.prototype.getStatus = function (view) {

    var messageId, clientConfig, query, headers, endpoint;

    messageId = this.structure.id;

    if (!messageId) {
        throw new Error('Message id is not provided. Cannot get Message Responses');
    }

    clientConfig = this.client();

    view = view || 'summary';

    query = 'view=' + (['summary', 'detailed'].indexOf(view.toLowerCase()) !== -1 ? view : 'summary');

    headers = {
        'Accept': 'application/vnd.whispir.messagestatus-v1+json'
    };

    endpoint = '/messages/' + messageId + '/messagestatus' + '?apikey=' + clientConfig.apiKey + '&' + query;

    return trigger.apply(this, ['get', endpoint, headers]);
};

Message.prototype.getMessageResponses = Message.prototype.getResponses = function (view, filter) {

    var messageId, clientConfig, query, headers, endpoint;

    messageId = this.structure.id;

    if (!messageId) {
        throw new Error('Message id is not provided. Cannot get Message Responses');
    }

    clientConfig = this.client();

    view = view || 'summary';

    query = 'view=' + (['summary', 'detailed'].indexOf(view.toLowerCase()) !== -1 ? view : 'summary');

    if (filter) {
        query += '&' + filter;
    }

    headers = {
        'Accept': 'application/vnd.whispir.messageresponses-v1+json'
    };

    endpoint = '/messages/' + messageId + '/messageresponses' + '?apikey=' + clientConfig.apiKey + '&' + query;

    return trigger.apply(this, ['get', endpoint, headers]);
};

Message.prototype.retrieveEventMessages = function () {
    
    var messageLabel = this.structure.label, clientConfig, headers, endpoint;

    if (!messageLabel) {
        throw new Error('Message label is used to retrieve Event Messages. Please provide the label that is used to send the messages under that event. Usually it will be the eventLabel value.')
    }

    clientConfig = this.client();

    headers = { /*because [HTTP/1.1 4.2 Message Headers | http://www.w3.org/Protocols/rfc2616/rfc2616-sec4.html#sec4.2] are case insensitive*/
        'Accept': 'application/vnd.whispir.message-v1+json',
    };

    messageLabel = messageLabel.replace(/ /g, '%20'); // make all spaces url encoded

    endpoint = '/messages' + '?apikey=' + clientConfig.apiKey + '&label=' + messageLabel;

    return trigger.apply(this, ['get', endpoint, headers]);

};

Message.prototype.send = Message.prototype.make = function (raw) {

    var clientConfig, headers, endpoint, data;

    clientConfig = this.client();

    headers = { /*because [HTTP/1.1 4.2 Message Headers | http://www.w3.org/Protocols/rfc2616/rfc2616-sec4.html#sec4.2] are case insensitive*/
        'Accept': 'application/vnd.whispir.message-v1+json',
        'Content-Type': 'application/vnd.whispir.message-v1+json'
    };

    endpoint = '/messages' + '?apikey=' + clientConfig.apiKey;

    data = raw || this.toJsonString();

    return trigger.apply(this, ['post', endpoint, headers, data]);
};

Message.prototype.bulkSend = function () {

    if (this.structure.resource === undefined || typeof this.structure.resource !== 'object') {
        throw new Error('Invalid or No Resource information is defined in the Message Object for bulkSend.');
    }

    var clientConfig, headers, endpoint, data;

    clientConfig = this.client();

    headers = {
        'Accept': 'application/vnd.whispir.bulkmessage-v1+json',
        'Content-Type': 'application/vnd.whispir.bulkmessage-v1+json'
    };

    endpoint = '/messages' + '?apikey=' + clientConfig.apiKey;

    data = this.toJsonString();

    return trigger.apply(this, ['post', endpoint, headers, data]);
};

Message.prototype.attach = function (type, attachments) {
    
    if('email, voice'.indexOf(type) === -1){
        throw new Error('Invalid type passed. Valid types are email, voice');
    }
    
    if (!this.structure.hasOwnProperty(type)) {
       throw new Error(type + ' is not defined in the message structure yet. Please do it first. ex: Message.' + type + '({})');
    }

    $.extend(this.structure[type], {
            resources: {
                attachment: attachments
            }
        });

    return this;
};

function trigger(method, fullyFormedEndpoint, headers, data) {
    var deferred = Q.defer(),
        successCodes = [200, 202],
        clientConfig = this.client(),
        url = function url(clientProperties, endpoint) {
            return clientProperties.host
                + (clientProperties.workSpaceId === 0 ? '' : ('/workspaces/' + clientProperties.workSpaceId))
                + endpoint;
        },
        reqOptions = {
            method: method,
            url: url(clientConfig, fullyFormedEndpoint),
            auth: clientConfig.auth,
            headers: $.extend(headers, clientConfig.headers)
        };

    if (data) {
        reqOptions.body = (typeof data === 'string') ? data : JSON.stringify(data);
    }

    function callback(err, response, body) {
        var result = {
                id: null,
                location: null,
                statusCode: response.statusCode,
                body: ''
            };
        try {
            result.body = JSON.parse(body);
            if (result.body.hasOwnProperty('status') && result.body.status === "No records found") {
                result.body = [];
            }
        } catch (e) {
            result.body = body;
        }

        if (!err && successCodes.indexOf(response.statusCode) !== -1) {

            if (reqOptions.method === 'post') {
                result.location = response.headers.location;
                result.id = response.headers.location.split("?")[0].split("/").slice(-1)[0];
            }

            deferred.resolve(result);
        } else {
            deferred.reject(result);
        }
    }
    // console.log(reqOptions);
    // making the request with the reqOptions and call the callback function which will resolve/reject the promise
    request(reqOptions, callback);

    return deferred.promise;
}

module.exports = Message;
