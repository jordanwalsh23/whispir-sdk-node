/*jslint node: true */
'use strict';

var Q = require('q');
var readMultipleFiles = require('read-multiple-files');
var path = require('path');

var FileReader = function FileReader(filesWithPaths) {
    this.filesWithPaths = filesWithPaths;
};

FileReader.prototype.toBase64 = function() {
    var deferred = Q.defer();
    readMultipleFiles(this.filesWithPaths, function (err, data) {
        if (err) {
            deferred.reject(err);
        } else {
            this.filesWithPaths.forEach( function(e, i, a) {
                a[i] = {
                    name: path.basename(e),
                    base64: new Buffer(data[i]).toString('base64')
                };
            });
            deferred.resolve(this.filesWithPaths);
        }
    });
    return deferred.promise;
};

module.exports = FileReader;
