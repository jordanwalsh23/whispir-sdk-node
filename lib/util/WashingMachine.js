/*jslint node: true */
'use strict';

/* makes things neat for you; dont get back to me on the bad naming convention */

var Q = require('q');

var WashingMachine = function (data, wAPIClientReference) {
    this.data = data;
    this.error = {
        status: false,
        description: ''
    };
    this.wAPIClient = wAPIClientReference;
};

WashingMachine.prototype.output = WashingMachine.prototype.afterDry = function () {
    var deferred = Q.defer();
    if (this.error.status) {
        deferred.reject(this.error);
    }
    deferred.resolve(this.data);
    return deferred.promise;
};

WashingMachine.prototype.attachIdFromLink = function () {
    var arrayOfObjects = this.data;

    function extractIdFromLink(link) {
        var id, res;
        res = link.split("?")[0].split("/");
        id = res[res.length - 1];
        return id;
    }

    /*add .id if not present*/
    if (arrayOfObjects && arrayOfObjects.length !== 0) {

        arrayOfObjects.forEach(function (e,i,a) {
            var links, url;
            if (!e.id || e.id === "") {
                links = e.hasOwnProperty("link") ? e.link : "";
                if (links.length !== 0) {
                    url = links[0].uri;
                    a[i].id = extractIdFromLink(url);
                }
            }
        });

        this.data = arrayOfObjects;
    } else {
        this.error = {
            status: true,
            description: 'no data found with resource - ' + arrayOfObjects
        };
    }

    return this;
};

WashingMachine.prototype.createNewResources = function (resource) {

    if (resource === 'undefined') {
        throw new Error('what kind of whispir resource object you want to create? Allowed types are activity, callback, contact, customlist, distributionlist, dl, event, import, message, resource, responserule, scenario, template, user, workspace, sms, email, calls.');
    }

    var self = this, arrayOfObjects = this.data;

    if (arrayOfObjects && arrayOfObjects.length !== 0) {
        try {
            arrayOfObjects.forEach(function (e,i,a) {
                a[i] = self.wAPIClient.Create(resource, e);
            });
            this.data = arrayOfObjects;
        } catch (e) {
            console.log(e);
            this.error = {
                status: true,
                description: 'error in creating new resource - ' + resource
            };
        }
    } else {
        this.error = {
            status: true,
            description: 'no data found with resource - ' + arrayOfObjects
        };
    }

    self = null;

    return this;

};

module.exports = WashingMachine;
