/*jslint node: true */
'use strict';

var request = require('request');
var Q = require('q');
var $ = require('underscore');

/*
 * How to loop for .all()
 * http://www.sebastianseilund.com/nodejs-async-in-practice
 * go all the way down -> "I need to iterate over a collection, perform an asynchronous task for each item, and when they're all done do something else"
 */


var resourceMapping = {
        activity: ['activities', 'application/vnd.whispir.activity-v1+json', [404]],
        activities: ['activities', 'application/vnd.whispir.activity-v1+json', [404]],
        callback: ['callbacks', 'application/vnd.whispir.api-callback-v1+json', [] ],
        callbacks: ['callbacks', 'application/vnd.whispir.api-callback-v1+json', [] ],
        contact: ['contacts', 'application/vnd.whispir.contact-v1+json', [] ],
        contacts: ['contacts', 'application/vnd.whispir.contact-v1+json', [] ],
        customlist: ['customlists', 'application/vnd.whispir.customlist-v1+json', [] ],
        customlists: ['customlists', 'application/vnd.whispir.customlist-v1+json', [] ],
        distributionlist: ['distributionlists', 'application/vnd.whispir.distributionlist-v1+json', [] ],
        distributionlists: ['distributionlists', 'application/vnd.whispir.distributionlist-v1+json', [] ],
        dl: ['distributionlists', 'application/vnd.whispir.distributionlist-v1+json', [] ],
        event: ['events', 'application/vnd.whispir.event-v1+json', [] ],
        events: ['events', 'application/vnd.whispir.event-v1+json', [] ],
        import: ['imports', 'application/vnd.whispir.import-v1+json', [] ],
        imports: ['imports', 'application/vnd.whispir.import-v1+json', [] ],
        message: ['messages', 'application/vnd.whispir.message-v1+json', [] ],
        messages: ['messages', 'application/vnd.whispir.message-v1+json', [] ],
        messagestatus: ['messagestatus', 'application/vnd.whispir.messagestatus-v1+json', [] ],
        messageresponses: ['messageresponses', 'application/vnd.whispir.messageresponse-v1+json', [] ],
        sms: ['messages', 'application/vnd.whispir.message-v1+json', [] ],
        email: ['messages', 'application/vnd.whispir.message-v1+json', [] ],
        calls: ['messages', 'application/vnd.whispir.message-v1+json', [] ],
        resource: ['resources', 'application/vnd.whispir.resource-v1+json', [] ],
        resources: ['resources', 'application/vnd.whispir.resource-v1+json', [] ],
        responserule: ['responserules', 'application/vnd.whispir.responserule-v1+json', [] ],
        responserules: ['responserules', 'application/vnd.whispir.responserule-v1+json', [] ],
        scenario: ['scenarios', 'application/vnd.whispir.scenario-v1+json', [] ],
        scenarios: ['scenarios', 'application/vnd.whispir.scenario-v1+json', [] ],
        template: ['templates', 'application/vnd.whispir.template-v1+json', [] ],
        templates: ['templates', 'application/vnd.whispir.template-v1+json', [] ],
        user: ['users', 'application/vnd.whispir.user-v1+json', [] ],
        users: ['users', 'application/vnd.whispir.user-v1+json', [] ],
        workspace: ['workspaces', 'application/vnd.whispir.workspace-v1+json', [] ],
        workspaces: ['workspaces', 'application/vnd.whispir.workspace-v1+json', [] ]
    };

var Fetch = function Fetch(options, clientConfig) {
    var structureKeys = ['id', 'type', 'url', 'link'],
        self = this;

    options = options || {};
    this.structure = {};

    // only add valid properties to Template structure
    if (options && options !== "" && Object.keys(options).length !== 0) {
        structureKeys.forEach(function (element) {
            if (options.hasOwnProperty(element)) {
                self.structure[element] = options[element];
            }
        });
    }

    // workSpaceId is pushed into the client config part. this does not update the client in any way
    clientConfig.workSpaceId = options.hasOwnProperty("workSpaceId") ? options.workSpaceId : clientConfig.workSpaceId;

    if (options.workSpaceId) {
        clientConfig.workSpaceId = options.workSpaceId;
    }

    this.client = function () {
        return clientConfig;
    };

    //return this;
};

Fetch.prototype.get = function (property) {
    if (this.structure.hasOwnProperty(property)) {
        return this.structure[property];
    }
    if (property === 'workSpaceId') {
        return this.client().workSpaceId;
    }
    throw new Error(property + ' is undefined in this Object');
};

Fetch.prototype.id = function (id) {
    this.structure.id = id;
    return this;
};

Fetch.prototype.type = function (type) {
    this.structure.type = type;
    return this;
};

Fetch.prototype.url = function (url) {
    this.structure.url = url;
    return this;
};

Fetch.prototype.go = function (options) { /*options = {type: message, id: 1, pagination: {limit 20, startAt: 0, all: true}, filter: urlQuery}*/

    var url, resType, clientConfig, headers, endPoint, addWorkSpace, endPointWithId, addAPIKey, addPaginationToURL, addFilterToURL, self = this;
    options = options || {};

    if (!options.hasOwnProperty('url') && !this.structure.url) {

        clientConfig = this.client();

        if (options.hasOwnProperty('type')) {
            resType = options.type;
        } else {
            resType = this.get('type');
        }

        headers = { 'Accept': resourceMapping[resType][1] };

        endPoint = resourceMapping[resType][0];

        addWorkSpace = function addWorkSpace() {
            if (endPoint === 'workspaces') {
                return '/';
            }
            return (clientConfig.workSpaceId === 0 ? '/' : ('/workspaces/' + clientConfig.workSpaceId + '/'));
        };

        endPointWithId = function endPointWithId() {
            try {
                return endPoint + '/' + self.get('id');
            } catch (e) {
                return endPoint;
            }
        };

        addAPIKey = function addAPIKey() {
            return '?apikey=' + clientConfig.apiKey;
        };

        addPaginationToURL = function addPagination() {
            if (options.hasOwnProperty('pagination') && options.pagination.all) {
                return ('&limit=' + (options.pagination.limit || '20')) + ('&offset=' + (options.pagination.startAt || '0'));
            }
            return '';
        };

        addFilterToURL = function addFilter() {
            if (options.hasOwnProperty('pagination') && options.pagination.filter) {
                return '&' + options.pagination.filter;
            }
            return '';
        };

        url = (addWorkSpace() + endPointWithId() + addAPIKey() + addPaginationToURL() + addFilterToURL());
        return trigger.apply(this, ['get', url, headers, {extraSuccessCodes: resourceMapping[resType][2], type: resourceMapping[resType][0]}]);
    }
    url = options.url || this.get('url');
    resType = guessHeaders(url);

    headers = { 'Accept': resourceMapping[resType][1] };

    url = url.replace(/http(s|):\/\/api\.whispir\.com/i, '');
    return trigger.apply(this, ['get', url, headers, {extraSuccessCodes: resourceMapping[resType][2], type: resourceMapping[resType][0]}]);
};

function guessHeaders(url) { /* https://api.whispir.com/workspaces?apiKey=apiKey || https://api.whispir.com/workspaces/1234?apiKey=apiKey GIVES workspaces*/
    var clue;
    url = url.split('?')[0].split('/');
    clue = url.pop(-1);
    if (!resourceMapping.hasOwnProperty(clue)) {
        clue = url.pop(-2);
    }
    if (!resourceMapping.hasOwnProperty(clue)) {
        throw new Error('Could not identify the endpoint based on given URL - `' + url + '`');
    }
    return clue;
}

function trigger(method, fullyFormedEndpoint, headers, options) {
    var deferred = Q.defer(),
        successCodes = [200, 204].concat(options.extraSuccessCodes),
        clientConfig = this.client(),
        reqOptions;

    function url(clientProperties, endpoint) {
        return clientProperties.host + endpoint;
    }

    function callback(err, response, body) {
        var result = {
                id: null,
                location: null,
                statusCode: response.statusCode,
                type: options.type,
                body: ''
            };
        try {
            result.body = JSON.parse(body);
            if (result.body.hasOwnProperty('status') && result.body.status === "No records found") {
                result.body = [];
            }
        } catch (e) {
            result.body = body;
        }

        if (!err && successCodes.indexOf(response.statusCode) !== -1) {
            deferred.resolve(result);
        } else {
            deferred.reject(result);
        }
    }
    // console.log(reqOptions);
    // making the request with the reqOptions and call the callback function which will resolve/reject the promise
    reqOptions = {
        method: method,
        url: url(clientConfig, fullyFormedEndpoint),
        auth: clientConfig.auth,
        headers: $.extend(headers, clientConfig.headers)
    };

    request(reqOptions, callback);

    return deferred.promise;
}

module.exports = Fetch;
