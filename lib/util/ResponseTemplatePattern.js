var ResponseTemplatePattern = function ResponseTemplatePattern(options) {
	var structureKeys = ['name', 'textPrompt', 'voicePrompt',
	                      'spokenVoicePrompt', 'pattern', 'colour', 'color'];

	options = options || {};
	this.structure = {};
	var self = this;
	
	if (options && options !== "" && Object.keys(options).length !== 0) {
		structureKeys.forEach(function (element) {
			if (options.hasOwnProperty(element)) {
				self.structure[element] = options[element];
			}
		});
	}

	if (options.color) { // for the american friends
		structureKeys.colour = options.color;
	}
};

ResponseTemplatePattern.prototype.name = function (name) {
	this.structure.name = name;
	return this;
};

ResponseTemplatePattern.prototype.textPrompt = ResponseTemplatePattern.prototype.textReply = function (textPrompt) {
	this.structure.textPrompt = textPrompt;
	return this;
};

ResponseTemplatePattern.prototype.voicePrompt = ResponseTemplatePattern.prototype.press = function (voicePrompt) {
	this.structure.voicePrompt = voicePrompt;
	return this;
};

ResponseTemplatePattern.prototype.spokenVoicePrompt = ResponseTemplatePattern.prototype.toSay = function (spokenVoicePrompt) {
	this.structure.spokenVoicePrompt = spokenVoicePrompt;
	return this;
};

ResponseTemplatePattern.prototype.pattern = function (pattern) {
	if ('startswith,contains,exactmatch'.indexOf(pattern.toLowerCase()) === -1) {
		throw new Error(pattern + ' is invalid. Valid patterns are startsWith, contains, exactmatch');
	}
	this.structure.pattern = pattern;
	return this;
};

ResponseTemplatePattern.prototype.colour = ResponseTemplatePattern.prototype.color = function (colour) {
	this.structure.colour = colour;
	return this;
};

ResponseTemplatePattern.prototype.toJsonString = function () {
	return JSON.stringify(this.structure);
};

ResponseTemplatePattern.prototype.toJson = function () {
	return this.structure;
};

module.exports = ResponseTemplatePattern;