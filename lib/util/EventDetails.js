var EventForm = function EventForm (options){
	var structureKeys = ['formName', 'eventFieldList'], self = this;

	options = options || {};
	this.structure = {
		formName: '',
		eventFieldList: []
	};
	
	if (options && options !== "" && Object.keys(options).length !== 0) {
		structureKeys.forEach(function (element) {
			if (options.hasOwnProperty(element)) {
				self.structure[element] = options[element];
			}
		});
	}
};

EventForm.prototype.formName = EventForm.prototype.name = function(formName) {
	this.structure.formName = formName;
	return this;
};

EventForm.prototype.eventFieldList = EventForm.prototype.fieldList = function(eventFieldList) {
	if (!Array.isArray(eventFieldList)) {
		throw new Error(eventFieldList + ' is not an Array. `eventFieldList` must be an array of eventFieldListItems');
	}
	this.structure.eventFieldList = eventFieldList;
	return this;
};

EventForm.prototype.eventFieldListItem = EventForm.prototype.field = function(name, value) {
	var anItem = {
		name: name,
		value: value
	}
	this.structure.eventFieldList.push(anItem);
	return this;
};

EventForm.prototype.action = function (owner, date, details, serial) {
	if (!serial || isNaN(serial)) {
		throw new Error('what is the serial order number for this action? 1, 2, 3? . serial should be a `number` and is the last parameter for this function');
	}
	this.structure.eventFieldList.push({
		name: 'actionOwner' + serial,
		value: owner
	});
	this.structure.eventFieldList.push({
		name: 'actionDate' + serial,
		value: date
	});
	this.structure.eventFieldList.push({
		name: 'actionDetails' + serial,
		value: details
	});
	return this;
};

EventForm.prototype.toJSON = function () {
	return this.structure;
};

module.exports = EventForm;