/*jslint node: true */
'use strict';

var MessageStatus = function MessageStatus(jsonData) {
    var status = {
            sent: {count: 0, percentage: '0.0%'},
            pending: {count: 0, percentage: '0.0%'},
            received: {count: 0, percentage: '0.0%'},
            acknowledged: {count: 0, percentage: '0.0%'},
            undeliverable: {count: 0, percentage: '0.0%'},
            total: {count: 0, percentage: 0},
            final: ''
        },
        categories = [];

    if ((typeof jsonData === "object") && (jsonData !== null)) {
        if (jsonData.body) {
            jsonData = jsonData.body;
        }
        try {
            categories = jsonData.messageStatuses[0].categories;
            categories.forEach(function (category) {
                var catgName = category.name.toLowerCase(),
                    catgCount = category.recipientCount,
                    catgPercentage = category.percentageTotal;

                status[catgName].count = catgCount;
                status[catgName].percentage = catgPercentage;

                if (catgPercentage === '100.0%') {
                    status.final = catgName;
                }

                status.total.count += catgCount;
                status.total.percentage += parseFloat(catgPercentage);
            });
        } catch (e) {
            throw new Error('Invalid Status Data Or `Detailed` status data. This util only supports `summary` status');
        }
    }
    return status;
};

module.exports = MessageStatus;
