/*jslint node: true */
/* global process */
'use strict';

var WhispirClient = require('./client/Whispir');

/* The Api giving you the client */

var WhispirApi = function WhispirApi(username, password, apiKey) {
    var homedir, config, configFilePath;

    if (!username || !password || !apiKey) {
        //attempt to read the file from the user's home directory
        homedir = process.env.HOME || process.env.USERPROFILE;

        if (homedir) {
            try {
                configFilePath = homedir + '/.whispir/auth.json';
                config = require(configFilePath);
                username = config.username;
                password = config.password;
                apiKey = config.apikey;
            } catch (e) {
                console.error('Couldn\'t find auth.json in your home dir [' + homedir + '/.whispir]. Exiting...');
            }
        }
    }

    if (!username || !password || !apiKey) {
        throw new Error("parameters not provided or auth.json could not be read. Please configure auth.json and try again.");
    }
    return new WhispirClient(username, password, apiKey);
};

module.exports = WhispirApi;
