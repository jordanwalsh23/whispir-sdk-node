/*jslint node: true */
/* global process */
'use strict';

var $ = require('underscore');

var WActivity = require('../resources/Activity');
var WCallback = require('../resources/Callback');
var WContact = require('../resources/Contact');
var WCustomList = require('../resources/CustomList');
var WDistList = require('../resources/DistributionList');
var WEvent = require('../resources/Event');
var WImport = require('../resources/Import');
var WMessage = require('../resources/Message');
var WResource = require('../resources/Resource');
var WResponseRule = require('../resources/ResponseRule');
var WScenario = require('../resources/Scenario');
var WTemplate = require('../resources/Template');
var WUser = require('../resources/User');
var WWorkspace = require('../resources/Workspace');

var UTIL = {
    Fetch: require('../util/Fetch'),
    FileReader: require('../util/FileReader'),
    wEventForm: require('../util/EventDetails'),
    wMessageResponse: require('../util/MessageResponses'),
    wMessageStatus: require('../util/MessageStatus'),
    wResponseTemplatePattern: require('../util/ResponseTemplatePattern'),
    WashingMachine: require('../util/WashingMachine')
};

var WhispirClient = function WhispirClient(username, password, apiKey) {
    this.config = {
        host: 'https://api.whispir.com',
        auth: {
            user: username,
            pass: password
        },
        apiKey: apiKey,
        workSpaceId: 0,
        headers: {
            'X-Originating-SDK': 'nodejs-v.2.1.1'
        }
    };
    this.workSpaceId = this.config.workSpaceId;
};

WhispirClient.prototype.setWorkSpaceId = function setWorkSpaceId(workspaceId) {
    this.workSpaceId = workspaceId;
    this.config.workSpaceId = workspaceId;
    return this;
};

WhispirClient.prototype.getWorkSpaceId = function getWorkSpaceId() {
    return this.workSpaceId;
};

WhispirClient.prototype.Activity = function Activity(options) {
    var xConfig = $.clone(this.config);
    return new WActivity(options, xConfig);
};

WhispirClient.prototype.Callback = function Callback(options) {
    var xConfig = $.clone(this.config);
    return new WCallback(options, xConfig);
};

WhispirClient.prototype.Contact = function Contact(options) {
    var xConfig = $.clone(this.config);
    return new WContact(options, xConfig);
};

WhispirClient.prototype.CustomList = function CustomList(options) {
    var xConfig = $.clone(this.config);
    return new WCustomList(options, xConfig);
};

WhispirClient.prototype.DistributionList = WhispirClient.prototype.DL = function DistributionList(options) {
    var xConfig = $.clone(this.config);
    return new WDistList(options, xConfig);
};

WhispirClient.prototype.Event = function Event(options) {
    var xConfig = $.clone(this.config);
    return new WEvent(options, xConfig);
};

WhispirClient.prototype.Import = function Import(options) {
    var xConfig = $.clone(this.config);
    return new WImport(options, xConfig);
};

WhispirClient.prototype.Message = WhispirClient.prototype.SMS = WhispirClient.prototype.Email = WhispirClient.prototype.Call = WhispirClient.prototype.Calls = function Message(options) {
    var xConfig = $.clone(this.config);
    return new WMessage(options, xConfig);
};

WhispirClient.prototype.Resource = function Resource(options) {
    var xConfig = $.clone(this.config);
    return new WResource(options, xConfig);
};

WhispirClient.prototype.ResponseRule = function ResponseRule(options) {
    var xConfig = $.clone(this.config);
    return new WResponseRule(options, xConfig);
};

WhispirClient.prototype.Scenario = function Scenario(options) {
    var xConfig = $.clone(this.config);
    return new WScenario(options, xConfig);
};

WhispirClient.prototype.Template = function Template(options) {
    var xConfig = $.clone(this.config);
    return new WTemplate(options, xConfig);
};

WhispirClient.prototype.User = function User(options) {
    var xConfig = $.clone(this.config);
    return new WUser(options, xConfig);
};

WhispirClient.prototype.WorkSpace = function WorkSpace(options) {
    var xConfig = $.clone(this.config);
    return new WWorkspace(options, xConfig);
};

WhispirClient.prototype.New = WhispirClient.prototype.Create = function (type, options) {
    if (typeof options === 'string') {
        throw new Error('options should be an `object` or `undefined`(no options passed) only');
    }
    type = type.toLowerCase();
    var map = {
        activity: 'Activity',
        callback: 'Callback',
        contact: 'Contact',
        customlist: 'CustomList',
        distributionlist: 'DistributionList',
        dl: 'DistributionList',
        event: 'Event',
        import: 'Import',
        message: 'Message',
        resource: 'Resource',
        responserule: 'ResponseRule',
        scenario: 'Scenario',
        template: 'Template',
        user: 'User',
        workspace: 'WorkSpace',
        sms: 'SMS',
        email: 'Email',
        call: 'Call',
        calls: "Calls"
    };

    if (map.hasOwnProperty(type)) {
        return this[map[type]](options);
    }
    throw new Error(type + ' is not available in Whispir Client. Valid types are - ' + Object.keys(map));
};

WhispirClient.prototype.Fetch = function (type, options) {
    if (typeof options === 'string') {
        throw new Error('options should be an `object` or `undefined`(no options passed) only');
    }
    options = options || {};
    type = type.toLowerCase();
    var map = ["activity", "activities", "callback", "callbacks", "contact", "contacts", "customlist", "customlists",
        "distributionlist", "distributionlists", "dl", "event", "events", "import", "imports",
        "message", "messages", "messagestatus", "messageresponses", "sms", "email", "calls", "call",
        "resource", "resources", "responserule", "responserules", "scenario", "scenarios",
        "template", "templates", "user", "users", "workspace", "workspaces"],
        xConfig = $.clone(this.config);

    if (map.indexOf(type) !== -1) {
        options.type = type;
        return new UTIL.Fetch(options, xConfig);
    }
    throw new Error(type + ' is not available in Whispir Client. Valid types are - ' + map.join());
};

WhispirClient.prototype.Util = WhispirClient.prototype.util = function (type, data) {
    var alias = { messagestatus: 'wMessageStatus', responsetemplatepattern: 'ResponseTemplatePattern', eventform: 'wEventForm' };
    if (typeof type !== 'string' || type === '') {
        throw new Error('Specify the util type. its a string');
    }
    if(alias.hasOwnProperty(type.toLowerCase())){
        type = alias[type.toLowerCase()];
    }
    if (UTIL.hasOwnProperty(type)) {
        return new UTIL[type](data, this);
    }
    throw new Error(type + ' is not available in Whispir Client Util. Valid types are - ' + Object.keys(UTIL));
};

module.exports = WhispirClient;
