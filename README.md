Whispir SDK for NodeJS
======================

Deliver communications to your users, team members, customers, stakeholders or staff members via simple node.js commands.

Powered by [Whispir.io](https://whispir.io)

Whispir supports communication via:

-	sms
	-	2 way
	-	number masking
-	email

	-	email to sms

-	voice (ivr)

-	web

	-	rich messaging

-	push notifications

-	rss

-	social media

	-	twitter
	-	facebook
	-	linkedin

The value of Whispir lies in how quickly one can orchestrate a rich workflow by pivoting from one channel to another without the need for changing the providers. So, you can begin with SMS, depending on the reply, send an email or trigger a voice call.

An Example:

**Customer satisfaction survey**

-	Send an SMS asking for rating of your service (1[bad] - 10[good])
-	Customer replies to your SMS (whispir supports SMS replies for free) as 3
-	Immediately you can
	-	Send a `callback` response to your support application to add rating comments OR send a support email to your Customer service agent
	-	May choose to call the customer via voice channel to get in-person input
	-	...
	-	...

For more details and use cases visit https://www.whispir.com and https://whispir.io

Note: This SDK is built to support the API definitions provided at https://whispir.io. If there is any bug/issue in implementation of this library, please refer to the API Documentation first and then raise a ticket.

Getting Started
---------------

You'll need to sign up for an account at [Whispir.io](https://whispir.io). This will give you your username, password and apikey.

Installing the SDK
------------------

Whispir SDK is available on NPM. Dependencies are q (for promises), _ (underscore), request, chai-as-promised, chai, nock, mock-fs, mocha.

`npm install whispir-node-sdk`

Initiating a client
-------------------

To use the SDK library, you have to create a client object first. This client provides you with all the resources (for each endpoint and some util packages)

```
var WhispirSDK = require('whispir-node-sdk');

// sending in credentials, and apiKey
var wAPIClient = new WhispirSDK('username', 'password', 'apikey');

// whispir sdk also supports passing in the credentials, key via a fixed auth.json file placed in the home directory (shown below)
// when no params are passed, the SDK searches in your home directory
var wAPIClient = new WhispirSDK();
```

*Create a file in your home directory:*

```
mkdir ~/.whispir
touch ~/.whispir/auth.json
```

The auth.json file should look as follows:

```
{
  "username":"john.smith",
  "password":"one2three",
  "apikey":"ds0dsf09hdfsh90df90hdfsj"
}
```

The whispir-node-sdk looks in your home directory for these properties so without them set, it won't be able to connect to the API.

Resources (endpoints)
---------------------

The SDK supports the following API Endpoints

| END POINT          | RESOURCE             | Use                                                                                               |
|--------------------|----------------------|---------------------------------------------------------------------------------------------------|
| /messages          | Message              | `wAPIClient.Message()` <br> `wAPIClient.SMS()` <br> `wAPIClient.Email()` <br> `wAPIClient.Call()` |
| /templates         | Template             | `wAPIClient.Template()`                                                                           |
| /contacts          | Contact              | `wAPIClient.Contact()`                                                                            |
| /distributionlists | DistributionList, DL | `wAPIClient.DistributionList()`, `wAPIClient.DL()`                                                |
| /users             | User                 | `wAPIClient.User()`                                                                               |
| /callbacks         | Callback             | `wAPIClient.Callback()`                                                                           |
| /workspaces        | Workspace            | `wAPIClient.WorkSpace()`                                                                          |
| /activities        | Activity             | `wAPIClient.Activity()`                                                                           |
| /event             | Event                | `wAPIClient.Event()`                                                                              |
| /customlists       | CustomList           | `wAPIClient.CustomList()`                                                                         |
| /responserules     | ResponseRule         | `wAPIClient.ResponseRule()`                                                                       |
| /scenarios         | Scenario             | `wAPIClient.Scenario()`                                                                           |
| /resources         | Resource             | `wAPIClient.Resource()`                                                                           |
| /import            | Import               | `wAPIClient.Import()`                                                                             |

How to Use ?
------------

Using Whispir-node-sdk is easy. Here's an example of sending a Message.

```
var WhispirSDK = require('whispir-node-sdk');

var wAPIClient = new WhispirSDK(); // uses the auth.json file as no params are passed in


// creating the message | `new` prefix is not needed. its automatic
var wMessage = wAPIClient.Message({
        to: '+10000000',
        subject: 'Hello',
        body: 'Sending a message using whispir node sdk'
    });

    //send() triggers the API call and it returns a promise.
    wMessage.send().then(function(result){
        //result contains the location, id of the sent message
        console.log('The id is `%s`, and location is `%s`', result.id, result.location);
    }, function(fail){
        // contains the response code and error
    });
```

**To send a message in a particular workspace, you can -**

-	set the workspace at client level
	-	all the API Calls made using this client uses the workspaceId "1234ABCD"
	-	you can use the `wAPIClient.getWorkSpaceId()` to return you the current workSpaceId of the client object (set at global level)

```
    /* set workspace at client level */
    wAPIClient.setWorkSpaceId('1234ABCD');

```

-	set the workspace at the RESOURCE level
	-	this will NOT overwrite the client's workSpaceId.
	-	It is temporary and is only limited to this Resource (API Call) you are doing
	-	you can use the `wMessage.get('workSpaceId')` to return you the current workSpaceId of the client object that is sending making this API Call
	-	helps you to use a single client and switch between multiple workspaces with ease
	-	there is no `wMessage.workSpaceId('1234ABCD')` method available to set the workSpaceId. It must be passed in when the RESOURCE is being created. `wAPIClient.wMessage({workSpaceId: '1234ABCD'})`

```
var WhispirSDK = require('whispir-node-sdk');

var wAPIClient = new WhispirSDK(); // uses the auth.json file as no params are passed in
    wAPIClient.setWorkSpaceId('1234ABCD');

/* set workspace at resource level */

var wMessage = wAPIClient.Message({workSpaceId : '4567MNOP'});

/* assertion */
wAPIClient.getWorkSpaceId() === wMessage.get('workSpaceId') // return false

```

Method Chaining and Promises
----------------------------

WhispirSDK supports method chaining and also returns a promise after an API call is made. So, you can do -

```
var wMessage = wAPIClient.Message({workSpaceId: 'ABCD1234'})
                .to('+100000000'); // no dashes please

var wTemplate = wAPIClient.Template({workSpaceId: 'ABCD1234'})
                .subject('Hello')
                .body('Sample Message');

/* create a template and then send a message using that template */
wTemplate.create().then( function (template) {
        return wMessage.useTemplateWithIdAs(template.id).send(); //sending a message returns a promise again
    }).then( function (message) {
        console.log('The id is `%s`, and location is `%s`', message.id, message.location);
    }.catch( function (err) {
        console.log('Error occurred', err);
    });
```

How to ?
--------

### Message

-	To send a message via SMS , Email, Voice, Social Media channels
-	Message also comes with neat synonyms - SMS, Email, Call. These 3 are in all parts similar to message. they are just a naming convenience to state the nature of the message object and the channel of communication.

```
var wMessage = wAPIClient.Message();

var wSMS = wAPIClient.SMS(); // === wMessage
var wEmail = wAPIClient.Email();
var wCall = wAPIClient.Call();
```

**Methods Available**

| Method name                                                   | Definition                                        | Use                                                                                                                                                                                                                                                                                          |
|---------------------------------------------------------------|---------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| client                                                        | function() {} <br> - takes no arguments           | returns the current client configuration object (apiKey, username, password) <br> <br> `var clientConfig = wMessage().client()`                                                                                                                                                              |
| describe                                                      | function() {} <br> - takes no arguments           | returns simple help about this endpoint <br> <br> `wMessage().describe()`                                                                                                                                                                                                                    |
| get                                                           | function (*string* propertyName) {}               | returns the value of a property in this object <br> <br> `wMessage().get('to'); \\ - returns the value of 'to' property` <br><br> Throws an Error "*property* is undefined in *RESOURCE* Object" if requested for an invalid property Or property whose value is not yet set                 |
| id                                                            | function (*string* id) {}                         | sets the message Id value. Used during the retrieve, getMessageStatus, getMessageResponses actions <br> <br> - returns the object instance for method chaining                                                                                                                               |
| to                                                            | function (*string* to) {}                         | sets the `to` value. Your string could be comma separated values of phone numbers, email, contact MRI, DistributionList MRI values <br> <br> `wMessage().to('6598765432, sam@example.com'); \\ sets the value for 'to' property` <br> <br> - returns the object instance for method chaining |
| subject                                                       | function (*string* subject) {}                    | sets the `subject` value. Common for both SMS, Email <br> <br> - returns the object instance for method chaining                                                                                                                                                                             |
| body                                                          | function (*string* body) {}                       | sets the `body` value (goes in as SMS text) <br> <br> - returns the object instance for method chaining                                                                                                                                                                                      |
| email                                                         | function (*object* email) {}                      | sets the `email` value <br> <br> - returns the object instance for method chaining                                                                                                                                                                                                           |
| voice, call                                                   | function (*object* voice) {}                      | sets the voice value <br> <br> - returns the object instance for method chaining                                                                                                                                                                                                             |
| web                                                           | function (*object* web) {}                        | sets the web value <br> <br> - returns the object instance for method chaining                                                                                                                                                                                                               |
| social                                                        | function (*object* social) {}                     | sets the social value <br> <br> - returns the object instance for method chaining                                                                                                                                                                                                            |
| type                                                          | function (*string* type) {}                       | sets the type value <br> <br> - returns the object instance for method chaining                                                                                                                                                                                                              |
| social                                                        | function (*object* social) {}                     | sets the social value <br> <br> - returns the object instance for method chaining                                                                                                                                                                                                            |
| messageattributes                                             | function (*object* messageattributes) {}          | sets the messageattributes value <br> <br> - returns the object instance for method chaining                                                                                                                                                                                                 |
| resource                                                      | function (*object* resource) {}                   | sets the resource value <br> <br> - returns the object instance for method chaining                                                                                                                                                                                                          |
| features                                                      | function (*object* features) {}                   | sets the features value <br> <br> - returns the object instance for method chaining                                                                                                                                                                                                          |
| schedule                                                      | function (*object* schedule) {}                   | sets the schedule value <br> <br> accepts the following in the object `{ count: '', days: '', hours: '', minutes: '', startDate: '', type: ''}` <br>scheduleType can be `ONCE` or `REPEAT` <br> <br> - returns the object instance for method chaining                                       |
| messageTemplateId, useTemplateWithIdAs, usingTemplateId       | function (*string* templateId) {}                 | sets the messageTemplateId value <br> <br> - returns the object instance for method chaining                                                                                                                                                                                                 |
| messageTemplateName, useTemplateWithNameAs, usingTemplateName | function (*string* templateName) {}               | sets the messageTemplateName value <br> <br> - returns the object instance for method chaining                                                                                                                                                                                               |
| callbackId                                                    | function (*string* callbackName) {}               | sets the callbackId value. Actually its the name of the callback. Not the Id. So provide the `name`, not the `Id` <br> <br> - returns the object instance for method chaining                                                                                                                |
| callbackParameters                                            | function (*object* callbackParameters) {}         | sets the callbackParameters value. These values are returned back to you via the callback response. <br> <br> - returns the object instance for method chaining                                                                                                                              |
| eventId, usingEventId                                         | function (*string* eventId) {}                    | sets the eventId. The messages sent under this eventId <br> <br> - returns the object instance for method                                                                                                                                                                                    |
| label                                                         | function (*string* label) {}                      | sets the label value. <br><br> - returns the object instance for method chaining                                                                                                                                                                                                             |
| attach                                                        | function (*string* type, *Array* attachments) { } | Useful to send attachment for Email or Custom voice messages in Voice Calls <br> <br> - returns the object instance for method                                                                                                                                                               |
| toJsonString                                                  | function () {}                                    | - returns the object as string <br><br> you cannot chain a method after this                                                                                                                                                                                                                 |

**Methods that trigger the API Call**

| Method name                       | Definition                                                                                                                                                                                                                                           | Use                                                                                                                                                                                                                                                                                                                                                                                        |
|-----------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| search                            | function (*string* queryParams) {}                                                                                                                                                                                                                   | searches for message with the given queryParams <br><br> returns a promise with results                                                                                                                                                                                                                                                                                                    |
| getStatus, getMessageStatus       | function (*string* view) {} <br><br> - view can be either 'summary' or 'detailed' <br> - the `id` property (messageId) must have been set to the object already                                                                                      | provides the status for the given messageId <br><br> - returns a promise with status in results.body                                                                                                                                                                                                                                                                                       |
| getResponses, getMessageResponses | function (*string* view) {} <br><br> - view can be either 'summary' or 'detailed' <br> - the `id` property (messageId) must have been set to the object already                                                                                      | provides the responses for the given messageId <br><br> - returns a promise with responses in results.body                                                                                                                                                                                                                                                                                 |
| retrieveEventMessages             | function () {}                                                                                                                                                                                                                                       | retrieves all the messages sent for a specific event. The Message `label` has to be provided for this method to succeed <br><br> - returns a promise with responses in results.body                                                                                                                                                                                                        |
| send, make                        | function (*string* message-properties-object) {} <br> <br> the parameter for this function can be a json object with all the message properties sent directly such that you do not need to define each property separately. (see example code below) | does the API Call to send the message. send - make are synonyms here. send for message (sms), email. where as make for Call. <br><br> - returns a promise with id, location of the sent message                                                                                                                                                                                            |
| bulkSend                          | function () {}                                                                                                                                                                                                                                       | to be used ONLY when doing a bulk sending of messages via the `resourceId` as VSMT headers would differ from normal send. For this to succeed, the resourceId property must be defined. Refer to [/resources endpoint](https://whispir.github.io/api/#sending-messages-using-template-&-resources) for more information <br><br> - returns a promise with id, location of the sent message |

### Code Examples for message object

#### Sending a Message / SMS

-	Refer to Online API Documentation [Link](https://whispir.github.io/api/#sms-messages)

```
var messageDefn = {
    workSpaceId: 'AZYPK123ER54HG23',
    to : '+100000000',
    subject: 'This is a Subject',
    body: 'This is body of message'
};

var onSuccess = function (result) {
    // result contains the location, id of the sent message
    console.log('The id is `%s`, and location is `%s`', result.id, result.location);
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

/* Defining the message when Initiating it */
wAPIClient.Message(messageDefn).send().then(onSuccess, onFailure);

/* workSpaceId can only be sent while Initiating; then using method chains to define the rest of message properties */

var messageOptions = {
        workSpaceId: 1
};
var messageProperties = {
        to: '+6598765432',
        subject: 'message subject',
        body: 'message body'
};

wAPIClient.Message(messageOptions)
    .to(messageProperties.to)
    .subject(messageProperties.subject)
    .body(messageProperties.body)
    .send().then(onSuccess, onFailure);

/* sending the message property directly to the send method */
wAPIClient.Message(messageOptions)
    .send(messageProperties).then(onSuccess, onFailure);

// Note: If workspaceId is not passed, the message will be sent in your Default workspace
wAPIClient.Message()
    .to(messageProperties.to)
    .subject(messageProperties.subject)
    .body(messageProperties.body)
    .send().then(onSuccess, onFailure); // message sent in Default workspace

/* you may also use .SMS() in place of .Message() in all the examples above */
wAPIClient.SMS() is same as wAPIClient.Message();
```

#### Sending an Email

-	Refer to Online API Documentation [Link](https://whispir.github.io/api/#email-messages)

```
var EmailDefn = {
    workSpaceId: 'AZYPK123ER54HG23',
    to : "john.smith@test.com",
    subject : "Test Email Message",
    email : {
        body : "Email Body goes here.",
        footer : "Email signature goes here.",
        type : "text/plain"
    }
};

var onSuccess = function (result) {
    // result contains the location, id of the sent message
    console.log('The id is `%s`, and location is `%s`', result.id, result.location);
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

/* Defining the message when Initiating it */
wAPIClient.Message(EmailDefn).send().then(onSuccess, onFailure);

/* workSpaceId can only be sent while Initiating; then using method chains to define the rest of message properties */

var messageOptions = {
    workSpaceId: 1
};
var messageProperties = {
    to : "john.smith@test.com",
    subject : "Test Email Message",
    email : {
        body : "Email Body goes here.",
        footer : "Email signature goes here.",
        type : "text/plain"
    }
};

wAPIClient.Message(messageOptions)
    .to(messageProperties.to)
    .subject(messageProperties.subject)
    .email(messageProperties.email)
    .send().then(onSuccess, onFailure);

/* sending the message property directly to the send method */
wAPIClient.Message(messageOptions)
    .send(messageProperties).then(onSuccess, onFailure);

// you may also use .Email() in place of .Message() in all the examples above
wAPIClient.Email() is same as wAPIClient.Message();
```

#### Making a Voice call, and Conference calls

-	Refer to Online API Documentation [Link](https://whispir.github.io/api/#voice-messages)

```
var VoiceDefn = {
    workSpaceId: 'AZYPK123ER54HG23',
    to : "john.smith@test.com",
    subject : "Test Email Message",
    voice : {
        header : "This is the introduction of the voice call",
        body : "This is the body of the message",
        type : "ConfCall:,ConfAccountNo:,ConfPinNo:,ConfModPinNo:,Pin:"
    }
};

var onSuccess = function (result) {
    // result contains the location, id of the sent message
    console.log('The id is `%s`, and location is `%s`', result.id, result.location);
};

var onFailure = function (fail) {
    // contains the response code and error
    console.log('Error Code: %s, Reason: %s', fail.statusCode, fail.body);
};

/* Defining the message when Initiating it */
wAPIClient.Message(VoiceDefn).send().then(onSuccess, onFailure);

/* workSpaceId can only be sent while Initiating; then using method chains to define the rest of message properties */

var messageOptions = {
    workSpaceId: 1
};
var messageProperties = {
    to : "john.smith@test.com",
    subject : "Test Email Message",
    voice : {
        header : "This is the introduction of the voice call",
        body : "This is the body of the message",
        type : "ConfCall:,ConfAccountNo:,ConfPinNo:,ConfModPinNo:,Pin:"
    }
};

wAPIClient.Call(messageOptions)
    .to(messageProperties.to)
    .subject(messageProperties.subject)
    .voice(messageProperties.voice)
    .make().then(onSuccess, onFailure);

/* sending the message property directly to the send method */
wAPIClient.Message(messageOptions)
    .send(messageProperties).then(onSuccess, onFailure);

// you may also use .Email() in place of .Message() in all the examples above
wAPIClient.Call() === wAPIClient.Message()

// send and make are same too
wAPIClient.Call().make() === wAPIClient.Call().send() === wAPIClient.Message().send() === wAPIClient.Message().make()
```

#### Get Message Status, Get Message Responses

-	Message Status - [Online Docs](https://whispir.github.io/api/#message-status)
-	Message Responses - [Online Docs](https://whispir.github.io/api/#message-responses)

```
var messageOptions = {
    workSpaceId: 1,
    id: 'HJ8702734SDNFN7823'
};

/* getting status of a message */
wAPIClient.Message(messageOptions).getMessageStatus(); // returns a promise

// similarly
wAPIClient.Message(messageOptions).getStatus(); // getStatus() is alias for getMessageStatus(); returns a promise

// you can also pass in workSpaceId, id (messageId) separately
wAPIClient.Message({workSpaceId: messageOptions.workSpaceId}).id(messageOptions.id).getStatus();

/* getting message Responses */
wAPIClient.Message(messageOptions).getMessageResponses(); // returns a promise

// similarly
wAPIClient.Message(messageOptions).getResponses(); // getResponses() is alias for getMessageResponses(); returns a promise
```

### Templates

-	Refer to Online Documentation for [Templates](https://whispir.github.io/api/#templates)
-	Refer to examples/Template

**Methods Available**

| Method name                      | Definition                                | Use                                                                                                                                                                                                                                                                           |
|----------------------------------|-------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| client                           | function() {} <br> - takes no arguments   | returns the current client configuration object (apiKey, username, password) <br> <br> `var clientConfig = wTemplate().client()`                                                                                                                                              |
| describe                         | function() {} <br> - takes no arguments   | returns simple help about this endpoint <br> <br> `wTemplate().describe()`                                                                                                                                                                                                    |
| get                              | function (*string* propertyName) {}       | returns the value of a property in this object <br> <br> `wTemplate().get('to'); \\ - returns the value of 'to' property` <br><br> Throws an Error "*property* is undefined in *RESOURCE* Object" if requested for an invalid property Or property whose value is not yet set |
| id                               | function (*string* id) {}                 | sets the template Id value. Used during the retrieve, update, delete actions <br> <br> - returns the object instance for method chaining                                                                                                                                      |
| name                             | function (*string* name) {}               | sets the template name value. Used by when message sending and identification of a template <br><br> - returns the object instance for method chaining                                                                                                                        |
| description                      | function (*string* description) {}        | sets the template description value. <br><br> - returns the object instance for method chaining                                                                                                                                                                               |
| responseRule, responseTemplateId | function (*string* responseTemplateId) {} | sets the responserule associated with this template. <br><br> - returns the object instance for method chaining                                                                                                                                                               |
| subject                          | function (*string* subject) {}            | sets the `subject` value. Common for both SMS, Email <br> <br> - returns the object instance for method chaining                                                                                                                                                              |
| body                             | function (*string* body) {}               | sets the `body` value (goes in as SMS text) <br> <br> - returns the object instance for method chaining                                                                                                                                                                       |
| email                            | function (*object* email) {}              | sets the `email` value <br> <br> - returns the object instance for method chaining                                                                                                                                                                                            |
| voice, call                      | function (*object* voice) {}              | sets the voice value <br> <br> - returns the object instance for method chaining                                                                                                                                                                                              |
| web                              | function (*object* web) {}                | sets the web value <br> <br> - returns the object instance for method chaining                                                                                                                                                                                                |
| social                           | function (*object* social) {}             | sets the social value <br> <br> - returns the object instance for method chaining                                                                                                                                                                                             |
| type                             | function (*string* type) {}               | sets the type value <br> <br> - returns the object instance for method chaining                                                                                                                                                                                               |
| social                           | function (*object* social) {}             | sets the social value <br> <br> - returns the object instance for method chaining                                                                                                                                                                                             |
| features                         | function (*object* features) {}           | sets the features value <br> <br> - returns the object instance for method chaining                                                                                                                                                                                           |
| toJsonString                     | function () {}                            | - returns the object as string <br> <br> you cannot chain a method after this                                                                                                                                                                                                 |

**Methods that trigger the API Call**

| Method name | Definition                                                                                                                                                                                                                                                                                                                       | Use                                                                                                                                                                                       |
|-------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| create      | function (*object* template-properties-object) {} <br> <br> the parameter for this function can be a json object with all the template properties sent directly such that you do not need to define each property separately. (see example code)                                                                                 | does the API Call to create the template. <br><br> - returns a promise with id, location of the created template                                                                          |
| retrieve    | function () {}                                                                                                                                                                                                                                                                                                                   | does the API Call to get the template details. The template id must be already set to the object before this method is invoked. <br><br> - returns a promise with details of the template |
| update      | function (*object* template-properties-object) {} <br> <br> the parameter for this function can be a json object with all the template properties sent directly such that you do not need to define each property separately. (see example code. However the template id must be set explicitly via `wTemplate.id(*string* id)`. | - returns a promise with success/ fail                                                                                                                                                    |
| delete      | function () {}                                                                                                                                                                                                                                                                                                                   | Deletes the template with given Id. Id has to set explicitly via `wTemplate.id(*string* id)` <br> <br> - returns a promise with success/ fail                                             |

Unit test
=========

-	npm test
