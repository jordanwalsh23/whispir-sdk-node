/* global describe */
/* global it */

'use strict';
var chai = require("chai");
var chaiAsPromised = require("chai-as-promised");
var expect = require("chai").expect;
var WUtilFetch = require("../../lib/util/Fetch");
var nock = require('nock');
var _ = require('underscore');

chai.use(chaiAsPromised);

//nock.recorder.rec();
//nock.enableNetConnect();


var resourceMapping = {
		activity: ['activities', 'application/vnd.whispir.activity-v1+json', [404]], 
		callback: ['callbacks', 'application/vnd.whispir.callback-v1+json',[]], 
		contact: ['contacts', 'application/vnd.whispir.contact-v1+json',[]], 
		customlist: ['customlists', 'application/vnd.whispir.customlist-v1+json',[]], 
		distributionlist: ['distributionlists', 'application/vnd.whispir.distributionlist-v1+json',[]], 
		dl: ['distributionlists', 'application/vnd.whispir.distributionlist-v1+json',[]], 
		event: ['events', 'application/vnd.whispir.event-v1+json',[]], 
		import: ['imports', 'application/vnd.whispir.import-v1+json',[]], 
		message: ['messages', 'application/vnd.whispir.message-v1+json',[]], 
		sms: ['messages', 'application/vnd.whispir.message-v1+json',[]], 
		email: ['messages', 'application/vnd.whispir.message-v1+json',[]], 
		calls: ['messages', 'application/vnd.whispir.message-v1+json',[]],	
		resource: ['resources', 'application/vnd.whispir.resource-v1+json',[]], 
		responserule: ['responserules', 'application/vnd.whispir.responserule-v1+json',[]], 
		scenario: ['scenarios', 'application/vnd.whispir.scenario-v1+json',[]], 
		template: ['templates', 'application/vnd.whispir.template-v1+json',[]], 
		user: ['users', 'application/vnd.whispir.user-v1+json',[]], 
		workspace: ['workspaces', 'application/vnd.whispir.workspace-v1+json',[]] 
};

var clientConfig, _rand;

describe('util/Fetch', function (){
	beforeEach(function () {
		_rand = Math.random().toString().split(".")[1];
		clientConfig = {
				host: 'https://api.whispir.com',
				auth: { user: 'username', pass: 'password' },
				apiKey: 'apiKey',
				workSpaceId: 0,
				headers: {
					'X-Originating-SDK': 'nodejs-v.1.0.0'
				}
		};
	});
	it('should fetch the given resource type records', function(){
		var _fetchOptions = {
				type: 'workspace'
		};
		var wUtilFetch = new WUtilFetch(_fetchOptions, clientConfig);
		var _200Response = {
				statusCode: 200
		};
		nock(clientConfig.host, {
			reqheaders: {
				'Accept': resourceMapping[_fetchOptions.type][1]
			}
		})
		.get('/' + resourceMapping[_fetchOptions.type][0])
		.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_200Response.statusCode);

		expect(wUtilFetch.get('type')).to.equal(_fetchOptions.type);

		return wUtilFetch.go().then(function(success){
			//console.log('success',results);
			expect(success.statusCode).to.equal(_200Response.statusCode);
		}, function(fail){
			console.log('fail', fail);
		});

	});
	it('should fetch the given resource type records by URL', function(){
		var _fetchOptions = {
				url: 'http://api.whispir.com' + '/workspaces/1' + '?apikey=' + clientConfig.apiKey
		};
		var wUtilFetch = new WUtilFetch({}, clientConfig);
		var _200Response = {
				statusCode: 200
		};
		
		var _type = 'workspace';
		
		nock(clientConfig.host, {
			reqheaders: {
				'Accept': resourceMapping[_type][1]
			}
		})
		.get('/' + resourceMapping[_type][0] + '/1')
		.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_200Response.statusCode);
		
		return wUtilFetch.url(_fetchOptions.url).go().then(function(success){
			//console.log('success',results);
			expect(success.statusCode).to.equal(_200Response.statusCode);
		}, function(fail){
			console.log('fail', fail);
		});
		
	});
});