/*jslint node: true */
/*global beforeEach, afterEach, describe, expect, it */

'use strict';
var chai = require("chai");
var expect = require("chai").expect;
var wUtilMessageStatus = require("../../lib/util/MessageStatus");

var mStatus = {
        messageStatuses : [ {
            "link" : [ {
                "uri" : "https://api.whispir.com/messages/1/messagestatus?view=summary&apikey=498nadsasdff09fewdsafjaa90f",
                "rel" : "self",
                "method" : "GET"
            } ],
            "categories" : [ {
                "name" : "Sent",
                "recipientCount" : 0,
                "percentageTotal" : "0.0%"
            }, {
                "name" : "Pending",
                "recipientCount" : 0,
                "percentageTotal" : "0.0%"
            }, {
                "name" : "Received",
                "recipientCount" : 0,
                "percentageTotal" : "0.0%"
            }, {
                "name" : "Acknowledged",
                "recipientCount" : 2,
                "percentageTotal" : "100.0%"
            }, {
                "name" : "Undeliverable",
                "recipientCount" : 0,
                "percentageTotal" : "0.0%"
            } ]
        } ],
        "link" : [ ]
    };

describe('util/MessageStatus', function () {
    it('should return the default status when no data is passed', function () {
        var defaultStatus = {
                sent: {count: 0, percentage: '0.0%'},
                pending: {count: 0, percentage: '0.0%'},
                received: {count: 0, percentage: '0.0%'},
                acknowledged: {count: 0, percentage: '0.0%'},
                undeliverable: {count: 0, percentage: '0.0%'},
                total: {count: 0, percentage: 0},
                final: ''
            },
            uMessageStatus = wUtilMessageStatus();

        expect(uMessageStatus).to.eql(defaultStatus);
    });

    it('should return the message status when messageStatus JSON is passed', function () {
        var expectedStatus = {
                sent: {count: 0, percentage: '0.0%'},
                pending: {count: 0, percentage: '0.0%'},
                received: {count: 0, percentage: '0.0%'},
                acknowledged: {count: 2, percentage: '100.0%'},
                undeliverable: {count: 0, percentage: '0.0%'},
                total: {count: 2, percentage: 100},
                final: 'acknowledged'
            },
            uMessageStatus = wUtilMessageStatus(mStatus);

        expect(uMessageStatus).to.eql(expectedStatus);
    });

    it('should return the message status picking the body when messageStatus JSON has body element', function () {
        var expectedStatus = {
                sent: {count: 0, percentage: '0.0%'},
                pending: {count: 0, percentage: '0.0%'},
                received: {count: 0, percentage: '0.0%'},
                acknowledged: {count: 2, percentage: '100.0%'},
                undeliverable: {count: 0, percentage: '0.0%'},
                total: {count: 2, percentage: 100},
                final: 'acknowledged'
            },
            statusWithinBody = {
                body: mStatus
            },
            uMessageStatus = wUtilMessageStatus(statusWithinBody);

        expect(uMessageStatus).to.eql(expectedStatus);
    });

    it('should return a final status when one of the message status percentage is 100 when messageStatus JSON is passed', function () {
        var expectedStatus = {
                sent: {count: 0, percentage: '0.0%'},
                pending: {count: 0, percentage: '0.0%'},
                received: {count: 0, percentage: '0.0%'},
                acknowledged: {count: 2, percentage: '100.0%'},
                undeliverable: {count: 0, percentage: '0.0%'},
                total: {count: 2, percentage: 100},
                final: 'acknowledged'
            },
            statusWithinBody = {
                body: mStatus
            },
            uMessageStatus = wUtilMessageStatus(statusWithinBody);

        expect(uMessageStatus).to.eql(expectedStatus);
        expect(uMessageStatus.final).to.eql(expectedStatus.final);
    });

    it('should throw an error if invalid or detailed status is sent (aka -no categories)', function () {
        var statusWithNothing = {};
        expect(function () { wUtilMessageStatus(statusWithNothing); }).to.throw('Invalid Status Data Or `Detailed` status data. This util only supports `summary` status');
    });
});
