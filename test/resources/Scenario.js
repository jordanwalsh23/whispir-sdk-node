/* global describe */
/* global beforeEach */
/* global it */

'use strict';
var chai = require("chai");
var chaiAsPromised = require("chai-as-promised");
var expect = require("chai").expect;
var WScenario = require("../../lib/resources/Scenario.js");
var nock = require('nock');
var _ = require('underscore');

chai.use(chaiAsPromised);

var clientConfig, _rand, _randContact;

describe('resources/Scenario', function () {
	beforeEach(function () {
		_rand = Math.random().toString().split(".")[1];
		_randContact = '65'+ _rand;
		clientConfig = {
				host: 'https://api.whispir.com',
				auth: { user: 'username', pass: 'password' },
				apiKey: 'apiKey',
				workSpaceId: 0,
				headers: {
					'X-Originating-SDK': 'nodejs-v.1.0.0'
				}
		};
	});
	it("sets an empty scenario structure when no options is sent", function () {
		var wScenarioWithNoOptions = new WScenario("", clientConfig);
		expect(wScenarioWithNoOptions.toJsonString()).to.equal('{}');
		wScenarioWithNoOptions = null;
	});
	it("sets a scenario structure exactly as in passed options", function () {
		var _scenarioOptions = {
				workSpaceId: 1,
				id: 'AB' + _rand + 'YZ',
				name: 'Scenario' + _rand,
				description: 'Description for Scenario' + _rand,
				message: {
					to: _randContact, 
					subject: 'message subject',
					body: 'message body'
				},
				createdTime: Date.now()
		};
		var wScenarioWithOptions = new WScenario(_scenarioOptions, clientConfig);
		var wScenarioWithNoOptions = new WScenario("", clientConfig);

		expect(wScenarioWithOptions.get('workSpaceId')).to.equal(_scenarioOptions.workSpaceId);
		expect(wScenarioWithOptions.get('id')).to.equal(_scenarioOptions.id);
		expect(wScenarioWithOptions.get('name')).to.equal(_scenarioOptions.name);
		expect(wScenarioWithOptions.get('description')).to.equal(_scenarioOptions.description);
		expect(wScenarioWithOptions.get('createdTime')).to.equal(_scenarioOptions.createdTime);

		var _msg = wScenarioWithOptions.get('message');
		expect(_msg).to.be.an('object');
		expect(_msg.to).to.equal(_scenarioOptions.message.to);
		expect(_msg.subject).to.equal(_scenarioOptions.message.subject);
		expect(_msg.body).to.equal(_scenarioOptions.message.body);

		expect(function () {wScenarioWithOptions.get('to');}).to.throw('to is undefined in Scenario Object');

		expect(wScenarioWithOptions).to.not.equal(wScenarioWithNoOptions);

		wScenarioWithOptions = null;
	});
	it("should support method chaining and options", function () {
		var _scenarioOptions = {
				workSpaceId: 1,
				id: 1,
				createdTime: Date.now()
		};
		var _scenarioProperties = {
				name: 'Scenario' + _rand,
				description: 'Description for Scenario' + _rand,
				message: {
					to: _randContact, 
					subject: 'message subject',
					body: 'message body'
				}
		};
		var wScenarioWithOptionsAtInitAndChainProperties = new WScenario(_scenarioOptions, clientConfig);

		wScenarioWithOptionsAtInitAndChainProperties
		.name(_scenarioProperties.name)
		.description(_scenarioProperties.description)
		.message(_scenarioProperties.message);

		expect(wScenarioWithOptionsAtInitAndChainProperties.get('workSpaceId')).to.equal(_scenarioOptions.workSpaceId);
		expect(wScenarioWithOptionsAtInitAndChainProperties.get('id')).to.equal(_scenarioOptions.id);
		expect(wScenarioWithOptionsAtInitAndChainProperties.get('name')).to.equal(_scenarioProperties.name);
		expect(wScenarioWithOptionsAtInitAndChainProperties.get('description')).to.equal(_scenarioProperties.description);
		expect(wScenarioWithOptionsAtInitAndChainProperties.get('createdTime')).to.equal(_scenarioOptions.createdTime);

		var _msg = wScenarioWithOptionsAtInitAndChainProperties.get('message');
		expect(_msg).to.be.an('object');
		expect(_msg.to).to.equal(_scenarioProperties.message.to);
		expect(_msg.subject).to.equal(_scenarioProperties.message.subject);
		expect(_msg.body).to.equal(_scenarioProperties.message.body);

		expect(function() {wScenarioWithOptionsAtInitAndChainProperties.get('to');}).to.throw('to is undefined in Scenario Object');

		wScenarioWithOptionsAtInitAndChainProperties = null;
	});
	it("should not add an `invalid property` to scenario structure -send via options", function () {
		var _scenarioOptions = {
				workSpaceId: 1,
				id: 1,
				invalidProperty: 'somevalue'
		};

		var wScenarioWithOptionsAndInvalidProperty = new WScenario(_scenarioOptions, clientConfig);

		expect(wScenarioWithOptionsAndInvalidProperty.get('workSpaceId')).to.equal(_scenarioOptions.workSpaceId);
		expect(wScenarioWithOptionsAndInvalidProperty.get('id')).to.equal(_scenarioOptions.id);
		expect(function () { wScenarioWithOptionsAndInvalidProperty.get('invalidProperty'); }).to.throw('invalidProperty is undefined in Scenario Object');
		wScenarioWithOptionsAndInvalidProperty = null;
	});

	var _notAllowedViaDirectMethods = ['link', 'createdTime', 'from'];
	_notAllowedViaDirectMethods.forEach(function(key){
		it("should not allow setting " + key + " via direct method", function () {
			var _scenarioOptions = {
					workSpaceId: 1,
					id: 1,
					invalidProperty: 'somevalue'
			};

			var wScenarioWithOptionsAndInvalidDirectSetByMethod = new WScenario(_scenarioOptions, clientConfig);

			expect(wScenarioWithOptionsAndInvalidDirectSetByMethod.get('workSpaceId')).to.equal(_scenarioOptions.workSpaceId);
			expect(wScenarioWithOptionsAndInvalidDirectSetByMethod.get('id')).to.equal(_scenarioOptions.id);
			expect(function () { wScenarioWithOptionsAndInvalidDirectSetByMethod[key](key); }).to.throw(TypeError, 'wScenarioWithOptionsAndInvalidDirectSetByMethod[key] is not a function');
			wScenarioWithOptionsAndInvalidDirectSetByMethod = null;
		});
	});

	describe('- Invalid params to methods', function () {
		var _methods = ['message'];
		_methods.forEach(function (method) {
			it("should throw an error if params to " + method + "() is not an object", function () {
				var _scenarioOptions = {
						workSpaceId: 1,
						id: 1
				};

				var wScenario = new WScenario(_scenarioOptions, clientConfig);

				expect(function () { wScenario[method](); }).to.throw(method + ' should be an object');
				wScenario = null;
			});
		});
	});

	it("should POST /scenarios and receive `201` for valid scenario structure", function () {
		var _scenarioOptions = {
				workSpaceId: 1
		};
		var _scenarioProperties = {
				name: 'Scenario' + _rand,
				description: 'Description for Scenario' + _rand,
				message: {
					to: _randContact, 
					subject: 'message subject',
					body: 'message body'
				}
		};
		var _201Response = {
				statusCode: 201,
				headers: {
					location: 'https://api.whispir.com/workspaces/1/scenarios/1?apikey=apiKey'
				}
		};

		var wScenario = new WScenario(_scenarioOptions, clientConfig);

		//using nock to mock the request
		nock('https://api.whispir.com', {
			reqheaders: {
				'Accept': 'application/vnd.whispir.scenario-v1+json',
				'Content-Type': 'application/vnd.whispir.scenario-v1+json'
			}
		})
		.post('/workspaces/1/scenarios')
		.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_201Response.statusCode, '', _201Response.headers);

		return wScenario
		.name(_scenarioProperties.name)
		.description(_scenarioProperties.description)
		.message(_scenarioProperties.message)
		.create().then(function (success) {
			expect(success.statusCode).to.equal(_201Response.statusCode);
			expect(success.location).to.equal(_201Response.headers.location);
		}, function (fail) {
			throw Error('this is supposed to pass with a 201');
		});
	});
	it("should POST /scenarios and receive `401` for valid Scenario structure but invalid Auth credentials", function () {
		var _clientConfig = _.clone(clientConfig);
		_clientConfig.auth = { user: 'username', pass: 'invalidPassword' };
		var _scenarioOptions = {
				workSpaceId: 1
		};
		var _scenarioProperties = {
				name: 'Scenario' + _rand,
				description: 'Description for Scenario' + _rand,
				message: {
					to: _randContact, 
					subject: 'message subject',
					body: 'message body'
				}
		};
		var _401Response = {
				statusCode: 401,
				body: {
					"links": null,
					"errorSummary": "Your username and password combination was incorrect. Please check your authentication details and try again",
					"errorText": "Unauthorized",
					"errorDetail": null
				}
		};

		var wScenario = new WScenario(_scenarioOptions, _clientConfig);

		//using nock to mock the request
		nock('https://api.whispir.com', {
			reqheaders: {
				'Accept': 'application/vnd.whispir.scenario-v1+json',
				'Content-Type': 'application/vnd.whispir.scenario-v1+json'
			}
		})
		.post('/workspaces/1/scenarios')
		.basicAuth(_clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_401Response.statusCode, _401Response.body);

		return wScenario
		.create().then(function (itShouldNotBeHere) {
			throw Error('this is supposed to fail with a 401');
		}, function (fail) {
			expect(fail.statusCode).to.equal(_401Response.statusCode);
			expect(fail.body).to.be.an('object');
		});
	});
	it("should POST /scenarios and receive `422` if there a mandatory property missing in the scenario structure", function () {
		var _scenarioProperties = {
				workSpaceId: 1,
				description: 'Description for Scenario' + _rand,
				message: {
					to: _randContact, 
					subject: 'message subject',
					body: 'message body'
				}
		};
		var _422Response = {
				statusCode: 422,
				body: {
					"errorSummary": "Your request did not contain all of the information required to perform this method. Please check your request for the required fields to be passed in and try again. Alternatively, contact your administrator or support@whispir.com for more information",
					"errorText": "name is Mandatory \n",
					"link": []
				}
		};

		var wScenario = new WScenario(_scenarioProperties, clientConfig);

		//using nock to mock the request
		nock('https://api.whispir.com', {
			reqheaders: {
				'Accept': 'application/vnd.whispir.scenario-v1+json',
				'Content-Type': 'application/vnd.whispir.scenario-v1+json'
			}
		})
		.post('/workspaces/1/scenarios')
		.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_422Response.statusCode, _422Response.body);

		return wScenario.create()
		.then(function (itShouldNotBeHere) {
			throw Error('this is supposed to fail with a 422');
		}, function (fail) {
			expect(fail.statusCode).to.equal(_422Response.statusCode);
			expect(fail.body).to.be.an('object');
		});
	});
	it("should POST /scenarios and receive `201` for valid scenario structure in default workspace when no workspace Id is provided", function () {

		var _scenarioProperties = {
				name: 'Scenario' + _rand,
				description: 'Description for Scenario' + _rand,
				message: {
					to: _randContact, 
					subject: 'message subject',
					body: 'message body'
				}
		};
		var _201Response = {
				statusCode: 201,
				headers: {
					location: 'https`://api.whispir.com/scenarios/1?apikey=apiKey'
				},
				body: {}
		};

		var wScenario = new WScenario(_scenarioProperties, clientConfig);

		//using nock to mock the request
		nock('https://api.whispir.com', {
			reqheaders: {
				'Accept': 'application/vnd.whispir.scenario-v1+json',
				'Content-Type': 'application/vnd.whispir.scenario-v1+json'
			}
		})
		.post('/scenarios')
		.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_201Response.statusCode, _201Response.body, _201Response.headers);

		return wScenario
		.create()
		.then(function (success) {
			expect(success.statusCode).to.equal(_201Response.statusCode);
			expect(success.location).to.equal(_201Response.headers.location);
			expect(success.body).to.be.an("object");
		}, function (fail) {
			throw Error('this is supposed to pass with a 201');
		});
	});
	it("should allow to send raw JSON directly via create(rawJSON) - POST /scenarios and receive `201` for valid scenario structure", function () {

		var _scenarioProperties = {
				name: 'Scenario' + _rand,
				description: 'Description for Scenario' + _rand,
				message: {
					to: _randContact, 
					subject: 'message subject',
					body: 'message body'
				}
		};
		var _201Response = {
				statusCode: 201,
				headers: {
					location: 'https://api.whispir.com/scenarios/1?apikey=apiKey'
				},
				body: {}
		};

		var wScenario = new WScenario({}, clientConfig);

		//using nock to mock the request
		nock('https://api.whispir.com', {
			reqheaders: {
				'Accept': 'application/vnd.whispir.scenario-v1+json',
				'Content-Type': 'application/vnd.whispir.scenario-v1+json'
			}
		})
		.post('/scenarios')
		.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_201Response.statusCode, _201Response.body, _201Response.headers);

		return wScenario.create(JSON.stringify(_scenarioProperties))
		.then(function (success) {
			expect(success.statusCode).to.equal(_201Response.statusCode);
			expect(success.location).to.equal(_201Response.headers.location);
			expect(success.body).to.be.an("object");
		}, function (fail) {
			throw Error('this is supposed to pass with a 201');
		});
	});
	describe('.retrieve()', function () {
		it("should throw Error when no ScenarioID is passed", function () {
			var wScenario = new WScenario({}, clientConfig);
			expect(function () { wScenario.retrieve(); }).to.throw('Scenario id is not provided. You should provide one like this \n\t - wClient.Scenario().id(1) \n\t - wClient.Scenario({id: 1})');
		});
		it("should get Scenario data for a given scenario with Id and status 200", function () {

			var _scenarioProperties = {
					id: 1
			};
			var _200Response = {
					statusCode: 200,
					body: {}
			};

			var wScenario = new WScenario(_scenarioProperties, clientConfig);

			//using nock to mock the request
			nock(clientConfig.host, {
				reqheaders: {
					'Accept': 'application/vnd.whispir.scenario-v1+json'
				}
			})
			.get('/scenarios/1')
			.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
			.reply(_200Response.statusCode);

			return wScenario.retrieve()
			.then(function (success) {
				expect(success.statusCode).to.equal(_200Response.statusCode);
			}, function (fail) {
				throw Error('this is supposed to pass with a 200');
			});
		});
	});
	describe('.update()', function () {
		it("should throw Error when no ScenarioID is passed", function () {
			var wScenario = new WScenario({}, clientConfig);
			expect(function () { wScenario.update(); }).to.throw('Scenario id is not provided. You should provide one like this \n\t - wClient.Scenario().id(1) \n\t - wClient.Scenario({id: 1})');
		});
		it("should update Scenario data for a given scenario with Id and status 204", function () {

			var _scenarioProperties = {
					id: 1,
					name: 'Scenario' + _rand,
					description: 'Description for Scenario' + _rand,
					message: {
						to: _randContact, 
						subject: 'message subject new',
						body: 'message body new'
					}
			};
			var _204Response = {
					statusCode: 204
			};

			var wScenario = new WScenario(_scenarioProperties, clientConfig);

			//using nock to mock the request
			nock(clientConfig.host, {
				reqheaders: {
					'Accept': 'application/vnd.whispir.scenario-v1+json'
				}
			})
			.put('/scenarios/1')
			.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
			.reply(_204Response.statusCode);

			return wScenario.update()
			.then(function (success) {
				expect(success.statusCode).to.equal(_204Response.statusCode);
			}, function (fail) {
				throw Error('this is supposed to pass with a 204');
			});
		});
	});
	describe('.delete()', function () {
		it("should throw Error when no ScenarioID is passed", function () {
			var wScenario = new WScenario({}, clientConfig);
			expect(function () { wScenario.delete(); }).to.throw('Scenario id is not provided. You should provide one like this \n\t - wClient.Scenario().id(1) \n\t - wClient.Scenario({id: 1})');
		});
		it("should delete Scenario data for a given scenario with Id and status 204", function () {

			var _scenarioProperties = {
					id: 1
			};
			var _204Response = {
					statusCode: 204
			};

			var wScenario = new WScenario(_scenarioProperties, clientConfig);

			//using nock to mock the request
			nock(clientConfig.host, {
				reqheaders: {
					'Accept': 'application/vnd.whispir.scenario-v1+json'
				}
			})
			.delete('/scenarios/1')
			.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
			.reply(_204Response.statusCode);

			return wScenario.delete()
			.then(function (success) {
				expect(success.statusCode).to.equal(_204Response.statusCode);
			}, function (fail) {
				throw Error('this is supposed to pass with a 204');
			});
		});
	});

});