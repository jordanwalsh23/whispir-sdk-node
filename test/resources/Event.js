/* global describe */
/* global beforeEach */
/* global it */

'use strict';
var chai = require("chai");
var chaiAsPromised = require("chai-as-promised");
var expect = require("chai").expect;
var WEvent = require("../../lib/resources/Event.js");
var nock = require('nock');
var _ = require('underscore');

chai.use(chaiAsPromised);

var clientConfig, _rand;

describe('resources/Event', function () {
	beforeEach(function () {
		_rand = Math.random().toString().split(".")[1];
		clientConfig = {
				host: 'https://api.whispir.com',
				auth: { user: 'username', pass: 'password' },
				apiKey: 'apiKey',
				workSpaceId: 0,
				headers: {
					'X-Originating-SDK': 'nodejs-v.1.0.0'
				}
		};
	});
	it("sets an empty event structure when no options is sent", function () {
		var wEventWithNoOptions = new WEvent("", clientConfig);
		expect(wEventWithNoOptions.toJsonString()).to.equal('{}');
		wEventWithNoOptions = null;
	});
	it("sets a event structure exactly as in passed options", function () {
		var _eventOptions = {
				workSpaceId: 1,
				eventLabel : _rand + ' Outage of local systems in Data Centre',
				status : 'Active',
				eventFormList : []
		};
		var wEventWithOptions = new WEvent(_eventOptions, clientConfig);
		var wEventWithNoOptions = new WEvent("", clientConfig);

		expect(wEventWithOptions.get('workSpaceId')).to.equal(_eventOptions.workSpaceId);
		expect(wEventWithOptions.get('eventLabel')).to.equal(_eventOptions.eventLabel);
		expect(wEventWithOptions.get('status')).to.equal(_eventOptions.status);
		expect(wEventWithOptions.get('eventFormList')).to.equal(_eventOptions.eventFormList);
		expect(wEventWithOptions).to.not.equal(wEventWithNoOptions);

		wEventWithOptions = null;
	});
	it("should support method chaining and options", function () {
		var _eventOptions = {
				workSpaceId: 1,
				id: 1
		};
		var _eventProperties = {
				eventLabel : _rand + ' Outage of local systems in Data Centre',
				status : 'Active',
				eventFormList : []
		};
		var wEventWithOptionsAtInitAndChainProperties = new WEvent(_eventOptions, clientConfig);

		wEventWithOptionsAtInitAndChainProperties
		.eventLabel(_eventProperties.eventLabel)
		.status(_eventProperties.status)
		.eventFormList(_eventProperties.eventFormList);

		expect(wEventWithOptionsAtInitAndChainProperties.get('workSpaceId')).to.equal(_eventOptions.workSpaceId);
		expect(wEventWithOptionsAtInitAndChainProperties.get('id')).to.equal(_eventOptions.id);
		expect(wEventWithOptionsAtInitAndChainProperties.get('eventLabel')).to.equal(_eventProperties.eventLabel);
		expect(wEventWithOptionsAtInitAndChainProperties.get('status')).to.equal(_eventProperties.status);
		expect(wEventWithOptionsAtInitAndChainProperties.get('eventFormList')).to.equal(_eventProperties.eventFormList);
		wEventWithOptionsAtInitAndChainProperties = null;
	});
	it("should POST /events and receive `201` for valid event structure", function () {
		var _eventOptions = {
				workSpaceId: 1
		};
		var _eventProperties = {
				eventLabel : _rand + ' Outage of local systems in Data Centre',
				status : 'Active',
				eventFormList : []
		};
		var _201Response = {
				statusCode: 201,
				headers: {
					location: 'https://api.whispir.com/workspaces/1/events/1?apikey=apiKey'
				},
				body: {}
		};

		var wEvent = new WEvent(_eventOptions, clientConfig);

		// using nock to mock the request
		nock(clientConfig.host, {
			reqheaders: {
				'Accept': 'application/vnd.whispir.event-v1+json',
				'Content-Type': 'application/vnd.whispir.event-v1+json'
			}
		})
		.post('/workspaces/1/events')
		.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_201Response.statusCode, _201Response.body, _201Response.headers);

		return wEvent
		.eventLabel(_eventProperties.eventLabel)
		.status(_eventProperties.status)
		.eventFormList(_eventProperties.eventFormList)
		.create().then(function (success) {
			expect(success.statusCode).to.equal(_201Response.statusCode);
			expect(success.location).to.equal(_201Response.headers.location);
			expect(success.id).to.equal('1');
		}, function (fail) {
			throw Error('this is supposed to pass with a 201');
		});
	});
	it("should POST /events and receive `401` for valid Event structure but invalid Auth credentials", function () {
		var _clientConfig = _.clone(clientConfig);
		_clientConfig.auth = { user: 'username', pass: 'invalidPassword' };
		var _eventOptions = {
				workSpaceId: 1
		};
		var _401Response = {
				statusCode: 401,
				body: {
					"links": null,
					"errorSummary": "Your username and password combination was incorrect. Please check your authentication details and try again",
					"errorText": "Unauthorized",
					"errorDetail": null
				}
		};

		var wEvent = new WEvent(_eventOptions, _clientConfig);

		// using nock to mock the request
		nock(clientConfig.host, {
			reqheaders: {
				'Accept': 'application/vnd.whispir.event-v1+json',
				'Content-Type': 'application/vnd.whispir.event-v1+json'
			}
		})
		.post('/workspaces/1/events')
		.basicAuth(_clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_401Response.statusCode, _401Response.body);

		return wEvent
		.create().then(function (itShouldNotBeHere) {
			throw Error('this is supposed to fail with a 401');
		}, function (fail) {
			expect(fail.statusCode).to.equal(_401Response.statusCode);
			expect(fail.body).to.be.an('object');
		});
	});
	it("should POST /events and receive `422` if there a mandatory property missing in the message structure", function () {
		var _eventProperties = {
				workSpaceId: 1,
				status : 'Active',
				eventFormList : []
		};
		var _422Response = {
				statusCode: 422,
				body: {
					"errorSummary": "Your request did not contain all of the information required to perform this method. Please check your request for the required fields to be passed in and try again. Alternatively, event your administrator or support@whispir.com for more information",
					"errorText": "eventLabel is Mandatory \n",
					"link": []
				}
		};

		var wEvent = new WEvent(_eventProperties, clientConfig);

		// using nock to mock the request
		nock(clientConfig.host, {
			reqheaders: {
				'Accept': 'application/vnd.whispir.event-v1+json',
				'Content-Type': 'application/vnd.whispir.event-v1+json'
			}
		})
		.post('/workspaces/1/events')
		.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_422Response.statusCode, _422Response.body);

		return wEvent.create()
		.then(function (itShouldNotBeHere) {
			throw Error('this is supposed to fail with a 422');
		}, function (fail) {
			expect(fail.statusCode).to.equal(_422Response.statusCode);
			expect(fail.body).to.be.an('object');
		});
	});
	it("should POST /events and receive `201` for valid event structure in default workspace when no workspace Id is provided", function () {

		var _eventProperties = {
				eventLabel : _rand + ' Outage of local systems in Data Centre',
				status : 'Active',
				eventFormList : []
		};
		var _201Response = {
				statusCode: 201,
				headers: {
					location: 'https://api.whispir.com/events/1?apikey=apiKey'
				},
				body: {}
		};

		var wEvent = new WEvent(_eventProperties, clientConfig);

		// using nock to mock the request
		nock(clientConfig.host, {
			reqheaders: {
				'Accept': 'application/vnd.whispir.event-v1+json',
				'Content-Type': 'application/vnd.whispir.event-v1+json'
			}
		})
		.post('/events')
		.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_201Response.statusCode, _201Response.body, _201Response.headers);

		return wEvent
		.create()
		.then(function (success) {
			expect(success.statusCode).to.equal(_201Response.statusCode);
			expect(success.location).to.equal(_201Response.headers.location);
			expect(success.id).to.equal('1');
			expect(success.body).to.be.an("object");
		}, function (fail) {
			throw Error('this is supposed to pass with a 201');
		});
	});
	it("should allow to send raw JSON directly via create(rawJSON) - POST /events and receive `201` for valid event structure", function () {

		var _eventProperties = {
				eventLabel : _rand + ' Outage of local systems in Data Centre',
				status : 'Active',
				eventFormList : []
		};
		var _201Response = {
				statusCode: 201,
				headers: {
					location: 'https://api.whispir.com/events/1?apikey=apiKey'
				},
				body: {}
		};

		var wEvent = new WEvent({}, clientConfig);

		// using nock to mock the request
		nock(clientConfig.host, {
			reqheaders: {
				'Accept': 'application/vnd.whispir.event-v1+json',
				'Content-Type': 'application/vnd.whispir.event-v1+json'
			}
		})
		.post('/events')
		.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_201Response.statusCode, _201Response.body, _201Response.headers);

		return wEvent.create(JSON.stringify(_eventProperties))
		.then(function (success) {
			expect(success.statusCode).to.equal(_201Response.statusCode);
			expect(success.location).to.equal(_201Response.headers.location);
			expect(success.id).to.equal('1');
			expect(success.body).to.be.an("object");
		}, function (fail) {
			throw Error('this is supposed to pass with a 201');
		});
	});
	describe('.retrieve()', function () {
		it("should throw Error when no EventID is passed", function () {
			var wEvent = new WEvent({}, clientConfig);
			expect(function () { wEvent.retrieve(); }).to.throw('Event id is not provided. You should provide one like this \n\t - wClient.Event().id(1) \n\t - wClient.Event({id: 1})');
		});
		it("should get Event data for a given event with Id and status 200", function () {

			var _eventProperties = {
					id: 1
			};
			var _200Response = {
					statusCode: 200,
					body: {}
			};

			var wEvent = new WEvent(_eventProperties, clientConfig);

			// using nock to mock the request
			nock(clientConfig.host, {
				reqheaders: {
					'Accept': 'application/vnd.whispir.event-v1+json'
				}
			})
			.get('/events/1')
			.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey})
			.reply(_200Response.statusCode);

			return wEvent.retrieve()
			.then(function (success) {
				expect(success.statusCode).to.equal(_200Response.statusCode);
			}, function (fail) {
				throw Error('this is supposed to pass with a 200');
			});
		});
	});
	describe('.update()', function () {
		it("should throw Error when no EventID is passed", function () {
			var wEvent = new WEvent({}, clientConfig);
			expect(function () { wEvent.update(); }).to.throw('Event id is not provided. You should provide one like this \n\t - wClient.Event().id(1) \n\t - wClient.Event({id: 1})');
		});
		it("should update Event data for a given event with Id and status 204", function () {

			var _eventProperties = {
					id: 1,
					eventLabel : _rand + ' Outage of local systems in Data Centre',
					status : 'Active',
					eventFormList : []
			};
			var _204Response = {
					statusCode: 204
			};

			var wEvent = new WEvent(_eventProperties, clientConfig);

			// using nock to mock the request
			nock(clientConfig.host, {
				reqheaders: {
					'Accept': 'application/vnd.whispir.event-v1+json'
				}
			})
			.put('/events/1')
			.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
			.reply(_204Response.statusCode);

			return wEvent.update()
			.then(function (success) {
				expect(success.statusCode).to.equal(_204Response.statusCode);
			}, function (fail) {
				throw Error('this is supposed to pass with a 204');
			});
		});
	});
	describe('.delete()', function () {
		it("should throw Error when no EventID is passed", function () {
			var wEvent = new WEvent({}, clientConfig);
			expect(function () { wEvent.delete(); }).to.throw('Event id is not provided. You should provide one like this \n\t - wClient.Event().id(1) \n\t - wClient.Event({id: 1})');
		});
		it("should delete Event data for a given event with Id and status 204", function () {

			var _eventProperties = {
					id: 1
			};
			var _204Response = {
					statusCode: 204
			};

			var wEvent = new WEvent(_eventProperties, clientConfig);

			// using nock to mock the request
			nock(clientConfig.host, {
				reqheaders: {
					'Accept': 'application/vnd.whispir.event-v1+json'
				}
			})
			.delete('/events/1')
			.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
			.reply(_204Response.statusCode);

			return wEvent.delete()
			.then(function (success) {
				expect(success.statusCode).to.equal(_204Response.statusCode);
			}, function (fail) {
				throw Error('this is supposed to pass with a 204');
			});
		});
	});

});