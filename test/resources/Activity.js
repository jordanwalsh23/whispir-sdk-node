/* global describe */
/* global beforeEach */
/* global it */

'use strict';
var chai = require("chai");
var chaiAsPromised = require("chai-as-promised");
var expect = require("chai").expect;
var WActivity = require("../../lib/resources/Activity.js");
var nock = require('nock');
var _ = require('underscore');

chai.use(chaiAsPromised);

//nock.recorder.rec();
//nock.enableNetConnect();

var clientConfig, _rand;

describe('resources/Activity', function () {
	beforeEach(function () {
		_rand = Math.random().toString().split(".")[1];
		clientConfig = {
				host: 'https://api.whispir.com',
				auth: { user: 'username', pass: 'password' },
				apiKey: 'apiKey',
				workSpaceId: 0,
				headers: {
					'X-Originating-SDK': 'nodejs-v.1.0.0'
				}
		};
	});
	it("sets an empty activity structure when no options is sent", function () {
		var wActivityWithNoOptions = new WActivity("", clientConfig);
		expect(wActivityWithNoOptions.toJsonString()).to.equal('{}');
		wActivityWithNoOptions = null;
	});
	it("sets a activity structure exactly as in passed options", function () {
		var _activityOptions = {
				workSpaceId: 1,
				module: 'Message',
				action: 'Send',
				status: 'Successful',
				description: 'Message sent via the Whispir NodeJS SDK'
		};
		var wActivityWithOptions = new WActivity(_activityOptions, clientConfig);
		var wActivityWithNoOptions = new WActivity("", clientConfig);

		expect(wActivityWithOptions.get('workSpaceId')).to.equal(_activityOptions.workSpaceId);
		expect(wActivityWithOptions.get('module')).to.equal(_activityOptions.module);
		expect(wActivityWithOptions.get('action')).to.equal(_activityOptions.action);
		expect(wActivityWithOptions.get('status')).to.equal(_activityOptions.status);
		expect(wActivityWithOptions.get('description')).to.equal(_activityOptions.description);
		expect(wActivityWithOptions).to.not.equal(wActivityWithNoOptions);

		wActivityWithOptions = null;
	});
	it("should support method chaining and options", function () {
		var _activityOptions = {
				workSpaceId: 1,
				id: 1
		};
		var _activityProperties = {
				module: 'Message',
				action: 'Send',
				status: 'Successful',
				description: 'Message sent via the Whispir NodeJS SDK'
		};
		var wActivityWithOptionsAtInitAndChainProperties = new WActivity(_activityOptions, clientConfig);

		wActivityWithOptionsAtInitAndChainProperties
		.action(_activityProperties.action)
		.module(_activityProperties.module)
		.status(_activityProperties.status)
		.description(_activityProperties.description);

		expect(wActivityWithOptionsAtInitAndChainProperties.get('workSpaceId')).to.equal(_activityOptions.workSpaceId);
		expect(wActivityWithOptionsAtInitAndChainProperties.get('id')).to.equal(_activityOptions.id);
		expect(wActivityWithOptionsAtInitAndChainProperties.get('module')).to.equal(_activityProperties.module);
		expect(wActivityWithOptionsAtInitAndChainProperties.get('action')).to.equal(_activityProperties.action);
		expect(wActivityWithOptionsAtInitAndChainProperties.get('status')).to.equal(_activityProperties.status);
		expect(wActivityWithOptionsAtInitAndChainProperties.get('description')).to.equal(_activityProperties.description);
		wActivityWithOptionsAtInitAndChainProperties = null;
	});
	it("should not add an `invalid property` to Activity structure -send via options", function () {
		var _activityOptions = {
				workSpaceId: 1,
				id: 1,
				invalidProperty: 'somevalue'
		};

		var wActivityWithOptionsAndInvalidProperty = new WActivity(_activityOptions, clientConfig);

		expect(wActivityWithOptionsAndInvalidProperty.get('workSpaceId')).to.equal(_activityOptions.workSpaceId);
		expect(wActivityWithOptionsAndInvalidProperty.get('id')).to.equal(_activityOptions.id);
		expect(function() {wActivityWithOptionsAndInvalidProperty.get('invalidProperty');}).to.throw('invalidProperty is undefined in Activity Object');
		wActivityWithOptionsAndInvalidProperty = null;
	});
	it("should throw an error if invalid actions are passed to action() method", function () {
		var _activityOptions = {
				workSpaceId: 1,
				id: 1
		};

		var wActivityActions = new WActivity(_activityOptions, clientConfig);

		expect(wActivityActions.action('Create').get('action')).to.equal('Create');
		expect(function() {wActivityActions.action('invalidAction');}).to.throw('invalidAction is invalid. Valid actions are Create,Update,Move,Copy,Draft,Send,Modified,Delete,Contact Import File,Login,Approve,Reject,Dispatch,Register,Accept,Closed,Map,Un-map');
		wActivityActions = null;
	});
	it("should throw an error if invalid modules are passed to module() method", function () {
		var _activityOptions = {
				workSpaceId: 1,
				id: 1
		};

		var wActivityModules = new WActivity(_activityOptions, clientConfig);

		expect(wActivityModules.module('Message').get('module')).to.equal('Message');
		expect(function() {wActivityModules.module('Massage');}).to.throw('Massage is invalid. Valid modules are System,Message,Scheduled Message,User,Contact,DistributionList,Template,Workspace,Event,WebService,Settings,Conversation,Gateway,Workspace Mapping,Folders,Team,RSS,API Mapping,Asset,Instruction');
		wActivityModules = null;
	});
	it("should POST /activities and receive `201` for valid activity structure", function () {
		var _activityOptions = {
				workSpaceId: 1,
				module: 'Message',
				action: 'Send',
				status: 'Successful',
				description: 'Message sent via the Whispir NodeJS SDK'
		};
		var _201Response = {
				statusCode: 201,
				headers: {
					location: 'https://api.whispir.com/workspaces/1/activities/1?apikey=apiKey'
				},
				body: {}
		};

		var wActivity = new WActivity(_activityOptions, clientConfig);

		// using nock to mock the request
		nock(clientConfig.host, {
			reqheaders: {
				'Accept': 'application/vnd.whispir.activity-v1+json',
				'Content-Type': 'application/vnd.whispir.activity-v1+json'
			}
		})
		.post('/workspaces/1/activities')
		.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_201Response.statusCode, _201Response.body, _201Response.headers);

		return wActivity
		.create().then(function (success) {
			expect(success.statusCode).to.equal(_201Response.statusCode);
			expect(success.location).to.equal(_201Response.headers.location);
			expect(success.body).to.be.an('object');
		}, function (fail) {
			throw Error('this is supposed to pass with a 201');
		});
	});
	it("should POST /activities and receive `401` for valid activity structure but invalid Auth credentials", function () {
		var _clientConfig = _.clone(clientConfig);
		_clientConfig.auth = { user: 'username', pass: 'invalidPassword' };

		var _activityOptions = {
				workSpaceId: 1
		};
		var _401Response = {
				statusCode: 401,
				body: {
					"links": null,
					"errorSummary": "Your username and password combination was incorrect. Please check your authentication details and try again",
					"errorText": "Unauthorized",
					"errorDetail": null
				}
		};

		var wActivity = new WActivity(_activityOptions, _clientConfig);

		// using nock to mock the request
		nock(clientConfig.host, {
			reqheaders: {
				'Accept': 'application/vnd.whispir.activity-v1+json',
				'Content-Type': 'application/vnd.whispir.activity-v1+json'
			}
		})
		.post('/workspaces/1/activities')
		.basicAuth(_clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_401Response.statusCode, _401Response.body);

		return wActivity
		.create().then(function (itShouldNotBeHere) {
			throw Error('this is supposed to fail with a 401');
		}, function (fail) {
			expect(fail.statusCode).to.equal(_401Response.statusCode);
			expect(fail.body).to.be.an('object');
		});
	});
	it("should POST /activities and receive `422` if there a mandatory property missing in the activity structure", function () {
		var _activityProperties = {
				workSpaceId: 1,
				action: 'Send',
				status: 'Successful',
				description: 'Message sent via the Whispir NodeJS SDK'
		};
		var _422Response = {
				statusCode: 422,
				body: {
					"errorSummary": "Your request did not contain all of the information required to perform this method. Please check your request for the required fields to be passed in and try again. Alternatively, activity your administrator or support@whispir.com for more information",
					"errorText": "module is Mandatory \n",
					"link": []
				}
		};

		var wActivity = new WActivity(_activityProperties, clientConfig);

		// using nock to mock the request
		nock(clientConfig.host, {
			reqheaders: {
				'Accept': 'application/vnd.whispir.activity-v1+json',
				'Content-Type': 'application/vnd.whispir.activity-v1+json'
			}
		})
		.post('/workspaces/1/activities')
		.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_422Response.statusCode, _422Response.body);

		return wActivity.create()
		.then(function (itShouldNotBeHere) {
			throw Error('this is supposed to fail with a 422');
		}, function (fail) {
			expect(fail.statusCode).to.equal(_422Response.statusCode);
			expect(fail.body).to.be.an('object');
		});
	});
	it("should POST /activities and receive `201` for valid activity structure in default workspace when no workspace Id is provided", function () {

		var _activityProperties = {
				module: 'Message',
				action: 'Send',
				status: 'Successful',
				description: 'Message sent via the Whispir NodeJS SDK'
		};
		var _201Response = {
				statusCode: 201,
				headers: {
					location: 'https://api.whispir.com/activities/1?apikey=apiKey'
				},
				body: {}
		};

		var wActivity = new WActivity(_activityProperties, clientConfig);

		// using nock to mock the request
		nock(clientConfig.host, {
			reqheaders: {
				'Accept': 'application/vnd.whispir.activity-v1+json',
				'Content-Type': 'application/vnd.whispir.activity-v1+json'
			}
		})
		.post('/activities')
		.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_201Response.statusCode, _201Response.body, _201Response.headers);

		return wActivity
		.create()
		.then(function (success) {
			expect(success.statusCode).to.equal(_201Response.statusCode);
			expect(success.location).to.equal(_201Response.headers.location);
			expect(success.body).to.be.an("object");
		}, function (fail) {
			throw Error('this is supposed to pass with a 201');
		});
	});
	it("should allow to send raw JSON directly via create(rawJSON) - POST /activities and receive `201` for valid activity structure", function () {

		var _activityProperties = {
				module: 'Message',
				action: 'Send',
				status: 'Successful',
				description: 'Message sent via the Whispir NodeJS SDK'
		};
		var _201Response = {
				statusCode: 201,
				headers: {
					location: 'https://api.whispir.com/activities/1?apikey=apiKey'
				},
				body: {}
		};

		var wActivity = new WActivity({}, clientConfig);

		// using nock to mock the request
		nock(clientConfig.host, {
			reqheaders: {
				'Accept': 'application/vnd.whispir.activity-v1+json',
				'Content-Type': 'application/vnd.whispir.activity-v1+json'
			}
		})
		.post('/activities')
		.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_201Response.statusCode, _201Response.body, _201Response.headers);

		return wActivity.create(JSON.stringify(_activityProperties))
		.then(function (success) {
			expect(success.statusCode).to.equal(_201Response.statusCode);
			expect(success.location).to.equal(_201Response.headers.location);
			expect(success.body).to.be.an("object");
		}, function (fail) {
			throw Error('this is supposed to pass with a 201');
		});
	});
	it("should not respect the workSpaceId when passed in via raw JSON directly to create(rawJSON) - POST /activities and receive `201` for valid activity structure", function () {

		var _activityProperties = {
				workSpaceId: 1,
				module: 'Message',
				action: 'Send',
				status: 'Successful',
				description: 'Message sent via the Whispir NodeJS SDK'
		};
		var _201Response = {
				statusCode: 201,
				headers: {
					location: 'https://api.whispir.com/activities/1?apikey=apiKey'
				},
				body: {}
		};

		var wActivity = new WActivity({}, clientConfig);

		// using nock to mock the request
		nock(clientConfig.host, {
			reqheaders: {
				'Accept': 'application/vnd.whispir.activity-v1+json',
				'Content-Type': 'application/vnd.whispir.activity-v1+json'
			}
		})
		.post('/activities')
		.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_201Response.statusCode, _201Response.body, _201Response.headers);

		return wActivity.create(JSON.stringify(_activityProperties))
		.then(function (success) {
			expect(success.statusCode).to.equal(_201Response.statusCode);
			expect(success.location).to.equal(_201Response.headers.location);
			expect(success.location.indexOf('workspaces')).to.equal(-1);
			expect(success.body).to.be.an("object");
		}, function (fail) {
			throw Error('this is supposed to pass with a 201');
		});
	});
	describe('.retrieve()', function () {
		it("should throw Error when no ActivityID is passed", function () {
			var wActivity = new WActivity({}, clientConfig);
			expect(function () { wActivity.retrieve(); }).to.throw('Activity id is not provided. You should provide one like this \n\t - wClient.Activity().id(1) \n\t - wClient.Activity({id: 1})');
		});
		it("should get Activity data for a given activity with Id and status 200", function () {

			var _activityProperties = {
					id: 1
			};
			var _200Response = {
					statusCode: 200,
					body: {}
			};

			var wActivity = new WActivity(_activityProperties, clientConfig);

			// using nock to mock the request
			nock(clientConfig.host, {
				reqheaders: {
					'Accept': 'application/vnd.whispir.activity-v1+json'
				}
			})
			.get('/activities/1')
			.basicAuth(clientConfig.auth)
			.query({ apikey: clientConfig.apiKey })
			.reply(_200Response.statusCode, _200Response.body);

			return wActivity.retrieve()
			.then(function (success) {
				expect(success.statusCode).to.equal(_200Response.statusCode);
				expect(success.body).to.be.an("object");
			}, function (fail) {
				throw Error('this is supposed to pass with a 200');
			});
		});
	});
	describe('.search()', function () {
		it("should search and get Activity data with passed in params and status 200", function () {

			var _search = 'module=Message&action=Send';
			var _200Response = {
					statusCode: 200,
					body: {}
			};

			var wActivity = new WActivity({}, clientConfig);

			// using nock to mock the request
			nock(clientConfig.host, {
				reqheaders: {
					'Accept': 'application/vnd.whispir.activity-v1+json'
				}
			})
			.get('/activities')
			.basicAuth(clientConfig.auth)
			.query({ apikey: clientConfig.apiKey, module: 'Message', action: 'Send'  })
			.reply(_200Response.statusCode, _200Response.body);

			return wActivity.search(_search)
			.then(function (success) {
				expect(success.statusCode).to.equal(_200Response.statusCode);
			}, function (fail) {
				throw Error('this is supposed to pass with a 200');
			});
		});
		it("should not add anything to search query when no params are passed, but still get a status 200", function () {

			var _200Response = {
					statusCode: 200,
					body: {}
			};

			var wActivity = new WActivity({}, clientConfig);

			// using nock to mock the request
			nock(clientConfig.host, {
				reqheaders: {
					'Accept': 'application/vnd.whispir.activity-v1+json'
				}
			})
			.get('/activities')
			.basicAuth(clientConfig.auth)
			.query({ apikey: clientConfig.apiKey })
			.reply(_200Response.statusCode, _200Response);

			return wActivity.search()
			.then(function (success) {
				expect(success.statusCode).to.equal(_200Response.statusCode);
			}, function (fail) {
				throw Error('this is supposed to pass with a 200');
			});
		});
		it("should get a 404 when no activity data is found", function () {

			var _search = 'module=Message&action=Send';
			var _404Response = {
					statusCode: 404,
					body: ''
			};

			var wActivity = new WActivity({}, clientConfig);

			// using nock to mock the request
			nock(clientConfig.host, {
				reqheaders: {
					'Accept': 'application/vnd.whispir.activity-v1+json'
				}
			})
			.get('/activities')
			.basicAuth(clientConfig.auth)
			.query({ apikey: clientConfig.apiKey })
			.reply(_404Response.statusCode, _404Response.body);

			return wActivity.search()
			.then(function (success) {
				expect(success.statusCode).to.equal(_404Response.statusCode);
				expect(success.body).to.equal("");
			}, function (fail) {
				throw Error('this is supposed to pass with a 404');
			});
		});
	});
	describe('.update()', function () {
		it("should throw Error when update is called - Update is not allowed for Activity", function () {
			var wActivity = new WActivity({}, clientConfig);
			expect(function () { wActivity.update(); }).to.throw('Updating an Activity is not supported');
		});
	});
	describe('.delete()', function () {
		it("should throw Error when delete is called - Delete is not allowed for Activity", function () {
			var wActivity = new WActivity({}, clientConfig);
			expect(function () { wActivity.delete(); }).to.throw('Deleting an Activity is not supported');
		});
	});

});