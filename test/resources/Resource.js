/* global describe */
/* global beforeEach */
/* global it */

'use strict';
var chai = require("chai");
var chaiAsPromised = require("chai-as-promised");
var expect = require("chai").expect;
var WResource = require("../../lib/resources/Resource.js");
var nock = require('nock');
var _ = require('underscore');
var mock = require('mock-fs');

chai.use(chaiAsPromised);

var clientConfig, _rand;

describe('resources/Resource', function () {
	beforeEach(function () {
		_rand = Math.random().toString().split(".")[1];
		clientConfig = {
				host: 'https://api.whispir.com',
				auth: { user: 'username', pass: 'password' },
				apiKey: 'apiKey',
				workSpaceId: 0,
				headers: {
					'X-Originating-SDK': 'nodejs-v.1.0.0'
				}
		};
	});
	afterEach(function (){
		// restore the fs module after the test runs
		mock.restore();
	});
	it("sets an empty resource structure when no options is sent", function () {
		var wResourceWithNoOptions = new WResource("", clientConfig);
		expect(wResourceWithNoOptions.toJsonString()).to.equal('{}');
		wResourceWithNoOptions = null;
	});
	it("sets a resource structure exactly as in passed options", function () {
		var _resourceOptions = {
				workSpaceId: 1,
				name : 'sample-file-'+ _rand +'.csv',
				scope : 'private',
				mimeType : 'text/csv',
				derefUri : '...base64 representation...'
		};
		var wResourceWithOptions = new WResource(_resourceOptions, clientConfig);
		var wResourceWithNoOptions = new WResource("", clientConfig);

		expect(wResourceWithOptions.get('workSpaceId')).to.equal(_resourceOptions.workSpaceId);
		expect(wResourceWithOptions.get('name')).to.equal(_resourceOptions.name);
		expect(wResourceWithOptions.get('scope')).to.equal(_resourceOptions.scope);
		expect(wResourceWithOptions.get('mimeType')).to.equal(_resourceOptions.mimeType);
		expect(wResourceWithOptions.get('derefUri')).to.equal(_resourceOptions.derefUri);
		expect(wResourceWithOptions).to.not.equal(wResourceWithNoOptions);

		wResourceWithOptions = null;
	});
	it("should support method chaining and options", function () {
		var _resourceOptions = {
				workSpaceId: 1,
				id: 1
		};
		var _resourceProperties = {
				name : 'sample-file-'+ _rand +'.csv',
				scope : 'private',
				mimeType : 'text/csv',
				derefUri : '...base64 representation...'
		};
		var wResourceWithOptionsAtInitAndChainProperties = new WResource(_resourceOptions, clientConfig);

		wResourceWithOptionsAtInitAndChainProperties
		.name(_resourceProperties.name)
		.scope(_resourceProperties.scope)
		.mimeType(_resourceProperties.mimeType)
		.base64Content(_resourceProperties.derefUri);

		expect(wResourceWithOptionsAtInitAndChainProperties.get('workSpaceId')).to.equal(_resourceOptions.workSpaceId);
		expect(wResourceWithOptionsAtInitAndChainProperties.get('id')).to.equal(_resourceOptions.id);
		expect(wResourceWithOptionsAtInitAndChainProperties.get('name')).to.equal(_resourceProperties.name);
		expect(wResourceWithOptionsAtInitAndChainProperties.get('scope')).to.equal(_resourceProperties.scope);
		expect(wResourceWithOptionsAtInitAndChainProperties.get('mimeType')).to.equal(_resourceProperties.mimeType);
		expect(wResourceWithOptionsAtInitAndChainProperties.get('derefUri')).to.equal(_resourceProperties.derefUri);
		wResourceWithOptionsAtInitAndChainProperties = null;
	});
	it('should only support private and public as scope types', function() {
		var wResourceWithOptions = new WResource("", clientConfig);
		expect(function() {wResourceWithOptions.scope('protected');}).to.throw('protected is invalid scope. Valid ones are public,private');
		wResourceWithOptions.scope('private');
		expect(wResourceWithOptions.get('scope')).to.equal('private');
		wResourceWithOptions.scope('public');
		expect(wResourceWithOptions.get('scope')).to.equal('public');
	});
	it('should read a file with readFileSync() method and populate the data into derefUri variable', function () {
		var _resourceOptions = {
				workSpaceId: 1,
				id: 1
		};
		var _resourceProperties = {
				name : 'sample-file-'+ _rand +'.csv',
				scope : 'private',
				mimeType : 'text/csv',
				derefUri : 'ZmlsZSxjb250ZW50LGlzLGEsY3N2'
		};
		var wResourceWithFileSyncRead = new WResource(_resourceOptions, clientConfig);

		var _mockFile = {};
		_mockFile[_resourceProperties.name] = 'file,content,is,a,csv';

		mock(_mockFile);

		wResourceWithFileSyncRead
		.name(_resourceProperties.name)
		.scope(_resourceProperties.scope)
		.mimeType(_resourceProperties.mimeType)
		.readFileSync(_resourceProperties.name);

		expect(wResourceWithFileSyncRead.get('workSpaceId')).to.equal(_resourceOptions.workSpaceId);
		expect(wResourceWithFileSyncRead.get('id')).to.equal(_resourceOptions.id);
		expect(wResourceWithFileSyncRead.get('name')).to.equal(_resourceProperties.name);
		expect(wResourceWithFileSyncRead.get('scope')).to.equal(_resourceProperties.scope);
		expect(wResourceWithFileSyncRead.get('mimeType')).to.equal(_resourceProperties.mimeType);
		expect(wResourceWithFileSyncRead.get('derefUri')).to.equal(_resourceProperties.derefUri);
		wResourceWithFileSyncRead = null;
	});
	it("should POST /resources and receive `201` for valid resource structure", function () {
		var _resourceOptions = {
				workSpaceId: 1
		};
		var _resourceProperties = {
				name : 'sample-file-'+ _rand +'.csv',
				scope : 'public',
				mimeType : 'text/csv',
				derefUri: 'ZmlsZSxjb250ZW50LGlzLGEsY3N2LGZpbGU='
		};
		var _201Response = {
				statusCode: 201,
				headers: {
					location: 'https://api.whispir.com/workspaces/1/resources/1?apikey=apiKey'
				},
				body: {
					url: 'https://cdn-ap.whispir.com/public/resources/2163b29d4edf1bd77d71a36210d472360cd.csv'
				}
		};
		/*
		var _mockFile = {};
		_mockFile[_resourceProperties.name] = 'file,content,is,a,csv,file';

		mock(_mockFile);
		 */
		var wResource = new WResource(_resourceOptions, clientConfig);

		// using nock to mock the request
		nock(clientConfig.host, {
			reqheaders: {
				'Accept': 'application/vnd.whispir.resource-v1+json',
				'Content-Type': 'application/vnd.whispir.resource-v1+json'
			}
		})
		.post('/workspaces/1/resources')
		.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_201Response.statusCode, _201Response.body, _201Response.headers);

		return wResource
		.name(_resourceProperties.name)
		.scope(_resourceProperties.scope)
		.mimeType(_resourceProperties.mimeType)
		.base64Content(_resourceProperties.derefUri)
		.create().then(function (success) {
			expect(success.statusCode).to.equal(_201Response.statusCode);
			expect(success.location).to.equal(_201Response.headers.location);
			expect(success.body.url).to.equal(_201Response.body.url);
		}, function (fail) {
			throw Error('this is supposed to pass with a 201');
		});
	});
	it("should POST /resources and receive `401` for valid Resource structure but invalid Auth credentials", function () {
		var _clientConfig = _.clone(clientConfig);
		_clientConfig.auth = { user: 'username', pass: 'invalidPassword' };
		var _resourceOptions = {
				workSpaceId: 1
		};
		var _401Response = {
				statusCode: 401,
				body: {
					"links": null,
					"errorSummary": "Your username and password combination was incorrect. Please check your authentication details and try again",
					"errorText": "Unauthorized",
					"errorDetail": null
				}
		};

		var wResource = new WResource(_resourceOptions, _clientConfig);

		// using nock to mock the request
		nock(clientConfig.host, {
			reqheaders: {
				'Accept': 'application/vnd.whispir.resource-v1+json',
				'Content-Type': 'application/vnd.whispir.resource-v1+json'
			}
		})
		.post('/workspaces/1/resources')
		.basicAuth(_clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_401Response.statusCode, _401Response.body);

		return wResource
		.create().then(function (itShouldNotBeHere) {
			throw Error('this is supposed to fail with a 401');
		}, function (fail) {
			expect(fail.statusCode).to.equal(_401Response.statusCode);
			expect(fail.body).to.be.an('object');
		});
	});
	it("should POST /resources and receive `422` if there a mandatory property missing in the message structure", function () {
		var _resourceProperties = {
				workSpaceId: 1,
				scope : 'private',
				mimeType : 'text/csv',
				derefUri : '...base64 representation...'
		};
		var _422Response = {
				statusCode: 422,
				body: {
					"errorSummary": "Your request did not contain all of the information required to perform this method. Please check your request for the required fields to be passed in and try again. Alternatively, resource your administrator or support@whispir.com for more information",
					"errorText": "name is Mandatory \n",
					"link": []
				}
		};

		var wResource = new WResource(_resourceProperties, clientConfig);

		// using nock to mock the request
		nock(clientConfig.host, {
			reqheaders: {
				'Accept': 'application/vnd.whispir.resource-v1+json',
				'Content-Type': 'application/vnd.whispir.resource-v1+json'
			}
		})
		.post('/workspaces/1/resources')
		.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_422Response.statusCode, _422Response.body);

		return wResource.create()
		.then(function (itShouldNotBeHere) {
			throw Error('this is supposed to fail with a 422');
		}, function (fail) {
			expect(fail.statusCode).to.equal(_422Response.statusCode);
			expect(fail.body).to.be.an('object');
		});
	});
	it("should POST /resources and receive `201` for valid resource structure in default workspace when no workspace Id is provided", function () {

		var _resourceProperties = {
				name : 'sample-file-'+ _rand +'.csv',
				scope : 'private',
				mimeType : 'text/csv',
				derefUri : '...base64 representation...'
		};
		var _201Response = {
				statusCode: 201,
				headers: {
					location: 'https://api.whispir.com/resources/1?apikey=apiKey'
				},
				body: {}
		};

		var wResource = new WResource(_resourceProperties, clientConfig);

		// using nock to mock the request
		nock(clientConfig.host, {
			reqheaders: {
				'Accept': 'application/vnd.whispir.resource-v1+json',
				'Content-Type': 'application/vnd.whispir.resource-v1+json'
			}
		})
		.post('/resources')
		.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_201Response.statusCode, _201Response.body, _201Response.headers);

		return wResource
		.create()
		.then(function (success) {
			expect(success.statusCode).to.equal(_201Response.statusCode);
			expect(success.location).to.equal(_201Response.headers.location);
			expect(success.mri).to.equal(_201Response.body.mri);
			expect(success.body).to.be.an("object");
		}, function (fail) {
			throw Error('this is supposed to pass with a 201');
		});
	});
	it("should allow to send raw JSON directly via create(rawJSON) - POST /resources and receive `201` for valid resource structure", function () {

		var _resourceProperties = {
				name : 'sample-file-'+ _rand +'.csv',
				scope : 'private',
				mimeType : 'text/csv',
				derefUri : '...base64 representation...'
		};
		var _201Response = {
				statusCode: 201,
				headers: {
					location: 'https://api.whispir.com/resources/1?apikey=apiKey'
				},
				body: {}
		};

		var wResource = new WResource({}, clientConfig);

		// using nock to mock the request
		nock(clientConfig.host, {
			reqheaders: {
				'Accept': 'application/vnd.whispir.resource-v1+json',
				'Content-Type': 'application/vnd.whispir.resource-v1+json'
			}
		})
		.post('/resources')
		.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_201Response.statusCode, _201Response.body, _201Response.headers);

		return wResource.create(JSON.stringify(_resourceProperties))
		.then(function (success) {
			expect(success.statusCode).to.equal(_201Response.statusCode);
			expect(success.location).to.equal(_201Response.headers.location);
			expect(success.mri).to.equal(_201Response.body.mri);
			expect(success.body).to.be.an("object");
		}, function (fail) {
			throw Error('this is supposed to pass with a 201');
		});
	});
	describe('.retrieve()', function () {
		it("should throw Error when no ResourceID is passed", function () {
			var wResource = new WResource({}, clientConfig);
			expect(function () { wResource.retrieve(); }).to.throw('Resource id is not provided. You should provide one like this \n\t - wClient.Resource().id(1) \n\t - wClient.Resource({id: 1})');
		});
		it("should get Resource data for a given resource with Id and status 200", function () {

			var _resourceProperties = {
					id: 1
			};
			var _200Response = {
					statusCode: 200,
					body: {}
			};

			var wResource = new WResource(_resourceProperties, clientConfig);

			// using nock to mock the request
			nock(clientConfig.host, {
				reqheaders: {
					'Accept': 'application/vnd.whispir.resource-v1+json'
				}
			})
			.get('/resources/1')
			.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey})
			.reply(_200Response.statusCode);

			return wResource.retrieve()
			.then(function (success) {
				expect(success.statusCode).to.equal(_200Response.statusCode);
			}, function (fail) {
				throw Error('this is supposed to pass with a 200');
			});
		});
	});
	describe('.update()', function () {
		it("should throw Error when no ResourceID is passed", function () {
			var wResource = new WResource({}, clientConfig);
			expect(function () { wResource.update(); }).to.throw('Resource id is not provided. You should provide one like this \n\t - wClient.Resource().id(1) \n\t - wClient.Resource({id: 1})');
		});
		it("should update Resource data for a given resource with Id and status 204", function () {

			var _resourceProperties = {
					id: 1,
					name : 'sample-file-'+ _rand +'.csv',
					scope : 'private',
					mimeType : 'text/csv',
					derefUri : '...base64 representation...'
			};
			var _204Response = {
					statusCode: 204
			};

			var wResource = new WResource(_resourceProperties, clientConfig);

			// using nock to mock the request
			nock(clientConfig.host, {
				reqheaders: {
					'Accept': 'application/vnd.whispir.resource-v1+json',
					'Content-Type': 'application/vnd.whispir.resource-v1+json'
				}
			})
			.put('/resources/1')
			.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
			.reply(_204Response.statusCode);

			return wResource.update()
			.then(function (success) {
				expect(success.statusCode).to.equal(_204Response.statusCode);
			}, function (fail) {
				throw Error('this is supposed to pass with a 204');
			});
		});
	});
	describe('.delete()', function () {
		it("should throw Error when no ResourceID is passed", function () {
			var wResource = new WResource({}, clientConfig);
			expect(function () { wResource.delete(); }).to.throw('Resource id is not provided. You should provide one like this \n\t - wClient.Resource().id(1) \n\t - wClient.Resource({id: 1})');
		});
		it("should delete Resource data for a given resource with Id and status 204", function () {

			var _resourceProperties = {
					id: 1
			};
			var _204Response = {
					statusCode: 204
			};

			var wResource = new WResource(_resourceProperties, clientConfig);

			// using nock to mock the request
			nock(clientConfig.host, {
				reqheaders: {
					'Accept': 'application/vnd.whispir.resource-v1+json'
				}
			})
			.delete('/resources/1')
			.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
			.reply(_204Response.statusCode);

			return wResource.delete()
			.then(function (success) {
				expect(success.statusCode).to.equal(_204Response.statusCode);
			}, function (fail) {
				throw Error('this is supposed to pass with a 204');
			});
		});
	});

});