/* global describe */
/* global beforeEach */
/* global it */

'use strict';
var chai = require("chai");
var chaiAsPromised = require("chai-as-promised");
var expect = require("chai").expect;
var WDistributionList = require("../../lib/resources/DistributionList.js");
var nock = require('nock');
var _ = require('underscore');

chai.use(chaiAsPromised);

var clientConfig, _rand;

describe('resources/DistributionList', function () {
	beforeEach(function () {
		_rand = Math.random().toString().split(".")[1];
		clientConfig = {
				host: 'https://api.whispir.com',
				auth: { user: 'username', pass: 'password' },
				apiKey: 'apiKey',
				workSpaceId: 0,
				headers: {
					'X-Originating-SDK': 'nodejs-v.1.0.0'
				}
		};
	});
	it("sets an empty distributionlist structure when no options is sent", function () {
		var wDistributionListWithNoOptions = new WDistributionList("", clientConfig);
		expect(wDistributionListWithNoOptions.toJsonString()).to.equal('{}');
		wDistributionListWithNoOptions = null;
	});
	it("sets a distributionlist structure exactly as in passed options", function () {
		var _distributionlistOptions = {
				workSpaceId: 1,
				name: 'DL_' + _rand,
				description: 'A test dl created - ' + _rand,
				access: 'Open',
				visibility: 'Public',
				contactIds: '',	
				userIds: '',
				distListIds: ''
		};
		var wDistributionListWithOptions = new WDistributionList(_distributionlistOptions, clientConfig);
		var wDistributionListWithNoOptions = new WDistributionList("", clientConfig);

		expect(wDistributionListWithOptions.get('workSpaceId')).to.equal(_distributionlistOptions.workSpaceId);
		expect(wDistributionListWithOptions.get('name')).to.equal(_distributionlistOptions.name);
		expect(wDistributionListWithOptions.get('description')).to.equal(_distributionlistOptions.description);
		expect(wDistributionListWithOptions.get('access')).to.equal(_distributionlistOptions.access);
		expect(wDistributionListWithOptions.get('visibility')).to.equal(_distributionlistOptions.visibility);
		expect(wDistributionListWithOptions.get('contactIds')).to.equal(_distributionlistOptions.contactIds);
		expect(wDistributionListWithOptions.get('userIds')).to.equal(_distributionlistOptions.userIds);
		expect(wDistributionListWithOptions.get('distListIds')).to.equal(_distributionlistOptions.distListIds);
		expect(wDistributionListWithOptions).to.not.equal(wDistributionListWithNoOptions);

		wDistributionListWithOptions = null;
	});
	it("should support method chaining and options", function () {
		var _distributionlistOptions = {
				workSpaceId: 1,
				id: 1
		};
		var _distributionlistProperties = {
				name: 'DL_' + _rand,
				description: 'A test dl created - ' + _rand,
				access: 'Open',
				visibility: 'Public',
				contactIds: 'CB4558257DD86D09,AF48A9EC3F02E43C',	
				userIds: 'E49ED9EC343CF02E,C727BCE3A813E2B1',
				distListIds: 'CF5AF1AE49ED07A6,9FF7C2B470CCEC1E'
		};
		var wDistributionListWithOptionsAtInitAndChainProperties = new WDistributionList(_distributionlistOptions, clientConfig);

		wDistributionListWithOptionsAtInitAndChainProperties
		.set('name', _distributionlistProperties.name)
		.description(_distributionlistProperties.description)
		.access(_distributionlistProperties.access)
		.visibility(_distributionlistProperties.visibility)
		.addMember('contact',_distributionlistProperties.contactIds)
		.addMember('user',_distributionlistProperties.userIds)
		.set('distListIds',_distributionlistProperties.distListIds);

		expect(wDistributionListWithOptionsAtInitAndChainProperties.get('workSpaceId')).to.equal(_distributionlistOptions.workSpaceId);
		expect(wDistributionListWithOptionsAtInitAndChainProperties.get('id')).to.equal(_distributionlistOptions.id);
		expect(wDistributionListWithOptionsAtInitAndChainProperties.get('name')).to.equal(_distributionlistProperties.name);
		expect(wDistributionListWithOptionsAtInitAndChainProperties.get('description')).to.equal(_distributionlistProperties.description);
		expect(wDistributionListWithOptionsAtInitAndChainProperties.get('access')).to.equal(_distributionlistProperties.access);
		expect(wDistributionListWithOptionsAtInitAndChainProperties.get('visibility')).to.equal(_distributionlistProperties.visibility);
		expect(wDistributionListWithOptionsAtInitAndChainProperties.get('contactIds')).to.equal(_distributionlistProperties.contactIds);
		expect(wDistributionListWithOptionsAtInitAndChainProperties.get('userIds')).to.equal(_distributionlistProperties.userIds);
		expect(wDistributionListWithOptionsAtInitAndChainProperties.get('distListIds')).to.equal(_distributionlistProperties.distListIds);
		wDistributionListWithOptionsAtInitAndChainProperties = null;
	});
	it("should support object via set() method and set() chaining and options", function () {
		var _distributionlistOptions = {
				workSpaceId: 1,
				id: 1
		};
		var _distributionlistProperties = {
				name: 'DL_' + _rand,
				description: 'A test dl created - ' + _rand,
				access: 'Open',
				visibility: 'Public',
				contactIds: 'CB4558257DD86D09,AF48A9EC3F02E43C',	
				userIds: 'E49ED9EC343CF02E,C727BCE3A813E2B1',
				distListIds: 'CF5AF1AE49ED07A6,9FF7C2B470CCEC1E'
		};
		var _mri = 'Docs_Distribution_List_x.Singapore_Metrocity@list.company.whispir.sg';
		var wDistributionListWithOptionsAtInitAndChainProperties = new WDistributionList(_distributionlistOptions, clientConfig);

		wDistributionListWithOptionsAtInitAndChainProperties
		.set(_distributionlistProperties)
		.set('mri', _mri);

		expect(wDistributionListWithOptionsAtInitAndChainProperties.get('workSpaceId')).to.equal(_distributionlistOptions.workSpaceId);
		expect(wDistributionListWithOptionsAtInitAndChainProperties.get('id')).to.equal(_distributionlistOptions.id);
		expect(wDistributionListWithOptionsAtInitAndChainProperties.get('name')).to.equal(_distributionlistProperties.name);
		expect(wDistributionListWithOptionsAtInitAndChainProperties.get('description')).to.equal(_distributionlistProperties.description);
		expect(wDistributionListWithOptionsAtInitAndChainProperties.get('access')).to.equal(_distributionlistProperties.access);
		expect(wDistributionListWithOptionsAtInitAndChainProperties.get('visibility')).to.equal(_distributionlistProperties.visibility);
		expect(wDistributionListWithOptionsAtInitAndChainProperties.get('contactIds')).to.equal(_distributionlistProperties.contactIds);
		expect(wDistributionListWithOptionsAtInitAndChainProperties.get('userIds')).to.equal(_distributionlistProperties.userIds);
		expect(wDistributionListWithOptionsAtInitAndChainProperties.get('distListIds')).to.equal(_distributionlistProperties.distListIds);
		expect(wDistributionListWithOptionsAtInitAndChainProperties.get('mri')).to.equal(_mri);
		wDistributionListWithOptionsAtInitAndChainProperties = null;
	});
	it("should not let workspaceId be set via the set() method", function () {
		var _distributionlistOptions = {
				workSpaceId: 1,
				id: 1
		};
		var _newWorkSpace = 2;
		var wDistributionListTryingToAddWorkSpaceIdViaSet = new WDistributionList(_distributionlistOptions, clientConfig);

		expect(function(){wDistributionListTryingToAddWorkSpaceIdViaSet.set('workSpaceId', _newWorkSpace);}).to.throw('workSpaceId cannot be set using this method. It should only be sent in while creating the DistributionList instance. Ex: `wClient.DistributionList({workSpaceId: \'' + _newWorkSpace + '\', ...})`');

		wDistributionListTryingToAddWorkSpaceIdViaSet = null;
	});
	it("should de-dupe contactIds, userIds, distListIds ONLY when sent via addMember/addMembers methods", function () {
		var _distributionlistOptions = {
				workSpaceId: 1,
				id: 1
		};
		var _distributionlistProperties = {
				name: 'DL_' + _rand,
				description: 'A test dl created - ' + _rand,
				access: 'Open',
				visibility: 'Public',
				contactIds: 'CB4558257DD86D09,AF48A9EC3F02E43C',	
				userIds: 'E49ED9EC343CF02E,C727BCE3A813E2B1',
				distListIds: 'CF5AF1AE49ED07A6,9FF7C2B470CCEC1E',
				mri: 'Docs_Distribution_List_x.Singapore_Metrocity@list.company.whispir.sg'
		};

		var _duplContactIds = _distributionlistProperties.contactIds + ',CB4558257DD86D09';
		var _duplDistListIds = _distributionlistProperties.distListIds + ',CF5AF1AE49ED07A6';

		var wDistributionListWithOptionsAtInitAndChainProperties = new WDistributionList(_distributionlistOptions, clientConfig);

		wDistributionListWithOptionsAtInitAndChainProperties
		.set('name', _distributionlistProperties.name)
		.set('mri', _distributionlistProperties.mri)
		.description(_distributionlistProperties.description)
		.access(_distributionlistProperties.access)
		.visibility(_distributionlistProperties.visibility)
		.addMembers('contact', _duplContactIds)
		.addMembers('user', _distributionlistProperties.userIds)
		.addMember('user', 'E49ED9EC343CF02E')
		.set('distListIds',_duplDistListIds);

		expect(wDistributionListWithOptionsAtInitAndChainProperties.get('workSpaceId')).to.equal(_distributionlistOptions.workSpaceId);
		expect(wDistributionListWithOptionsAtInitAndChainProperties.get('id')).to.equal(_distributionlistOptions.id);
		expect(wDistributionListWithOptionsAtInitAndChainProperties.get('name')).to.equal(_distributionlistProperties.name);
		expect(wDistributionListWithOptionsAtInitAndChainProperties.get('description')).to.equal(_distributionlistProperties.description);
		expect(wDistributionListWithOptionsAtInitAndChainProperties.get('access')).to.equal(_distributionlistProperties.access);
		expect(wDistributionListWithOptionsAtInitAndChainProperties.get('visibility')).to.equal(_distributionlistProperties.visibility);
		expect(wDistributionListWithOptionsAtInitAndChainProperties.get('contactIds')).to.equal(_distributionlistProperties.contactIds);
		expect(wDistributionListWithOptionsAtInitAndChainProperties.get('userIds')).to.equal(_distributionlistProperties.userIds);
		expect(wDistributionListWithOptionsAtInitAndChainProperties.get('distListIds')).to.not.equal(_distributionlistProperties.distListIds);
		expect(wDistributionListWithOptionsAtInitAndChainProperties.get('distListIds')).to.equal(_duplDistListIds);
		expect(wDistributionListWithOptionsAtInitAndChainProperties.get('mri')).to.equal(_distributionlistProperties.mri);
		wDistributionListWithOptionsAtInitAndChainProperties = null;
	});
	it("should add via addMember/addMembers(type, value) and remove via removeMember/removeMembers(type, value) for contactIds, userIds, distListIds", function () {
		var _distributionlistOptions = {
				workSpaceId: 1,
				id: 1
		};
		var _distributionlistProperties = {
				name: 'DL_' + _rand,
				description: 'A test dl created - ' + _rand,
				access: 'Open',
				visibility: 'Public',
				contactIds: 'CB4558257DD86D09,AF48A9EC3F02E43C',	
				userIds: 'E49ED9EC343CF02E,C727BCE3A813E2B1,AF48A9EC3F02E43C',
				distListIds: 'CF5AF1AE49ED07A6,9FF7C2B470CCEC1E',
				mri: 'Docs_Distribution_List_x.Singapore_Metrocity@list.company.whispir.sg'
		};

		var _duplDistListIds = _distributionlistProperties.distListIds + ',CF5AF1AE49ED07A6';

		var wDistributionListWithOptionsAtInitAndChainProperties = new WDistributionList(_distributionlistOptions, clientConfig);

		wDistributionListWithOptionsAtInitAndChainProperties
		.set('name', _distributionlistProperties.name)
		.set('mri', _distributionlistProperties.mri)
		.description(_distributionlistProperties.description)
		.access(_distributionlistProperties.access)
		.visibility(_distributionlistProperties.visibility)
		.addMembers('contact', _distributionlistProperties.contactIds)
		.addMember('contact', 'CB4558257DD86D09')
		.addMembers('user',  _distributionlistProperties.userIds)
		.addMember('user', 'E49ED9EC343CF02E')
		.set('distListIds',_duplDistListIds)
		.removeMember('contact', 'CB4558257DD86D09')
		.removeMembers('user', ['E49ED9EC343CF02E','AF48A9EC3F02E43C'])
		.removeMember('user', 'E49ED9EC343CF02E') /*yes removing it twice; it should not have an effect when the value is absent*/
		.removeMember('dl', 'CF5AF1AE49ED07A6');

		expect(wDistributionListWithOptionsAtInitAndChainProperties.get('workSpaceId')).to.equal(_distributionlistOptions.workSpaceId);
		expect(wDistributionListWithOptionsAtInitAndChainProperties.get('id')).to.equal(_distributionlistOptions.id);
		expect(wDistributionListWithOptionsAtInitAndChainProperties.get('name')).to.equal(_distributionlistProperties.name);
		expect(wDistributionListWithOptionsAtInitAndChainProperties.get('description')).to.equal(_distributionlistProperties.description);
		expect(wDistributionListWithOptionsAtInitAndChainProperties.get('access')).to.equal(_distributionlistProperties.access);
		expect(wDistributionListWithOptionsAtInitAndChainProperties.get('visibility')).to.equal(_distributionlistProperties.visibility);
		expect(wDistributionListWithOptionsAtInitAndChainProperties.get('contactIds')).to.equal('AF48A9EC3F02E43C');
		expect(wDistributionListWithOptionsAtInitAndChainProperties.get('userIds')).to.equal('C727BCE3A813E2B1');
		expect(wDistributionListWithOptionsAtInitAndChainProperties.get('distListIds')).to.equal('9FF7C2B470CCEC1E');
		expect(wDistributionListWithOptionsAtInitAndChainProperties.get('mri')).to.equal(_distributionlistProperties.mri);
		expect(function(){wDistributionListWithOptionsAtInitAndChainProperties.addMember('contact', '');}).to.throw('You did not specify the memberId to add. Do it like this, `wDistributionList.addMember(\'contact\', \'CB4558257DD86D09\')`');
		expect(function(){wDistributionListWithOptionsAtInitAndChainProperties.addMember('contact');}).to.throw('You did not specify the memberId to add. Do it like this, `wDistributionList.addMember(\'contact\', \'CB4558257DD86D09\')`');
		expect(function(){wDistributionListWithOptionsAtInitAndChainProperties.addMember('1234');}).to.throw('You did not specify the memberId to add. Do it like this, `wDistributionList.addMember(\'contact\', \'CB4558257DD86D09\')`');
		expect(function(){wDistributionListWithOptionsAtInitAndChainProperties.removeMember('name', '1234');}).to.throw('Cannot remove a member from `name` type. It is invalid. Valid ones are - contact, user, dl');
		expect(function(){wDistributionListWithOptionsAtInitAndChainProperties.removeMember('1234');}).to.throw('Remove no one? You did not specify whom to remove. Do it like this, `wDistributionList.removeMember(\'contact\', \'CB4558257DD86D09\')`');
		expect(function(){wDistributionListWithOptionsAtInitAndChainProperties.access('super');}).to.throw('super is invalid. Valid access are - Open, ByApproval, Restricted');
		expect(function(){wDistributionListWithOptionsAtInitAndChainProperties.visibility('protected');}).to.throw('protected is invalid. Valid visibility are - Public, Private');
		wDistributionListWithOptionsAtInitAndChainProperties = null;
	});
	it("addMember/addMembers(type, memberId) and remove via removeMember/removeMembers(type, memberId) should take both array and string for memberId part", function () {
		var _distributionlistOptions = {
				workSpaceId: 1,
				id: 1
		};
		var _distributionlistProperties = {
				name: 'DL_' + _rand,
				description: 'A test dl created - ' + _rand,
				access: 'Open',
				visibility: 'Public',
				contactIds: ['CB4558257DD86D09','AF48A9EC3F02E43C'],	
				userIds: ['E49ED9EC343CF02E','C727BCE3A813E2B1','AF48A9EC3F02E43C'],
				distListIds: 'CF5AF1AE49ED07A6,9FF7C2B470CCEC1E',
				mri: 'Docs_Distribution_List_x.Singapore_Metrocity@list.company.whispir.sg'
		};
		
		var _duplDistListIds = _distributionlistProperties.distListIds + ',CF5AF1AE49ED07A6';
		
		var wDistributionListWithOptionsAtInitAndChainProperties = new WDistributionList(_distributionlistOptions, clientConfig);
		
		wDistributionListWithOptionsAtInitAndChainProperties
		.set('name', _distributionlistProperties.name)
		.set('mri', _distributionlistProperties.mri)
		.description(_distributionlistProperties.description)
		.access(_distributionlistProperties.access)
		.visibility(_distributionlistProperties.visibility)
		.addMembers('contact', _distributionlistProperties.contactIds)
		.addMember('contact', 'CB4558257DD86D09')
		.addMembers('user',  _distributionlistProperties.userIds)
		.addMember('user', 'E49ED9EC343CF02E')
		.set('distListIds',_duplDistListIds)
		.removeMember('contact', 'CB4558257DD86D09')
		.removeMembers('user', ['E49ED9EC343CF02E','AF48A9EC3F02E43C'])
		.removeMember('user', 'E49ED9EC343CF02E') /*yes removing it twice; it should not have an effect when the value is absent*/
		.removeMember('dl', 'CF5AF1AE49ED07A6');
		
		expect(wDistributionListWithOptionsAtInitAndChainProperties.get('workSpaceId')).to.equal(_distributionlistOptions.workSpaceId);
		expect(wDistributionListWithOptionsAtInitAndChainProperties.get('id')).to.equal(_distributionlistOptions.id);
		expect(wDistributionListWithOptionsAtInitAndChainProperties.get('name')).to.equal(_distributionlistProperties.name);
		expect(wDistributionListWithOptionsAtInitAndChainProperties.get('description')).to.equal(_distributionlistProperties.description);
		expect(wDistributionListWithOptionsAtInitAndChainProperties.get('access')).to.equal(_distributionlistProperties.access);
		expect(wDistributionListWithOptionsAtInitAndChainProperties.get('visibility')).to.equal(_distributionlistProperties.visibility);
		expect(wDistributionListWithOptionsAtInitAndChainProperties.get('contactIds')).to.equal('AF48A9EC3F02E43C');
		expect(wDistributionListWithOptionsAtInitAndChainProperties.get('userIds')).to.equal('C727BCE3A813E2B1');
		expect(wDistributionListWithOptionsAtInitAndChainProperties.get('distListIds')).to.equal('9FF7C2B470CCEC1E');
		expect(wDistributionListWithOptionsAtInitAndChainProperties.get('mri')).to.equal(_distributionlistProperties.mri);
		wDistributionListWithOptionsAtInitAndChainProperties = null;
	});
	it("should POST /distributionlists and receive `201` for valid distributionlist structure", function () {
		var _distributionlistOptions = {
				workSpaceId: 1
		};
		var _distributionlistProperties = {
				name: 'DL_' + _rand,
				description: 'A test dl created - ' + _rand,
				access: 'Open',
				visibility: 'Public',
				contactIds: 'CB4558257DD86D09,AF48A9EC3F02E43C',	
				userIds: 'E49ED9EC343CF02E,C727BCE3A813E2B1',
				distListIds: 'CF5AF1AE49ED07A6,9FF7C2B470CCEC1E'
		};
		var _201Response = {
				statusCode: 201,
				headers: {
					location: 'https://api.whispir.com/workspaces/1/distributionlists/1?apikey=apiKey'
				},
				body: {
					mri: 'Docs_Distribution_List_x.Singapore_Metrocity@list.company.whispir.sg'
				}
		};

		var wDistributionList = new WDistributionList(_distributionlistOptions, clientConfig);

		// using nock to mock the request
		nock('https://api.whispir.com', {
			reqheaders: {
				'Accept': 'application/vnd.whispir.distributionlist-v1+json',
				'Content-Type': 'application/vnd.whispir.distributionlist-v1+json'
			}
		})
		.post('/workspaces/1/distributionlists')
		.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_201Response.statusCode, _201Response.body, _201Response.headers);

		return wDistributionList
		.set(_distributionlistProperties)
		.create().then(function (success) {
			expect(success.statusCode).to.equal(_201Response.statusCode);
			expect(success.location).to.equal(_201Response.headers.location);
			expect(success.mri).to.equal(_201Response.body.mri);
		}, function (fail) {
			throw Error('this is supposed to pass with a 201');
		});
	});
	it("should POST /distributionlists and receive `401` for valid DL structure but invalid Auth credentials", function () {
		var _clientConfig = _.clone(clientConfig);
		_clientConfig.auth = { user: 'username', pass: 'invalidPassword' };
		var _distributionlistOptions = {
				workSpaceId: 1
		};
		var _401Response = {
				statusCode: 401,
				body: {
					"links": null,
					"errorSummary": "Your username and password combination was incorrect. Please check your authentication details and try again",
					"errorText": "Unauthorized",
					"errorDetail": null
				}
		};

		var wDistributionList = new WDistributionList(_distributionlistOptions, _clientConfig);

		// using nock to mock the request
		nock('https://api.whispir.com', {
			reqheaders: {
				'Accept': 'application/vnd.whispir.distributionlist-v1+json',
				'Content-Type': 'application/vnd.whispir.distributionlist-v1+json'
			}
		})
		.post('/workspaces/1/distributionlists')
		.basicAuth(_clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_401Response.statusCode, _401Response.body);

		return wDistributionList
		.create().then(function (itShouldNotBeHere) {
			throw Error('this is supposed to fail with a 401');
		}, function (fail) {
			expect(fail.statusCode).to.equal(_401Response.statusCode);
			expect(fail.body).to.be.an('object');
		});
	});
	it("should POST /distributionlists and receive `422` if there a mandatory property missing in the DL structure", function () {
		var _distributionlistProperties = {
				workSpaceId: 1,
				description: 'A test dl created - ' + _rand,
				access: 'Open',
				visibility: 'Public',
				contactIds: 'CB4558257DD86D09,AF48A9EC3F02E43C',	
				userIds: 'E49ED9EC343CF02E,C727BCE3A813E2B1',
				distListIds: 'CF5AF1AE49ED07A6,9FF7C2B470CCEC1E'
		};
		var _422Response = {
				statusCode: 422,
				body: {
					"errorSummary": "Your request did not contain all of the information required to perform this method. Please check your request for the required fields to be passed in and try again. Alternatively, distributionlist your administrator or support@whispir.com for more information",
					"errorText": "name is Mandatory \n",
					"link": []
				}
		};

		var wDistributionList = new WDistributionList(_distributionlistProperties, clientConfig);

		// using nock to mock the request
		nock('https://api.whispir.com', {
			reqheaders: {
				'Accept': 'application/vnd.whispir.distributionlist-v1+json',
				'Content-Type': 'application/vnd.whispir.distributionlist-v1+json'
			}
		})
		.post('/workspaces/1/distributionlists')
		.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_422Response.statusCode, _422Response.body);

		return wDistributionList.create()
		.then(function (itShouldNotBeHere) {
			throw Error('this is supposed to fail with a 422');
		}, function (fail) {
			expect(fail.statusCode).to.equal(_422Response.statusCode);
			expect(fail.body).to.be.an('object');
		});
	});
	it("should POST /distributionlists and receive `201` for valid distributionlist structure in default workspace when no workspace Id is provided", function () {

		var _distributionlistProperties = {
				name: 'DL_' + _rand,
				description: 'A test dl created - ' + _rand,
				access: 'Open',
				visibility: 'Public',
				contactIds: 'CB4558257DD86D09,AF48A9EC3F02E43C',	
				userIds: 'E49ED9EC343CF02E,C727BCE3A813E2B1',
				distListIds: 'CF5AF1AE49ED07A6,9FF7C2B470CCEC1E'
		};
		var _201Response = {
				statusCode: 201,
				headers: {
					location: 'https`://api.whispir.com/distributionlists/1?apikey=apiKey'
				},
				body: {
					mri: 'Docs_Distribution_List_x.Singapore_Metrocity@list.company.whispir.sg'
				}
		};

		var wDistributionList = new WDistributionList(_distributionlistProperties, clientConfig);

		// using nock to mock the request
		nock('https://api.whispir.com', {
			reqheaders: {
				'Accept': 'application/vnd.whispir.distributionlist-v1+json',
				'Content-Type': 'application/vnd.whispir.distributionlist-v1+json'
			}
		})
		.post('/distributionlists')
		.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_201Response.statusCode, _201Response.body, _201Response.headers);

		return wDistributionList
		.create()
		.then(function (success) {
			expect(success.statusCode).to.equal(_201Response.statusCode);
			expect(success.location).to.equal(_201Response.headers.location);
			expect(success.mri).to.equal(_201Response.body.mri);
			expect(success.body).to.be.an("object");
		}, function (fail) {
			throw Error('this is supposed to pass with a 201');
		});
	});
	it("should allow to send raw JSON directly via create(rawJSON) - POST /distributionlists and receive `201` for valid DL structure", function () {

		var _distributionlistProperties = {
				name: 'DL_' + _rand,
				description: 'A test dl created - ' + _rand,
				access: 'Open',
				visibility: 'Public',
				contactIds: 'CB4558257DD86D09,AF48A9EC3F02E43C',	
				userIds: 'E49ED9EC343CF02E,C727BCE3A813E2B1',
				distListIds: 'CF5AF1AE49ED07A6,9FF7C2B470CCEC1E'
		};
		var _201Response = {
				statusCode: 201,
				headers: {
					location: 'https://api.whispir.com/distributionlists/1?apikey=apiKey'
				},
				body: {
					mri: 'Docs_Distribution_List_x.Singapore_Metrocity@list.company.whispir.sg'
				}
		};

		var wDistributionList = new WDistributionList({}, clientConfig);

		// using nock to mock the request
		nock('https://api.whispir.com', {
			reqheaders: {
				'Accept': 'application/vnd.whispir.distributionlist-v1+json',
				'Content-Type': 'application/vnd.whispir.distributionlist-v1+json'
			}
		})
		.post('/distributionlists')
		.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_201Response.statusCode, _201Response.body, _201Response.headers);

		return wDistributionList.create(JSON.stringify(_distributionlistProperties))
		.then(function (success) {
			expect(success.statusCode).to.equal(_201Response.statusCode);
			expect(success.location).to.equal(_201Response.headers.location);
			expect(success.mri).to.equal(_201Response.body.mri);
			expect(success.body).to.be.an("object");
		}, function (fail) {
			throw Error('this is supposed to pass with a 201');
		});
	});
	describe('.retrieve()', function () {
		it("should throw Error when no DistributionListID is passed", function () {
			var wDistributionList = new WDistributionList({}, clientConfig);
			expect(function () { wDistributionList.retrieve(); }).to.throw('DistributionList id is not provided. You should provide one like this \n\t - wClient.DistributionList().id(1) \n\t - wClient.DistributionList({id: 1})');
		});
		it("should get DistributionList data for a given distributionlist with Id and status 200", function () {

			var _distributionlistProperties = {
					id: 1
			};
			var _200Response = {
					statusCode: 200,
					body: {}
			};

			var wDistributionList = new WDistributionList(_distributionlistProperties, clientConfig);

			// using nock to mock the request
			nock('https://api.whispir.com', {
				reqheaders: {
					'Accept': 'application/vnd.whispir.distributionlist-v1+json'
				}
			})
			.get('/distributionlists/1')
			.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
			.reply(_200Response.statusCode);

			return wDistributionList.retrieve()
			.then(function (success) {
				expect(success.statusCode).to.equal(_200Response.statusCode);
			}, function (fail) {
				throw Error('this is supposed to pass with a 200');
			});
		});
	});
	describe('.update()', function () {
		it("should throw Error when no DistributionListID is passed", function () {
			var wDistributionList = new WDistributionList({}, clientConfig);
			expect(function () { wDistributionList.update(); }).to.throw('DistributionList id is not provided. You should provide one like this \n\t - wClient.DistributionList().id(1) \n\t - wClient.DistributionList({id: 1})');
		});
		it("should update DistributionList data for a given distributionlist with Id and status 204", function () {

			var _distributionlistProperties = {
					id: 1,
					name: 'DL_' + _rand,
					description: 'A test dl created - ' + _rand,
					access: 'Open',
					visibility: 'Public',
					contactIds: 'CB4558257DD86D09,AF48A9EC3F02E43C',	
					userIds: 'E49ED9EC343CF02E,C727BCE3A813E2B1',
					distListIds: 'CF5AF1AE49ED07A6,9FF7C2B470CCEC1E',
					_mri: 'Docs_Distribution_List_x.Singapore_Metrocity@list.company.whispir.sg'
			};
			var _204Response = {
					statusCode: 204
			};

			var wDistributionList = new WDistributionList(_distributionlistProperties, clientConfig);

			// using nock to mock the request
			nock('https://api.whispir.com', {
				reqheaders: {
					'Accept': 'application/vnd.whispir.distributionlist-v1+json'
				}
			})
			.put('/distributionlists/1')
			.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
			.reply(_204Response.statusCode);

			return wDistributionList.update()
			.then(function (success) {
				expect(success.statusCode).to.equal(_204Response.statusCode);
			}, function (fail) {
				throw Error('this is supposed to pass with a 204');
			});
		});
	});
	describe('.delete()', function () {
		it("should throw Error when no DistributionListID is passed", function () {
			var wDistributionList = new WDistributionList({}, clientConfig);
			expect(function () { wDistributionList.delete(); }).to.throw('DistributionList id is not provided. You should provide one like this \n\t - wClient.DistributionList().id(1) \n\t - wClient.DistributionList({id: 1})');
		});
		it("should delete DistributionList data for a given distributionlist with Id and status 204", function () {

			var _distributionlistProperties = {
					id: 1
			};
			var _204Response = {
					statusCode: 204
			};

			var wDistributionList = new WDistributionList(_distributionlistProperties, clientConfig);

			// using nock to mock the request
			nock('https://api.whispir.com', {
				reqheaders: {
					'Accept': 'application/vnd.whispir.distributionlist-v1+json'
				}
			})
			.delete('/distributionlists/1')
			.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
			.reply(_204Response.statusCode);

			return wDistributionList.delete()
			.then(function (success) {
				expect(success.statusCode).to.equal(_204Response.statusCode);
			}, function (fail) {
				throw Error('this is supposed to pass with a 204');
			});
		});
	});

});