/* global describe */
/* global beforeEach */
/* global it */

'use strict';
var chai = require("chai");
var chaiAsPromised = require("chai-as-promised");
var expect = require("chai").expect;
var WUser = require("../../lib/resources/User.js");
var nock = require('nock');
var _ = require('underscore');

chai.use(chaiAsPromised);

var clientConfig, _rand;

describe('resources/User', function () {
	beforeEach(function () {
		_rand = Math.random().toString().split(".")[1];
		clientConfig = {
				host: 'https://api.whispir.com',
				auth: { user: 'username', pass: 'password' },
				apiKey: 'apiKey',
				workSpaceId: 0,
				headers: {
					'X-Originating-SDK': 'nodejs-v.1.0.0'
				}
		};
	});
	it("sets an empty user structure when no options is sent", function () {
		var wUserWithNoOptions = new WUser("", clientConfig);
		expect(wUserWithNoOptions.toJsonString()).to.equal('{}');
		wUserWithNoOptions = null;
	});
	it("sets a user structure exactly as in passed options", function () {
		var _userOptions = {
				workSpaceId: 1,
				firstName: 'John',
				lastName: 'Wick ' + _rand,
				userName: 'John.Wick' + _rand,
				password: 'AmF10gtx',
				timezone: 'Australia/Melbourne',
				workEmailAddress1: 'jwick' + _rand + '@testcompany.com',
				workMobilePhone1: '61' + _rand,
				workCountry: 'Australia'
		};
		var wUserWithOptions = new WUser(_userOptions, clientConfig);
		var wUserWithNoOptions = new WUser("", clientConfig);

		expect(wUserWithOptions.get('workSpaceId')).to.equal(_userOptions.workSpaceId);
		expect(wUserWithOptions.get('firstName')).to.equal(_userOptions.firstName);
		expect(wUserWithOptions.get('lastName')).to.equal(_userOptions.lastName);
		expect(wUserWithOptions.get('userName')).to.equal(_userOptions.userName);
		expect(wUserWithOptions.get('password')).to.equal(_userOptions.password);
		expect(wUserWithOptions.get('timezone')).to.equal(_userOptions.timezone);
		expect(wUserWithOptions.get('workEmailAddress1')).to.equal(_userOptions.workEmailAddress1);
		expect(wUserWithOptions.get('workMobilePhone1')).to.equal(_userOptions.workMobilePhone1);
		expect(wUserWithOptions.get('workCountry')).to.equal(_userOptions.workCountry);
		expect(wUserWithOptions).to.not.equal(wUserWithNoOptions);

		wUserWithOptions = null;
	});
	it("should support method chaining and options", function () {
		var _userOptions = {
				workSpaceId: 1,
				id: 1
		};
		var _userProperties = {
				firstName: 'John',
				lastName: 'Wick ' + _rand,
				userName: 'John.Wick' + _rand,
				password: 'AmF10gtx',
				timezone: 'Australia/Melbourne',
				workEmailAddress1: 'jwick' + _rand + '@testcompany.com',
				workMobilePhone1: '61' + _rand,
				workCountry: 'Australia'
		};
		var wUserWithOptionsAtInitAndChainProperties = new WUser(_userOptions, clientConfig);

		wUserWithOptionsAtInitAndChainProperties
		.set('firstName', _userProperties.firstName)
		.set('lastName', _userProperties.lastName)
		.set('userName', _userProperties.userName)
		.set('password', _userProperties.password)
		.set('timezone', _userProperties.timezone)
		.set('workEmailAddress1', _userProperties.workEmailAddress1)
		.set('workMobilePhone1', _userProperties.workMobilePhone1)
		.set('workCountry', _userProperties.workCountry);

		expect(wUserWithOptionsAtInitAndChainProperties.get('workSpaceId')).to.equal(_userOptions.workSpaceId);
		expect(wUserWithOptionsAtInitAndChainProperties.get('id')).to.equal(_userOptions.workSpaceId);
		expect(wUserWithOptionsAtInitAndChainProperties.get('firstName')).to.equal(_userProperties.firstName);
		expect(wUserWithOptionsAtInitAndChainProperties.get('lastName')).to.equal(_userProperties.lastName);
		expect(wUserWithOptionsAtInitAndChainProperties.get('userName')).to.equal(_userProperties.userName);
		expect(wUserWithOptionsAtInitAndChainProperties.get('password')).to.equal(_userProperties.password);
		expect(wUserWithOptionsAtInitAndChainProperties.get('timezone')).to.equal(_userProperties.timezone);
		expect(wUserWithOptionsAtInitAndChainProperties.get('workEmailAddress1')).to.equal(_userProperties.workEmailAddress1);
		expect(wUserWithOptionsAtInitAndChainProperties.get('workMobilePhone1')).to.equal(_userProperties.workMobilePhone1);
		expect(wUserWithOptionsAtInitAndChainProperties.get('workCountry')).to.equal(_userProperties.workCountry);
		wUserWithOptionsAtInitAndChainProperties = null;
	});
	it("should support object via set() method and set() chaining and options", function () {
		var _userOptions = {
				workSpaceId: 1,
				id: 1
		};
		var _userProperties = {
				firstName: 'John',
				lastName: 'Wick ' + _rand,
				userName: 'John.Wick' + _rand,
				password: 'AmF10gtx',
				timezone: 'Australia/Melbourne',
				workEmailAddress1: 'jwick' + _rand + '@testcompany.com',
				workMobilePhone1: '61' + _rand,
				workCountry: 'Australia'
		};
		var wUserWithOptionsAtInitAndChainProperties = new WUser(_userOptions, clientConfig);

		wUserWithOptionsAtInitAndChainProperties
		.set(_userProperties)
		.set('workMobilePhone1', '6598765432')
		.set('workCountry', 'Singapore');

		expect(wUserWithOptionsAtInitAndChainProperties.get('workSpaceId')).to.equal(_userOptions.workSpaceId);
		expect(wUserWithOptionsAtInitAndChainProperties.get('id')).to.equal(_userOptions.workSpaceId);
		expect(wUserWithOptionsAtInitAndChainProperties.get('firstName')).to.equal(_userProperties.firstName);
		expect(wUserWithOptionsAtInitAndChainProperties.get('lastName')).to.equal(_userProperties.lastName);
		expect(wUserWithOptionsAtInitAndChainProperties.get('userName')).to.equal(_userProperties.userName);
		expect(wUserWithOptionsAtInitAndChainProperties.get('password')).to.equal(_userProperties.password);
		expect(wUserWithOptionsAtInitAndChainProperties.get('timezone')).to.equal(_userProperties.timezone);
		expect(wUserWithOptionsAtInitAndChainProperties.get('workEmailAddress1')).to.equal(_userProperties.workEmailAddress1);
		expect(wUserWithOptionsAtInitAndChainProperties.get('workMobilePhone1')).to.equal('6598765432');
		expect(wUserWithOptionsAtInitAndChainProperties.get('workCountry')).to.equal('Singapore');
		wUserWithOptionsAtInitAndChainProperties = null;
	});
	it("should not let workspaceId be set via the set() method", function () {
		var _userOptions = {
				workSpaceId: 1,
				id: 1
		};
		var _newWorkSpace = 2;
		var wUserTryingToAddWorkSpaceIdViaSet = new WUser(_userOptions, clientConfig);

		expect(function(){wUserTryingToAddWorkSpaceIdViaSet.set('workSpaceId', 2);}).to.throw('workSpaceId cannot be set using this method. It should only be sent in while creating the User instance. Ex: `wClient.User({workSpaceId: \'' + _newWorkSpace + '\', ...})`');

		wUserTryingToAddWorkSpaceIdViaSet = null;
	});
	it("should POST /users and receive `201` for valid user structure", function () {
		var _userOptions = {
				workSpaceId: 1
		};
		var _userProperties = {
				firstName: 'John',
				lastName: 'Wick ' + _rand,
				userName: 'John.Wick' + _rand,
				password: 'AmF10gtx',
				timezone: 'Australia/Melbourne',
				workEmailAddress1: 'jwick' + _rand + '@testcompany.com',
				workMobilePhone1: '61' + _rand,
				workCountry: 'Australia'
		};
		var _201Response = {
				statusCode: 201,
				headers: {
					location: 'https://api.whispir.com/workspaces/1/users/1?apikey=apiKey'
				}
		};

		var wUser = new WUser(_userOptions, clientConfig);

		// using nock to mock the request
		nock('https://api.whispir.com', {
			reqheaders: {
				'Accept': 'application/vnd.whispir.user-v1+json',
				'Content-Type': 'application/vnd.whispir.user-v1+json'
			}
		})
		.post('/workspaces/1/users')
		.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_201Response.statusCode, '', _201Response.headers);

		return wUser
		.set(_userProperties)
		.create().then(function (success) {
			expect(success.statusCode).to.equal(_201Response.statusCode);
			expect(success.location).to.equal(_201Response.headers.location);
		}, function (fail) {
			throw Error('this is supposed to pass with a 201');
		});
	});
	it("should POST /users and receive `401` for valid User structure but invalid Auth credentials", function () {
		var _clientConfig = _.clone(clientConfig);
		_clientConfig.auth = { user: 'username', pass: 'invalidPassword' };
		
		var _userOptions = {
				workSpaceId: 1
		};
		var _401Response = {
				statusCode: 401,
				body: {
					"links": null,
					"errorSummary": "Your username and password combination was incorrect. Please check your authentication details and try again",
					"errorText": "Unauthorized",
					"errorDetail": null
				}
		};

		var wUser = new WUser(_userOptions, _clientConfig);

		// using nock to mock the request
		nock('https://api.whispir.com', {
			reqheaders: {
				'Accept': 'application/vnd.whispir.user-v1+json',
				'Content-Type': 'application/vnd.whispir.user-v1+json'
			}
		})
		.post('/workspaces/1/users')
		.basicAuth(_clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_401Response.statusCode, _401Response.body);

		return wUser
		.create().then(function (itShouldNotBeHere) {
			throw Error('this is supposed to fail with a 401');
		}, function (fail) {
			expect(fail.statusCode).to.equal(_401Response.statusCode);
			expect(fail.body).to.be.an('object');
		});
	});
	it("should POST /users and receive `422` if there a mandatory property missing in the user structure", function () {
		var _userProperties = {
				workSpaceId: 1,
				lastName: 'Wick ' + _rand,
				userName: 'John.Wick' + _rand,
				password: 'AmF10gtx',
				timezone: 'Australia/Melbourne',
				workEmailAddress1: 'jwick' + _rand + '@testcompany.com',
				workMobilePhone1: '61' + _rand,
				workCountry: 'Australia'
		};
		var _422Response = {
				statusCode: 422,
				body: {
					"errorSummary": "Your request did not contain all of the information required to perform this method. Please check your request for the required fields to be passed in and try again. Alternatively, user your administrator or support@whispir.com for more information",
					"errorText": "firstName is Mandatory \n",
					"link": []
				}
		};

		var wUser = new WUser(_userProperties, clientConfig);

		// using nock to mock the request
		nock('https://api.whispir.com', {
			reqheaders: {
				'Accept': 'application/vnd.whispir.user-v1+json',
				'Content-Type': 'application/vnd.whispir.user-v1+json'
			}
		})
		.post('/workspaces/1/users')
		.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_422Response.statusCode, _422Response.body);

		return wUser.create()
		.then(function (itShouldNotBeHere) {
			throw Error('this is supposed to fail with a 422');
		}, function (fail) {
			expect(fail.statusCode).to.equal(_422Response.statusCode);
			expect(fail.body).to.be.an('object');
		});
	});
	it("should POST /users and receive `201` for valid user structure in default workspace when no workspace Id is provided", function () {

		var _userProperties = {
				firstName: 'John',
				lastName: 'Wick ' + _rand,
				userName: 'John.Wick' + _rand,
				password: 'AmF10gtx',
				timezone: 'Australia/Melbourne',
				workEmailAddress1: 'jwick' + _rand + '@testcompany.com',
				workMobilePhone1: '61' + _rand,
				workCountry: 'Australia'
		};
		var _201Response = {
				statusCode: 201,
				headers: {
					location: 'https`://api.whispir.com/users/1?apikey=apiKey'
				},
				body: {}
		};

		var wUser = new WUser(_userProperties, clientConfig);

		// using nock to mock the request
		nock('https://api.whispir.com', {
			reqheaders: {
				'Accept': 'application/vnd.whispir.user-v1+json',
				'Content-Type': 'application/vnd.whispir.user-v1+json'
			}
		})
		.post('/users')
		.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_201Response.statusCode, _201Response.body, _201Response.headers);

		return wUser
		.create()
		.then(function (success) {
			expect(success.statusCode).to.equal(_201Response.statusCode);
			expect(success.location).to.equal(_201Response.headers.location);
			expect(success.body).to.be.an("object");
		}, function (fail) {
			throw Error('this is supposed to pass with a 201');
		});
	});
	it("should allow to send raw JSON directly via create(rawJSON) - POST /users and receive `201` for valid user structure", function () {

		var _userProperties = {
				firstName: 'John',
				lastName: 'Wick ' + _rand,
				userName: 'John.Wick' + _rand,
				password: 'AmF10gtx',
				timezone: 'Australia/Melbourne',
				workEmailAddress1: 'jwick' + _rand + '@testcompany.com',
				workMobilePhone1: '61' + _rand,
				workCountry: 'Australia'
		};
		var _201Response = {
				statusCode: 201,
				headers: {
					location: 'https://api.whispir.com/users/1?apikey=apiKey'
				},
				body: {}
		};

		var wUser = new WUser({}, clientConfig);

		// using nock to mock the request
		nock('https://api.whispir.com', {
			reqheaders: {
				'Accept': 'application/vnd.whispir.user-v1+json',
				'Content-Type': 'application/vnd.whispir.user-v1+json'
			}
		})
		.post('/users')
		.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_201Response.statusCode, _201Response.body, _201Response.headers);

		return wUser.create(JSON.stringify(_userProperties))
		.then(function (success) {
			expect(success.statusCode).to.equal(_201Response.statusCode);
			expect(success.location).to.equal(_201Response.headers.location);
			expect(success.body).to.be.an("object");
		}, function (fail) {
			throw Error('this is supposed to pass with a 201');
		});
	});
	describe('.retrieve()', function () {
		it("should throw Error when no UserID is passed", function () {
			var wUser = new WUser({}, clientConfig);
			expect(function () { wUser.retrieve(); }).to.throw('User id is not provided. You should provide one like this \n\t - wClient.User().set(\'id\',1) \n\t - wClient.User({id: 1})');
		});
		it("should get User data for a given user with Id and status 200", function () {

			var _userProperties = {
					id: 1
			};
			var _200Response = {
					statusCode: 200,
					body: {}
			};

			var wUser = new WUser(_userProperties, clientConfig);

			// using nock to mock the request
			nock('https://api.whispir.com', {
				reqheaders: {
					'Accept': 'application/vnd.whispir.user-v1+json'
				}
			})
			.get('/users/1')
			.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
			.reply(_200Response.statusCode);

			return wUser.retrieve()
			.then(function (success) {
				expect(success.statusCode).to.equal(_200Response.statusCode);
			}, function (fail) {
				throw Error('this is supposed to pass with a 200');
			});
		});
	});
	describe('.search()', function () {
		it("should search and get User data with passed in params and status 200", function () {

			var _search = 'firstName=John';
			var _200Response = {
					statusCode: 200,
					body: {}
			};

			var wUser = new WUser({}, clientConfig);

			// using nock to mock the request
			nock('https://api.whispir.com', {
				reqheaders: {
					'Accept': 'application/vnd.whispir.user-v1+json'
				}
			})
			.get('/users')
			.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey, firstName: 'John' })
			.reply(_200Response.statusCode);

			return wUser.search(_search)
			.then(function (success) {
				expect(success.statusCode).to.equal(_200Response.statusCode);
			}, function (fail) {
				throw Error('this is supposed to pass with a 200');
			});
		});
		it("should not add anything to search query when no params are passed, but still get a status 200", function () {

			var _200Response = {
					statusCode: 200,
					body: {}
			};

			var wUser = new WUser({}, clientConfig);

			// using nock to mock the request
			nock('https://api.whispir.com', {
				reqheaders: {
					'Accept': 'application/vnd.whispir.user-v1+json'
				}
			})
			.get('/users')
			.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
			.reply(_200Response.statusCode);

			return wUser.search()
			.then(function (success) {
				expect(success.statusCode).to.equal(_200Response.statusCode);
			}, function (fail) {
				throw Error('this is supposed to pass with a 200');
			});
		});
	});
	describe('.update()', function () {
		it("should throw Error when no UserID is passed", function () {
			var wUser = new WUser({}, clientConfig);
			expect(function () { wUser.update(); }).to.throw('User id is not provided. You should provide one like this \n\t - wClient.User().set(\'id\',1) \n\t - wClient.User({id: 1})');
		});
		it("should update User data for a given user with Id and status 204", function () {

			var _userProperties = {
					id: 1,
					firstName: 'John',
					lastName: 'Wick ' + _rand,
					userName: 'John.Wick' + _rand,
					password: 'AmF10gtx',
					timezone: 'Australia/Melbourne',
					workEmailAddress1: 'jwick' + _rand + '@testcompany.com',
					workMobilePhone1: '61' + _rand,
					workCountry: 'Australia'
			};
			var _204Response = {
					statusCode: 204
			};

			var wUser = new WUser(_userProperties, clientConfig);

			// using nock to mock the request
			nock('https://api.whispir.com', {
				reqheaders: {
					'Accept': 'application/vnd.whispir.user-v1+json'
				}
			})
			.put('/users/1')
			.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
			.reply(_204Response.statusCode);

			return wUser.update()
			.then(function (success) {
				expect(success.statusCode).to.equal(_204Response.statusCode);
			}, function (fail) {
				throw Error('this is supposed to pass with a 204');
			});
		});
	});
	describe('.delete()', function () {
		it("should throw Error when no UserID is passed", function () {
			var wUser = new WUser({}, clientConfig);
			expect(function () { wUser.delete(); }).to.throw('User id is not provided. You should provide one like this \n\t - wClient.User().set(\'id\',1) \n\t - wClient.User({id: 1})');
		});
		it("should delete User data for a given user with Id and status 204", function () {

			var _userProperties = {
					id: 1
			};
			var _204Response = {
					statusCode: 204
			};

			var wUser = new WUser(_userProperties, clientConfig);

			// using nock to mock the request
			nock('https://api.whispir.com', {
				reqheaders: {
					'Accept': 'application/vnd.whispir.user-v1+json'
				}
			})
			.delete('/users/1')
			.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
			.reply(_204Response.statusCode);

			return wUser.delete()
			.then(function (success) {
				expect(success.statusCode).to.equal(_204Response.statusCode);
			}, function (fail) {
				throw Error('this is supposed to pass with a 204');
			});
		});
	});

});