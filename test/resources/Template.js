/* global describe */
/* global beforeEach */
/* global it */

'use strict';
var chai = require("chai");
var chaiAsPromised = require("chai-as-promised");
var expect = require("chai").expect;
var WTemplate = require("../../lib/resources/Template.js");
var nock = require('nock');
var _ = require('underscore');

chai.use(chaiAsPromised);

var clientConfig;

describe('resources/Template', function () {
    beforeEach(function () {
        clientConfig = {
            host: 'https://api.whispir.com',
            auth: { user: 'username', pass: 'password' },
            apiKey: 'apiKey',
            workSpaceId: 0,
            headers: {
                'X-Originating-SDK': 'nodejs-v.1.0.0'
            }
        };
    });
    it("sets an empty template structure when no options is sent", function () {
        var wTemplateWithNoOptions = new WTemplate("", clientConfig);
        expect(wTemplateWithNoOptions.toJsonString()).to.equal('{}');
        wTemplateWithNoOptions = null;
    });
    it("sets a template structure exactly as in passed options", function () {
        var _templateOptions = {
            workSpaceId: 1,
            messageTemplateName: 'Sample Template',
            messageTemplateDescription: 'Template Description',
            subject: 'message subject',
            body: 'message body',
            responseTemplateId: 'ABCD'
        };
        var wTemplateWithOptions = new WTemplate(_templateOptions, clientConfig);
        var wTemplateWithNoOptions = new WTemplate("", clientConfig);

        expect(wTemplateWithOptions.get('workSpaceId')).to.equal(_templateOptions.workSpaceId);
        expect(wTemplateWithOptions.get('messageTemplateName')).to.equal(_templateOptions.messageTemplateName);
        expect(wTemplateWithOptions.get('messageTemplateDescription')).to.equal(_templateOptions.messageTemplateDescription);
        expect(wTemplateWithOptions.get('subject')).to.equal(_templateOptions.subject);
        expect(wTemplateWithOptions.get('body')).to.equal(_templateOptions.body);
        expect(wTemplateWithOptions.get('responserule')).to.equal(_templateOptions.responseTemplateId);
        expect(wTemplateWithOptions).to.not.equal(wTemplateWithNoOptions);

        wTemplateWithOptions = null;
    });
    it("should support method chaining and options", function () {
        var _templateOptions = {
            workSpaceId: 1,
            id: 1
        };
        var _templateProperties = {
            name: 'Sample Template',
            description: 'Template Description',
            subject: 'message subject',
            body: 'message body',
            responserule: 'ABCD1234'
        };
        var wTemplateWithOptionsAtInitAndChainProperties = new WTemplate(_templateOptions, clientConfig);

        wTemplateWithOptionsAtInitAndChainProperties
            .name(_templateProperties.name)
            .description(_templateProperties.description)
            .subject(_templateProperties.subject)
            .body(_templateProperties.body)
            .responseTemplateId(_templateProperties.responserule);

        expect(wTemplateWithOptionsAtInitAndChainProperties.get('workSpaceId')).to.equal(_templateOptions.workSpaceId);
        expect(wTemplateWithOptionsAtInitAndChainProperties.get('id')).to.equal(_templateOptions.id);
        expect(wTemplateWithOptionsAtInitAndChainProperties.get('name')).to.equal(_templateProperties.name);
        expect(wTemplateWithOptionsAtInitAndChainProperties.get('messageTemplateName')).to.equal(_templateProperties.name);
        expect(wTemplateWithOptionsAtInitAndChainProperties.get('description')).to.equal(_templateProperties.description);
        expect(wTemplateWithOptionsAtInitAndChainProperties.get('messageTemplateDescription')).to.equal(_templateProperties.description);
        expect(wTemplateWithOptionsAtInitAndChainProperties.get('subject')).to.equal(_templateProperties.subject);
        expect(wTemplateWithOptionsAtInitAndChainProperties.get('body')).to.equal(_templateProperties.body);
        expect(wTemplateWithOptionsAtInitAndChainProperties.get('responserule')).to.equal(_templateProperties.responserule);
        wTemplateWithOptionsAtInitAndChainProperties = null;
    });
    it("should not add an `invalid property` to template structure -send via options", function () {
        var _templateOptions = {
            workSpaceId: 1,
            id: 1,
            invalidProperty: 'somevalue'
        };

        var wTemplateWithOptionsAndInvalidProperty = new WTemplate(_templateOptions, clientConfig);

        expect(wTemplateWithOptionsAndInvalidProperty.get('workSpaceId')).to.equal(_templateOptions.workSpaceId);
        expect(wTemplateWithOptionsAndInvalidProperty.get('id')).to.equal(_templateOptions.id);
        expect(function () { wTemplateWithOptionsAndInvalidProperty.get('invalidProperty') }).to.throw('invalidProperty is undefined in Template Object');
        wTemplateWithOptionsAndInvalidProperty = null;
    });
    describe('- Invalid params to methods', function () {
        var _methods = ['email', 'voice', 'web', 'social', 'features'];
        _methods.forEach(function (method) {
            it("should throw an error if params to " + method + "() is not an object", function () {
                var _templateOptions = {
                    workSpaceId: 1,
                    id: 1
                };

                var wTemplate = new WTemplate(_templateOptions, clientConfig);

                expect(function () { wTemplate[method]() }).to.throw(method + ' should be an object');
                wTemplate = null;
            });
        });
    });

    it("should POST /templates and receive `201` for valid template structure", function () {
        var _templateOptions = {
            workSpaceId: 1
        };
        var _templateProperties = {
            name: 'Sample Template',
            description: 'Template Description',
            subject: 'message subject',
            body: 'message body',
            responseTemplateId: 'ABCD'
        };
        var _201Response = {
            statusCode: 201,
            headers: {
                location: 'https://api.whispir.com/workspaces/1/templates/1?apikey=apiKey'
            }
        };

        var wTemplate = new WTemplate(_templateOptions, clientConfig);
        
        //using nock to mock the request
        nock('https://api.whispir.com', {
            reqheaders: {
                'Accept': 'application/vnd.whispir.template-v1+json',
                'Content-Type': 'application/vnd.whispir.template-v1+json'
            }
        })
            .post('/workspaces/1/templates')
            .basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
            .reply(_201Response.statusCode, '', _201Response.headers);

        return wTemplate
            .name(_templateProperties.name)
            .description(_templateProperties.description)
            .subject(_templateProperties.subject)
            .body(_templateProperties.body)
            .email({})
            .voice({})
            .web({})
            .responseRule(_templateProperties.responseTemplateId)
            .create().then(function (success) {
                expect(success.statusCode).to.equal(_201Response.statusCode);
                expect(success.location).to.equal(_201Response.headers.location);
            }, function (fail) {
                throw Error('this is supposed to pass with a 201');
            });
    });
    it("should POST /templates and receive `401` for valid Template structure but invalid Auth credentials", function () {
        var _clientConfig = _.clone(clientConfig);
        _clientConfig.auth = { user: 'username', pass: 'invalidPassword' };
        
        var _templateOptions = {
            workSpaceId: 1
        };
        var _templateProperties = {
            name: 'Sample Template',
            description: 'Template Description',
            subject: 'message subject',
            body: 'message body'
        };
        var _401Response = {
            statusCode: 401,
            body: {
                "links": null,
                "errorSummary": "Your username and password combination was incorrect. Please check your authentication details and try again",
                "errorText": "Unauthorized",
                "errorDetail": null
            }
        };

        var wTemplate = new WTemplate(_templateOptions, _clientConfig);
        
        //using nock to mock the request
        nock('https://api.whispir.com', {
            reqheaders: {
                'Accept': 'application/vnd.whispir.template-v1+json',
                'Content-Type': 'application/vnd.whispir.template-v1+json'
            }
        })
            .post('/workspaces/1/templates')
            .basicAuth(_clientConfig.auth)
            .query({ apikey: clientConfig.apiKey })
            .reply(_401Response.statusCode, _401Response.body);

        return wTemplate
            .create().then(function (itShouldNotBeHere) {
                throw Error('this is supposed to fail with a 401');
            }, function (fail) {
                expect(fail.statusCode).to.equal(_401Response.statusCode);
                expect(fail.body).to.be.an('object');
            });
    });
    it("should POST /templates and receive `422` if there a mandatory property missing in the template structure", function () {
        var _templateProperties = {
			workSpaceId: 1,
            description: 'Template Description',
            subject: 'message subject',
            body: 'message body'
        };
        var _422Response = {
            statusCode: 422,
            body: {
                "errorSummary": "Your request did not contain all of the information required to perform this method. Please check your request for the required fields to be passed in and try again. Alternatively, contact your administrator or support@whispir.com for more information",
                "errorText": "messageTemplateName is Mandatory \n",
                "link": []
            }
        };

        var wTemplate = new WTemplate(_templateProperties, clientConfig);
        
        //using nock to mock the request
        nock('https://api.whispir.com', {
            reqheaders: {
                'Accept': 'application/vnd.whispir.template-v1+json',
                'Content-Type': 'application/vnd.whispir.template-v1+json'
            }
        })
            .post('/workspaces/1/templates')
            .basicAuth(clientConfig.auth)
		    .query({ apikey: clientConfig.apiKey })
            .reply(_422Response.statusCode, _422Response.body);

        return wTemplate.create()
            .then(function (itShouldNotBeHere) {
                throw Error('this is supposed to fail with a 422');
            }, function (fail) {
                expect(fail.statusCode).to.equal(_422Response.statusCode);
                expect(fail.body).to.be.an('object');
            });
    });
    it("should POST /templates and receive `201` for valid template structure in default workspace when no workspace Id is provided", function () {

        var _templateProperties = {
            name: 'Sample Template',
            description: 'Template Description',
            subject: 'message subject',
            body: 'message body'
        };
        var _201Response = {
            statusCode: 201,
            headers: {
                location: 'https`://api.whispir.com/templates/1?apikey=apiKey'
            },
            body: {}
        };

        var wTemplate = new WTemplate(_templateProperties, clientConfig);
        
        //using nock to mock the request
        nock('https://api.whispir.com', {
            reqheaders: {
                'Accept': 'application/vnd.whispir.template-v1+json',
                'Content-Type': 'application/vnd.whispir.template-v1+json'
            }
        })
            .post('/templates')
            .basicAuth(clientConfig.auth)
		    .query({ apikey: clientConfig.apiKey })
            .reply(_201Response.statusCode, _201Response.body, _201Response.headers);

        return wTemplate
            .create()
            .then(function (success) {
                expect(success.statusCode).to.equal(_201Response.statusCode);
                expect(success.location).to.equal(_201Response.headers.location);
                expect(success.body).to.be.an("object");
            }, function (fail) {
                throw Error('this is supposed to pass with a 201');
            });
    });
    it("should allow to send raw JSON directly via create(rawJSON) - POST /templates and receive `201` for valid template structure", function () {

        var _templateProperties = {
            name: 'Sample Template',
            description: 'Template Description',
            subject: 'message subject',
            body: 'message body'
        };
        var _201Response = {
            statusCode: 201,
            headers: {
                location: 'https://api.whispir.com/templates/1?apikey=apiKey'
            },
            body: {}
        };

        var wTemplate = new WTemplate({}, clientConfig);
        
        //using nock to mock the request
        nock('https://api.whispir.com', {
            reqheaders: {
                'Accept': 'application/vnd.whispir.template-v1+json',
                'Content-Type': 'application/vnd.whispir.template-v1+json'
            }
        })
            .post('/templates')
            .basicAuth(clientConfig.auth)
		    .query({ apikey: clientConfig.apiKey })
            .reply(_201Response.statusCode, _201Response.body, _201Response.headers);

        return wTemplate.create(JSON.stringify(_templateProperties))
            .then(function (success) {
                expect(success.statusCode).to.equal(_201Response.statusCode);
                expect(success.location).to.equal(_201Response.headers.location);
                expect(success.body).to.be.an("object");
            }, function (fail) {
                throw Error('this is supposed to pass with a 201');
            });
    });
    describe('.retrieve()', function () {
        it("should throw Error when no TemplateID is passed", function () {
            var wTemplate = new WTemplate({}, clientConfig);
            expect(function () { wTemplate.retrieve(); }).to.throw('Template id is not provided. You should provide one like this \n\t - wClient.Template().id(1) \n\t - wClient.Template({id: 1})');
        });
        it("should get Template data for a given template with Id and status 200", function () {

            var _templateProperties = {
                id: 1
            };
            var _200Response = {
                statusCode: 200,
                body: {}
            };

            var wTemplate = new WTemplate(_templateProperties, clientConfig);
        
            //using nock to mock the request
            nock('https://api.whispir.com', {
                reqheaders: {
                    'Accept': 'application/vnd.whispir.template-v1+json'
                }
            })
                .get('/templates/1')
                .basicAuth(clientConfig.auth)
                .query({ apikey: clientConfig.apiKey })
                .reply(_200Response.statusCode);

            return wTemplate.retrieve()
                .then(function (success) {
                    expect(success.statusCode).to.equal(_200Response.statusCode);
                }, function (fail) {
                    throw Error('this is supposed to pass with a 200');
                });
        });
    });
    describe('.update()', function () {
        it("should throw Error when no TemplateID is passed", function () {
            var wTemplate = new WTemplate({}, clientConfig);
            expect(function () { wTemplate.update(); }).to.throw('Template id is not provided. You should provide one like this \n\t - wClient.Template().id(1) \n\t - wClient.Template({id: 1})');
        });
        it("should update Template data for a given template with Id and status 204", function () {

            var _templateProperties = {
                id: 1,
                name: 'Sample Template',
                description: 'Template Description',
                subject: 'message subject',
                body: 'message body'
            };
            var _204Response = {
                statusCode: 204
            };

            var wTemplate = new WTemplate(_templateProperties, clientConfig);
        
            //using nock to mock the request
            nock('https://api.whispir.com', {
                reqheaders: {
                    'Accept': 'application/vnd.whispir.template-v1+json'
                }
            })
                .put('/templates/1')
                .basicAuth(clientConfig.auth)
                .query({ apikey: clientConfig.apiKey })
                .reply(_204Response.statusCode);

            return wTemplate.update()
                .then(function (success) {
                    expect(success.statusCode).to.equal(_204Response.statusCode);
                }, function (fail) {
                    throw Error('this is supposed to pass with a 204');
                });
        });
    });
    describe('.delete()', function () {
        it("should throw Error when no TemplateID is passed", function () {
            var wTemplate = new WTemplate({}, clientConfig);
            expect(function () { wTemplate.delete(); }).to.throw('Template id is not provided. You should provide one like this \n\t - wClient.Template().id(1) \n\t - wClient.Template({id: 1})');
        });
        it("should delete Template data for a given template with Id and status 204", function () {

            var _templateProperties = {
                id: 1
            };
            var _204Response = {
                statusCode: 204
            };

            var wTemplate = new WTemplate(_templateProperties, clientConfig);
        
            //using nock to mock the request
            nock('https://api.whispir.com', {
                reqheaders: {
                    'Accept': 'application/vnd.whispir.template-v1+json'
                }
            })
                .delete('/templates/1')
                .basicAuth(clientConfig.auth)
                .query({ apikey: clientConfig.apiKey })
                .reply(_204Response.statusCode);

            return wTemplate.delete()
                .then(function (success) {
                    expect(success.statusCode).to.equal(_204Response.statusCode);
                }, function (fail) {
                    throw Error('this is supposed to pass with a 204');
                });
        });
    });

});