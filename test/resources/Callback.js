/* global describe */
/* global beforeEach */
/* global it */

'use strict';
var chai = require("chai");
var chaiAsPromised = require("chai-as-promised");
var expect = require("chai").expect;
var WCallback = require("../../lib/resources/Callback.js");
var nock = require('nock');
var _ = require('underscore');

chai.use(chaiAsPromised);

var clientConfig, _cbURL, _rand;

describe('resources/Callback', function () {
	beforeEach(function () {
		_rand = Math.random().toString().split(".")[1];
		_cbURL = 'https://www.example.com/callBackPage';
		clientConfig = {
				host: 'https://api.whispir.com',
				auth: { user: 'username', pass: 'password' },
				apiKey: 'apiKey',
				workSpaceId: 0,
				headers: {
					'X-Originating-SDK': 'nodejs-v.1.0.0'
				}
		};
	});
	it("sets an empty callback structure when no options is sent", function () {
		var wCallbackWithNoOptions = new WCallback("", clientConfig);
		expect(wCallbackWithNoOptions.toJsonString()).to.equal('{}');
		wCallbackWithNoOptions = null;
	});
	it("sets a callback structure exactly as in passed options", function () {
		var _callbackOptions = {
				workSpaceId: 1,
				id: _rand,
				name: 'Callback_' + _rand,
				url: _cbURL,
				auth: {
					type: 'querystring',
					key: 'MY_AUTH_KEY'
				},
				contentType: 'XML',
				removeHTML: 'disabled',
				email: 'me@example.com',
				callbacks: {
					reply: 'enabled',
					undeliverable: 'enabled'
				}
		};
		var wCallbackWithOptions = new WCallback(_callbackOptions, clientConfig);
		var wCallbackWithNoOptions = new WCallback("", clientConfig);

		expect(wCallbackWithOptions.get('workSpaceId')).to.equal(_callbackOptions.workSpaceId);
		expect(wCallbackWithOptions.get('name')).to.equal(_callbackOptions.name);
		expect(wCallbackWithOptions.get('url')).to.equal(_callbackOptions.url);
		expect(wCallbackWithOptions.get('auth')).to.equal(_callbackOptions.auth);
		expect(wCallbackWithOptions.get('contentType')).to.equal(_callbackOptions.contentType);
		expect(wCallbackWithOptions.get('removeHTML')).to.equal(_callbackOptions.removeHTML)
		expect(wCallbackWithOptions.get('email')).to.equal(_callbackOptions.email)
		expect(wCallbackWithOptions.get('callbacks')).to.equal(_callbackOptions.callbacks)
		expect(wCallbackWithOptions.get('id')).to.equal(_callbackOptions.id)
		expect(wCallbackWithOptions).to.not.equal(wCallbackWithNoOptions);

		wCallbackWithOptions = null;
	});
	it("should support method chaining and options", function () {
		var _callbackOptions = {
				workSpaceId: 1,
				id: _rand
		};
		var _callbackProperties = {
				name: 'Callback_' + _rand,
				url: _cbURL,
				auth: {
					type: 'querystring',
					key: 'MY_AUTH_KEY'
				},
				contentType: 'JSON',
				removeHTML: 'disabled',
				email: 'me@example.com',
				callbacks: {
					reply: 'enabled',
					undeliverable: 'enabled'
				}
		};
		var wCallbackWithOptionsAtInitAndChainProperties = new WCallback(_callbackOptions, clientConfig);

		wCallbackWithOptionsAtInitAndChainProperties
		.name(_callbackProperties.name)
		.url(_callbackProperties.url)
		.authByQueryString(_callbackProperties.auth.key)
		.contentTypeAsJSON()
		.email(_callbackProperties.email)
		.removeHTMLInReplies(false)
		.enableForReply()
		.enableForUndeliverable();

		expect(wCallbackWithOptionsAtInitAndChainProperties.get('workSpaceId')).to.equal(_callbackOptions.workSpaceId);
		expect(wCallbackWithOptionsAtInitAndChainProperties.get('id')).to.equal(_callbackOptions.id);
		expect(wCallbackWithOptionsAtInitAndChainProperties.get('name')).to.equal(_callbackProperties.name);
		expect(wCallbackWithOptionsAtInitAndChainProperties.get('url')).to.equal(_callbackProperties.url);
		expect(wCallbackWithOptionsAtInitAndChainProperties.get('auth')).to.be.an('object');
		expect(wCallbackWithOptionsAtInitAndChainProperties.get('contentType')).to.equal(_callbackProperties.contentType);
		expect(wCallbackWithOptionsAtInitAndChainProperties.get('removeHTML')).to.equal(_callbackProperties.removeHTML);
		expect(wCallbackWithOptionsAtInitAndChainProperties.get('email')).to.equal(_callbackProperties.email);
		expect(wCallbackWithOptionsAtInitAndChainProperties.get('callbacks')).to.be.an('object');
		wCallbackWithOptionsAtInitAndChainProperties = null;
	});
	it("should not add an `invalid property` to message structure -send via options", function () {
		var _callbackOptions = {
				workSpaceId: 1,
				id: 1,
				invalidProperty: 'somevalue'
		};

		var wCallbackWithOptionsAndInvalidProperty = new WCallback(_callbackOptions, clientConfig);

		expect(wCallbackWithOptionsAndInvalidProperty.get('workSpaceId')).to.equal(_callbackOptions.workSpaceId);
		expect(wCallbackWithOptionsAndInvalidProperty.get('id')).to.equal(_callbackOptions.id);
		expect(function () { wCallbackWithOptionsAndInvalidProperty.get('invalidProperty') }).to.throw('invalidProperty is undefined in Callback Object');
		wCallbackWithOptionsAndInvalidProperty = null;
	});
	describe('- Invalid params to methods', function () {
		var _methods = ['auth', 'callbacks'];
		_methods.forEach(function (method) {
			it("should throw an error if params to " + method + "() is not an object", function () {
				var _callbackOptions = {
						workSpaceId: 1,
						id: 1
				};

				var wCallback = new WCallback(_callbackOptions, clientConfig);

				expect(function () { wCallback[method]() }).to.throw(method + ' should be an object');
				wCallback = null;
			});
		});
	});

	it("should POST /callbacks and receive `201` for valid callback structure", function () {
		var _callbackOptions = {
				workSpaceId: 1
		};
		var _callbackProperties = {
				name: 'Callback_' + _rand,
				url: _cbURL,
				auth: {
					type: 'querystring',
					key: 'MY_AUTH_KEY'
				},
				contentType: 'XML',
				removeHTML: 'disabled',
				email: 'me@example.com',
				callbacks: {
					reply: 'enabled',
					undeliverable: 'enabled'
				}
		};
		var _201Response = {
				statusCode: 201,
				headers: {
					location: 'https://api.whispir.com/workspaces/1/callbacks/1?apikey=apiKey'
				}
		};

		var wCallback = new WCallback(_callbackOptions, clientConfig);

		//using nock to mock the request
		nock(clientConfig.host, {
			reqheaders: {
				'Accept': 'application/vnd.whispir.api-callback-v1+json',
				'Content-Type': 'application/vnd.whispir.api-callback-v1+json'
			}
		})
		.post('/workspaces/1/callbacks')
		.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_201Response.statusCode, '', _201Response.headers);

		return wCallback
		.name(_callbackProperties.name)
		.url(_callbackProperties.url)
		.auth(_callbackProperties.auth)
		.contentTypeAsJSON()
		.email(_callbackProperties.email)
		.removeHTMLInReplies()
		.callbacks(_callbackProperties.callbacks)
		.create().then(function (success) {
			expect(success.statusCode).to.equal(_201Response.statusCode);
			expect(success.location).to.equal(_201Response.headers.location);
		}, function (fail) {
			throw Error('this is supposed to pass with a 201');
		});
	});
	it("should POST /callbacks and receive `401` for valid callback structure but invalid Auth credentials", function () {
		var _clientConfig = _.clone(clientConfig);
		_clientConfig.auth = { user: 'username', pass: 'invalidPassword' };

		var _callbackOptions = {
				workSpaceId: 1
		};
		var _callbackProperties = {
				name: 'Callback_' + _rand,
				url: _cbURL,
				auth: {
					type: 'querystring',
					key: 'MY_AUTH_KEY'
				},
				contentType: 'XML',
				removeHTML: 'disabled',
				email: 'me@example.com',
				callbacks: {
					reply: 'enabled',
					undeliverable: 'enabled'
				}
		};
		var _401Response = {
				statusCode: 401,
				body: {
					"links": null,
					"errorSummary": "Your username and password combination was incorrect. Please check your authentication details and try again",
					"errorText": "Unauthorized",
					"errorDetail": null
				}
		};

		var wCallback = new WCallback(_callbackOptions, _clientConfig);

		//using nock to mock the request
		nock(clientConfig.host, {
			reqheaders: {
				'Accept': 'application/vnd.whispir.api-callback-v1+json',
				'Content-Type': 'application/vnd.whispir.api-callback-v1+json'
			}
		})
		.post('/workspaces/1/callbacks')
		.basicAuth(_clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_401Response.statusCode, _401Response.body);

		return wCallback
		.create().then(function (itShouldNotBeHere) {
			throw Error('this is supposed to fail with a 401');
		}, function (fail) {
			expect(fail.statusCode).to.equal(_401Response.statusCode);
			expect(fail.body).to.be.an('object');
		});
	});
	it("should POST /callbacks and receive `422` if there a mandatory property missing in the callback structure", function () {
		var _callbackProperties = {
				workSpaceId: 1,
				url: _cbURL,
				auth: {
					type: 'querystring',
					key: 'MY_AUTH_KEY'
				},
				contentType: 'XML',
				removeHTML: 'disabled',
				email: 'me@example.com',
				callbacks: {
					reply: 'enabled',
					undeliverable: 'enabled'
				}
		};
		var _422Response = {
				statusCode: 422,
				body: {
					"errorSummary": "Your request did not contain all of the information required to perform this method. Please check your request for the required fields to be passed in and try again. Alternatively, contact your administrator or support@whispir.com for more information",
					"errorText": "name is Mandatory \n",
					"link": []
				}
		};

		var wCallback = new WCallback(_callbackProperties, clientConfig);

		//using nock to mock the request
		nock(clientConfig.host, {
			reqheaders: {
				'Accept': 'application/vnd.whispir.api-callback-v1+json',
				'Content-Type': 'application/vnd.whispir.api-callback-v1+json'
			}
		})
		.post('/workspaces/1/callbacks')
		.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_422Response.statusCode, _422Response.body);

		return wCallback.create()
		.then(function (itShouldNotBeHere) {
			throw Error('this is supposed to fail with a 422');
		}, function (fail) {
			expect(fail.statusCode).to.equal(_422Response.statusCode);
			expect(fail.body).to.be.an('object');
		});
	});
	it("should POST /callbacks and receive `201` for valid callback structure in default workspace when no workspace Id is provided", function () {

		var _callbackProperties = {
				url: _cbURL,
				auth: {
					type: 'querystring',
					key: 'MY_AUTH_KEY'
				},
				contentType: 'XML',
				removeHTML: 'disabled',
				email: 'me@example.com',
				callbacks: {
					reply: 'enabled',
					undeliverable: 'enabled'
				}
		};
		var _201Response = {
				statusCode: 201,
				headers: {
					location: 'https`://api.whispir.com/callbacks/1?apikey=apiKey'
				},
				body: {}
		};

		var wCallback = new WCallback(_callbackProperties, clientConfig);

		//using nock to mock the request
		nock(clientConfig.host, {
			reqheaders: {
				'Accept': 'application/vnd.whispir.api-callback-v1+json',
				'Content-Type': 'application/vnd.whispir.api-callback-v1+json'
			}
		})
		.post('/callbacks')
		.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_201Response.statusCode, _201Response.body, _201Response.headers);

		return wCallback
		.create()
		.then(function (success) {
			expect(success.statusCode).to.equal(_201Response.statusCode);
			expect(success.location).to.equal(_201Response.headers.location);
			expect(success.body).to.be.an("object");
		}, function (fail) {
			throw Error('this is supposed to pass with a 201');
		});
	});
	it("should allow to send raw JSON directly via create(rawJSON) - POST /callbacks and receive `201` for valid callback structure", function () {

		var _callbackProperties = {
				workSpaceId: 1,
				url: _cbURL,
				auth: {
					type: 'querystring',
					key: 'MY_AUTH_KEY'
				},
				contentType: 'XML',
				removeHTML: 'disabled',
				email: 'me@example.com',
				callbacks: {
					reply: 'enabled',
					undeliverable: 'enabled'
				}
		};
		var _201Response = {
				statusCode: 201,
				headers: {
					location: 'https://api.whispir.com/workspaces/1/callbacks/1?apikey=apiKey'
				},
				body: {}
		};

		var wCallback = new WCallback({}, clientConfig);

		//using nock to mock the request
		nock(clientConfig.host, {
			reqheaders: {
				'Accept': 'application/vnd.whispir.api-callback-v1+json',
				'Content-Type': 'application/vnd.whispir.api-callback-v1+json'
			}
		})
		.post('/callbacks')
		.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_201Response.statusCode, _201Response.body, _201Response.headers);

		return wCallback.create(JSON.stringify(_callbackProperties))
		.then(function (success) {
			expect(success.statusCode).to.equal(_201Response.statusCode);
			expect(success.location).to.equal(_201Response.headers.location);
			expect(success.body).to.be.an("object");
		}, function (fail) {
			throw Error('this is supposed to pass with a 201');
		});
	});
	describe('.retrieve()', function () {
		it("should throw Error when no CallbackID is passed", function () {
			var wCallback = new WCallback({}, clientConfig);
			expect(function () { wCallback.retrieve(); }).to.throw('Callback id is not provided. You should provide one like this \n\t - wClient.Callback().id(1) \n\t - wClient.Callback({id: 1})');
		});
		it("should get Callback data for a given callback with Id and status 200", function () {

			var _callbackProperties = {
					id: 1,
					workSpaceId: 1
			};
			var _200Response = {
					statusCode: 200,
					body: {}
			};

			var wCallback = new WCallback(_callbackProperties, clientConfig);

			//using nock to mock the request
			nock(clientConfig.host, {
				reqheaders: {
					'Accept': 'application/vnd.whispir.api-callback-v1+json'
				}
			})
			.get('/workspaces/1/callbacks/1')
			.basicAuth(clientConfig.auth)
			.query({ apikey: clientConfig.apiKey })
			.reply(_200Response.statusCode);

			return wCallback.retrieve()
			.then(function (success) {
				expect(success.statusCode).to.equal(_200Response.statusCode);
			}, function (fail) {
				throw Error('this is supposed to pass with a 200');
			});
		});
	});
	describe('.update()', function () {
		it("should throw Error when no CallbackID is passed", function () {
			var wCallback = new WCallback({}, clientConfig);
			expect(function () { wCallback.update(); }).to.throw('Callback id is not provided. You should provide one like this \n\t - wClient.Callback().id(1) \n\t - wClient.Callback({id: 1})');
		});
		it("should update Callback data for a given callback with Id and status 204", function () {

			var _callbackProperties = {
					workSpaceId: 1,
					id: 1,
					url: _cbURL,
					auth: {
						type: 'querystring',
						key: 'MY_AUTH_KEY'
					},
					contentType: 'XML',
					removeHTML: 'disabled',
					email: 'me@example.com',
					callbacks: {
						reply: 'enabled',
						undeliverable: 'enabled'
					}
			};
			var _204Response = {
					statusCode: 204
			};

			var wCallback = new WCallback(_callbackProperties, clientConfig);

			//using nock to mock the request
			nock(clientConfig.host, {
				reqheaders: {
					'Accept': 'application/vnd.whispir.api-callback-v1+json'
				}
			})
			.put('/workspaces/1/callbacks/1')
			.basicAuth(clientConfig.auth)
			.query({ apikey: clientConfig.apiKey })
			.reply(_204Response.statusCode);

			return wCallback.update()
			.then(function (success) {
				expect(success.statusCode).to.equal(_204Response.statusCode);
			}, function (fail) {
				throw Error('this is supposed to pass with a 204');
			});
		});
	});
	describe('.delete()', function () {
		it("should throw Error when no CallbackID is passed", function () {
			var wCallback = new WCallback({}, clientConfig);
			expect(function () { wCallback.delete(); }).to.throw('Callback id is not provided. You should provide one like this \n\t - wClient.Callback().id(1) \n\t - wClient.Callback({id: 1})');
		});
		it("should delete Callback data for a given callback with Id and status 204", function () {

			var _callbackProperties = {
					id: 1,
					workSpaceId: 1
			};
			var _204Response = {
					statusCode: 204
			};

			var wCallback = new WCallback(_callbackProperties, clientConfig);

			//using nock to mock the request
			nock(clientConfig.host, {
				reqheaders: {
					'Accept': 'application/vnd.whispir.api-callback-v1+json'
				}
			})
			.delete('/workspaces/1/callbacks/1')
			.basicAuth(clientConfig.auth)
			.query({ apikey: clientConfig.apiKey })
			.reply(_204Response.statusCode);

			return wCallback.delete()
			.then(function (success) {
				expect(success.statusCode).to.equal(_204Response.statusCode);
			}, function (fail) {
				throw Error('this is supposed to pass with a 204');
			});
		});
	});

});