/* global describe */
/* global beforeEach */
/* global it */

'use strict';
var chai = require("chai");
var chaiAsPromised = require("chai-as-promised");
var expect = require("chai").expect;
var WWorkSpace = require("../../lib/resources/WorkSpace.js");
var nock = require('nock');
var _ = require('underscore');

chai.use(chaiAsPromised);

var clientConfig;

describe('resources/WorkSpace', function () {
    beforeEach(function () {
        clientConfig = {
            host: 'https://api.whispir.com',
            auth: { user: 'username', pass: 'password' },
            apiKey: 'apiKey',
            workSpaceId: 0,
            headers: {
                'X-Originating-SDK': 'nodejs-v.1.0.0'
            }
        };
    });
    it("sets an empty workspace structure when no options is sent", function () {
        var wWorkSpaceWithNoOptions = new WWorkSpace("", clientConfig);
        expect(wWorkSpaceWithNoOptions.toJsonString()).to.equal('{}');
        wWorkSpaceWithNoOptions = null;
    });
    it("sets a workspace structure exactly as in passed options", function () {
        var _workspaceProperties = {
            id: 1,
            name: 'Sample WorkSpace',
            number: 'W001',
            status: 'A',
            billingcostcentre: 'BC1'
        };
        var wWorkSpaceWithOptions = new WWorkSpace(_workspaceProperties, clientConfig);
        var wWorkSpaceWithNoOptions = new WWorkSpace("", clientConfig);

        expect(wWorkSpaceWithOptions.get('id')).to.equal(_workspaceProperties.id);
        expect(wWorkSpaceWithOptions.get('name')).to.equal(_workspaceProperties.name);
        expect(wWorkSpaceWithOptions.get('number')).to.equal(_workspaceProperties.number);
        expect(wWorkSpaceWithOptions.get('status')).to.equal(_workspaceProperties.status);
        expect(wWorkSpaceWithOptions.get('billingcostcentre')).to.equal(_workspaceProperties.billingcostcentre);
        expect(wWorkSpaceWithOptions).to.not.equal(wWorkSpaceWithNoOptions);

        wWorkSpaceWithOptions = null;
        wWorkSpaceWithNoOptions = null;
    });
    it("should support method chaining and options", function () {
        var _workspaceProperties = {
            id: 1,
            name: 'Sample WorkSpace',
            number: 'W001',
            status: 'A',
            billingcostcentre: 'BC1'
        };
        var wWorkSpaceWithOptionsAtInitAndChainProperties = new WWorkSpace({}, clientConfig);

        wWorkSpaceWithOptionsAtInitAndChainProperties
            .id(_workspaceProperties.id)
            .name(_workspaceProperties.name)
            .number(_workspaceProperties.number)
            .status(_workspaceProperties.status)
            .billingcostcentre(_workspaceProperties.billingcostcentre);

        expect(wWorkSpaceWithOptionsAtInitAndChainProperties.get('id')).to.equal(_workspaceProperties.id);
        expect(wWorkSpaceWithOptionsAtInitAndChainProperties.get('name')).to.equal(_workspaceProperties.name);
        expect(wWorkSpaceWithOptionsAtInitAndChainProperties.get('projectName')).to.equal(_workspaceProperties.name);
        expect(wWorkSpaceWithOptionsAtInitAndChainProperties.get('number')).to.equal(_workspaceProperties.number);
        expect(wWorkSpaceWithOptionsAtInitAndChainProperties.get('projectNumber')).to.equal(_workspaceProperties.number);
        expect(wWorkSpaceWithOptionsAtInitAndChainProperties.get('status')).to.equal(_workspaceProperties.status);
        expect(wWorkSpaceWithOptionsAtInitAndChainProperties.get('billingcostcentre')).to.equal(_workspaceProperties.billingcostcentre);
        wWorkSpaceWithOptionsAtInitAndChainProperties = null;
    });
    it("should not add an `invalid property` to message structure -send via options", function () {
        var _workspaceProperties = {
            workSpaceId: 1,
            id: 1
        };

        var wWorkSpaceWithOptionsAndInvalidProperty = new WWorkSpace(_workspaceProperties, clientConfig);

        expect(wWorkSpaceWithOptionsAndInvalidProperty.get('id')).to.equal(_workspaceProperties.id);
        expect(function () { wWorkSpaceWithOptionsAndInvalidProperty.get('workSpaceId'); }).to.throw('workSpaceId is undefined in WorkSpace Object');
        wWorkSpaceWithOptionsAndInvalidProperty = null;
    });

    it("should POST /workspace and receive `201` for valid workspace structure", function () {
        var _workspaceProperties = {
            id: 1,
            name: 'Sample WorkSpace',
            number: 'W001',
            status: 'A',
            billingcostcentre: 'BC1'
        };
        var _201Response = {
            statusCode: 201,
            headers: {
                location: clientConfig.host + '/workspaces/1?apikey=apiKey'
            }
        };

        var wWorkSpace = new WWorkSpace(_workspaceProperties, clientConfig);
        
        //using nock to mock the request
        nock(clientConfig.host, {
            reqheaders: {
                'Accept': 'application/vnd.whispir.workspace-v1+json',
                'Content-Type': 'application/vnd.whispir.workspace-v1+json'
            }
        })
            .post('/workspaces')
            .basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
            .reply(_201Response.statusCode, '', _201Response.headers);

        return wWorkSpace
            .create().then(function (success) {
                expect(success.statusCode).to.equal(_201Response.statusCode);
                expect(success.location).to.equal(_201Response.headers.location);
            }, function (fail) {
                throw Error('this is supposed to pass with a 201');
            });
    });
    it("should POST /workspaces and receive `401` for valid workspace structure but invalid Auth credentials", function () {
        var _clientConfig = _.clone(clientConfig);
        _clientConfig.auth = { user: 'username', pass: 'invalidPassword' };
        
        var _workspaceProperties = {
            id: 1,
            name: 'Sample WorkSpace',
            number: 'W001',
            status: 'A',
            billingcostcentre: 'BC1'
        };
        var _401Response = {
            statusCode: 401,
            body: {
                "links": null,
                "errorSummary": "Your username and password combination was incorrect. Please check your authentication details and try again",
                "errorText": "Unauthorized",
                "errorDetail": null
            }
        };

        var wWorkSpace = new WWorkSpace(_workspaceProperties, _clientConfig);
        
        //using nock to mock the request
        nock(clientConfig.host, {
            reqheaders: {
                'Accept': 'application/vnd.whispir.workspace-v1+json',
                'Content-Type': 'application/vnd.whispir.workspace-v1+json'
            }
        })
            .post('/workspaces')
            .basicAuth(_clientConfig.auth)
        .query({ apikey: clientConfig.apiKey })
            .reply(_401Response.statusCode, _401Response.body);

        return wWorkSpace
            .create().then(function (itShouldNotBeHere) {
                throw Error('this is supposed to fail with a 401');
            }, function (fail) {
                expect(fail.statusCode).to.equal(_401Response.statusCode);
                expect(fail.body).to.be.an('object');
            });
    });
    it("should POST /workspaces and receive `422` if there a mandatory property missing in the workspace structure", function () {
        var _workspaceProperties = {
            number: 'W001',
            status: 'A',
            billingcostcentre: 'BC1'
        };
        var _422Response = {
            statusCode: 422,
            body: {
                "errorSummary": "Your request did not contain all of the information required to perform this method. Please check your request for the required fields to be passed in and try again. Alternatively, contact your administrator or support@whispir.com for more information",
                "errorText": "projectName is Mandatory \n",
                "link": []
            }
        };

        var wWorkSpace = new WWorkSpace(_workspaceProperties, clientConfig);
        
        //using nock to mock the request
        nock(clientConfig.host, {
            reqheaders: {
                'Accept': 'application/vnd.whispir.workspace-v1+json',
                'Content-Type': 'application/vnd.whispir.workspace-v1+json'
            }
        })
            .post('/workspaces')
            .basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
            .reply(_422Response.statusCode, _422Response.body);

        return wWorkSpace.create()
            .then(function (itShouldNotBeHere) {
                throw Error('this is supposed to fail with a 422');
            }, function (fail) {
                expect(fail.statusCode).to.equal(_422Response.statusCode);
                expect(fail.body).to.be.an('object');
            });
    });
    it("should allow to send raw JSON directly via create(rawJSON) - POST /workspaces and receive `201` for valid workspace structure", function () {

        var _workspaceProperties = {
            name: 'Sample WorkSpace',
            number: 'W001',
            status: 'A',
            billingcostcentre: 'BC1'
        };
        var _201Response = {
            statusCode: 201,
            headers: {
                location: clientConfig.host + '/workspaces/1?apikey=apiKey'
            },
            body: {}
        };

        var wWorkSpace = new WWorkSpace({}, clientConfig);
        
        //using nock to mock the request
        nock(clientConfig.host, {
            reqheaders: {
                'Accept': 'application/vnd.whispir.workspace-v1+json',
                'Content-Type': 'application/vnd.whispir.workspace-v1+json'
            }
        })
            .post('/workspaces')
            .basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
            .reply(_201Response.statusCode, _201Response.body, _201Response.headers);

        return wWorkSpace.create(JSON.stringify(_workspaceProperties))
            .then(function (success) {
                expect(success.statusCode).to.equal(_201Response.statusCode);
                expect(success.location).to.equal(_201Response.headers.location);
                expect(success.body).to.be.an("object");
            }, function (fail) {
                throw Error('this is supposed to pass with a 201');
            });
    });

    describe('.retrieve()', function () {
        it("should throw Error when no workSpaceId is passed", function () {
            var wWorkSpace = new WWorkSpace({}, clientConfig);
            expect(function () { wWorkSpace.retrieve(); }).to.throw('WorkSpace id is not provided. You should provide one like this \n\t - wClient.WorkSpace().id(1) \n\t - wClient.WorkSpace({id: 1})');
        });
        it("should get WorkSpace data for a given WorkSpace with Id and status 200", function () {

            var _templateProperties = {
                id: 1
            };
            var _200Response = {
                statusCode: 200,
                body: {}
            };

            var wWorkSpace = new WWorkSpace(_templateProperties, clientConfig);
        
            //using nock to mock the request
            nock(clientConfig.host, {
                reqheaders: {
                    'Accept': 'application/vnd.whispir.workspace-v1+json'
                }
            })
                .get('/workspaces/1')
                .basicAuth(clientConfig.auth)
        .query({ apikey: clientConfig.apiKey })
                .reply(_200Response.statusCode);

            return wWorkSpace.retrieve()
                .then(function (success) {
                    expect(success.statusCode).to.equal(_200Response.statusCode);
                }, function (fail) {
                    throw Error('this is supposed to pass with a 200');
                });
        });
    });
    describe('.update()', function () {
        it("should throw Error when update is called - Update is not allowed for WorkSpace", function () {
            var wWorkSpace = new WWorkSpace({}, clientConfig);
            expect(function () { wWorkSpace.update(); }).to.throw('Updating a WorkSpace is not supported via the API');
        });
    });
    describe('.delete()', function () {
        it("should throw Error when delete is called - Delete is not allowed for WorkSpace", function () {
            var wWorkSpace = new WWorkSpace({}, clientConfig);
            expect(function () { wWorkSpace.delete(); }).to.throw('Deleting a WorkSpace is not supported via the API');
        });
    });

});