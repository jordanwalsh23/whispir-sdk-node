/* global describe */
/* global beforeEach */
/* global it */

'use strict';
var chai = require("chai");
var chaiAsPromised = require("chai-as-promised");
var expect = require("chai").expect;
var WMessage = require("../../lib/resources/Message.js");
var nock = require('nock');
var $ = require('underscore');

chai.use(chaiAsPromised);

var clientConfig;

describe('resources/Message', function () {
	beforeEach(function () {
		clientConfig = {
				host: 'https://api.whispir.com',
				auth: { user: 'username', pass: 'password' },
				apiKey: 'apiKey',
				workSpaceId: 0,
				headers: {
					'X-Originating-SDK': 'nodejs-v.1.0.0'
				}
		};
	});
	it("sets an empty message structure when no options is sent", function () {
		var wMessageWithNoOptions = new WMessage("", clientConfig);
		expect(wMessageWithNoOptions.toJsonString()).to.equal('{}');
	});
	it("sets a message structure exactly as in passed options", function () {
		var messageOptions = {
				workSpaceId: 1,
				to: '+6598765432',
				subject: 'message subject',
				body: 'message body'
		};
		var wMessageWithOptions = new WMessage(messageOptions, clientConfig);
		var wMessageWithNoOptions = new WMessage("", clientConfig);

		expect(wMessageWithOptions.get('workSpaceId')).to.equal(messageOptions.workSpaceId);
		expect(wMessageWithOptions.get('to')).to.equal(messageOptions.to);
		expect(wMessageWithOptions.get('subject')).to.equal(messageOptions.subject);
		expect(wMessageWithOptions.get('body')).to.equal(messageOptions.body);
		expect(wMessageWithOptions).to.not.equal(wMessageWithNoOptions);

		wMessageWithOptions = null;
	});
	it("should support method chaining and options", function () {
		var messageOptions = {
				workSpaceId: 1,
				id: 1
		};
		var messageProperties = {
				to: '+6598765432',
				subject: 'message subject',
				body: 'message body'
		}
		var wMessageWithOptionsAtInitAndChainProperties = new WMessage(messageOptions, clientConfig);

		wMessageWithOptionsAtInitAndChainProperties
		.to(messageProperties.to)
		.subject(messageProperties.subject)
		.body(messageProperties.body);

		expect(wMessageWithOptionsAtInitAndChainProperties.get('workSpaceId')).to.equal(messageOptions.workSpaceId);
		expect(wMessageWithOptionsAtInitAndChainProperties.get('id')).to.equal(messageOptions.id);
		expect(wMessageWithOptionsAtInitAndChainProperties.get('to')).to.equal(messageProperties.to);
		expect(wMessageWithOptionsAtInitAndChainProperties.get('subject')).to.equal(messageProperties.subject);
		expect(wMessageWithOptionsAtInitAndChainProperties.get('body')).to.equal(messageProperties.body);
		wMessageWithOptionsAtInitAndChainProperties = null;
	});
	it("should not add an `invalid property` to message structure -send via options", function () {
		var messageOptions = {
				workSpaceId: 1,
				id: 1,
				invalidProperty: 'somevalue'
		};

		var wMessageWithOptionsAndInvalidProperty = new WMessage(messageOptions, clientConfig);

		expect(wMessageWithOptionsAndInvalidProperty.get('workSpaceId')).to.equal(messageOptions.workSpaceId);
		expect(wMessageWithOptionsAndInvalidProperty.get('id')).to.equal(messageOptions.id);
		expect(function() {wMessageWithOptionsAndInvalidProperty.get('invalidProperty');}).to.throw('invalidProperty is undefined in Message Object');
		wMessageWithOptionsAndInvalidProperty = null;
	});
	describe('- Invalid params to methods', function () {
		var _methods = ['resource', 'email', 'voice', 'web', 'social', 'messageattributes', 'schedule'];
		_methods.forEach(function (method) {
			it("should throw an error if params to " + method + "() is not an object", function () {
				var messageOptions = {
						workSpaceId: 1,
						id: 1
				};

				var wMessage = new WMessage(messageOptions, clientConfig);

				expect(function () { wMessage[method]() }).to.throw(method + ' should be an object');
				wMessage = null;
			});
		});
	});
	describe('handling schedule() method for scheduled messages', function () {
		it('should pass when all values are properly sent', function () {
			var messageOptions = {
					workSpaceId: 1,
					id: 1
			};

			var _scheduleOptions = { type: 'repeat', startDate: '14/02/2013 23:41', count: '1', days: '0', hours: '0', minutes: 0 };
			var wMessage = new WMessage(messageOptions, clientConfig).schedule(_scheduleOptions);

			expect(wMessage.get('messageType')).to.equal('SCHEDULED');
			expect(wMessage.get('scheduleType')).to.equal(_scheduleOptions.type.toUpperCase());
			expect(wMessage.get('scheduleDate')).to.equal(_scheduleOptions.startDate);
			expect(wMessage.get('repetitionCount')).to.equal(_scheduleOptions.count);
			expect(wMessage.get('repeatDays')).to.equal(_scheduleOptions.days);
			expect(wMessage.get('repeatHrs')).to.equal(_scheduleOptions.hours);
			expect(wMessage.get('repeatMin')).to.equal(_scheduleOptions.minutes + '');
			expect(wMessage.get('repeatMin')).to.not.equal(_scheduleOptions.minutes);
			wMessage = null;
		});

		var _inValids = [
		                 { args: { 'type': 'repeat', startDate: '14/02/2013 23:41', count: 1, days: 1, hours: 0 }, expected: 'missing/invalid schedule params. Need - count, days, hours, minutes, startDate, type. Got - count, days, hours, startDate, type' },
		                 { args: { 'type': 'repeat', startDate: '14/02/2013 23:41', count: 1, days: 1, minutes: 0 }, expected: 'missing/invalid schedule params. Need - count, days, hours, minutes, startDate, type. Got - count, days, minutes, startDate, type' },
		                 { args: { 'type': 'repeat', startDate: '14/02/2013 23:41', count: 1, hours: 0, minutes: 0 }, expected: 'missing/invalid schedule params. Need - count, days, hours, minutes, startDate, type. Got - count, hours, minutes, startDate, type' },
		                 { args: { 'type': 'repeat', startDate: '14/02/2013 23:41', days: 1, hours: 0, minutes: 0 }, expected: 'missing/invalid schedule params. Need - count, days, hours, minutes, startDate, type. Got - days, hours, minutes, startDate, type' },
		                 { args: { 'type': 'repeat', count: 1, days: 1, hours: 0, minutes: 0 }, expected: 'missing/invalid schedule params. Need - count, days, hours, minutes, startDate, type. Got - count, days, hours, minutes, type' },
		                 { args: { startDate: '14/02/2013 23:41', count: 1, days: 1, hours: 0, minutes: 0 }, expected: 'missing/invalid schedule params. Need - count, days, hours, minutes, startDate, type. Got - count, days, hours, minutes, startDate' }
		                 ];

		_inValids.forEach(function (params) {
			it("should throw an error if params is not fully sent", function () {
				var messageOptions = {
						workSpaceId: 1,
						id: 1
				};

				var wMessage = new WMessage(messageOptions, clientConfig);

				expect(function () { wMessage.schedule(params.args) }).to.throw(params.expected);
				wMessage = null;
			});
		});
	});
	it("should POST /messages and receive `202` for valid message structure", function () {
		var messageOptions = {
				workSpaceId: 1
		};
		var messageProperties = {
				to: '+6598765432',
				subject: 'message subject',
				body: 'message body'
		};
		var _202Response = {
				statusCode: 202,
				headers: {
					location: 'https://api.whispir.com/workspaces/1/messages/1?apikey=apiKey'
				},
				body: 'Your request has been accepted for processing.'
		};

		var wMessage = new WMessage(messageOptions, clientConfig);

		//using nock to mock the request
		nock(clientConfig.host, {
			reqheaders: {
				'Accept': 'application/vnd.whispir.message-v1+json',
				'Content-Type': 'application/vnd.whispir.message-v1+json'
			}
		})
		.post('/workspaces/1/messages')
		.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_202Response.statusCode, _202Response.body, _202Response.headers);

		return wMessage
		.to(messageProperties.to)
		.subject(messageProperties.subject)
		.body(messageProperties.body)
		.send().then(function (success) {
			expect(success.statusCode).to.equal(_202Response.statusCode);
			expect(success.location).to.equal(_202Response.headers.location);
			expect(success.body).to.equal(_202Response.body);
			expect(success.body).to.be.a('string');
		}, function (fail) {
			throw Error('this is supposed to pass with a 202');
		});
	});
	it("should POST /messages and receive `202` for valid message structure, with resource type as bulk-message when resource{} is provided", function () {
		var messageOptions = {
				workSpaceId: 1
		};
		var messageProperties = {
				to: '+6598765432',
				resource: {
					resourceId: 1
				},
				messageTemplateId: 1
		};
		var _202Response = {
				statusCode: 202,
				headers: {
					location: 'https://api.whispir.com/workspaces/1/messages/1?apikey=apiKey'
				},
				body: 'Your request has been accepted for processing.'
		};

		var wMessage = new WMessage(messageOptions, clientConfig);

		//using nock to mock the request
		nock(clientConfig.host, {
			reqheaders: {
				'Accept': 'application/vnd.whispir.bulkmessage-v1+json',
				'Content-Type': 'application/vnd.whispir.bulkmessage-v1+json'
			}
		})
		.post('/workspaces/1/messages')
		.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_202Response.statusCode, _202Response.body, _202Response.headers);

		return wMessage
        .to(messageProperties.to)
        .resource(messageProperties.resource)
        .useTemplateWithIdAs(messageProperties.messageTemplateId)
		.bulkSend().then(function (success) {
			expect(success.statusCode).to.equal(_202Response.statusCode);
			expect(success.location).to.equal(_202Response.headers.location);
			expect(success.body).to.equal(_202Response.body);
			expect(success.body).to.be.a('string');
		}, function (fail) {
			throw Error('this is supposed to pass with a 202');
		});
	});
    it("should throw an error for bulkSend POST /messages when resource{} is not provided", function () {
		var messageOptions = {
				workSpaceId: 1
		};
		var messageProperties = {
				to: '+6598765432',
				messageTemplateId: 1
		};

		var wMessage = new WMessage(messageOptions, clientConfig);

        expect(function () { wMessage.bulkSend() }).to.throw('Invalid or No Resource information is defined in the Message Object for bulkSend.');

	});
	it("should POST /messages and receive `401` for valid message structure but invalid Auth credentials", function () {
		var _clientConfig = $.clone(clientConfig);
		_clientConfig.auth = { user: 'username', pass: 'invalidPassword' };
		var messageOptions = {
				workSpaceId: 1
		};
		var messageProperties = {
				to: '+6598765432',
				subject: 'message subject',
				body: 'message body'
		};
		var _401Response = {
				statusCode: 401,
				body: {
					"links": null,
					"errorSummary": "Your username and password combination was incorrect. Please check your authentication details and try again",
					"errorText": "Unauthorized",
					"errorDetail": null
				}
		};

		var wMessage = new WMessage(messageOptions, _clientConfig);

		//using nock to mock the request
		nock(clientConfig.host, {
			reqheaders: {
				'Accept': 'application/vnd.whispir.message-v1+json',
				'Content-Type': 'application/vnd.whispir.message-v1+json'
			}
		})
		.post('/workspaces/1/messages')
		.basicAuth(_clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_401Response.statusCode, _401Response.body);

		return wMessage
		.to(messageProperties.to)
		.subject(messageProperties.subject)
		.body(messageProperties.body)
		.send().then(function (itShouldNotBeHere) {
			throw Error('this is supposed to fail with a 401');
		}, function (fail) {
			expect(fail.statusCode).to.equal(_401Response.statusCode);
			expect(fail.body).to.be.an('object');
			//expect(fail.body).to.equal(_401Response.body);
		});
	});
	it("should POST /messages and receive `422` if there a mandatory property missing in the message structure", function () {
		var messageOptions = {
				to: '+6598765432',
				body: 'message body',
				workSpaceId: 1
		};
		var _422Response = {
				statusCode: 422,
				body: {
					"errorSummary": "Your request did not contain all of the information required to perform this method. Please check your request for the required fields to be passed in and try again. Alternatively, contact your administrator or support@whispir.com for more information",
					"errorText": "Message body cannot be empty\n",
					"link": []
				}
		};

		var wMessage = new WMessage(messageOptions, clientConfig);

		//using nock to mock the request
		nock(clientConfig.host, {
			reqheaders: {
				'Accept': 'application/vnd.whispir.message-v1+json',
				'Content-Type': 'application/vnd.whispir.message-v1+json'
			}
		})
		.post('/workspaces/1/messages')
		.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_422Response.statusCode, _422Response.body);

		return wMessage.send()
		.then(function (itShouldNotBeHere) {
			throw Error('this is supposed to fail with a 422');
		}, function (fail) {
			expect(fail.statusCode).to.equal(_422Response.statusCode);
			expect(fail.body).to.be.an('object');
			//expect(fail.body).to.equal(_422Response.body);
		});
	});
	it("should POST /messages and receive `202` for valid message structure in default workspace when no workspace Id is provided", function () {

		var messageProperties = {
				to: '+6598765432',
				subject: 'message subject',
				body: 'message body'
		};
		var _202Response = {
				statusCode: 202,
				headers: {
					location: 'https`://api.whispir.com/messages/1?apikey=apiKey'
				},
				body: 'Your request has been accepted for processing.'
		};

		var wMessage = new WMessage({}, clientConfig);

		//using nock to mock the request
		nock(clientConfig.host, {
			reqheaders: {
				'Accept': 'application/vnd.whispir.message-v1+json',
				'Content-Type': 'application/vnd.whispir.message-v1+json'
			}
		})
		.post('/messages')
		.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_202Response.statusCode, _202Response.body, _202Response.headers);

		return wMessage
		.to(messageProperties.to)
		.subject(messageProperties.subject)
		.body(messageProperties.body)
		.send()
		.then(function (success) {
			expect(success.statusCode).to.equal(_202Response.statusCode);
			expect(success.location).to.equal(_202Response.headers.location);
			expect(success.body).to.equal(_202Response.body);
		}, function (fail) {
			throw Error('this is supposed to pass with a 202');
		});
	});
	it("should allow to send raw JSON directly via send(rawJSON) - POST /messages and receive `202` for valid message structure", function () {

		var messageProperties = {
				to: '+6598765432',
				subject: 'message subject',
				body: 'message body'
		};
		var _202Response = {
				statusCode: 202,
				headers: {
					location: 'https://api.whispir.com/messages/1?apikey=apiKey'
				},
				body: 'Your request has been accepted for processing.'
		};

		var wMessage = new WMessage({}, clientConfig);

		//using nock to mock the request
		nock(clientConfig.host, {
			reqheaders: {
				'Accept': 'application/vnd.whispir.message-v1+json',
				'Content-Type': 'application/vnd.whispir.message-v1+json'
			}
		})
		.post('/messages')
		.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_202Response.statusCode, _202Response.body, _202Response.headers);

		return wMessage.send(JSON.stringify(messageProperties))
		.then(function (success) {
			expect(success.statusCode).to.equal(_202Response.statusCode);
			expect(success.location).to.equal(_202Response.headers.location);
			expect(success.body).to.equal(_202Response.body);
		}, function (fail) {
			throw Error('this is supposed to pass with a 202');
		});
	});
	it("should allow to send attachments with email", function () {

		var messageProperties = {
				to: '+6598765432',
				subject: 'message subject',
				email: {
					body: 'message body',
					footer : 'Email signature goes here.',
					type: 'text/plain'
				}
			},
			attachments = [{
	                attachmentName : 'fileName.pdf',
	                attachmentDesc : 'fileDescription.pdf',
	                derefUri : '...base64 representation of the file being uploaded...'
	            }],
			expectedMessageProperties = {
			    to: '+6598765432',
			    subject: 'message subject',
			    email : {
			        body: 'message body',
					footer : 'Email signature goes here.',
					type: 'text/plain',
			        resources : {
			            attachment : [{
			                attachmentName : 'fileName.pdf',
			                attachmentDesc : 'fileDescription.pdf',
			                derefUri : '...base64 representation of the file being uploaded...'
			            }]
			        }
			    }
			};

		var wMessage = new WMessage({}, clientConfig);

		wMessage
			.to(messageProperties.to)
    		.subject(messageProperties.subject)
    		.email(messageProperties.email)
    		.attach('email', attachments);

    	expect(wMessage.toJsonString()).to.equal(JSON.stringify(expectedMessageProperties));

	});
	it("should allow to use custom wav files for voice calls", function () {

		var messageProperties = {
				to: '+6598765432',
				subject: 'Test voice call with attachments',
				voice: {
					header: 'Will be replaced by the voiceintro.wav file',
					body : 'Will be replaced by the voicebody.wav file',
        			type : 'ConfCall:,ConfAccountNo:,ConfPinNo:,ConfModPinNo:,Pin:'
				}
			},
			attachments = [{
                attachmentName : 'Introduction.wav',
                attachmentDesc : 'voiceintro.wav',
                derefUri : '...'
            },{
                attachmentName : 'Body.wav',
                attachmentDesc : 'voicebody.wav',
                derefUri : '...'
            }],
			expectedMessageProperties = {
			    to: '+6598765432',
			    subject: 'Test voice call with attachments',
			    voice: {
					header: 'Will be replaced by the voiceintro.wav file',
					body : 'Will be replaced by the voicebody.wav file',
        			type : 'ConfCall:,ConfAccountNo:,ConfPinNo:,ConfModPinNo:,Pin:',
			        resources : {
			            attachment : [{
			                attachmentName : 'Introduction.wav',
			                attachmentDesc : 'voiceintro.wav',
			                derefUri : '...'
			            },{
			                attachmentName : 'Body.wav',
			                attachmentDesc : 'voicebody.wav',
			                derefUri : '...'
			            }]
			        }
			    }
			};

		var wMessage = new WMessage({}, clientConfig);

		wMessage
			.to(messageProperties.to)
    		.subject(messageProperties.subject)
    		.voice(messageProperties.voice)
    		.attach('voice', attachments);

    	expect(wMessage.toJsonString()).to.equal(JSON.stringify(expectedMessageProperties));

	});
	describe('.MessageStatus()', function () {
		it("should throw Error when no MessageID is passed", function () {
			var wMessage = new WMessage({}, clientConfig);
			expect(function () { wMessage.getMessageStatus(); }).to.throw('Message id is not provided. Cannot get Message Responses');
		});
		it("should get MessageStatus for a given message with Id and status 200", function () {

			var messageProperties = {
					id: 1
			};
			var _200Response = {
					statusCode: 200,
					body: 'Your request has been accepted for processing.'
			};

			var wMessage = new WMessage(messageProperties, clientConfig);

			//using nock to mock the request
			nock(clientConfig.host, {
				reqheaders: {
					'Accept': 'application/vnd.whispir.messagestatus-v1+json'
				}
			})
			.get('/messages/1/messagestatus')
			.basicAuth(clientConfig.auth)
			.query({ apikey: clientConfig.apiKey, view: 'summary' })
			.reply(_200Response.statusCode);

			return wMessage.getMessageStatus()
			.then(function (success) {
				expect(success.statusCode).to.equal(_200Response.statusCode);
			}, function (fail) {
				throw Error('this is supposed to pass with a 200');
			});
		});
		it("should choose view as summary if no view is passed", function () {

			var messageProperties = {
					id: 1
			};
			var _200Response = {
					statusCode: 200,
					body: 'Your request has been accepted for processing.'
			};

			var wMessage = new WMessage(messageProperties, clientConfig);

			//using nock to mock the request
			nock(clientConfig.host, {
				reqheaders: {
					'Accept': 'application/vnd.whispir.messagestatus-v1+json'
				}
			})
			.get('/messages/1/messagestatus')
			.basicAuth(clientConfig.auth)
			.query({ apikey: clientConfig.apiKey, view: 'summary' })
			.reply(_200Response.statusCode);

			return wMessage.getMessageStatus()
			.then(function (success) {
				expect(success.statusCode).to.equal(_200Response.statusCode);
			}, function (fail) {
				throw Error('this is supposed to pass with a 200');
			});
		});
		it("should choose view as passed getMessageStatus(\'detailed\')", function () {

			var messageProperties = {
					id: 1
			};
			var _200Response = {
					statusCode: 200,
					body: 'Your request has been accepted for processing.'
			};

			var wMessage = new WMessage(messageProperties, clientConfig);

			//using nock to mock the request
			nock(clientConfig.host, {
				reqheaders: {
					'Accept': 'application/vnd.whispir.messagestatus-v1+json'
				}
			})
			.get('/messages/1/messagestatus')
			.basicAuth(clientConfig.auth)
			.query({ apikey: clientConfig.apiKey, view: 'detailed' })
			.reply(_200Response.statusCode);

			return wMessage.getMessageStatus('detailed')
			.then(function (success) {
				expect(success.statusCode).to.equal(_200Response.statusCode);
			}, function (fail) {
				throw Error('this is supposed to pass with a 200');
			});
		});
		it("should default to \'summary\' view if invalid view is passed getMessageStatus(\'invalid\')", function () {

			var messageProperties = {
					id: 1
			};
			var _200Response = {
					statusCode: 200,
					body: 'Your request has been accepted for processing.'
			};

			var wMessage = new WMessage(messageProperties, clientConfig);

			//using nock to mock the request
			nock(clientConfig.host, {
				reqheaders: {
					'Accept': 'application/vnd.whispir.messagestatus-v1+json'
				}
			})
			.get('/messages/1/messagestatus')
			.basicAuth(clientConfig.auth)
			.query({ apikey: clientConfig.apiKey, view: 'summary' })
			.reply(_200Response.statusCode);

			return wMessage.getMessageStatus('invalid')
			.then(function (success) {
				expect(success.statusCode).to.equal(_200Response.statusCode);
			}, function (fail) {
				throw Error('this is supposed to pass with a 200');
			});
		});
	});
	describe('.MessageResponses()', function () {
		it("should throw Error when no MessageID is passed", function () {
			var wMessage = new WMessage({}, clientConfig);
			expect(function () { wMessage.getMessageResponses(); }).to.throw('Message id is not provided. Cannot get Message Responses');
		});
		it("should get MessageResponses for a given message with Id and status 200", function () {

			var messageProperties = {
					id: 1
			};
			var _200Response = {
					statusCode: 200,
					body: 'Your request has been accepted for processing.'
			};

			var wMessage = new WMessage(messageProperties, clientConfig);

			//using nock to mock the request
			nock(clientConfig.host, {
				reqheaders: {
					'Accept': 'application/vnd.whispir.messageresponses-v1+json'
				}
			})
			.get('/messages/1/messageresponses')
			.basicAuth(clientConfig.auth)
			.query({ apikey: clientConfig.apiKey, view: 'summary' })
			.reply(_200Response.statusCode);

			return wMessage.getMessageResponses()
			.then(function (success) {
				expect(success.statusCode).to.equal(_200Response.statusCode);
			}, function (fail) {
				throw Error('this is supposed to pass with a 200');
			});
		});
		it("should choose view as summary if no view is passed", function () {

			var messageProperties = {
					id: 1
			};
			var _200Response = {
					statusCode: 200,
					body: 'Your request has been accepted for processing.'
			};

			var wMessage = new WMessage(messageProperties, clientConfig);

			//using nock to mock the request
			nock(clientConfig.host, {
				reqheaders: {
					'Accept': 'application/vnd.whispir.messageresponses-v1+json'
				}
			})
			.get('/messages/1/messageresponses')
			.basicAuth(clientConfig.auth)
			.query({ apikey: clientConfig.apiKey, view: 'summary' })
			.reply(_200Response.statusCode);

			return wMessage.getMessageResponses()
			.then(function (success) {
				expect(success.statusCode).to.equal(_200Response.statusCode);
			}, function (fail) {
				throw Error('this is supposed to pass with a 200');
			});
		});
		it("should choose view as passed getMessageResponses(\'detailed\')", function () {

			var messageProperties = {
					id: 1
			};
			var _200Response = {
					statusCode: 200,
					body: 'Your request has been accepted for processing.'
			};

			var wMessage = new WMessage(messageProperties, clientConfig);

			//using nock to mock the request
			nock(clientConfig.host, {
				reqheaders: {
					'Accept': 'application/vnd.whispir.messageresponses-v1+json'
				}
			})
			.get('/messages/1/messageresponses')
			.basicAuth(clientConfig.auth)
			.query({ apikey: clientConfig.apiKey, view: 'detailed' })
			.reply(_200Response.statusCode);

			return wMessage.getMessageResponses('detailed')
			.then(function (success) {
				expect(success.statusCode).to.equal(_200Response.statusCode);
			}, function (fail) {
				throw Error('this is supposed to pass with a 200');
			});
		});
		it("should default to \'summary\' view if invalid view is passed getMessageResponses(\'invalid\')", function () {

			var messageProperties = {
					id: 1
			};
			var _200Response = {
					statusCode: 200,
					body: 'Your request has been accepted for processing.'
			};

			var wMessage = new WMessage(messageProperties, clientConfig);

			//using nock to mock the request
			nock(clientConfig.host, {
				reqheaders: {
					'Accept': 'application/vnd.whispir.messageresponses-v1+json'
				}
			})
			.get('/messages/1/messageresponses')
			.basicAuth(clientConfig.auth)
			.query({ apikey: clientConfig.apiKey, view: 'summary' })
			.reply(_200Response.statusCode);

			return wMessage.getMessageResponses('invalid')
			.then(function (success) {
				expect(success.statusCode).to.equal(_200Response.statusCode);
			}, function (fail) {
				throw Error('this is supposed to pass with a 200');
			});
		});
	});

});
