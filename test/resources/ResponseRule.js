/* global describe */
/* global beforeEach */
/* global it */

'use strict';
var chai = require("chai");
var chaiAsPromised = require("chai-as-promised");
var expect = require("chai").expect;
var WResponseRule = require("../../lib/resources/ResponseRule.js");
var nock = require('nock');
var _ = require('underscore');

chai.use(chaiAsPromised);

var clientConfig, _rand;

describe('resources/ResponseRule', function () {
	beforeEach(function () {
		_rand = Math.random().toString().split(".")[1];
		clientConfig = {
				host: 'https://api.whispir.com',
				auth: { user: 'username', pass: 'password' },
				apiKey: 'apiKey',
				workSpaceId: 0,
				headers: {
					'X-Originating-SDK': 'nodejs-v.1.0.0'
				}
		};
	});
	it("sets an empty responserule structure when no options is sent", function () {
		var wResponseRuleWithNoOptions = new WResponseRule("", clientConfig);
		expect(wResponseRuleWithNoOptions.toJsonString()).to.equal('{}');
		wResponseRuleWithNoOptions = null;
	});
	it("sets a responserule structure exactly as in passed options", function () {
		var _responseruleOptions = {
				workSpaceId: 1,
				name : 'Response Rule' + _rand,
				description : 'Response Rule can also be called as ResponseTemplate #' + _rand,
				responseTemplatePatterns : {
					responseTemplatePattern : [ {
						name : 'Yes Rule',
						textPrompt : 'YES',
						voicePrompt : '1',
						spokenVoicePrompt : 'to select YES',
						pattern : 'startswith',
						colour : '#00947d'
					} ]
				}
		};
		var wResponseRuleWithOptions = new WResponseRule(_responseruleOptions, clientConfig);
		var wResponseRuleWithNoOptions = new WResponseRule("", clientConfig);

		expect(wResponseRuleWithOptions.get('workSpaceId')).to.equal(_responseruleOptions.workSpaceId);
		expect(wResponseRuleWithOptions.get('name')).to.equal(_responseruleOptions.name);
		expect(wResponseRuleWithOptions.get('description')).to.equal(_responseruleOptions.description);
		var _patterns = wResponseRuleWithOptions.get('responseTemplatePatterns');
		expect(_patterns).to.be.an('object');
		expect(_patterns).to.include.keys('responseTemplatePattern');
		expect(_patterns.responseTemplatePattern).to.be.an('array');
		expect(_patterns.responseTemplatePattern).to.have.length(1);
		expect(_patterns.responseTemplatePattern[0].name).to.equal(_responseruleOptions.responseTemplatePatterns.responseTemplatePattern[0].name);
		expect(_patterns.responseTemplatePattern[0].textPrompt).to.equal(_responseruleOptions.responseTemplatePatterns.responseTemplatePattern[0].textPrompt);
		expect(_patterns.responseTemplatePattern[0].voicePrompt).to.equal(_responseruleOptions.responseTemplatePatterns.responseTemplatePattern[0].voicePrompt);
		expect(_patterns.responseTemplatePattern[0].spokenVoicePrompt).to.equal(_responseruleOptions.responseTemplatePatterns.responseTemplatePattern[0].spokenVoicePrompt);
		expect(_patterns.responseTemplatePattern[0].pattern).to.equal(_responseruleOptions.responseTemplatePatterns.responseTemplatePattern[0].pattern);
		expect(_patterns.responseTemplatePattern[0].colour).to.equal(_responseruleOptions.responseTemplatePatterns.responseTemplatePattern[0].colour);
		expect(wResponseRuleWithOptions).to.not.equal(wResponseRuleWithNoOptions);

		wResponseRuleWithOptions = null;
	});
	it("should support method chaining and options", function () {
		var _responseruleOptions = {
				workSpaceId: 1,
				id: 1
		};
		var _responseruleProperties = {
				name : 'Response Rule' + _rand,
				description : 'Response Rule can also be called as ResponseTemplate #' + _rand,
				responseTemplatePatterns : {
					responseTemplatePattern : [ {
						name : 'Yes Rule',
						textPrompt : 'YES',
						voicePrompt : '1',
						spokenVoicePrompt : 'to select YES',
						pattern : 'startswith',
						colour : '#00947d'
					} ]
				}
		};
		var wResponseRuleWithOptionsAtInitAndChainProperties = new WResponseRule(_responseruleOptions, clientConfig);

		wResponseRuleWithOptionsAtInitAndChainProperties
		.name(_responseruleProperties.name)
		.description(_responseruleProperties.description)
		.responseTemplatePatterns(_responseruleProperties.responseTemplatePatterns);

		expect(wResponseRuleWithOptionsAtInitAndChainProperties.get('workSpaceId')).to.equal(_responseruleOptions.workSpaceId);
		expect(wResponseRuleWithOptionsAtInitAndChainProperties.get('id')).to.equal(_responseruleOptions.id);
		expect(wResponseRuleWithOptionsAtInitAndChainProperties.get('name')).to.equal(_responseruleProperties.name);
		expect(wResponseRuleWithOptionsAtInitAndChainProperties.get('description')).to.equal(_responseruleProperties.description);
		var _patterns = wResponseRuleWithOptionsAtInitAndChainProperties.get('responseTemplatePatterns');
		expect(_patterns).to.be.an('object');
		expect(_patterns).to.include.keys('responseTemplatePattern');
		expect(_patterns.responseTemplatePattern).to.be.an('array');
		expect(_patterns.responseTemplatePattern).to.have.length(1);
		expect(_patterns.responseTemplatePattern[0].name).to.equal(_responseruleProperties.responseTemplatePatterns.responseTemplatePattern[0].name);
		expect(_patterns.responseTemplatePattern[0].textPrompt).to.equal(_responseruleProperties.responseTemplatePatterns.responseTemplatePattern[0].textPrompt);
		expect(_patterns.responseTemplatePattern[0].voicePrompt).to.equal(_responseruleProperties.responseTemplatePatterns.responseTemplatePattern[0].voicePrompt);
		expect(_patterns.responseTemplatePattern[0].spokenVoicePrompt).to.equal(_responseruleProperties.responseTemplatePatterns.responseTemplatePattern[0].spokenVoicePrompt);
		expect(_patterns.responseTemplatePattern[0].pattern).to.equal(_responseruleProperties.responseTemplatePatterns.responseTemplatePattern[0].pattern);
		expect(_patterns.responseTemplatePattern[0].colour).to.equal(_responseruleProperties.responseTemplatePatterns.responseTemplatePattern[0].colour);
		wResponseRuleWithOptionsAtInitAndChainProperties = null;
	});
	it("should throw an error if params to responseTemplatePatterns() is not an object", function () {
		var _responseruleOptions = {
				workSpaceId: 1,
				id: 1
		};

		var wResponseRule = new WResponseRule(_responseruleOptions, clientConfig);

		expect(function () { wResponseRule.responseTemplatePatterns('') }).to.throw('patterns should be an object');
		wResponseRule = null;
	});
	it("should POST /responserules and receive `201` for valid responserule structure", function () {
		var _responseruleOptions = {
				workSpaceId: 1
		};
		var _responseruleProperties = {
				name : 'Response Rule' + _rand,
				description : 'Response Rule can also be called as ResponseTemplate #' + _rand,
				responseTemplatePatterns : {
					responseTemplatePattern : [ {
						name : 'Yes Rule',
						textPrompt : 'YES',
						voicePrompt : '1',
						spokenVoicePrompt : 'to select YES',
						pattern : 'startswith',
						colour : '#00947d'
					} ]
				}
		};
		var _201Response = {
				statusCode: 201,
				headers: {
					location: 'https://api.whispir.com/workspaces/1/responserules/1?apikey=apiKey'
				},
				body: {}
		};

		var wResponseRule = new WResponseRule(_responseruleOptions, clientConfig);

		// using nock to mock the request
		nock(clientConfig.host, {
			reqheaders: {
				'Accept': 'application/vnd.whispir.responserule-v1+json',
				'Content-Type': 'application/vnd.whispir.responserule-v1+json'
			}
		})
		.post('/workspaces/1/responserules')
		.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_201Response.statusCode, _201Response.body, _201Response.headers);

		return wResponseRule
		.name(_responseruleProperties.name)
		.description(_responseruleProperties.description)
		.responseTemplatePatterns(_responseruleProperties.responseTemplatePatterns)
		.create().then(function (success) {
			expect(success.statusCode).to.equal(_201Response.statusCode);
			expect(success.location).to.equal(_201Response.headers.location);
		}, function (fail) {
			throw Error('this is supposed to pass with a 201');
		});
	});
	it("should POST /responserules and receive `401` for valid ResponseRule structure but invalid Auth credentials", function () {
		var _clientConfig = _.clone(clientConfig);
		_clientConfig.auth = { user: 'username', pass: 'invalidPassword' };
		var _responseruleOptions = {
				workSpaceId: 1
		};
		var _401Response = {
				statusCode: 401,
				body: {
					"links": null,
					"errorSummary": "Your username and password combination was incorrect. Please check your authentication details and try again",
					"errorText": "Unauthorized",
					"errorDetail": null
				}
		};

		var wResponseRule = new WResponseRule(_responseruleOptions, _clientConfig);

		// using nock to mock the request
		nock(clientConfig.host, {
			reqheaders: {
				'Accept': 'application/vnd.whispir.responserule-v1+json',
				'Content-Type': 'application/vnd.whispir.responserule-v1+json'
			}
		})
		.post('/workspaces/1/responserules')
		.basicAuth(_clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_401Response.statusCode, _401Response.body);

		return wResponseRule
		.create().then(function (itShouldNotBeHere) {
			throw Error('this is supposed to fail with a 401');
		}, function (fail) {
			expect(fail.statusCode).to.equal(_401Response.statusCode);
			expect(fail.body).to.be.an('object');
		});
	});
	it("should POST /responserules and receive `422` if there a mandatory property missing in the message structure", function () {
		var _responseruleProperties = {
				workSpaceId: 1,
				name : 'Response Rule' + _rand,
				description : 'Response Rule can also be called as ResponseTemplate #' + _rand
		};
		var _422Response = {
				statusCode: 422,
				body: {
					"errorSummary": "Your request did not contain all of the information required to perform this method. Please check your request for the required fields to be passed in and try again. Alternatively, responserule your administrator or support@whispir.com for more information",
					"errorText": "responseTemplatePatterns is Mandatory \n",
					"link": []
				}
		};

		var wResponseRule = new WResponseRule(_responseruleProperties, clientConfig);

		// using nock to mock the request
		nock(clientConfig.host, {
			reqheaders: {
				'Accept': 'application/vnd.whispir.responserule-v1+json',
				'Content-Type': 'application/vnd.whispir.responserule-v1+json'
			}
		})
		.post('/workspaces/1/responserules')
		.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_422Response.statusCode, _422Response.body);

		return wResponseRule.create()
		.then(function (itShouldNotBeHere) {
			throw Error('this is supposed to fail with a 422');
		}, function (fail) {
			expect(fail.statusCode).to.equal(_422Response.statusCode);
			expect(fail.body).to.be.an('object');
		});
	});
	it("should POST /responserules and receive `201` for valid responserule structure in default workspace when no workspace Id is provided", function () {

		var _responseruleProperties = {
				name : 'Response Rule' + _rand,
				description : 'Response Rule can also be called as ResponseTemplate #' + _rand,
				responseTemplatePatterns : {
					responseTemplatePattern : [ {
						name : 'Yes Rule',
						textPrompt : 'YES',
						voicePrompt : '1',
						spokenVoicePrompt : 'to select YES',
						pattern : 'startswith',
						colour : '#00947d'
					} ]
				}
		};
		var _201Response = {
				statusCode: 201,
				headers: {
					location: 'https://api.whispir.com/responserules/1?apikey=apiKey'
				},
				body: {}
		};

		var wResponseRule = new WResponseRule(_responseruleProperties, clientConfig);

		// using nock to mock the request
		nock(clientConfig.host, {
			reqheaders: {
				'Accept': 'application/vnd.whispir.responserule-v1+json',
				'Content-Type': 'application/vnd.whispir.responserule-v1+json'
			}
		})
		.post('/responserules')
		.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_201Response.statusCode, _201Response.body, _201Response.headers);

		return wResponseRule
		.create()
		.then(function (success) {
			expect(success.statusCode).to.equal(_201Response.statusCode);
			expect(success.location).to.equal(_201Response.headers.location);
			expect(success.body).to.be.an("object");
		}, function (fail) {
			throw Error('this is supposed to pass with a 201');
		});
	});
	it("should allow to send raw JSON directly via create(rawJSON) - POST /responserules and receive `201` for valid responserule structure", function () {

		var _responseruleProperties = {
				name : 'Response Rule' + _rand,
				description : 'Response Rule can also be called as ResponseTemplate #' + _rand,
				responseTemplatePatterns : {
					responseTemplatePattern : [ {
						name : 'Yes Rule',
						textPrompt : 'YES',
						voicePrompt : '1',
						spokenVoicePrompt : 'to select YES',
						pattern : 'startswith',
						colour : '#00947d'
					} ]
				}
		};
		var _201Response = {
				statusCode: 201,
				headers: {
					location: 'https://api.whispir.com/responserules/1?apikey=apiKey'
				},
				body: {}
		};

		var wResponseRule = new WResponseRule({}, clientConfig);

		// using nock to mock the request
		nock(clientConfig.host, {
			reqheaders: {
				'Accept': 'application/vnd.whispir.responserule-v1+json',
				'Content-Type': 'application/vnd.whispir.responserule-v1+json'
			}
		})
		.post('/responserules')
		.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_201Response.statusCode, _201Response.body, _201Response.headers);

		return wResponseRule.create(JSON.stringify(_responseruleProperties))
		.then(function (success) {
			expect(success.statusCode).to.equal(_201Response.statusCode);
			expect(success.location).to.equal(_201Response.headers.location);
			expect(success.body).to.be.an("object");
		}, function (fail) {
			throw Error('this is supposed to pass with a 201');
		});
	});
	describe('.retrieve()', function () {
		it("should throw Error when no ResponseRuleID is passed", function () {
			var wResponseRule = new WResponseRule({}, clientConfig);
			expect(function () { wResponseRule.retrieve(); }).to.throw('ResponseRule id is not provided. You should provide one like this \n\t - wClient.ResponseRule().id(1) \n\t - wClient.ResponseRule({id: 1})');
		});
		it("should get ResponseRule data for a given responserule with Id and status 200", function () {

			var _responseruleProperties = {
					id: 1
			};
			var _200Response = {
					statusCode: 200,
					body: {}
			};

			var wResponseRule = new WResponseRule(_responseruleProperties, clientConfig);

			// using nock to mock the request
			nock(clientConfig.host, {
				reqheaders: {
					'Accept': 'application/vnd.whispir.responserule-v1+json'
				}
			})
			.get('/responserules/1')
			.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey})
			.reply(_200Response.statusCode);

			return wResponseRule.retrieve()
			.then(function (success) {
				expect(success.statusCode).to.equal(_200Response.statusCode);
			}, function (fail) {
				throw Error('this is supposed to pass with a 200');
			});
		});
	});
	describe('.update()', function () {
		it("should throw Error when no ResponseRuleID is passed", function () {
			var wResponseRule = new WResponseRule({}, clientConfig);
			expect(function () { wResponseRule.update(); }).to.throw('ResponseRule id is not provided. You should provide one like this \n\t - wClient.ResponseRule().id(1) \n\t - wClient.ResponseRule({id: 1})');
		});
		it("should update ResponseRule data for a given responserule with Id and status 204", function () {

			var _responseruleProperties = {
					id: 1,
					name : 'Response Rule' + _rand,
					description : 'Response Rule can also be called as ResponseTemplate #' + _rand,
					responseTemplatePatterns : {
						responseTemplatePattern : [ {
							name : 'Yes Rule',
							textPrompt : 'YES',
							voicePrompt : '1',
							spokenVoicePrompt : 'to select YES',
							pattern : 'startswith',
							colour : '#00947d'
						} ]
					}
			};
			var _204Response = {
					statusCode: 204
			};

			var wResponseRule = new WResponseRule(_responseruleProperties, clientConfig);

			// using nock to mock the request
			nock(clientConfig.host, {
				reqheaders: {
					'Accept': 'application/vnd.whispir.responserule-v1+json'
				}
			})
			.put('/responserules/1')
			.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
			.reply(_204Response.statusCode);

			return wResponseRule.update()
			.then(function (success) {
				expect(success.statusCode).to.equal(_204Response.statusCode);
			}, function (fail) {
				throw Error('this is supposed to pass with a 204');
			});
		});
	});
	describe('.delete()', function () {
		it("should throw Error when no ResponseRuleID is passed", function () {
			var wResponseRule = new WResponseRule({}, clientConfig);
			expect(function () { wResponseRule.delete(); }).to.throw('ResponseRule id is not provided. You should provide one like this \n\t - wClient.ResponseRule().id(1) \n\t - wClient.ResponseRule({id: 1})');
		});
		it("should delete ResponseRule data for a given responserule with Id and status 204", function () {

			var _responseruleProperties = {
					id: 1
			};
			var _204Response = {
					statusCode: 204
			};

			var wResponseRule = new WResponseRule(_responseruleProperties, clientConfig);

			// using nock to mock the request
			nock(clientConfig.host, {
				reqheaders: {
					'Accept': 'application/vnd.whispir.responserule-v1+json'
				}
			})
			.delete('/responserules/1')
			.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
			.reply(_204Response.statusCode);

			return wResponseRule.delete()
			.then(function (success) {
				expect(success.statusCode).to.equal(_204Response.statusCode);
			}, function (fail) {
				throw Error('this is supposed to pass with a 204');
			});
		});
	});

});