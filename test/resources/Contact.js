/* global describe */
/* global beforeEach */
/* global it */

'use strict';
var chai = require("chai");
var chaiAsPromised = require("chai-as-promised");
var expect = require("chai").expect;
var WContact = require("../../lib/resources/Contact.js");
var nock = require('nock');
var _ = require('underscore');

chai.use(chaiAsPromised);

var clientConfig, _rand;

describe('resources/Contact', function () {
	beforeEach(function () {
		_rand = Math.random().toString().split(".")[1];
		clientConfig = {
				host: 'https://api.whispir.com',
				auth: { user: 'username', pass: 'password' },
				apiKey: 'apiKey',
				workSpaceId: 0,
				headers: {
					'X-Originating-SDK': 'nodejs-v.1.0.0'
				}
		};
	});
	it("sets an empty contact structure when no options is sent", function () {
		var wContactWithNoOptions = new WContact("", clientConfig);
		expect(wContactWithNoOptions.toJsonString()).to.equal('{}');
		wContactWithNoOptions = null;
	});
	it("sets a contact structure exactly as in passed options", function () {
		var _contactOptions = {
				workSpaceId: 1,
				firstName: 'John',
				lastName: 'Wick ' + _rand,
				status: 'A',
				userName: 'John.Wick' + _rand,
				password: 'AmF10gtx',
				timezone: 'Australia/Melbourne',
				workEmailAddress1: 'jwick' + _rand + '@testcompany.com',
				workMobilePhone1: '61' + _rand,
				workCountry: 'Australia'
		};
		var wContactWithOptions = new WContact(_contactOptions, clientConfig);
		var wContactWithNoOptions = new WContact("", clientConfig);

		expect(wContactWithOptions.get('workSpaceId')).to.equal(_contactOptions.workSpaceId);
		expect(wContactWithOptions.get('firstName')).to.equal(_contactOptions.firstName);
		expect(wContactWithOptions.get('lastName')).to.equal(_contactOptions.lastName);
		expect(wContactWithOptions.get('status')).to.equal(_contactOptions.status);
		expect(wContactWithOptions.get('userName')).to.equal(_contactOptions.userName);
		expect(wContactWithOptions.get('password')).to.equal(_contactOptions.password);
		expect(wContactWithOptions.get('timezone')).to.equal(_contactOptions.timezone);
		expect(wContactWithOptions.get('workEmailAddress1')).to.equal(_contactOptions.workEmailAddress1);
		expect(wContactWithOptions.get('workMobilePhone1')).to.equal(_contactOptions.workMobilePhone1);
		expect(wContactWithOptions.get('workCountry')).to.equal(_contactOptions.workCountry);
		expect(wContactWithOptions).to.not.equal(wContactWithNoOptions);

		wContactWithOptions = null;
	});
	it("should support method chaining and options", function () {
		var _contactOptions = {
				workSpaceId: 1,
				id: 1
		};
		var _contactProperties = {
				firstName: 'John',
				lastName: 'Wick ' + _rand,
				status: 'A',
				userName: 'John.Wick' + _rand,
				password: 'AmF10gtx',
				timezone: 'Australia/Melbourne',
				workEmailAddress1: 'jwick' + _rand + '@testcompany.com',
				workMobilePhone1: '61' + _rand,
				workCountry: 'Australia'
		};
		var wContactWithOptionsAtInitAndChainProperties = new WContact(_contactOptions, clientConfig);

		wContactWithOptionsAtInitAndChainProperties
		.set('firstName', _contactProperties.firstName)
		.set('lastName', _contactProperties.lastName)
		.set('status', _contactProperties.status)
		.set('userName', _contactProperties.userName)
		.set('password', _contactProperties.password)
		.set('timezone', _contactProperties.timezone)
		.set('workEmailAddress1', _contactProperties.workEmailAddress1)
		.set('workMobilePhone1', _contactProperties.workMobilePhone1)
		.set('workCountry', _contactProperties.workCountry);

		expect(wContactWithOptionsAtInitAndChainProperties.get('workSpaceId')).to.equal(_contactOptions.workSpaceId);
		expect(wContactWithOptionsAtInitAndChainProperties.get('id')).to.equal(_contactOptions.id);
		expect(wContactWithOptionsAtInitAndChainProperties.get('firstName')).to.equal(_contactProperties.firstName);
		expect(wContactWithOptionsAtInitAndChainProperties.get('lastName')).to.equal(_contactProperties.lastName);
		expect(wContactWithOptionsAtInitAndChainProperties.get('status')).to.equal(_contactProperties.status);
		expect(wContactWithOptionsAtInitAndChainProperties.get('userName')).to.equal(_contactProperties.userName);
		expect(wContactWithOptionsAtInitAndChainProperties.get('password')).to.equal(_contactProperties.password);
		expect(wContactWithOptionsAtInitAndChainProperties.get('timezone')).to.equal(_contactProperties.timezone);
		expect(wContactWithOptionsAtInitAndChainProperties.get('workEmailAddress1')).to.equal(_contactProperties.workEmailAddress1);
		expect(wContactWithOptionsAtInitAndChainProperties.get('workMobilePhone1')).to.equal(_contactProperties.workMobilePhone1);
		expect(wContactWithOptionsAtInitAndChainProperties.get('workCountry')).to.equal(_contactProperties.workCountry);
		wContactWithOptionsAtInitAndChainProperties = null;
	});
	it("should support object via set() method and set() chaining and options", function () {
		var _contactOptions = {
				workSpaceId: 1,
				id: 1
		};
		var _contactProperties = {
				firstName: 'John',
				lastName: 'Wick ' + _rand,
				status: 'A',
				userName: 'John.Wick' + _rand,
				password: 'AmF10gtx',
				timezone: 'Australia/Melbourne',
				workEmailAddress1: 'jwick' + _rand + '@testcompany.com',
				workMobilePhone1: '61' + _rand,
				workCountry: 'Australia'
		};
		var wContactWithOptionsAtInitAndChainProperties = new WContact(_contactOptions, clientConfig);

		wContactWithOptionsAtInitAndChainProperties
		.set(_contactProperties)
		.set('workMobilePhone1', '6598765432')
		.set('workCountry', 'Singapore');

		expect(wContactWithOptionsAtInitAndChainProperties.get('workSpaceId')).to.equal(_contactOptions.workSpaceId);
		expect(wContactWithOptionsAtInitAndChainProperties.get('id')).to.equal(_contactOptions.id);
		expect(wContactWithOptionsAtInitAndChainProperties.get('firstName')).to.equal(_contactProperties.firstName);
		expect(wContactWithOptionsAtInitAndChainProperties.get('lastName')).to.equal(_contactProperties.lastName);
		expect(wContactWithOptionsAtInitAndChainProperties.get('status')).to.equal(_contactProperties.status);
		expect(wContactWithOptionsAtInitAndChainProperties.get('userName')).to.equal(_contactProperties.userName);
		expect(wContactWithOptionsAtInitAndChainProperties.get('password')).to.equal(_contactProperties.password);
		expect(wContactWithOptionsAtInitAndChainProperties.get('timezone')).to.equal(_contactProperties.timezone);
		expect(wContactWithOptionsAtInitAndChainProperties.get('workEmailAddress1')).to.equal(_contactProperties.workEmailAddress1);
		expect(wContactWithOptionsAtInitAndChainProperties.get('workMobilePhone1')).to.equal('6598765432');
		expect(wContactWithOptionsAtInitAndChainProperties.get('workCountry')).to.equal('Singapore');
		wContactWithOptionsAtInitAndChainProperties = null;
	});
	it("should not let workspaceId be set via the set() method", function () {
		var _contactOptions = {
				workSpaceId: 1,
				id: 1
		};
		var _newWorkSpace = 2;
		var wContactTryingToAddWorkSpaceIdViaSet = new WContact(_contactOptions, clientConfig);

		expect(function(){wContactTryingToAddWorkSpaceIdViaSet.set('workSpaceId', _newWorkSpace);}).to.throw('workSpaceId cannot be set using this method. It should only be sent in while creating the Contact instance. Ex: `wClient.Contact({workSpaceId: \'' + _newWorkSpace + '\', ...})`');

		wContactTryingToAddWorkSpaceIdViaSet = null;
	});
	it("should POST /contacts and receive `201` for valid contact structure", function () {
		var _contactOptions = {
				workSpaceId: 1
		};
		var _contactProperties = {
				firstName: 'John',
				lastName: 'Wick ' + _rand,
				status: 'A',
				userName: 'John.Wick' + _rand,
				password: 'AmF10gtx',
				timezone: 'Australia/Melbourne',
				workEmailAddress1: 'jwick' + _rand + '@testcompany.com',
				workMobilePhone1: '61' + _rand,
				workCountry: 'Australia'
		};
		var _201Response = {
				statusCode: 201,
				headers: {
					location: 'https://api.whispir.com/workspaces/1/contacts/1?apikey=apiKey'
				},
				body: {
					mri: _contactProperties.firstName + '_' + _contactProperties.lastName + '@Contact.whispir.sg'
				}
		};

		var wContact = new WContact(_contactOptions, clientConfig);

		// using nock to mock the request
		nock(clientConfig.host, {
			reqheaders: {
				'Accept': 'application/vnd.whispir.contact-v1+json',
				'Content-Type': 'application/vnd.whispir.contact-v1+json'
			}
		})
		.post('/workspaces/1/contacts')
		.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_201Response.statusCode, _201Response.body, _201Response.headers);

		return wContact
		.set(_contactProperties)
		.create().then(function (success) {
			expect(success.statusCode).to.equal(_201Response.statusCode);
			expect(success.location).to.equal(_201Response.headers.location);
			expect(success.mri).to.equal(_201Response.body.mri);
		}, function (fail) {
			throw Error('this is supposed to pass with a 201');
		});
	});
	it("should POST /contacts and receive `401` for valid Contact structure but invalid Auth credentials", function () {
		var _clientConfig = _.clone(clientConfig);
		_clientConfig.auth = { user: 'username', pass: 'invalidPassword' };
		var _contactOptions = {
				workSpaceId: 1
		};
		var _401Response = {
				statusCode: 401,
				body: {
					"links": null,
					"errorSummary": "Your username and password combination was incorrect. Please check your authentication details and try again",
					"errorText": "Unauthorized",
					"errorDetail": null
				}
		};

		var wContact = new WContact(_contactOptions, _clientConfig);

		// using nock to mock the request
		nock(clientConfig.host, {
			reqheaders: {
				'Accept': 'application/vnd.whispir.contact-v1+json',
				'Content-Type': 'application/vnd.whispir.contact-v1+json'
			}
		})
		.post('/workspaces/1/contacts')
		.basicAuth(_clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_401Response.statusCode, _401Response.body);

		return wContact
		.create().then(function (itShouldNotBeHere) {
			throw Error('this is supposed to fail with a 401');
		}, function (fail) {
			expect(fail.statusCode).to.equal(_401Response.statusCode);
			expect(fail.body).to.be.an('object');
		});
	});
	it("should POST /contacts and receive `422` if there a mandatory property missing in the message structure", function () {
		var _contactProperties = {
				workSpaceId: 1,
				lastName: 'Wick ' + _rand,
				userName: 'John.Wick' + _rand,
				status: 'A',
				password: 'AmF10gtx',
				timezone: 'Australia/Melbourne',
				workEmailAddress1: 'jwick' + _rand + '@testcompany.com',
				workMobilePhone1: '61' + _rand,
				workCountry: 'Australia'
		};
		var _422Response = {
				statusCode: 422,
				body: {
					"errorSummary": "Your request did not contain all of the information required to perform this method. Please check your request for the required fields to be passed in and try again. Alternatively, contact your administrator or support@whispir.com for more information",
					"errorText": "firstName is Mandatory \n",
					"link": []
				}
		};

		var wContact = new WContact(_contactProperties, clientConfig);

		// using nock to mock the request
		nock(clientConfig.host, {
			reqheaders: {
				'Accept': 'application/vnd.whispir.contact-v1+json',
				'Content-Type': 'application/vnd.whispir.contact-v1+json'
			}
		})
		.post('/workspaces/1/contacts')
		.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_422Response.statusCode, _422Response.body);

		return wContact.create()
		.then(function (itShouldNotBeHere) {
			throw Error('this is supposed to fail with a 422');
		}, function (fail) {
			expect(fail.statusCode).to.equal(_422Response.statusCode);
			expect(fail.body).to.be.an('object');
		});
	});
	it("should POST /contacts and receive `201` for valid contact structure in default workspace when no workspace Id is provided", function () {

		var _contactProperties = {
				firstName: 'John',
				lastName: 'Wick ' + _rand,
				status: 'A',
				userName: 'John.Wick' + _rand,
				password: 'AmF10gtx',
				timezone: 'Australia/Melbourne',
				workEmailAddress1: 'jwick' + _rand + '@testcompany.com',
				workMobilePhone1: '61' + _rand,
				workCountry: 'Australia'
		};
		var _201Response = {
				statusCode: 201,
				headers: {
					location: 'https://api.whispir.com/contacts/1?apikey=apiKey'
				},
				body: {
					mri: _contactProperties.firstName + '_' + _contactProperties.lastName + '@Contact.whispir.sg'
				}
		};

		var wContact = new WContact(_contactProperties, clientConfig);

		// using nock to mock the request
		nock(clientConfig.host, {
			reqheaders: {
				'Accept': 'application/vnd.whispir.contact-v1+json',
				'Content-Type': 'application/vnd.whispir.contact-v1+json'
			}
		})
		.post('/contacts')
		.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_201Response.statusCode, _201Response.body, _201Response.headers);

		return wContact
		.create()
		.then(function (success) {
			expect(success.statusCode).to.equal(_201Response.statusCode);
			expect(success.location).to.equal(_201Response.headers.location);
			expect(success.mri).to.equal(_201Response.body.mri);
			expect(success.body).to.be.an("object");
		}, function (fail) {
			throw Error('this is supposed to pass with a 201');
		});
	});
	it("should allow to send raw JSON directly via create(rawJSON) - POST /contacts and receive `201` for valid contact structure", function () {

		var _contactProperties = {
				firstName: 'John',
				lastName: 'Wick ' + _rand,
				status: 'A',
				userName: 'John.Wick' + _rand,
				password: 'AmF10gtx',
				timezone: 'Australia/Melbourne',
				workEmailAddress1: 'jwick' + _rand + '@testcompany.com',
				workMobilePhone1: '61' + _rand,
				workCountry: 'Australia'
		};
		var _201Response = {
				statusCode: 201,
				headers: {
					location: 'https://api.whispir.com/contacts/1?apikey=apiKey'
				},
				body: {
					mri: _contactProperties.firstName + '_' + _contactProperties.lastName + '@Contact.whispir.sg'
				}
		};

		var wContact = new WContact({}, clientConfig);

		// using nock to mock the request
		nock(clientConfig.host, {
			reqheaders: {
				'Accept': 'application/vnd.whispir.contact-v1+json',
				'Content-Type': 'application/vnd.whispir.contact-v1+json'
			}
		})
		.post('/contacts')
		.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_201Response.statusCode, _201Response.body, _201Response.headers);

		return wContact.create(JSON.stringify(_contactProperties))
		.then(function (success) {
			expect(success.statusCode).to.equal(_201Response.statusCode);
			expect(success.location).to.equal(_201Response.headers.location);
			expect(success.mri).to.equal(_201Response.body.mri);
			expect(success.body).to.be.an("object");
		}, function (fail) {
			throw Error('this is supposed to pass with a 201');
		});
	});
	describe('.retrieve()', function () {
		it("should throw Error when no ContactID is passed", function () {
			var wContact = new WContact({}, clientConfig);
			expect(function () { wContact.retrieve(); }).to.throw('Contact id is not provided. You should provide one like this \n\t - wClient.Contact().set(\'id\',1) \n\t - wClient.Contact({id: 1})');
		});
		it("should get Contact data for a given contact with Id and status 200", function () {

			var _contactProperties = {
					id: 1
			};
			var _200Response = {
					statusCode: 200,
					body: {}
			};

			var wContact = new WContact(_contactProperties, clientConfig);

			// using nock to mock the request
			nock(clientConfig.host, {
				reqheaders: {
					'Accept': 'application/vnd.whispir.contact-v1+json'
				}
			})
			.get('/contacts/1')
			.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey, customFields: true })
			.reply(_200Response.statusCode);

			return wContact.retrieve()
			.then(function (success) {
				expect(success.statusCode).to.equal(_200Response.statusCode);
			}, function (fail) {
				throw Error('this is supposed to pass with a 200');
			});
		});
	});
	describe('.search()', function () {
		it("should search and get Contact data with passed in params and status 200", function () {

			var _search = 'firstName=John';
			var _200Response = {
					statusCode: 200,
					body: {}
			};

			var wContact = new WContact({}, clientConfig);

			// using nock to mock the request
			nock(clientConfig.host, {
				reqheaders: {
					'Accept': 'application/vnd.whispir.contact-v1+json'
				}
			})
			.get('/contacts')
			.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey, firstName: 'John' })
			.reply(_200Response.statusCode);

			return wContact.search(_search)
			.then(function (success) {
				expect(success.statusCode).to.equal(_200Response.statusCode);
			}, function (fail) {
				throw Error('this is supposed to pass with a 200');
			});
		});
		it("should not add anything to search query when no params are passed, but still get a status 200", function () {

			var _200Response = {
					statusCode: 200,
					body: {}
			};

			var wContact = new WContact({}, clientConfig);

			// using nock to mock the request
			nock(clientConfig.host, {
				reqheaders: {
					'Accept': 'application/vnd.whispir.contact-v1+json'
				}
			})
			.get('/contacts')
			.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
			.reply(_200Response.statusCode);

			return wContact.search()
			.then(function (success) {
				expect(success.statusCode).to.equal(_200Response.statusCode);
			}, function (fail) {
				throw Error('this is supposed to pass with a 200');
			});
		});
	});
	describe('.update()', function () {
		it("should throw Error when no ContactID is passed", function () {
			var wContact = new WContact({}, clientConfig);
			expect(function () { wContact.update(); }).to.throw('Contact id is not provided. You should provide one like this \n\t - wClient.Contact().set(\'id\',1) \n\t - wClient.Contact({id: 1})');
		});
		it("should update Contact data for a given contact with Id and status 204", function () {

			var _contactProperties = {
					id: 1,
					firstName: 'John',
					lastName: 'Wick ' + _rand,
					status: 'A',
					userName: 'John.Wick' + _rand,
					password: 'AmF10gtx',
					timezone: 'Australia/Melbourne',
					workEmailAddress1: 'jwick' + _rand + '@testcompany.com',
					workMobilePhone1: '61' + _rand,
					workCountry: 'Australia'
			};
			var _204Response = {
					statusCode: 204
			};

			var wContact = new WContact(_contactProperties, clientConfig);

			// using nock to mock the request
			nock(clientConfig.host, {
				reqheaders: {
					'Accept': 'application/vnd.whispir.contact-v1+json'
				}
			})
			.put('/contacts/1')
			.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
			.reply(_204Response.statusCode);

			return wContact.update()
			.then(function (success) {
				expect(success.statusCode).to.equal(_204Response.statusCode);
			}, function (fail) {
				throw Error('this is supposed to pass with a 204');
			});
		});
	});
	describe('.delete()', function () {
		it("should throw Error when no ContactID is passed", function () {
			var wContact = new WContact({}, clientConfig);
			expect(function () { wContact.delete(); }).to.throw('Contact id is not provided. You should provide one like this \n\t - wClient.Contact().set(\'id\',1) \n\t - wClient.Contact({id: 1})');
		});
		it("should delete Contact data for a given contact with Id and status 204", function () {

			var _contactProperties = {
					id: 1
			};
			var _204Response = {
					statusCode: 204
			};

			var wContact = new WContact(_contactProperties, clientConfig);

			// using nock to mock the request
			nock(clientConfig.host, {
				reqheaders: {
					'Accept': 'application/vnd.whispir.contact-v1+json'
				}
			})
			.delete('/contacts/1')
			.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
			.reply(_204Response.statusCode);

			return wContact.delete()
			.then(function (success) {
				expect(success.statusCode).to.equal(_204Response.statusCode);
			}, function (fail) {
				throw Error('this is supposed to pass with a 204');
			});
		});
	});

});