/* global describe */
/* global beforeEach */
/* global it */

'use strict';
var chai = require("chai");
var chaiAsPromised = require("chai-as-promised");
var expect = require("chai").expect;
var WCustomList = require("../../lib/resources/CustomList.js");
var nock = require('nock');
var _ = require('underscore');

chai.use(chaiAsPromised);

var clientConfig, _rand;

describe('resources/CustomList', function () {
	beforeEach(function () {
		_rand = Math.random().toString().split(".")[1];
		clientConfig = {
				host: 'https://api.whispir.com',
				auth: { user: 'username', pass: 'password' },
				apiKey: 'apiKey',
				workSpaceId: 0,
				headers: {
					'X-Originating-SDK': 'nodejs-v.1.0.0'
				}
		};
	});
	it("sets an empty customlist structure when no options is sent", function () {
		var wCustomListWithNoOptions = new WCustomList("", clientConfig);
		expect(wCustomListWithNoOptions.toJsonString()).to.equal('{}');
		wCustomListWithNoOptions = null;
	});
	it("sets a customlist structure exactly as in passed options", function () {
		var _customlistOptions = {
				workSpaceId: 1,
				name: "Category",
				type: "INCIDENT",
				createdDate: "02/08/11 14:20",
				sortType: "As Displayed",
				linked: "disabled",
				link: [
				         {
				        	 uri: "https://api.whispir.com/workspaces/1/customlists/1?apikey=apikey",
				        	 rel: "self",
				        	 method: "GET"
				         }
				         ]
		};
		var wCustomListWithOptions = new WCustomList(_customlistOptions, clientConfig);
		var wCustomListWithNoOptions = new WCustomList("", clientConfig);

		expect(wCustomListWithOptions.get('workSpaceId')).to.equal(_customlistOptions.workSpaceId);
		expect(wCustomListWithOptions.get('id')).to.equal('1');
		expect(wCustomListWithOptions.get('name')).to.equal(_customlistOptions.name);
		expect(wCustomListWithOptions.get('type')).to.equal(_customlistOptions.type);
		expect(wCustomListWithOptions.get('createdDate')).to.equal(_customlistOptions.createdDate);
		expect(wCustomListWithOptions.get('sortType')).to.equal(_customlistOptions.sortType);
		expect(wCustomListWithOptions.get('linked')).to.equal(_customlistOptions.linked);
		expect(wCustomListWithOptions).to.not.equal(wCustomListWithNoOptions);

		wCustomListWithOptions = null;
	});
	describe('.retrieve()', function () {
		it("should throw Error when no CustomListID is passed", function () {
			var wCustomList = new WCustomList({}, clientConfig);
			expect(function () { wCustomList.retrieve(); }).to.throw('CustomList id is not provided. You should provide one like this \n\t - wClient.CustomList().id(1) \n\t - wClient.CustomList({id: 1})');
		});
		it("should GET CustomList data for a given customlist with Id and status 200", function () {

			var _customlistProperties = {
					id: 1
			};
			var _200Response = {
					statusCode: 200,
					body: {}
			};

			var wCustomList = new WCustomList(_customlistProperties, clientConfig);

			// using nock to mock the request
			nock(clientConfig.host, {
				reqheaders: {
					'Accept': 'application/vnd.whispir.customlist-v1+json'
				}
			})
			.get('/customlists/1')
			.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
			.reply(_200Response.statusCode, _200Response.body);

			return wCustomList.retrieve()
			.then(function (success) {
				expect(success.statusCode).to.equal(_200Response.statusCode);
				expect(success.body).to.be.an('object');
			}, function (fail) {
				throw Error('this is supposed to pass with a 200');
			});
		});
		it("should GET CustomList data for a given workspace and status 200", function () {

			var _customlistProperties = {
					id: 1,
					workSpaceId: 1
			};
			var _200Response = {
					statusCode: 200,
					body: {}
			};

			var wCustomList = new WCustomList(_customlistProperties, clientConfig);

			// using nock to mock the request
			nock(clientConfig.host, {
				reqheaders: {
					'Accept': 'application/vnd.whispir.customlist-v1+json'
				}
			})
			.get('/workspaces/1/customlists/1')
			.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
			.reply(_200Response.statusCode);

			return wCustomList.retrieve()
			.then(function (success) {
				expect(success.statusCode).to.equal(_200Response.statusCode);
			}, function (fail) {
				throw Error('this is supposed to pass with a 200');
			});
		});
		it("should GET /customlists and receive `401` for valid CustomList structure but invalid Auth credentials", function () {
			var _clientConfig = _.clone(clientConfig);
			_clientConfig.auth = { user: 'username', pass: 'invalidPassword' };
			var _customlistOptions = {
					workSpaceId: 1,
					id: 1
			};
			var _401Response = {
					statusCode: 401,
					body: {
						"links": null,
						"errorSummary": "Your username and password combination was incorrect. Please check your authentication details and try again",
						"errorText": "Unauthorized",
						"errorDetail": null
					}
			};

			var wCustomList = new WCustomList(_customlistOptions, _clientConfig);

			// using nock to mock the request
			nock(clientConfig.host, {
				reqheaders: {
					'Accept': 'application/vnd.whispir.customlist-v1+json'
				}
			})
			.get('/workspaces/1/customlists/1')
			.basicAuth(_clientConfig.auth)
			.query({ apikey: clientConfig.apiKey })
			.reply(_401Response.statusCode, _401Response.body);

			return wCustomList
			.retrieve().then(function (itShouldNotBeHere) {
				throw Error('this is supposed to fail with a 401');
			}, function (fail) {
				expect(fail.statusCode).to.equal(_401Response.statusCode);
				expect(fail.body).to.be.an('object');
			});
		});
	});
	describe('.create()', function () {
		it("should throw Error when create is called - Create is not allowed for CustomList", function () {
			var wCustomList = new WCustomList({}, clientConfig);
			expect(function () { wCustomList.update(); }).to.throw('Updating a CustomList is not supported via the API');
		});
	});
	describe('.update()', function () {
		it("should throw Error when update is called - Update is not allowed for CustomList", function () {
			var wCustomList = new WCustomList({}, clientConfig);
			expect(function () { wCustomList.update(); }).to.throw('Updating a CustomList is not supported via the API');
		});
	});
	describe('.delete()', function () {
		it("should throw Error when delete is called - Delete is not allowed for CustomList", function () {
			var wCustomList = new WCustomList({}, clientConfig);
			expect(function () { wCustomList.delete(); }).to.throw('Deleting a CustomList is not supported via the API');
		});
	});

});