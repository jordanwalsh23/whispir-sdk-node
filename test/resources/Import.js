/* global describe */
/* global beforeEach */
/* global it */

'use strict';
var chai = require("chai");
var chaiAsPromised = require("chai-as-promised");
var expect = require("chai").expect;
var WImport = require("../../lib/resources/Import.js");
var nock = require('nock');
var _ = require('underscore');

chai.use(chaiAsPromised);

var clientConfig, _rand;

describe('resources/Import', function () {
	beforeEach(function () {
		_rand = Math.random().toString().split(".")[1];
		clientConfig = {
				host: 'https://api.whispir.com',
				auth: { user: 'username', pass: 'password' },
				apiKey: 'apiKey',
				workSpaceId: 0,
				headers: {
					'X-Originating-SDK': 'nodejs-v.1.0.0'
				}
		};
	});
	it("sets an import structure with importType as `contact` in the empty structure when no options is sent", function () {
		var wImportWithNoOptions = new WImport("", clientConfig);
		expect(wImportWithNoOptions.toJsonString()).to.equal('{"importType":"contact"}');
		wImportWithNoOptions = null;
	});
	it("sets an import structure exactly as in passed options + `contact` as default imporType", function () {
		var _importOptions = {
				workSpaceId: 1,
				resourceId: '4FBBC384BCE3DAABFE3' + _rand,
				importOptions: {
					fieldMapping : {
						firstName: 'firstName',
						lastName: 'lastName',
						workMobilePhone1: 'workEmailAddress1',
						workCountry: 'workCountry',
						timezone: 'timezone'
					},
					importMode: 'replace'
				}
		};
		var wImportWithOptions = new WImport(_importOptions, clientConfig);
		var wImportWithNoOptions = new WImport("", clientConfig);

		expect(wImportWithOptions.get('workSpaceId')).to.equal(_importOptions.workSpaceId);
		expect(wImportWithOptions.get('resourceId')).to.equal(_importOptions.resourceId);
		expect(wImportWithOptions.get('importType')).to.equal('contact');
		expect(wImportWithOptions.get('importOptions')).to.equal(_importOptions.importOptions);
		expect(wImportWithOptions).to.not.equal(wImportWithNoOptions);

		wImportWithOptions = null;
	});
	it("should support method chaining and options", function () {
		var _importOptions = {
				workSpaceId: 1,
				id: 1
		};
		var _importProperties = {
				resourceId: '4FBBC384BCE3DAABFE3' + _rand,
				importType: 'contact',
				importOptions: {
					fieldMapping : {
						firstName: 'firstName',
						lastName: 'lastName',
						workMobilePhone1: 'workEmailAddress1',
						workCountry: 'workCountry',
						timezone: 'timezone'
					},
					importMode: 'replace'
				}
		};
		var wImportWithOptionsAtInitAndChainProperties = new WImport(_importOptions, clientConfig);

		wImportWithOptionsAtInitAndChainProperties
		.resourceId(_importProperties.resourceId)
		.importType(_importProperties.importType)
		.importOptions(_importProperties.importOptions)
		.importMode('ignore');

		expect(wImportWithOptionsAtInitAndChainProperties.get('workSpaceId')).to.equal(_importOptions.workSpaceId);
		expect(wImportWithOptionsAtInitAndChainProperties.get('id')).to.equal(_importOptions.id);
		expect(wImportWithOptionsAtInitAndChainProperties.get('resourceId')).to.equal(_importProperties.resourceId);
		expect(wImportWithOptionsAtInitAndChainProperties.get('importType')).to.equal(_importProperties.importType);
		expect(wImportWithOptionsAtInitAndChainProperties.get('importOptions')).to.equal(_importProperties.importOptions);
		expect(wImportWithOptionsAtInitAndChainProperties.get('importOptions').importMode).to.equal(_importProperties.importOptions.importMode);
		wImportWithOptionsAtInitAndChainProperties = null;
	});
	it("should not add an `invalid property` to Import structure -send via options", function () {
		var _importOptions = {
				workSpaceId: 1,
				id: 1,
				invalidProperty: 'somevalue'
		};

		var wImportWithOptionsAndInvalidProperty = new WImport(_importOptions, clientConfig);

		expect(wImportWithOptionsAndInvalidProperty.get('workSpaceId')).to.equal(_importOptions.workSpaceId);
		expect(wImportWithOptionsAndInvalidProperty.get('id')).to.equal(_importOptions.id);
		expect(function() {wImportWithOptionsAndInvalidProperty.get('invalidProperty');}).to.throw('invalidProperty is undefined in Import Object');
		wImportWithOptionsAndInvalidProperty = null;
	});
	it("should throw an error if invalid importType (anything other than `contact`) is passed to importType() method", function () {
		var _importOptions = {
				workSpaceId: 1,
				id: 1
		};

		var wImportTypes = new WImport(_importOptions, clientConfig);

		expect(wImportTypes.importType('contact').get('importType')).to.equal('contact');
		expect(function() {wImportTypes.importType('user');}).to.throw('importType can only be `contact`');
		wImportTypes = null;
	});
	it("should throw an error if params passed to importOptions is not an object", function () {
		var _importOptions = {
				workSpaceId: 1,
				id: 1
		};

		var wImportOptions = new WImport(_importOptions, clientConfig);

		expect(function() {wImportOptions.importOptions('a string');}).to.throw('importOptions data should be an object');
		wImportOptions = null;
	});
	it("should POST /imports and receive `202` for valid import structure", function () {
		var _importOptions = {
				workSpaceId: 1,
				resourceId: '4FBBC384BCE3DAABFE3' + _rand,
				importType: 'contact',
				importOptions: {
					fieldMapping : {
						firstName: 'firstName',
						lastName: 'lastName',
						workMobilePhone1: 'workEmailAddress1',
						workCountry: 'workCountry',
						timezone: 'timezone'
					},
					importMode: 'ignore'
				}
		};
		var _202Response = {
				statusCode: 202,
				headers: {
					location: 'https://api.whispir.com/workspaces/1/imports/1?apikey=apiKey'
				},
				body: 'Your request has been accepted for processing.'
		};

		var wImport = new WImport(_importOptions, clientConfig);

		// using nock to mock the request
		nock(clientConfig.host, {
			reqheaders: {
				'Accept': 'application/vnd.whispir.import-v1+json',
				'Content-Type': 'application/vnd.whispir.import-v1+json'
			}
		})
		.post('/workspaces/1/imports')
		.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_202Response.statusCode, _202Response.body, _202Response.headers);

		return wImport
		.create().then(function (success) {
			expect(success.statusCode).to.equal(_202Response.statusCode);
			expect(success.location).to.equal(_202Response.headers.location);
			expect(success.body).to.equal(_202Response.body);
		}, function (fail) {
			console.log(fail);
			throw Error('this is supposed to pass with a 201');
		});
	});
	it("should POST /imports and receive `401` for valid import structure but invalid Auth credentials", function () {
		var _clientConfig = _.clone(clientConfig);
		_clientConfig.auth = { user: 'username', pass: 'invalidPassword' };
		var _importOptions = {
				workSpaceId: 1
		};
		var _401Response = {
				statusCode: 401,
				body: {
					"links": null,
					"errorSummary": "Your username and password combination was incorrect. Please check your authentication details and try again",
					"errorText": "Unauthorized",
					"errorDetail": null
				}
		};

		var wImport = new WImport(_importOptions, _clientConfig);

		// using nock to mock the request
		nock(clientConfig.host, {
			reqheaders: {
				'Accept': 'application/vnd.whispir.import-v1+json',
				'Content-Type': 'application/vnd.whispir.import-v1+json'
			}
		})
		.post('/workspaces/1/imports')
		.basicAuth(_clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_401Response.statusCode, _401Response.body);

		return wImport
		.create().then(function (itShouldNotBeHere) {
			throw Error('this is supposed to fail with a 401');
		}, function (fail) {
			expect(fail.statusCode).to.equal(_401Response.statusCode);
			expect(fail.body).to.be.an('object');
		});
	});
	it("should POST /imports and receive `422` if there a mandatory property missing in the import structure", function () {
		var _importProperties = {
				workSpaceId: 1,
				importType: 'contact',
				importOptions: {
					fieldMapping : {
						firstName: 'firstName',
						lastName: 'lastName',
						workMobilePhone1: 'workEmailAddress1',
						workCountry: 'workCountry',
						timezone: 'timezone'
					},
					importMode: 'ignore'
				}
		};
		var _422Response = {
				statusCode: 422,
				body: {
					"errorSummary": "Your request did not contain all of the information required to perform this method. Please check your request for the required fields to be passed in and try again. Alternatively, import your administrator or support@whispir.com for more information",
					"errorText": "resourceId is Mandatory \n",
					"link": []
				}
		};

		var wImport = new WImport(_importProperties, clientConfig);

		// using nock to mock the request
		nock(clientConfig.host, {
			reqheaders: {
				'Accept': 'application/vnd.whispir.import-v1+json',
				'Content-Type': 'application/vnd.whispir.import-v1+json'
			}
		})
		.post('/workspaces/1/imports')
		.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_422Response.statusCode, _422Response.body);

		return wImport.create()
		.then(function (itShouldNotBeHere) {
			throw Error('this is supposed to fail with a 422');
		}, function (fail) {
			expect(fail.statusCode).to.equal(_422Response.statusCode);
			expect(fail.body).to.be.an('object');
		});
	});
	it("should POST /imports and receive `202` for valid import structure in default workspace when no workspace Id is provided", function () {

		var _importProperties = {
				resourceId: '4FBBC384BCE3DAABFE3' + _rand,
				importType: 'contact',
				importOptions: {
					fieldMapping : {
						firstName: 'firstName',
						lastName: 'lastName',
						workMobilePhone1: 'workEmailAddress1',
						workCountry: 'workCountry',
						timezone: 'timezone'
					},
					importMode: 'ignore'
				}
		};
		var _202Response = {
				statusCode: 202,
				headers: {
					location: 'https://api.whispir.com/imports/1?apikey=apiKey'
				},
				body: 'Your request has been accepted for processing.'
		};

		var wImport = new WImport(_importProperties, clientConfig);

		// using nock to mock the request
		nock(clientConfig.host, {
			reqheaders: {
				'Accept': 'application/vnd.whispir.import-v1+json',
				'Content-Type': 'application/vnd.whispir.import-v1+json'
			}
		})
		.post('/imports')
		.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_202Response.statusCode, _202Response.body, _202Response.headers);

		return wImport
		.create()
		.then(function (success) {
			expect(success.statusCode).to.equal(_202Response.statusCode);
			expect(success.location).to.equal(_202Response.headers.location);
			expect(success.body).to.equal(_202Response.body);
		}, function (fail) {
			throw Error('this is supposed to pass with a 201');
		});
	});
	it("should allow to send raw JSON directly via create(rawJSON) - POST /imports and receive `201` for valid import structure", function () {

		var _importProperties = {
				resourceId: '4FBBC384BCE3DAABFE3' + _rand,
				importType: 'contact',
				importOptions: {
					fieldMapping : {
						firstName: 'firstName',
						lastName: 'lastName',
						workMobilePhone1: 'workEmailAddress1',
						workCountry: 'workCountry',
						timezone: 'timezone'
					},
					importMode: 'ignore'
				}
		};
		var _202Response = {
				statusCode: 202,
				headers: {
					location: 'https://api.whispir.com/imports/1?apikey=apiKey'
				},
				body: 'Your request has been accepted for processing.'
		};

		var wImport = new WImport({}, clientConfig);

		// using nock to mock the request
		nock(clientConfig.host, {
			reqheaders: {
				'Accept': 'application/vnd.whispir.import-v1+json',
				'Content-Type': 'application/vnd.whispir.import-v1+json'
			}
		})
		.post('/imports')
		.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_202Response.statusCode, _202Response.body, _202Response.headers);

		return wImport.create(_importProperties)
		.then(function (success) {
			expect(success.statusCode).to.equal(_202Response.statusCode);
			expect(success.location).to.equal(_202Response.headers.location);
			expect(success.body).to.equal(_202Response.body);
		}, function (fail) {
			throw Error('this is supposed to pass with a 201');
		});
	});
	it("should not respect the workSpaceId when passed in via raw JSON directly to create(rawJSON) - POST /imports and receive `201` for valid import structure", function () {

		var _importProperties = {
				workSpaceId: 1,
				module: 'Message',
				action: 'Send',
				status: 'Successful',
				description: 'Message sent via the Whispir NodeJS SDK'
		};
		var _202Response = {
				statusCode: 202,
				headers: {
					location: 'https://api.whispir.com/imports/1?apikey=apiKey'
				},
				body: {}
		};

		var wImport = new WImport({}, clientConfig);

		// using nock to mock the request
		nock(clientConfig.host, {
			reqheaders: {
				'Accept': 'application/vnd.whispir.import-v1+json',
				'Content-Type': 'application/vnd.whispir.import-v1+json'
			}
		})
		.post('/imports')
		.basicAuth(clientConfig.auth)
		.query({ apikey: clientConfig.apiKey })
		.reply(_202Response.statusCode, _202Response.body, _202Response.headers);

		return wImport.create(JSON.stringify(_importProperties))
		.then(function (success) {
			expect(success.statusCode).to.equal(_202Response.statusCode);
			expect(success.location).to.equal(_202Response.headers.location);
			expect(success.location.indexOf('workspaces')).to.equal(-1);
			expect(success.mri).to.equal(_202Response.body.mri);
			expect(success.body).to.be.an("object");
		}, function (fail) {
			throw Error('this is supposed to pass with a 201');
		});
	});
	describe('.retrieve()', function () {
		it("should throw Error when retrieve is called - Retrieve is not allowed for Import", function () {
			var wImport = new WImport({}, clientConfig);
			expect(function () { wImport.retrieve(); }).to.throw('Retrieving an Import is not supported via the API');
		});
	});
	describe('.update()', function () {
		it("should throw Error when update is called - Update is not allowed for Import", function () {
			var wImport = new WImport({}, clientConfig);
			expect(function () { wImport.update(); }).to.throw('Updating an Import is not supported via the API');
		});
	});
	describe('.delete()', function () {
		it("should throw Error when delete is called - Delete is not allowed for Import", function () {
			var wImport = new WImport({}, clientConfig);
			expect(function () { wImport.delete(); }).to.throw('Deleting an Import is not supported via the API');
		});
	});

});