/* global describe */
/* global it */
/* global before */
/* global after */
/* global process */
'use strict';

var expect = require("chai").expect;
var WClientMain = require("../../lib/client/Whispir.js");
var wClient = new WClientMain('username', 'password', 'apikey');

describe("Client", function () {
    describe("allows setting and getting workspace id", function () {
        it("sets the workspace Id to 1 from 0 and revert back", function () {
            expect(wClient.setWorkSpaceId(1).getWorkSpaceId()).to.equal(1);
            expect(wClient.setWorkSpaceId(0).getWorkSpaceId()).to.equal(0);
        });
        it("allows to create an object via .New(type, options) method", function () {
            var wMessageWithNew = wClient.New('message');
            var wMessageDirect = wClient.Message();
            expect(wMessageDirect.toString()).to.equal(wMessageWithNew.toString());
        });
    });
    describe(".New()", function () {
        describe("creates a New Object with passed in valid type and options{optional}", function () {
        	var _objectTypes = ['Activity', 'Callback', 'Contact', 'CustomList', 'DistributionList', 'Event', 'Import', 'Message', 'Resource', 'ResponseRule', 'Scenario', 'Template', 'User', 'SMS', 'Email', 'Calls', 'Call'];
        	_objectTypes.forEach(function(type){
        		it("returns a given Object instance (`" + type + "`) with the client config and optional options", function () {
        			var wTypeWithNew = wClient.New(type);
        			var wTypeDirect = wClient[type]();
        			expect(wTypeDirect.toString()).to.equal(wTypeWithNew.toString());
        		});
        		describe("." + type + "()", function () {
        	        describe("Create " + type, function () {
        	            var wType = wClient.New(type);
        	            it("returns a " + type + " Object with the client config and optional arguments", function () {
        	                expect(wType).to.be.an('object');
        	                expect(wType.client()).to.be.an('object');
        	            });
        	            it("sets the default workspace as that of the client when no workSpaceId is passed in the options", function () {
        	                var wClientWorkSpaceId = wClient.getWorkSpaceId();
        	                expect(wType.get('workSpaceId')).to.equal(wClientWorkSpaceId);
        	            });
        	            it("sets the workspace for client to the one passed in the message options, but only localized to Message\'s client, and should NOT alter the original client\'s workSpaceId", function () {
        	                var wTypeWithWorkSpaceAs1 = wClient[type]({ workSpaceId: 1 });
        	                var wClientWorkSpaceId = wClient.getWorkSpaceId();
        	                expect(wTypeWithWorkSpaceAs1.get('workSpaceId')).to.not.equal(wClientWorkSpaceId);

        	            });
        	        });
        	    });
        	});
            it("throws descriptive error if invalid type is sent", function () {
                expect(function () { wClient.New('Different'); }).to.throw('different is not available in Whispir Client. Valid types are - activity,callback,contact,customlist,distributionlist,dl,event,import,message,resource,responserule,scenario,template,user,workspace');
                //this test also covers the type.toLowerCase(); `Different` becomes `different`
            });
            it("throws descriptive error if invalid options format is sent", function () {
                expect(function () { wClient.New('Different', ''); }).to.throw('options should be an `object` or `undefined`(no options passed) only');
            });
        });
    });
    describe(".WorkSpace()", function () {
        describe("Create WorkSpace", function () {
            var wWorkSpace = wClient.WorkSpace();
            it("returns a workspace Object with the client config and optional arguments", function () {
                expect(wWorkSpace).to.be.an('object');
                expect(wWorkSpace.client()).to.be.an('object');
            });
            it("throws an error when workSpaceId of the workSpace object is requested", function () {
                expect(function() {wWorkSpace.get('workSpaceId');}).to.throw('workSpaceId is undefined in WorkSpace Object');
            });
            it("does not honour the workSpaceId passed. Its always set to 0 - exclusion workSpaceId and throws error if get('workSpaceId') is done", function () {
                var wWorkSpaceWithIdAs1 = wClient.WorkSpace({ workSpaceId: 1 });
                var wClientWorkSpaceId = wClient.getWorkSpaceId();
                expect(wClientWorkSpaceId).to.not.equal(1);
                expect(function() {wWorkSpaceWithIdAs1.get('workSpaceId');}).to.throw('workSpaceId is undefined in WorkSpace Object');
            });
            it("returns a workspace Object with the client config and optional arguments via New() Method", function () {
            	var wWorkSpaceViaNew = wClient.New('workspace');
            	expect(wWorkSpaceViaNew).to.be.an('object');
                expect(wWorkSpaceViaNew.client()).to.be.an('object');
            });
        });
    });
    /*describe.skip(".Fetch()", function(){
    	it('should fetch the given resource', function(){

    	});
    });
    describe.skip("UTIL.MessageStatus()", function(){
      it('should fetch the given resource', function(){

      });
    });
    */
});
