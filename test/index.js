/* mocha in windows - node ./node_modules/mocha/bin/_mocha --recursive*/
/* global describe */
/* global it */
/* global before */
/* global after */
/* global process */
'use strict';

var fs = require('fs');

var expect = require("chai").expect;
var wClientMain = require("../lib/index.js");
var wClient;

describe("Client", function () {
    describe.skip(".init() no params and no file provided", function () {
        before(function () {
            var homedir = process.env.HOME || process.env.USERPROFILE;
            fs.rename(homedir + '/.whispir/auth.json', homedir + '/.whispir/auth-temp.json', function (err) {
                if (err) console.log('ERROR: ' + err);
            });
        });
        it("should throw an error that auth.json does not exist or params not passed", function () {
            expect(function () { wClientMain(); }).to.throw('parameters not provided or auth.json could not be read. Please configure auth.json and try again.');
        });
        after(function () {
            var homedir = process.env.HOME || process.env.USERPROFILE;
            fs.rename(homedir + '/.whispir/auth-temp.json', homedir + '/.whispir/auth.json', function (err) {
                if (err) console.log('ERROR: ' + err);
            });
        });
    });

    describe(".init('username','password','apikey') with params", function () {
        before(function () {
            wClient = new wClientMain('username', 'password', 'apikey');
        });
        it("should init the client with given init values", function () {
            expect(wClient).to.be.an('object');
            expect(wClient.config.apiKey).to.equal("apikey");
            expect(wClient.config.auth.user).to.equal("username");
            expect(wClient.config.auth.pass).to.equal("password");
        });
        it("should have the api url as https://api.whispir.com", function () {
            expect(wClient.config.host).to.equal("https://api.whispir.com");
        });
        it("should have the default workspaceID as 0", function () {
            expect(wClient.config.workSpaceId).to.equal(0);
        });
        it("should have the version in headers as 'X-Originating-SDK' and version as 'nodejs-v.2.1.1'", function () {
            expect(wClient.config.headers).to.have.property('X-Originating-SDK');
            expect(wClient.config.headers['X-Originating-SDK']).to.equal('nodejs-v.2.1.1');
        });
        after(function () {
            wClient = null;
        });
    });

    describe(".init() with no params but file provided", function () {
        before(function () {
            wClient = new wClientMain();
            /*
            --- file values ---
            {
              "username":"from.file",
              "password":"one2three",
              "apikey":"ds0dsf09hdfsh90df90hdfsj"
            }*/
        });
        it("should init the client with given init values", function () {
            expect(wClient).to.be.an('object');
            expect(wClient.config.apiKey).to.equal("ds0dsf09hdfsh90df90hdfsj");
            expect(wClient.config.auth.user).to.equal("from.file");
            expect(wClient.config.auth.pass).to.equal("one2three");
        });
        it("should have the api url as https://api.whispir.com", function () {
            expect(wClient.config.host).to.equal("https://api.whispir.com");
        });
        it("should have the default workspaceID as 0", function () {
            expect(wClient.config.workSpaceId).to.equal(0);
        });
        it("should have the version in headers as 'X-Originating-SDK' and version as 'nodejs-v.2.1.1'", function () {
            expect(wClient.config.headers).to.have.property('X-Originating-SDK');
            expect(wClient.config.headers['X-Originating-SDK']).to.equal('nodejs-v.2.1.1');
        });
        after(function () {
            wClient = null;
        });
    });
});
